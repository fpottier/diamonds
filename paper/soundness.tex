\section{Soundness of \DIAMS}
\label{sec:soundness}

\DIAMS is an instance of Iris~\cite{iris}. This means that \DIAMS is set up
via the following steps:

\begin{enumerate}
\item define a certain amount of \emph{ghost state}
      that is needed in the next two steps;
\item define a central \emph{invariant}
      that ties the machine's physical state
      to the ghost state;
\item
  define the \emph{assertions} that an end user of \DIAMS exploits, namely
  points-to assertions, deallocation witnesses, space credits, and pointed-by
  assertions; all of these assertions are defined as the ownership
  of a fragment of the ghost state.
\end{enumerate}

Once these steps have been carried out, Iris provides a semantic definition of
the Hoare triple $\triple\pre\instr\post$. We can then use Hoare triples to
state the reasoning rules of \DIAMS (\fref{fig:reasoning}), to prove that
these rules are valid, and to state and prove the fact that \DIAMS is sound.
This is the topic of the next subsection (\sref{def:soundness:statement}).
%
After presenting these results, in \sref{sec:invariant}, we come back to the
three steps listed above and describe them at an informal level, without
diving into the concepts of Iris, which would be difficult to explain in a
thorough and concise way.
% For more information on Iris, the reader is referred to Jung \etal.'s
% paper~(\citeyear{iris}).

\subsection{The Soundness Statement}
\label{def:soundness:statement}

The operational semantics of \spacelang guarantees that the size of the store
never exceeds~$\maxsize$ (\sref{sec:valid}). A~program that attempts to
allocate memory beyond this limit becomes
stuck~(\sref{sec:spacelang:instructions}).
% Thus, the space consumption of every \spacelang program is bounded.
Thus, in order to prove that \DIAMS is sound, all we have to do is prove that
it rules out the possibility of a runtime failure. This is guaranteed by the
following theorem:

\begin{theorem}[Soundness of the Logic \DIAMS]
\label{th:adequacy}
  Suppose $\triple{\SC\maxsize}\instr\iTrue$ holds. Then, the
  execution of the instruction~$\instr$, beginning in an empty store, cannot
  result in a configuration where a thread is stuck.
\end{theorem}

This theorem states that if the program~$\instr$ satisfies a semantic Hoare
triple whose precondition is~$\SC\maxsize$, then this program cannot crash. In
particular, it cannot reach a situation where a memory allocation request
cannot be satisfied because the heap is full. The intuitive reason why this
holds is that $\SC\maxsize$ represents a permission to allocate at most
$\maxsize$ words of memory, and that is precisely the amount of memory that is
initially available.
%
The proof of this theorem is short: it is a direct consequence of Iris's
adequacy theorem~\cite[\S6.4]{iris}.

Separately, we prove that the reasoning rules of~\DIAMS are valid, that is,
each rule is a valid lemma:

\begin{theorem}[Validity of the Reasoning Rules]
\label{th:validity}
  Each of the rules in Figures~\ref{fig:laws}
  and~\ref{fig:reasoning} is valid.
\end{theorem}

The proof of this theorem represents more work: one lemma per reasoning rule
is required, not to mention a large number of auxiliary lemmas. The proofs of
these lemmas are fairly uninteresting; the key insights lie in the definitions
presented in the next subsection (\sref{sec:invariant}).

Together, Theorems~\ref{th:adequacy} and~\ref{th:validity} guarantee that, if
the Hoare triple $\triple{\SC\maxsize}\instr\iTrue$ can be obtained by applying
the reasoning rules of \DIAMS, then the program~$\instr$ is safe, that is, it
cannot crash and the size of its live heap data cannot exceed~$\maxsize$.

% Could recall that $\maxsize$ appears in this soundness statement but
% does not appear anywhere else; the rules are independent of it. Informally,
% one could actually universally quantify $\maxsize$ at the beginning of the
% soundness statement. (Formally, this does not quite work because the
% semantics is parameterized with S.)

% ------------------------------------------------------------------------------

\subsection{Key Definitions and Invariants}
\label{sec:invariant}

For the definitions that follow, we need some more terminology about stores.

\begin{definition}[More Terminology]
\label{def:closed}
  We write $\successors\store\loc$ for the multiset of the locations~$\loc'$
  such that there is an edge in the store~$\store$ from $\loc$ to $\loc'$.
  % This multiset can be defined as follows:
  % if $\loc\not\in\dom\store$ then $\successors\store\loc=\emptyset$;
  % if $\store(\loc)=\blk$ then $\successors\store\loc=\pointers\blk$.
  A store~$\store$ is \emph{closed} if
  the existence of an edge from~$\loc$ to $\loc'$
  implies $\loc'\in\dom\store$.
  % equivalent to:
  % for every location $\loc$,
  % $\successors\store\loc$ is contained in $\dom\store$.
  The \emph{available space} in a store~$\store$ is
  $\available\store = \maxsize-\sz\store$.
  %
  The set  $\freed\store$ is
  the set $\{ \loc \mid \store(\loc)=\bdeallocated \}$
  of the locations that have been allocated, then freed.
\end{definition}

To give meaning to pointed-by assertions, we introduce the concept
of a \emph{predecessor map}~$\predstore$, and define an invariant that relates
a store~$\store$ and a predecessor map~$\predstore$. Our intention is to
exploit Iris's invariants and ghost state to express the idea that, at every
point in time, there exists a global predecessor map, which is consistent with
the current \logical store, and that every pointed-by assertion corresponds
to a fragment of this predecessor map.

\begin{definition}[Predecessor Map]
\label{def:pred}
  A \emph{predecessor map}~$\predstore$ is a finite map of memory locations
  to finite multisets of memory locations.
  A store $\store$ and a predecessor map $\predstore$ are
  \emph{consistent} with one another, which we write
  $\phinvariant\store\predstore$,
  if the following three properties hold:
  \begin{enumerate}
  \item \label{coincidence}
        $\dom\store\setminus\freed\store = \dom\predstore$;
  \item \label{mirror}
        for all locations $\loc, \loc'$, the inequality
        \(
          \multiplicity{\loc'}{\successors\store\loc}
          \leq
          \multiplicity{\loc}{\predecessors\predstore{\loc'}}
        \)
        holds;
  \item \label{closure}
        for all locations $\loc, \loc'$,
        $\loc\in\predecessors\predstore{\loc'}$ implies
        $\loc\in\dom\store$.
  \end{enumerate}
\end{definition}

Property~(\ref{coincidence}) states that the domain of~$\predstore$ is the domain
of~$\store$, deprived of the locations that have been freed. This reflects the
fact that we do not need (and do not allow) a pointed-by assertion about a
deallocated location.
% Thus, the conjunction of a pointed-by assertion and a deallocation witness
% implies $\iFalse$. This is a reasoning rule that could be added if desired.

Property~(\ref{mirror}) reflects the fact that, for every forward edge in the
store~$\store$, there is a backward edge in the predecessor map~$\predstore$.
The property is stated so as to tolerate multiple edges
% \cref{multiplicity}
from~$\loc$ to~$\loc'$, which exist when the memory block at location~$\loc$
contains several pointers to~$\loc'$. Property~(\ref{mirror}) is an
inequality; the reverse inequality is not required. This reflects the fact
that we allow predecessor multisets to be over-approximated.
%
The validity of the law \RULE{\ConfrontMapstoMapsfrom} in \fref{fig:laws}
is a direct consequence of this property.

Property~(\ref{closure}) is a simple sanity requirement: the locations in the
multiset~$\predecessors\predstore{\loc'}$ must have been allocated in the
past. (They may or may not have been deallocated since.) In other words, a
predecessor multiset cannot contain a location that has never been allocated
at all.

Next, we must define the relation that ties the physical store to the
\logical store, two concepts that we have informally described earlier
(\sref{sec:logical}). From here on, we write~$\store$ for the physical
store and~$\logicalstore$ for the \logical store.

\begin{definition}[Relation between Physical and \Logical Stores]
\label{def:related}
  Two stores $\store$ and $\logicalstore$ are \emph{related},
  which we write $\related\store\logicalstore$, when
  they have the same domain,
  the same reachable locations,
  and the same content at every reachable location.
\end{definition}

Equipped with the above definitions, we are now prepared to enter the realm of
Iris and perform the three steps described at the beginning of
\sref{sec:soundness}, that is, define the ghost state, the central invariant,
and the assertions of \DIAMS. We give the definitions first and explain them
afterwards.

\begin{comment}
% fp: this is the beginning of an explanation of ghost state in Iris
%     but it quickly grows too long, even though many aspects are missing.
A ghost memory cell in Iris is a memory cell that does not exist at runtime.
The ownership and the content of a physical memory cell are described via a
points-to predicate such as $\loc\pointsto\val$. Similarly, the ownership and
content of a ghost cell are described by an Iris assertion, usually written
$\ownGhost\gamma{a}$, where $\gamma$ is the name of the ghost cell and $a$ is
its content. Whereas the content of a physical memory cell is a
programming-language value~$\val$, the content of a ghost cell inhabits a
mathematical structure, a \emph{camera}~\cite[\S4.4]{iris}, which the user
chooses when the ghost cell is created. A camera is equipped with a
composition operation~$\cdot$ and the ghost state assertion satisfies the law
$\ownGhost\gamma{a\cdot b} \equiv \ownGhost\gamma{a} \star
\ownGhost\gamma{b}$. Thus, the choice of a suitable camera determines in what
ways the ghost state can be split.

A key Iris idiom involves the use of the \emph{authoritative
  camera}~\cite[\S6.3.3]{iris} together with an Iris invariant.
The elements of the authoritative camera include
\emph{authoritative elements} $\authfull{c}$ and
\emph{fragmentary elements} $\authfrag{b}$.
The composition law is defined in such a way that
there always exists at most one authoritative element
and the composition of all fragmentary elements in existence is
contained in the authoritative element, etc.
\end{comment}

% The ghost locations.
\newcommand{\gammaDiamonds}{\gamma}
\newcommand{\gammaPred}{\delta}

% The predicate gen_heap_interp.
\newcommand{\genheapinterp}[1]{\mathit{Heap}\;#1}
% Our predicate pred (defined in ph.v), expanded.
\newcommand{\pred}[1]{\ownGhost\gammaPred{\authfull{(\mapone\predstore)}}}
% A notation for assigning fraction 1 to every element of \predstore.
\newcommand{\mapone}[1]{1.#1}
% The predicate mapsfrom_exact, expanded.
\newcommand{\mapsfromexact}[3]{
  \ownGhost\gammaPred{\authfrag{\singletonMap{#1}{(#2, #3)}}}
}
% A metavariable for a share.
\newcommand{\sh}{L'}
% The predicate mapsfrom, expanded.
\newcommand{\mapsfromdef}[3]{
  \exists\sh.\;
  \mapsfromexact{#1}{#2}{\sh} \star \pure{\sh \subseteq #3}
}
% A discarded fraction.
\newcommand{\discarded}{\square}

\begin{assumption}
\label{assumption}
  Iris defines a certain piece of ghost state,
  defines a predicate $\genheapinterp\store$
  that ties a store~$\store$ to this ghost state,
  and defines the points-to assertion $\loc\fpointsto\qv\blk$
  in terms of this ghost state.
  This is visible in the paper~\cite[\S6.3.2]{iris}
  and in Iris's \texttt{gen\_heap} library~\cite{genheap}.
  %
  We re-use this machinery without change,
  so we do not repeat these definitions.
  We mention the predicate $\genheapinterp\!$
  in our own invariant (Definition~\ref{def:invariant}),
  where it is applied to the \logical store~$\logicalstore$.
\end{assumption}

\begin{definition}[Ghost State]
Two \emph{ghost cells} $\gammaDiamonds$ and $\gammaPred$ are allocated at the
beginning of a program's execution. The cell $\gammaDiamonds$ stores an
element of the monoid $\authm(\textsc{Nat}, \mathord{+})$,
while the cell~$\gammaPred$
stores an element of the monoid
\newcommand\fpfn{\rightarrow_{\textrm{fin}}}
\(
  \authm(\;
    \Loc \;\fpfn\;
    (\textsc{Frac}, \mathord{+})
    \times
    (\textsc{Multiset}(\Loc), \mathord\uplus)
  \;)
\)
% \emph{authoritative camera}
\cite[\S6.3.3]{iris}.
\end{definition}

\begin{definition}[Central Invariant]
\label{def:invariant}
The central invariant of \DIAMS,
or \emph{state interpretation invariant} \cite[\S7.3]{iris},
is the following Iris assertion:
\[
\centralinvariant\;\store \triangleq
\exists\logicalstore. \left\{
\def\arraystretch{1.2}
\begin{array}{l@{\quad\star\quad}l@{\quad}l}
  \pure{\closed\store}  &
  \pure{\valid\store} & \star \\
  \pure{\closed\logicalstore} &
  \pure{\valid\logicalstore} & \star \\
  \pure{\related\store\logicalstore} &
  \genheapinterp\logicalstore & \star \\
  \ownGhost\gammaDiamonds{\authfull{\available\logicalstore}} &
  \exists\predstore.\;
    \pure{\phinvariant\logicalstore\predstore}
    \;\star\;
    \pred\predstore
\end{array}
\right.
\]
\end{definition}

\begin{definition}[Assertions]
\label{def:assertions}
  The assertions of \DIAMS are defined as follows:
  \begin{enumerate}
  \item
  % The definition of the points-to predicate is inherited from gen_heap.
  % It is implicitly parameterized by a ghost cell that holds the heap.
  The \emph{points-to assertion} $\loc\fpointsto\qv\blk$
  is inherited from Iris (Assumption~\ref{assumption}).
  \item
  The \emph{deallocation witness} for a single location,
  $\ddagsingleton\loc$,
  is defined as $\loc\fpointsto{\discarded}\bdeallocated$,
  where $\discarded$ is a \emph{discarded fraction}
  \cite{vindum-birkedal-21}.
  The deallocation witness for a set of locations, $\ddag\locs$,
  is defined as the iterated conjunction $\ast_{\loc\in\locs} \ddag\{\loc\}$.
  \item
  The \emph{space credits} $\SC{n}$
  are defined as $\ownGhost\gammaDiamonds{\authfrag{n}}$.
  \item
  The \emph{pointed-by assertion} $\loc\vfpointedby\qv\lsv$
  is defined as \mathsmash{\mapsfromdef\loc\qv\lsv}.
  \end{enumerate}
\end{definition}

% What do these definitions mean?

Let us try and explain the most important aspects of these definitions.

In Iris, the state interpretation invariant~$\centralinvariant\;\store$ is an assertion that
holds of the \emph{physical store}~$\store$ in between every two steps of
computation. It is used by Iris in the definition of Hoare triples; we inherit
this definition from Iris.

Our definition of $\centralinvariant\;\store$ begins with an existential quantification over
a \emph{\logical store}~$\logicalstore$.
%
The invariant records the fact that both stores are closed
(Definition~\ref{def:closed}) and valid (\S\ref{sec:valid}).
It is a simple property of \spacelang's semantics
that the physical store always remains closed and valid;
we require the same property of the \logical store.
%
The two stores are related by the relation $\related\store\logicalstore$
(Definition~\ref{def:related}), which means that their reachable fragments
coincide.

% The remaining parts of the invariant involve several bits of
% \emph{authoritative ghost state}, while the assertions introduced in
% Definition~\ref{def:assertions} involve \emph{fragments} of the same ghost
% state.

As noted earlier (Assumption~\ref{assumption}), the predicate
$\genheapinterp\!$ and the points-to assertion are inherited from Iris. What
this means, as far as we are concerned, is that the points-to assertion is
defined in a standard manner. One unusual aspect is
that $\genheapinterp\!$ is applied to the \logical store~$\theta$, not to
the physical store~$\store$, as is usually the case~\cite[\S6.3.2]{iris}.
Thus, the points-to assertion $\loc\fpointsto\qv\blk$ guarantees the
existence of the memory block~$\blk$ at address~$\loc$ \emph{in the \logical
  store}. If this address is reachable, then the relation
$\related\store\logicalstore$ guarantees that the block~$\blk$ also exists at
address~$\loc$ in the physical store. If the address~$\loc$ has become
unreachable, however, then it may be the case that $\loc$ has been deallocated
already in the physical store.

The deallocation witness~$\ddag{\{\loc\}}$ is in fact a special case of the
points-to assertion: it is defined as
$\loc\fpointsto{\discarded}\bdeallocated$, where $\discarded$ is a
\emph{discarded fraction} \cite{vindum-birkedal-21}. This persistent assertion
guarantees that the address~$\loc$ has been deallocated and will remain so
forever.

While the central invariant involves the \emph{authoritative assertion}
\mathsmash{\ownGhost\gammaDiamonds{\authfull{\available\logicalstore}}},
the assertion~$\SC{n}$ expands to the \emph{fragmentary assertion}
\mathsmash{\ownGhost\gammaDiamonds{\authfrag{n}}}~\cite[\S6.3.3]{iris}.
Furthermore, the ghost cell~$\gammaDiamonds$
ranges over the monoid $\authm(\textsc{Nat}, \mathord{+})$.
%
This means intuitively that the total number of space credits in circulation
is at most $\available\logicalstore$
% (the amount of available space in the \logical store)
and that to own $n$ space credits
is to own $n$ units out of this number.
%
In particular, according to Iris's laws for ghost state,
from the conjunction
\mathsmash{
  \ownGhost\gammaDiamonds{\authfull{\available\logicalstore}}
  \star
  \ownGhost\gammaDiamonds{\authfrag{n}}
},
one can deduce
$\exists n'.\; \available\logicalstore = n + n'$,
that is,
$\available\logicalstore \geq n$.
Thus, the ownership of $n$ space credits guarantees
that there exist $n$~words of available space in the \logical store.
%
From this fact and from the relation $\related\store\logicalstore$,
one cannot deduce that there exist $n$~words of available space in
the physical store, because the physical store may be partly
filled with garbage. However, one \emph{can} deduce that,
\emph{by letting the garbage collector run},
it is possible to reach a situation where
$n$ words of memory are available in
the physical store.

% One could state a lemma:
% if $\related\store\logicalstore$ holds
% and $\available\theta \geq n$
% then there exists $\store'$ such that
% $\related{\store'}\theta$ holds
% and $\gc\store{\store'}$ holds
% and $\available{\store'} \geq n$.
%
% That would be the lemma related_available in semantics.v.

The central invariant states that there exists a predecessor map $\predstore$
and that this map is related to the \logical store by the relation
$\phinvariant\logicalstore\predstore$ (Definition~\ref{def:pred}).
%
Again, the invariant involves the authoritative assertion
\mathsmash{\pred\predstore},
while the definition of the pointed-by assertion $\loc\vfpointedby\qv\lsv$
involves a fragmentary assertion \mathsmash{\mapsfromexact\loc\qv\sh}.%
\footnote{%
% In the assertion \mathsmash{\pred\predstore},
We write $\mapone\predstore$ for the finite map
$\mathit{map}\;(\lambda\lsv.\,(1, \lsv))\;\predstore$. That is,
whereas $\predstore$ maps every location to a multiset of predecessors,
$\mapone\predstore$ maps every location to a pair of the fraction 1 and
a multiset of predecessors.
%
% In the assertion \mathsmash{\mapsfromexact\loc\qv\sh},
We write $\singletonMap{\loc}{(\qv, \sh)}$ for the
singleton map that maps $\loc$ to the pair $(\qv, \sh)$.
}
%
This means that a pointed-by assertion represents
a fragment of the central predecessor map~$\predstore$.
%
The ghost cell~$\gammaPred$ ranges over a monoid that is chosen
so as to validate the law \RULE{\JoinPointedBy} in \fref{fig:laws}.
%
The existential quantification over $\sh$ in Definition~\ref{def:assertions},
together with the inequality $\sh\subseteq\lsv$, allows predecessor multisets
to be over-approximated;
this validates the law \RULE{\CovPointedBy} in \fref{fig:laws}.
%
% There is a fine technical point here.
% The definition of $\phinvariant\store\predstore$ (Definition~\ref{def:pred})
% already allows $\predstore$ to be over-approximated.
% We are adding a second layer of over-approximation here, which may seem
% redundant. What we gain by adding this second layer is that the law
% \RULE{\CovPointedBy} is a magic wand, not an update. This may make the
% law slightly more powerful / easier to use.
% If this is found not to be significant, then this second layer of
% over-approximation could be removed.

% could be removed to save space:
In summary, one idea that is worth taking away is that, in \DIAMS, all
assertions (points-to assertions; deallocation witnesses; space credits;
pointed-by assertions) are really about the \logical store. However, because
the physical store and the \logical store are tightly related, these
assertions can also be used to reason about the physical store.

The complete definition and proof of soundness of \DIAMS can be found in
\longciterepo.
