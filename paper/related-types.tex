% ------------------------------------------------------------------------------

% Hughes and Pareto.

\citet{hughes-pareto-99} present Embedded ML (MML), a pure, strict, first-order
programming language without garbage collection.
%
MML is the combination of a static region discipline~\cite{tofte-talpin-94},
which determines where deallocation can take place, and a type system that
keeps track of the size of data structures and regions.
%
% This type system bounds the heap usage of a program. (It also enforces
% termination, and bounds stack usage; these are separate aspects.)
%
Region management is explicit: the $\kw{letreg}$ construct introduces a new
region, whose lifetime coincides with its lexical scope.
%
% As far as I can tell, this is an old-style region system where a region
% exists exactly as long as its name is in scope. (This is enforced by not
% allowing a region to be destroyed before its name goes out of scope, and by
% disallowing existential quantification on regions. The `letreg` construct is
% the only construct that binds a region name.) Thus, provided every type is
% well-formed (which the typing rule for `letreg` can check) there can be no
% dangling pointers. (It is quite possible that the absence of first-class
% functions plays a role here, too. Otherwise, one would need to keep track of
% which regions are accessed by the function.)
%
% In fact, region resetting (which empties a region, but still allows
% allocating in it) is discussed at the end of the paper, but seems
% complex and is not explained in detail.
%
The size of a region is explicitly specified when this region is allocated,
and remains fixed. The programmer can allocate memory blocks in this region as
long as there is space in it.
%
% When a memory block is allocated, the user must specify in which region it
% should be allocated.
%
The type system ensures that this is the case.
%
To achieve this, every typing judgement and every function type carries a
\emph{put effect}~$p$, a finite map of regions to natural integers, which
indicates how much space this function allocates out of each region.
%
% E.g., it can indicate that it allocates 3k+1 words in region $r$.
%
It also carries a \emph{store effect}~$\phi$ that keeps track of the maximum
size that the store can reach. The size of the store, at each point in time,
is the sum of the sizes of the regions that exist at this point.
%
% The put effect and the store effect are different things: the put effect is
% about measuring the free space inside each region and guaranteeing that a
% region cannot become overfull; the store effect is about keeping track of
% the sum of the sizes of all regions, regardless of whether they are full.
%
% An encoding into \DIAMS is almost possible, but fails because MML allows
% dangling pointers, where \DIAMS does not. Here are some notes. On the one
% hand, one needs to understand how to encode put effects and store effects
% into space credits. Embedded ML has a two-level allocation system (allocate
% a region; allocate objects inside a region) which must be mapped to a single
% level of space credits. Probably one must require S♢ where S is the store
% effect, plus, for each region r in existence, P(r)♢ where P is the put
% effect. The typing rule for `letreg` requires the put effect P(r) to be at
% most the size s of the new region r; so we can take s♢ out of S♢ and use
% these space credits to obtain P(r)♢ for the new region. On the other hand,
% one must understand how to keep track of predecessors. I initially thought
% that Embedded ML has the following global invariant: because there is no
% mutable state, the pointers to a region can originate only in newer regions,
% so it is always safe to deallocate the newest region. It might have been
% possible to encode this invariant using Iris phantom state. However, this
% invariant does *not* hold. It is permitted to allocate in an old region,
% so an object in an old region can contain pointers to a newer region. The
% fact that region variables appear in types is used in the typing rule for
% letreg to guarantee that dangling pointers are never dereferenced; still,
% it allows dangling pointers to exist.
%
% Embedded ML is unfortunately somewhat inflexible: a region cannot be destroyed
% before its name goes out of scope; an object cannot be freed except by
% destroying the region that it inhabits.
%
MML and \DIAMS have incomparable power: while a program logic is in many ways
more expressive than a type system, \DIAMS is designed to reason about
garbage collection, which cannot deallocate a reachable object, whereas MML's
region discipline can give rise to dangling pointers, which the type
discipline guarantees are never dereferenced.

% ------------------------------------------------------------------------------

% Chin et al. (Two papers.)

\citet{chin-05} present a type system that keeps track of data structure
sizes. The type system incorporates an alias analysis, which
distinguishes between shared and unique objects and allows unique objects to
be explicitly deallocated.
% (which is safe).
The specification of a method includes a precondition and a postcondition,
which express constraints on the sizes of the method's arguments and result.
It also indicates how much memory the method may need (a high-water mark)
and how much memory it releases.
% Four modifiers: Unique, Shared, Read-Only, Borrowed.
% They give the spec of a stack that owns a list (but the stack elements
% are shared, so the system cannot prove that, once an element has been
% extracted out of the stack, the stack no longer points to it).
% Sizes are counted per class: e.g. (List, 1) represents one list cell.
% They use an explicit dispose instruction, and discuss how to automatically
% insert it. (This requires a non-nullness analysis.) It can be applied to
% Unique objects only.
As an example, the specification of a stack is provided. As expected, the
specification of $\stackpush$ indicates that this method allocates one list
cell and increments the size of the stack; symmetrically, $\stackpop$ frees
up one list cell and decrements the size of the stack. One limitation of the
system is that the stack's elements are considered shared, so they can never
be regarded as deallocated by the analysis. % (\logically deallocated)
%
In contrast, our specification of a stack (\fref{fig:stack:spec}) maintains
precise reference counts for elements, so, after extracting an element out of
a stack, it is possible to recognize that this element is not shared and to
\logically deallocate it.
%
% Another limitation is that the method \textt{emptyStack}, which recovers the
% memory occupied by the stack, is not a ghost method; it actually empties the
% stack by iterating \texttt{pop}. A way of abandoning the stack (without
% emptying it at runtime) and recovering the memory that it occupies is
% missing.
%
% The object "self" is marked L, which means "borrowed".
% The stack elements are marked S ("shared").
% The method "pop" returns nothing (weird).
%
In later work, \citet{chin-08} present an analysis that is entirely automated
and requires no user annotations.
% Automated analysis of assembly-level programs.
% Explicit deallocation.
% The analysis is defined as a composition of several successive analyses.
% The analysis relies on an alias type system that distinguishes unique/writable
% objects and shared/immutable objects.
% They seem to obtain precise (often exact) bounds in practice.
% The rules are hard to read.
% Restricted to linear bounds.
% The main difference with respect to chin-05 seems to be that chin-05
% requires explicit method specifications (aliasing information, size
% pre- and postconditions) whereas chin-08 is automated.
