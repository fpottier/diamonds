% ------------------------------------------------------------------------------

% Two papers by Chin et al.

\citet{nguyen-07} propose an automated verification system based on \SL.
They allow user-defined inductive predicates, which can be indexed with
sizes. There is no reasoning about deallocation.
%
\newcommand{\vheap}{\texttt{heap}\xspace}%
\newcommand{\vstack}{\texttt{stk}\xspace}%
%
\citet{he-09} re-use an existing \SL-based program verifier, Hip/Sleek, to
reason about stack and heap space. They consider a C-like imperative language
with explicit deallocation instructions. To reason about space,
% (without changes to the pre-existing program verification system)
they instrument the program with two global variables~\vstack and~\vheap of
type $\mathit{int}$, which represent the available space in the stack and in
the heap.
% These variables are typically mentioned in pre- and postconditions.
The special variable \vheap is decremented when a block is allocated and
incremented when a block is allocated.
% This is analogous to consuming and producing space credits.
% Space credits are arguably more modular and easier to use,
% especially in a concurrent setting.

% ------------------------------------------------------------------------------

% Cachera et al.

% \nocite{cachera-05}
% constraint-based static analysis
% intended application: embedded programs (such as JavaCard applets)
% that perform extremely little memory allocation. Automated.
% The analysis is purely concerned with counting allocations:
% there is no support for deallocation.
% According to Chin et al.,
%   "Cachera et al.'s loop-detecting algorithm can find methods and instructions
%   that execute for an unbounded number of times. This fact is only used to
%   check if memory usage is theoretically bounded or not."

% ------------------------------------------------------------------------------

% Albert et al.

In the setting of Java bytecode,
\citet{albert-ismm-07,albert-09,albert-10,albert-13} infer recurrence
equations that describe the heap space consumption of a method, expressed as a
function of the sizes of its arguments.
%
The system relies on an external analysis that infers object lifetimes and
determines when objects can be deallocated.
%
% (e.g., because they are unreachable, or because they are semantically dead).
%
% This analysis is not necessarily ``scope-based''.
%
For each method, several quantities are characterized via recurrence
equations: these include total memory allocation, active memory (memory that
is allocated by a method and cannot yet be deallocated when the method exits),
and peak heap space consumption.
%
\nocite{albert-ismm-07}
%
% Infers relations (recurrence equations) that describe the heap space
% consumption of a program as a function of the size of its arguments.
%
% An escape analysis is used to determine where deallocation can safely take
% place. In this case, the escape analysis marks allocation sites where the
% newly-allocated object will not escape the current method. (This seems
% roughly equivalent to wrapping every method in a letreg instruction and
% detecting that some objects can be allocated in this region. However, there
% is no way of allocating an object in a parent region, so this is weaker than
% a full-fledged region discipline.)
%
% Not restricted to linear bounds.
% Applied to Java bytecode.
%
\nocite{albert-09}
%
% A fully automatic live heap space analysis for an object-oriented bytecode
% language with a scoped-memory manager. (Therefore, not true GC; only an
% approximation of it.)
%
% Not restricted to a fixed complexity class.
%
% Produces a closed-form upper bound expressed in terms of the size of the
% input. (Therefore, cannot express data dependencies.)
%
\nocite{albert-10}
%
% Java-like programs.
%
% Assume that an external analysis infers when objects can be
% collected (e.g., because they are unreachable, or because
% they are semantically dead). This analysis is not necessarily
% scope-based.
%
% Generate and solve recurrence relations.
%  -- the relations are per-program-point
%  -- as opposed to per-method in many previous works
%
% See also \cite{albert-esop-07,costa} by the same authors.
% More focused on cost analysis = time complexity?
%
% Application to smart contracts:
% Don't run on fumes---Parametric gas bounds for smart contracts
% https://www.sciencedirect.com/science/article/pii/S0164121221000200
%
% Certified Abstract Cost Analysis
% https://www.dominic-steinhoefel.de/publication/albert-haehnle-ea-21/albert-haehnle-ea-21.pdf
%
\nocite{albert-13}
%
% Journal version of the previous three papers.
%
% In Section 3, the equations compute a "total memory allocation" figure
% that does not take deallocation into account.
%
% Section 4 defines the collectible objects. A per-type approxmiation is
% apparently used.
%
% Section 5 explains how to measure heap space consumption while taking
% deallocation into account. The intuition (Example 14) is that if an
% object allocated by a method f is provably deallocated before f exits,
% then it is not counted at all in the "active memory usage" of method f.
% -- The actual definition (Definition 15) is remarkably unclear.
% Then, the "peak heap space" is computed in terms of the active memory.
%
% ------------------------------------------------------------------------------
%
% Braberman et al.
%
Also in the setting of Java, \citet{braberman-06,braberman-08} and
\citet{garbervetsky-11} synthesize a formula that bounds the
amount of memory allocated by a method, as a function of
its parameters. The placement of deallocation operations
is inferred, but is scope-based. The focus seems to be on
loop nests; recursion is not supported.
%
\nocite{braberman-06}
% Applied to imperative (Java) programs.
% Synthesize a formula that gives an upper bound on the amount of memory
% allocated by a method, as a function of its parameters.
%   -- Does this assume that the parameters have type int?
%   -- Yes, this is confirmed on page 7.
% No annotations are required.
% The authors focus on non-recursive programs, mainly loop nests.
% They mainly want to count how many times each allocation instruction
% is executed.
% The authors write:
% ``Combining this algorithm with static pointer and escape analyses,
% we are able to compute memory region sizes to be used in scope-based
% memory management.''
% The idea is to posit that each method begins with a `letreg` instruction.
% The objects that can be allocated in this region are said to be *captured*
% by the method; those that cannot (because their lifetime is longer) are
% said to *escape*.
% The authors assume that an off-the-shelf escape analysis technique
% can tell which allocation sites are captured and which are escape.
%
% An important limitation of this work is that it counts allocation
% requests, but does not take deallocation into account. Thus, the
% bounds that are obtained are very pessimistic.
%
\nocite{braberman-08}
% Improve on the previous work by taking deallocation into account.
% Focus on a scope-based (region-based) discipline, that is,
% every method allocates a region, which is deallocated at the end.
%   -- They note that a GC can only do better (page 1).
% This system seems related to Hughes and Pareto's, but everything
% is inferred (and nonlinear bounds can be expressed).
% The focus is still on loops; recursion is not well supported
% (see the conclusion).
%
\nocite{garbervetsky-11}
% Journal version of previous two papers?
%
% ------------------------------------------------------------------------------
%
% Unnikrishnan, Stoller and Liu.
%
% \nocite{unnikrishnan-stoller-liu-01}
%
% For each function in the source program, they construct
% (1) a space function which takes the same input and returns the amount
%     of space used (what does that mean exactly?)
% (2) a space-bound function
%       (whose purpose is not really clear to me)
%
% The analysis concerns purely functional programs and uses reference
% counting to determine when an object can be deallocated.
%
% The program is explicitly instrumented (transformed) by introducing
% reference counting operations plus integer values "live" and "maxlive" that
% keep track (per constructor) of the current and maximum numbers of live
% objects.
%
% The bound functions that are obtained in the end are apparently still
% executable code, not mathematical expressions. According to Albert et al.,
% even their termination is not guaranteed.
%
% \nocite{unnikrishnan-stoller-liu-03}
%
% Seems to be an improved version of the previous paper; some parts are
% identical; a new "optimization" is mentioned, which can improve efficiency
% from exponential to polynomial.
%
In the setting of a pure, first-order functional language with garbage
collection, \citet{unnikrishnan-stoller-09} infer recurrence equations that
describe the heap space consumption of a function, expressed as a function of
the sizes of its arguments.
%
They rely on the fact that, when a function terminates, the objects that it
has allocated are either unreachable or reachable through its result.
%
% For each function f in the code, they construct three functions,
% whose parameters are the sizes of the parameters of f,
% which compute the following information:
% - (S) the maximum size of the live heap seen during the evaluation of $f$
% - (N) the newly-allocated space in the result of $f$
%       + this function has "size" and "newsize" arguments
%       + this seems required for compositionality
%       + they are the S and N values associated with free variables
% - (R) the size of the result of $f$
% The basic analysis is limited to lists of integers.
% - (S) uses max for conditionals, one for cons;
%       for sequences, a more complex formula
%       involving a maximum over all prefixes of the sequence
%       of a sum of N (for completed expressions) and S (for the current expression).
% - (N) is ... complex and relies on both N and R
%       N(let x = e1 in e2) is just N(e2) under appropriate bindings
% - (R) is simplest and depends only on itself
%
% Certain checks (MC1-MC3) are required to ensure that the analysis is
% monotonic (ie, keeping track of sizes only suffices).
% The analysis is complex and is not proved correct.
% They do not deal with tail recursion or higher-order functions.

% ------------------------------------------------------------------------------
