\section{Syntax and Semantics of \spacelang}
\label{sec:spacelang}

\input{fig-syntax}

% ------------------------------------------------------------------------------

\subsection{Values, Blocks, Stores}
\label{sec:spacelang:store}

We fix an infinite set $\Loc$ of \emph{memory locations}, also known as
addresses. We view memory locations as abstract identifiers: one cannot
convert a memory location to an integer, ask which of two locations is
smaller, or ask whether two locations are adjacent. We let $\loc$, $\sloc$,
$\src$, $\dst$ range over memory locations. By convention, we use $\loc$ for
an address in the heap and $\sloc$, $\src$, $\dst$ for addresses of stack
cells.

% Whereas the formalization distinguishes between states and stores, we gloss
% over this destruction on the paper. That is, \maxsize is viewed as a global
% parameter instead of a component of the machine state.

A \emph{store} $\store$ is a finite map of memory locations to blocks, where a
block can be a heap block, a stack cell, or a deallocated block. Thus, the
store encompasses the heap and the stacks. (In a concurrent setting, there is
one stack per thread.) Such a unified representation of the store is
convenient, as it contains enough information to define the action of the
garbage collector (\sref{sec:gc}). In particular, the roots are easily
identified: they are the stack cells.
%
% We rely on the fact that it is possible to distinguish a heap block and a
% stack cell. That said, an actual implementation can operate in a completely
% different way, and does not necessarily need to be able to make this
% distinction.
%
% It is not necessary to know what code is currently executed by each thread
% in order to determine the roots.

\newcommand{\recyclingfootnote}{\footnote{%
%
In a realistic implementation, memory locations are recycled. However, we must
work at a higher level of abstraction where memory locations are never reused.
Indeed, the reasoning rules of \SL guarantee that memory allocation always
produces a fresh address.
%
Ideally, one should prove that the semantics presented in this paper is
equivalent (regarding both functional behavior and live heap space) to a
semantics where memory is reused. We have not done so yet.
%
}}

The syntax of \emph{blocks} appears on the second line of \fref{fig:syntax}. A
heap block $\btuple\vals$ is a tuple: it stores a sequence $\vals$ of values.
A stack cell $\bcell\val$ stores a single value~$\val$. The special block
$\bdeallocated$ denotes a deallocated block. Our operational semantics does
not recycle memory locations:\recyclingfootnote{} when a heap block or stack
cell at address~$\loc$ is deallocated, the store is updated with a mapping of
$\loc$ to $\bdeallocated$.

The syntax of \emph{values} appears on the first line of \fref{fig:syntax}. A
value is a piece of data that fits in one word of memory. The
values include the unit value~$\vunit$, integer values~$\vk$, memory
locations~$\loc$,
% The reader may wonder whether the address of a stack cell is a value,
% i.e., whether there can be pointers from the stack or the heap into the
% stack. Our operational semantics allows it, and most of the setup of our
% program logic allows it as well; however, the high-level reasoning rule that
% we give for alloca forbids it by not giving the user access to the mapsfrom
% assertion for the newly-created stack cell.
and procedures $\vcode\vars\instr$.
%
A value of the form $\vcode\vars\instr$ is a closed procedure whose formal
parameters are the variables $\vars$ and whose body is the instruction
$\instr$.
% In the formalization, we do not require procedures to be closed, but it is
% impossible to prove anything useful about a procedure that is not closed,
% Since a reference to a free variable is unsafe. (Technically, references to
% free variables are permitted as long as they're never executed.)
It can be thought of as a code pointer.

% A variable is always bound to an address in the stack, never to an address
% in the heap. However, this invariant is never explicitly stated.
% Our reasoning rules require the user to prove, at every use of a variable,
% that this variable is bound to a memory location for which a pointsto
% fact is available and proves that this is the location of a stack cell.
% This imposes no additional burden on the user compared to a \SL where
% variables are viewed as resources~\cite{bornat-calcagno-yang-06}.

Every memory block~$\blk$ has a size~$\sz\blk$,
also defined in \fref{fig:syntax}. % (inlined, to save space)
%
The size of a heap block $\btuple\vals$ is one (to account for a header
word) plus the number of values stored in this block.
% (every value occupies one word)
% This definition is arbitrary, and could be altered if desired.
%
The size of a stack cell is zero, because we are interested only in measuring
the size of the heap. The size of a deallocated block is zero.
%
The size of a store is the sum of the sizes of its blocks.

\label{sec:valid}
We fix a \emph{maximum size}~$\maxsize$, and we say that a store~$\store$ is
\emph{valid} if $\sz\store\leq\maxsize$ holds. The parameter~$\maxsize$ plays
a role in the operational semantics (\sref{sec:semantics}) and in the
statement of soundness of \DIAMS (\sref{sec:soundness}). In~the semantics, it
is used to introduce the possibility of a runtime failure: an attempt to
allocate a new heap block fails if this allocation would cause the store to
grow too large and become invalid. Thus, by design, the semantics guarantees
that the size of the store never exceeds~$\maxsize$. The problem, therefore,
is to rule out the possibility of a runtime failure. This is what the
soundness statement is about: it guarantees that, if a program is correct
according to \DIAMS, then this program is \emph{safe}: it~cannot fail at
runtime.
%
The parameter~$\maxsize$ appears in this statement, and also plays a role in
its proof. Indeed, it is used at the most basic level of the proof, in the
definition of the semantics of space credits. One space credit~$\SC1$ is
defined roughly as ``the ownership of one unit of available space'', where the
available space in a store~$\store$ is defined as $\maxsize-\sz\store$.
%
Even though~$\maxsize$ plays a role in the metatheory of \DIAMS, it~does not
appear in any of its reasoning rules. Thus, users need not know about
$\maxsize$, and program specifications and proofs are independent
of the value of $\maxsize$.

We define the multiset $\pointers\val$ as follows: if $\val$ is a memory
location~$\loc$, then $\pointers\val$ is $\{\loc\}$; otherwise, it is
$\emptyset$. The multiset~$\pointers\blk$ is the multiset sum of the
multisets~$\pointers\val$, where~$\val$ ranges over the values stored in the
block~$\blk$.
%
It represents the outgoing pointers of the block~$\blk$.

% ------------------------------------------------------------------------------

\subsection{References}
\label{sec:spacelang:references}

Next in \fref{fig:syntax} come the syntaxes of \emph{references}~$\lval$ and
instructions~$\instr$. A reference~$\lval$ is either a variable~$\var$ or a
memory location $\sloc$, which represents the address of a stack cell.
\spacelang departs from ordinary call-by-value $\lambda$-calculus, whose
variables denote values, and whose syntax is stable under substitutions of
values for variables. In \spacelang, instead, a variable denotes the address
of a stack cell, and the syntax of instructions is stable under substitutions
of memory locations for variables. Put another way, whereas in ordinary
$\lambda$-calculus the stack is entirely implicit,
% One could say that the stack is the evaluation context,
% but there is clear notion of "current evaluation context".
in \spacelang, it is partly explicit:
every stack cell has an address and exists in the store.
% Actually, the stack is only partly explicit. Local variables are
% materialized as stack cells, but the evaluation context remains
% implicit as usual.
%
% je coupe car il me semble que ça a déjà été dit plus haut:
\begin{comment}
We make this design choice because it enables us to give a very simple
definition of garbage collection, where garbage collection has access only to
the store, and where the roots are the stack cells.%
%
\footnote{In ordinary $\lambda$-calculus, in contrast, it seems that garbage
  collection would need to have access not only to the store, but also to the
  code of every thread, so as to be able to determine which heap addresses
  appear in the code and therefore should be considered roots.}
%
\end{comment}

As a consequence of this design, there is no way for an
expression to return a value.
% Indeed, that would require referring to this value via a name, a variable;
% but variables stand for references, not values.
% Values are not a subset of expressions.
For this reason, \spacelang has instructions instead of expressions.
% which do not return a result.
Similarly, it has procedures instead of functions.
Procedure calls have call-by-reference semantics: the
formal parameters of a procedure denote references (addresses of stack cells),
not values. This lets a procedure read and write
stack cells in its caller's frame.
% or: the local variables of its caller.
%
Although this convention may seem baroque,
% to someone who is accustomed to call-by-value,
it respects our decision that variables denote references, not values.
%
Call-by-value can be simulated by letting the callee allocate its own local
storage, copy parameters into~it upon entry, and copy results out of it upon
exit.

% ICall is call-by-reference. We choose this convention because it is the
% simplest (most primitive) one; in particular, it does not involve stack
% allocation. Call-by-value can be simulated by letting the callee allocate
% new stack cells for its local copy of the parameters and copy the parameters
% into them. And, the other way around, by allocating a new stack cell to hold
% the result, and copying this stack cell into the caller's result cell at the
% end.

% ------------------------------------------------------------------------------

\subsection{Instructions}
\label{sec:spacelang:instructions}
\label{sec:semantics}

\input{fig-semantics}

The syntax of instructions is given in \fref{fig:syntax}. Their
small-step operational semantics, a~reduction relation of the form
$\hsfork{\instr}{\store}{\instr'}{\store'}{\instrs}$,
is defined in \fref{fig:semantics}.
%
The last argument~$\instrs$ is a list of newly-spawned threads;
when it is empty, we omit it
and write just $\hs{\instr}{\store}{\instr'}{\store'}$.
%
% Also, when it is a singleton, we omit the square brackets (StepFork).

% We briefly review the syntax and reduction rules.
%
The no-op ``$\iskip$'' and the sequencing construct
``$\iseq{\instr_1}{\instr_2}$'' are standard.
%
The conditional construct ``$\iif\lval{\instr_1}{\instr_2}$'' tests whether
the integer value stored in the stack cell~$\lval$ is nonzero or zero.
(We write $\metaif{\vk\ne0}{\instr_1}{\instr_2}$ for a meta-level conditional.)
%
The procedure call ``$\icall\lval\lvals$'' invokes the procedure whose address
is stored in the stack cell~$\lval$. The actual arguments~$\lvals$ are
\emph{addresses} of stack cells.

The instruction ``$\iconst\lval\val$'' writes~$\val$ into the stack
cell~$\lval$.%
%
\footnote{We write $\mupd\loc\blk\store$ for the store that maps $\loc$ to
$\blk$ and that maps every other location~$\loc'$ to $\store(\ell')$. This
represents either an extension or an update. For greater clarity, in
\fref{fig:semantics}, we write $\store' = \mext\loc\blk\store$ as a short-hand
for $\store' = \mupd\loc\blk\store$ together with the side condition
$\loc\not\in\dom\store$, which indicates that the store is extended.
We write $\writelval\dst\val\store{\store'}$ as a short-hand for
$\writelvalmaincondition\dst\val\store{\store'}$ together with the side
condition $\writelvalsidecondition\dst\val\store{\store'}$. This indicates
that an existing stack cell at address~$\dst$ is updated with~$\val$.
The side condition prevents updating a nonexistent stack cell
or turning a~heap block into a stack cell.}
% (see the definition of \writelval in Coq)
%
The side condition $\pointers\val = \emptyset$ in \RULE{StepConst} means that
$\val$ must be a constant, that is, a unit value, an integer value, or a code
pointer. It cannot be a memory location: a memory location is not a constant.
%
% This is in keeping with the design decision that instructions must not
% contain memory locations.

The move instruction ``$\imove{\lval_1}{\lval_2}$'' copies the content of the
stack cell $\lval_2$ into the stack cell $\lval_1$.

The allocation instruction ``$\ialloc\lval\allocsize$'' allocates a new heap
block with $\allocsize$ fields, initializes them with unit values,%
\footnote{We write $\btuple{\replicate\allocsize\vunit}$ for a tuple of
  $\allocsize$ unit values.}
%
and writes its address into the stack cell $\lval$. The side condition
$\valid{\store'}$ in \RULE{StepAlloc} means that this instruction fails at
runtime (it is stuck) if, as a result of this allocation, the
size of the heap exceeds~$\maxsize$.
%
The load instruction ``$\iload{\lval_1}{\lval_2}\ofs$'' fetches from the stack
cell~$\lval_2$ the address of a heap block, reads this block at offset~$\ofs$,
and stores the resulting value into the stack cell $\lval_1$.
%
The store instruction ``$\istore{\lval_1}\ofs{\lval_2}$'' fetches from the
stack cell $\lval_1$ the address of a heap block, then writes into this block,
at offset~$\ofs$, the value stored in the stack cell $\lval_2$.
%
The address comparison instruction ``$\iloceq\lval{\lval_1}{\lval_2}$''
compares the memory locations stored in the stack cells $\lval_1$ and
$\lval_2$ and writes the result of the comparison (encoded as the integer
value 0 or 1) into the stack cell $\lval$.

The stack allocation construct ``$\ialloca\var\instr$'' allocates a
fresh stack cell,
% initialized with a unit value,
binds the variable~$\var$ to the address of this cell, and executes the
instruction~$\instr$.
% which may refer to~$\var$.
% where $\var$ is in scope.
% This is a binding construct.
Once the execution of $\instr$ terminates, the stack cell is deallocated.
%
Technically, this process happens in three phases. First,
\RULE{StepAllocaEntry} lets ``$\ialloca\var\instr$'' reduce to
``$\iallocaactive\sloc{\subst\var\sloc\instr}$'', where $\sloc$ is the address
of the newly-allocated cell.
Then, since ``$\ialloca\sloc\hole$'' is an evaluation context,
\RULE{StepContext} allows reducing
the instruction~$\subst\var\sloc\instr$, in~zero, one or more steps,
to $\iskip$.
Finally, \RULE{StepAllocaExit}
deallocates the stack cell at
address~$\sloc$.
%
Thus, stack cells are indeed allocated and
deallocated according to a stack discipline.
%
If one wishes for a stack cell to be considered dead (that is, no longer a
root) before it is deallocated, then one must explicitly kill this cell,
that is, write a unit value into~it.
%
% I believe that this is not truly a limitation (in a translation from a
% high-level calculus into \spacelang, this write instruction could be
% automatically inserted) except in the case of tail calls, where the
% responsibility of overwriting the cell must be delegated to the callee,
% which is bad, since the callee should not have to know whether it has
% been called or tail-called.

The construct ``$\ifork\lval\var\instr$'' reads a value~$\val$ from the
stack cell~$\lval$, overwrites this cell with a unit value,
initializes a fresh stack cell with~$\val$,
and spawns a new thread that executes~$\instr$
under a binding of the variable~$\var$ to the new stack cell.
%
This seems the simplest way of allowing a value~$\val$ to be transmitted
from the original thread to the new thread.
%
The use of a destructive read
%
% removes the need for the original thread to kill the cell~$\lval$
%
% which would be problematic, as it would require a vmapsfrom assertion
% for the value~$\val$. Our reasoning rules would not tolerate it easily,
% and in fact it would be semantically dangerous, as it would mean that
% the new thread begins executing while the original thread still holds
% a pointer to $\val$; this could technically lead to higher space usage
% than desired.
%
ensures that there is no point in time where both threads hold
a pointer to $\val$.
% unless the original thread keeps a copy of it, of course
This makes ``$\kw{fork}$'' better-behaved and easier to reason about
in terms of space usage.

% ------------------------------------------------------------------------------

\subsection{Garbage Collection and Concurrency}
\label{sec:gc}

The relation $\hsfork{\instr}{\store}{\instr'}{\store'}{\instrs}$ describes
one step of computation taken by one thread. To fully describe the
semantics of \spacelang, there remains to introduce garbage collection and to
allow the interleaving of steps taken by multiple threads.

We model garbage collection as a nondeterministic relation between stores
$\gc\store{\store'}$. Its definition is standard: in short, garbage collection
\emph{may} deallocate any block that is not reachable from a root.

\begin{definition}[Terminology]
Fix a store $\store$.
With respect to this store,
there is an \emph{edge}
from $\loc$ to $\loc'$ if
% for some $\blk$
$\store(\loc)=\blk$ and $\pointers\blk\ni\loc'$.
%
A \emph{path} from $\loc$ to $\loc'$
is a succession of zero or more edges
leading from $\loc$ to $\loc'$.
%
A location~$\sloc$ is a \emph{root} if
% for some $\val$
$\store(\sloc)=\bcell\val$.
%
A location is \emph{reachable} if
there is a path from some root to this location.
\end{definition}

\begin{definition}[Garbage Collection]
  The relation $\gc\store{\store'}$ holds if
  (1) the stores~$\store$ and~$\store'$ have the same domain
  and
  (2) for every location~$\loc$ in this domain,
  either $\store'(\loc)=\store(\loc)$,
  or $\loc$ is unreachable (with respect to $\store$)
  and $\store'(\loc)=\bdeallocated$.
\end{definition}

The reduction relation is combined with garbage collection by sequential
composition:

\begin{definition}[Reduction Step with Garbage Collection]
The relation $\headstepgc$ is defined as follows:
\begin{mathpar}
\inferrule{
  \gc\store{\store'} \\
  \hsfork{\instr}{\store'}{\instr'}{\store''}{\instrs}
}{
  \hsgcfork{\instr}{\store}{\instr'}{\store''}{\instrs}
}
\end{mathpar}
\end{definition}

Garbage collection takes place immediately before every reduction step.
%
This confers an angelic character to the nondeterminism that is
inherent in garbage collection. Indeed, the relation
$\gcSymbol$ is nondeterministic, while the relation $\headstep$ is partial:
some machine configurations $\cfg\instr\store$ are unable to make a step. In
particular, according to the relation $\headstep$, memory allocation fails if
there is not enough space in the heap.
% this is visible in the reduction rule \RULE{StepAlloc}.
According to the relation $\headstepgc$, however,
allocation fails \emph{only if there is no way for the garbage collector to
  free up enough space}.

% fp: removed to save space
\begin{comment}
If desired, one could also define garbage collection as a deterministic
process that must deallocate \emph{all} unreachable heap blocks. That would be
a more restrictive semantics, with respect to which the program logic
presented in this paper is also sound.
\end{comment}

The last step in the definition of the semantics of \spacelang is to keep
track of the existence of multiple threads, sharing a single store, and to
allow arbitrary interleavings of their execution steps.
%
We omit the required definitions, which are standard: Iris provides
them~\cite[\S6.1]{iris}.
%
% It is worth noting, in our setting, that garbage
% collection applies to the whole store, which is shared by all threads.
%
% This is possible because the store contains every thread's stack,
% and stack cells are recognized as roots by the garbage collector.

% ------------------------------------------------------------------------------
