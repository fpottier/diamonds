\section{Conclusion}
\label{sec:conclusion}

We have presented \DIAMS, a \SL that allows reasoning about the heap space
usage of a program (or program component) in the presence of tracing garbage
collection. We believe that it is the first logic of this kind.

% ------------------------------------------------------------------------------

% How? (Ingredients.)
%
\emph{Space credits}, which can be understood as permissions to allocate
memory or as witnesses that memory is available, are consumed at allocation
sites and produced at deallocation sites. Their use in pre- and postconditions
allows describing how much space a function needs, consumes, or frees up.
Because garbage collection is automatic, \emph{deallocation is a ghost
  operation}: it is not explicit in the source code. The person who verifies
the code may use this operation wherever desired, but must prove that the
objects that are about to be \emph{\logically deallocated} are unreachable.
For this purpose, the logic keeps track of the predecessors of each object via
\emph{pointed-by assertions}. By default, these assertions keep track of a
multiset of predecessors. However, in some cases, one can get away with
less precise, therefore more tractable,
% easier to maintain
predecessor information. For instance, we propose a set of rules that keep
track of the \emph{number} of predecessors, a \emph{ghost reference count}. In
future work, we would like to investigate a way of recording what
\emph{regions} the predecessors inhabit, without necessarily keeping track of
the number or identity of the predecessors.

% ------------------------------------------------------------------------------

% A potential criticism.

Although we give the user a programming language with garbage collection, we
require her to be explicit about memory deallocation while verifying a
program, and we impose on her the burden of keeping track of predecessors. Our
work could be criticized on this basis: the reader may wonder, why not use a
programming language with manual memory management? Our answer is, there are
good reasons why many programmers choose a language with garbage collection:
simplicity, safety, efficiency, guaranteed collection of all unreachable
objects come to mind. Not every component in a program is verified. One pays
the cost of reasoning about predecessors and deallocation only at verification
time, and one chooses which program components are most in need of
verification.

% ------------------------------------------------------------------------------

% Soundness / trust.

We have proved the soundness of \DIAMS using the Coq proof
assistant~\citerepo. Because we have defined \DIAMS as an instance of
Iris~\cite{iris}, it inherits all of the power of Iris, including support for
shared-memory concurrency, shared invariants, ghost state, and more.

% ------------------------------------------------------------------------------

% Limitations and future work.

Although we believe that \DIAMS is an important step forward, much remains to
be done. First, \spacelang is low-level. It~requires values to be
explicitly stored in mutable stack cells; it sometimes requires writing a unit
value into a stack cell so as to indicate that this cell is no longer a root;
%
% Inserting these "kill" instructions into the code means that some tail calls
% are no longer tail calls. But I don't think this is really a problem, as a
% compiler can recognize that these "kill" instructions are superfluous and
% remove them. What is more problematic is what happens at a normal call: a
% stack cell that holds an argument cannot be killed by the caller (because
% that would make it impossible to transmit the argument), yet we would prefer
% not to give the responsibility of killing to the callee (because the callee
% should not care whether the caller does or does not retain a root).
% Something is imperfect here.
%
it uses a somewhat odd call-by-reference discipline; and it does not have
local functions with free variables.
%
% (no closures; they must be simulated)
%
In future work, we would like to propose a program logic for a higher-level
language, such as call-by-value \lc, either via a direct definition or via a
translation down to \spacelang.
%
% This is where closures would have to be macro-expressed. (Closure conversion.)
%
% We would also like to investigate the idea of allowing garbage collection to
% take place only when every thread has reached a ``safe point''---that would
% allow values to be ``in flight'' (i.e., not necessarily stored in stack cells)
% between two safe points.
%
Second, \DIAMS, too, is low-level: in particular, keeping track of
predecessors can be cumbersome. We believe that introducing abstractions, such
as the number of predecessors, or the regions that they inhabit, is a
promising way of making this task more tractable. Still, more experience is
needed in order to identify the most lightweight and most modular way of
keeping track of predecessors in specifications and proofs.
%
Third, although we have verified a few very small functions, we lack
experience with \DIAMS. We would like to verify more challenging examples,
including sequential and concurrent data structures.
%
% ------------------------------------------------------------------------------
%
% End-to-end guarantee.
%

% Although \DIAMS allows establishing a bound on the size of the live heap data,
% this does not immediately allow establishing a bound on the size of the heap.

\DIAMS allows verifying that the size of a program's live data cannot exceed a
certain bound~$\maxsize$. However, from this fact, one cannot immediately
conclude that a certain heap size~$N$ is sufficient for the program to be
safely executed. Indeed, the relationship between the size of the live data
and the heap size depends on which garbage collection algorithm is chosen. For
instance, if a copying collector~\cite{cheney-70} is used, then setting
$N = 2\maxsize$ suffices. Ideally, it seems desirable to use an implementation
of garbage collection whose correctness and memory requirement have been
verified:
% i.e., the relationship between N and S should be verified, too.
% I imagine that this is usually the case.
several such implementations are described in the literature
\cite{mccreight-07,mccreight-10,hawblitzel-petrank-10,%
  ericsson-myreen-pohjola-19,wang-cao-mohan-hobor-19}.
%
It would be particularly interesting to adapt \DIAMS to DataLang,
an~intermediate language of the CakeML compiler. \citet{gomez-londono-20} have
equipped DataLang with a formal cost semantics and have proved that the CakeML
compiler and garbage collector respect this semantics.
%
% Dans l'article page 5, on voit qu'ils ont fait la preuve pour deux GCs
% possibles, le GC simple de Myreen (2010) et le GC à générations de
% Sandberg Ericsson et al. (2019). Toutefois, je ne vois pas où le lien
% entre les paramètres S et N est discuté.
%
Developing a variant of \DIAMS for DataLang would offer CakeML programmers a
fully trustworthy methodology for verifying the space usage of their programs.

% fp: a longer version:
\begin{comment}
The operational semantics of \spacelang is a cost model: it specifies which
locations are regarded as roots by the garbage collector; this determines
the space complexity of the program.
%
\DIAMS is sound with respect to this cost model: if a program can be verified
under the precondition $\SC{n}$, then its execution under the semantics of
\spacelang does not need more than $n$ words of heap space.
% (not counting the overhead imposed by the GC, such as an empty to-space)
%
Now, suppose a compiler translates \spacelang down to some machine language.
Can the compiled program also be safely executed in $n$ words of heap space?
%
In order to obtain such a guarantee, one must prove that the compiler
preserves (or decreases) the space usage of the program.
%
\citet{gomez-londono-20} carry out such a proof. They define a cost semantics
for DataLang, one of the intermediate languages of the CakeML compiler, and
they prove that the CakeML compiler preserves
% the space usage bounds predicted by
this semantics.
%
% DataLang is equipped with garbage collection; its operational semantics
% includes a model of the operation of the garbage collector.
%
In future work, it would be interesting to adapt \DIAMS to DataLang, so as to
provide CakeML programmers with a fully trustworthy methodology for verifying
the space usage of their programs.
\end{comment}

\begin{comment}
Are there program transformations that change a program's space complexity?
Indeed, there are some: e.g., transforming ``if true then e1 else e2'' into
``e1'' implies that the local variables that occur in ``e2'' are no longer
considered roots; this can decrease the program's space complexity. In the
reverse direction, transforming ``let y = x.f in e[y]'' into ``let y = x.f in
e[x.f]'' can extend the live range of the variable~$x$, and therefore can
increase the program's space complexity. (We have observed that the OCaml
compiler does in some situations perform such a transformation, perhaps
because it can decrease the live range of the variable~$y$, thereby reducing
register pressure and leading to more efficient code.) Thus, the space bounds
that can be proved, at the level of a source program, using our program logic,
are not necessarily preserved by a real-world compiler.
\end{comment}

% ------------------------------------------------------------------------------

% Random comments.

\begin{comment}
Nothing forces the person who verifies the program to introduce \verb+free+
operations in a wise way (everywhere possible, as early as possible). However,
failure to do so causes a ``static memory leak'' and can make it impossible to
establish the desired space complexity bound, so it usually will not go
unnoticed.
\end{comment}

\begin{comment}
Exceptions (or effect handlers) are not currently supported. Probably
exceptions could be easily supported, but delimited continuations seem more
problematic, because they transform a stack segment into a heap-allocated
object, so we may have to abandon the rule that ``every stack cell is a
root''.
\end{comment}

\begin{comment}
Our program logic seems (is?) sound in the presence of shared-memory
concurrency. Two problems, though:
- not clear how to express fork in an elegant way in our
  language without variables or closures;
- not clear how to reclaim the space credits owned by a thread that
  has died.
Whether it is powerful enough to reason about the space
usage of realistic concurrent programs is another question.
\end{comment}

\begin{comment}
If stack frames are somehow explicit in the logic, they we can reason about
their size, and we can use a different currency so as to separately measure
the size of the stack and the size of the heap.
\end{comment}

\begin{comment}
We could use a distinct currency for each block size, so, instead of summing
everything and saying ``this code needs N words'', we could say ``this code
needs N1 blocks of size 1, N2 blocks of size 2, N3 blocks of size 3, etc.''
This could be useful in a scenario where the allocator maintains distinct
pools and one wishes to bound the size of each pool independently.
\end{comment}

\begin{comment}
Giving a precise cost model at the level of the source language can be
problematic: there is a risk of restricting the space of permitted
optimizations or of defining a cost model that is overly complex
because it attempts to anticipate a large space of optimizations.
One's best bet is probably to give a relatively pessimistic cost
model at the source level (thus giving the compiler more leeway)
and to prove asymptotic complexity bounds about programs.
\end{comment}

\begin{comment}
We have assumed that the garbage collector is allowed to reclaim all
unreachable blocks. If one wanted to use a weaker garbage collector,
e.g. one that is based on reference counting and therefore cannot
reclaim cycles, then the reasoning rule \RULE{Free} would need to be
adapted so as to reflect the limited power of the garbage collector.

An aggressive compiler can perform many program transformations
(standard/lightweight/selective closure conversion, defunctionalization,
monomorphization, specialization, inlining, worker/wrapper transformations,
etc.) so it can be hard to tell, by inspection of the source program, where
memory is allocated. Hopefully, every transformation performed by the compiler
should result in a space improvement. (Cite Sands et al.) Again, it is up to
the author of the compiler to guarantee this property.
\end{comment}

\begin{comment}
A potential future extension: in a programming language where the garbage
collector cannot be invoked anywhere but only at certain well-defined points,
we would still be able to reason. We would have to distinguish actual space
credits (consumed by allocation) and latent space credits (produced by logical
deallocation). An explicit invocation of the garbage collector would turn any
amount of latent credits into actual credits. Thus, programmers would be able
to limit the number of places where the garbage collector can cause a pause,
yet would still be able to reason about the space complexity of the program
and to predict how much memory it requires in order to run safely. The
distinction between actual and latent credits must appear as soon as the
memory allocation instruction does not include an implicit invocation of the
garbage collector.
\end{comment}

\begin{comment}
A reviewer points out that SpaceLang does not have global variables. They
should not be too difficult to add. They could also be simulated as local
variables of a main thread that runs forever (or waits for every other
thread).
\end{comment}

\begin{comment}
A reviewer points out that, in some lock-free data structures, it can be
difficult to know exactly when certain objects become unreachable.
\end{comment}

\begin{comment}
At the moment, we have a logic that reasons about tracing garbage collection
and that can use a form of static reference counting. A related but distinct
contribution would be a logic that reasons about runtime reference counting.
Such a logic would have to be slightly different; the reasoning rule
\RULE{Free} would presumably have to be restricted so as to disallow
collecting cycles.
\end{comment}

\begin{comment}
A reviewer notes that finalisers can bring an object back into scope. As the
reviewer points out, resurrection is possibly problematic, but there is an
even more basic problem: it is not clear how to justify the fact that the
finaliser is allowed to access the object at all; how does it acquire the
necessary points-to assertion? And, if it wishes to resurrect the object by
creating a new pointer to it, how does it acquire the necessary pointed-by
assertion? It is not clear how the finaliser could ``steal'' these permissions
from the program, which may still hold them.
\end{comment}
