% ------------------------------------------------------------------------------

% Our explicit treatment of stack cells is somewhat reminiscent of the
% treatment of variables as resource proposed by
% \citet{bornat-calcagno-yang-06}.

% ------------------------------------------------------------------------------

% Program logics that keep track of cost (time, space).

Several program logics that keep track of time or of a user-defined notion of
cost have been proposed.
%
For instance, \citet{aspinall-07} propose a VDM-style program logic where
postconditions depend not only on the pre-state, post-state, and return value,
but also on a cost.
%
\citet{atkey-11} extends \SL with an abstract notion of resource, such as time
or space, and introduces an assertion that denotes the ownership of a certain
amount of resources.
%
These systems can be used to count memory allocations, but do not model
garbage collection.

% Atkey considers a language where a $\kw{consume}$ instruction consumes a
% certain amount of resources, while an $\kw{acquire}$ instruction allows
% attempting to acquire more resources (and may fail at runtime). Such machinery
% can be used to reason about heap space in the absence of garbage collection:
% it is, in fact, inspired by Hofmann and Jost's work
% [\citeyear{hofmann-jost-03}].

% ------------------------------------------------------------------------------

% Separation Logic in the Presence of Garbage Collection.

A remarkable property of \SL is that it is applicable both to low-level
programming languages where memory deallocation is explicit
% (without garbage collection)
and to high-level languages equipped with garbage collection.
% (where deallocation is implicit).
%
% All it takes is to include or exclude a reasoning rule for $\kw{free}$.
%
An interesting middle ground is studied by \citet{hur-dreyer-vafeiadis-11},
who wish to reason about low-level code that must interact with the garbage
collector of a high-level language. This requires stating exactly what
invariant the garbage collector relies upon and when this invariant must hold.
% GCSL only accounts for the presence of a stop-the-world collector, not an
% incremental or concurrent one. Adapting GCSL to these other kinds of
% collectors is left to future work.
% Internal pointers are supported but can be stored only in variables, not
% in memory blocks.
Hur \emph{et al.} introduce a distinction between the physical heap and the
logical heap, and require that their reachable parts be isomorphic. We adopt a
very similar setup.
% Ours is slightly simpler because our garbage collector can only free memory,
% not move it. This is not a limitation of our system, just a consequence of
% the fact that we work at a slightly higher level of abstraction.
%
This resemblance is partly coincidental, as the two setups in fact serve
slightly different purposes. Hur \emph{et al.} aim to bridge the gap between a
physical heap where the address of a block may change over time, and where a
block may be deallocated, and a logical heap where a block is never moved or
deallocated. We aim to bridge the smaller gap between a physical heap where
blocks are deallocated (never moved) by the garbage collector and a \logical
heap where blocks are manually deallocated (never moved) by the user as
part of her reasoning about the program.
% They consider a simple WHILE language without procedures or threads, so the
% treatment of roots is probably simpler in their setting.

Incorrectness Separation Logic~\cite{raad-isl-20} introduces a negative heap
assertion $\loc\not\mapsto$, which means that the location~$\loc$ has been
deallocated (and not reallocated). This assertion is used to detect
use-after-free bugs. It is not duplicable, therefore somewhat different from
our persistent assertion $\ddagsingleton\loc$. Indeed, our locations are never
recycled, whereas Raad \etal. work in a lower-level setting where a freed
location can be reallocated.
% E.g., their rule Alloc2 shows that allocation *can* transform a
% deallocated location into an allocated location.

% ------------------------------------------------------------------------------

% Kassios and Kritikos.

\citet{kassios-kritikos-13} pioneer the use of pointed-by assertions in \SL.
The manner in which pointed-by assertions are used in the reasoning rule for
assignment is the same in our paper as in theirs.
% (modulo the fact that the languages are different)
There are minor design differences: their pointed-by assertions involve sets,
whereas we use multisets; their pointed-by assertions cannot be split, whereas
ours can; their pointed-by assertions are per-field and optional, %
% fp: removed to save space:
% \footnote{For every field~$f$, if the user so desires, a ghost field~$f^{-1}$
%  is introduced.}
whereas ours are mandatory and not per-field.
%
% They keep track of all predecessors, regardless of which field contains the pointer.
%
Kassios and Kritikos use pointed-by assertions to verify concurrent
copy-on-write lists, a data structure where reference counts are used to
determine whether an object should be copied before it is updated.
%
It would be interesting to transport the specification and proof of this data
structure to \DIAMS and to enrich its specification with space usage
guarantees.
%
% We should be able to reason in a pessimistic way, i.e. consider that a
% copy always takes place, even though sometimes sharing is possible and
% the space consumption is less than expected.
