\section{Introduction}

% Basic introductory paragraph: resources: time and space, zooming down
% onto heap space.

In a world where software can be found in almost every kind of device,
including critical embedded systems, mobile phones, and desktop computers, it
is desirable to statically guarantee not only that this software will run
safely and behaves in a correct manner, but also that it will not attempt to
consume more resources than expected.
%
Although one can think of many kinds of resources, including electric energy,
money, and more, two of the most basic computational resources are time and
space.
%
One must distinguish several kinds of space, or storage areas, that are
managed in different ways: these include stack space, heap space, disk space,
and more.
%
In this paper, we are specifically interested in reasoning about \emph{heap
  space usage} in the presence of \emph{tracing garbage collection}, a widely
used technique for automatic reclamation of unused memory.
%
The problem is challenging because it is not evident in the code where memory
can be reclaimed: there is no memory deallocation instruction. Instead,
objects implicitly become eligible for deallocation once they are unreachable.
%
The problem is to statically determine \emph{what data is live} at each
program point and to statically predict and control the size of this live
data.
%
We do not keep track of stack space;
others have done so before~\cite{carbonneaux-14}.

% ------------------------------------------------------------------------------

% Our goal: a program logic. Zoom onto \SL.

To analyze and control the space usage of programs, many techniques have been
developed, including static analyses, type systems, and program logics.
A~fraction of this rich literature is reviewed in \sref{sec:related}. In this
paper, we aim to develop a program logic, that is, a set of program
verification rules.
%
Naturally, these rules must be sound: a verified software component
should never consume more space than advertised by its specification.
%
They must be compositional: we wish to verify software components
independently, while being assured that verified components can be safely
linked.
%
Finally, they should be expressive: although this is an ill-defined goal, we
would like to be able to reproduce the informal reasoning patterns that
programmers commonly use.
%
% Examples of the kinds of specs that we want to express and prove.
%
As an example, we wish to be able to state and prove that a list copy
(\sref{sec:list:copy}) or list reversal function normally requires $2+4n$
words of available space,
%
where $n$ is the length of the original list. More subtly, in case no
pointer to the original list is retained, we wish to argue that no available
space is in fact required, as the new list cells can occupy the space that is
freed up as the original list cells become unreachable.
%
As another example (\sref{sec:stack}), we wish to be able to state and prove
that creating an empty stack and pushing an element onto this stack each
require $4$ words of memory, that popping an element off the stack frees up
$4$ words, and that letting go of the last pointer to the stack frees up
$4+4n$ words of memory, where $n$ is the number of elements in the stack.
%
We also need to keep track of the fact that pushing an element onto the stack
creates a pointer from the stack to the element, and that popping this element
off the stack (or abandoning the stack altogether) destroys this pointer.

We base our work on \SL~\cite{reynolds-02,brookes-ohearn-16,ohearn-19} and its
modern variants, specifically Iris~\cite{iris}, because they are natural and
powerful frameworks in which one can reason about programs.
%
% ------------------------------------------------------------------------------
%
The question is, how can one exploit \SL to reason about the amount of
available space in the heap?

A simple idea, inspired by \citet{hofmann-00}, is to
extend \SL with a \emph{space credit} $\SC1$, an
assertion that guarantees the existence of one word of free space in the heap,
and can therefore be understood as the ownership of one word of free
space, or as a permission to allocate one word. In a
programming language \emph{without garbage collection}, where memory
allocation and deallocation instructions are explicit in the source
code, one can express the idea that allocating a block~$\blk$ consumes
$\sz\blk$ space credits, whereas, symmetrically, deallocating this block
produces $\sz\blk$ credits, like so:
%
\[\begin{array}{r@{\;}c@{}l@{\hspace{10mm}}r}
\atriple
  {\SC{\sz\blk}}
  {x := \kw{alloc}(\blk)}
  {x\pointsto\blk}
& \hbox{(\TirNameStyle{BasicAlloc})}
\\
\atriple
  {x\pointsto\blk}
  {\kw{free}(x)}
  {\SC{\sz\blk}}
& \hbox{(\TirNameStyle{BasicFree})}
\end{array}\]
%
In such a variant of \SL, the precondition and postcondition of a program (or
program fragment) reveal how much free space it requires and how much free
space it leaves in the heap. This is what we desire: a compositional program
logic, where the space requirements of a (sub)program are explicit in its
specification.

In the presence of garbage collection, what can be said? Certainly, ordinary
\SL can be and has been used in such a setting~\cite{iris,chargueraud-20}: it
is just a matter of removing ``$\kw{free}$'' from the programming language and
removing the corresponding reasoning rule from the program logic.
%
However, by abandoning explicit control over memory deallocation, we seem to
lose the ability to determine where memory deallocation takes place.
Certainly, we lose the ability to recover space credits that is inherent in
the rule \RULE{BasicFree} above.

To remedy this problem, a second idea comes to mind: abandon the instruction
``$\kw{free}$'', but keep a variant of the reasoning rule \RULE{BasicFree},
along the following lines:
%
\newcommand{\ghostupdate}[2]{#1 \;\Rrightarrow\; #2}
%
\[\begin{array}{c@{\hspace{20mm}}r}
\ghostupdate
  {x\pointsto\blk}
  {\SC{\sz\blk}}
& \hbox{(\TirNameStyle{\FlawedLogicalFree})}
\end{array}\]
%
The logical connective $\Rrightarrow$ can be informally understood as a ghost
update. Equipped with this reasoning rule, the programmer can \emph{\logically
deallocate} a memory block, that is, highlight a point in the code where this
block becomes unused. Applying the update consumes the assertion
$x\pointsto\blk$ and produces the assertion $\SC{\sz\blk}$ in its place.
The point where the update is applied is not visible in the code,
but is explicit in the \SL derivation that the programmer builds while
verifying the code. Thus, we envision a hybrid approach that marries
\emph{manual reasoning about memory management at program-verification time}
and \emph{automatic memory management at runtime}. Several questions spring to
mind: is this approach practical? Is it sound?

% Address potential criticism: we have GC, but we are doing as much work in
% our reasoning as if we had manual memory management. True to some extent,
% but perhaps this load can be lessened by performing coarse-grained reasoning
% with regions.

Is this approach practical? The reader might fear that we get the worst of
both worlds, that is, the complexity and mental burden that often accompany
manual memory management, together with the latency issues
% the sometimes unpredictable pauses
sometimes caused at runtime by garbage collection. As we shall see, the need
to reason about memory deallocation does indeed lead to reasoning
rules, specifications, and proofs that are more complex than usual. This is a
risk that we are running, and it is up to us to show that this added
complication is tolerable and manageable at scale. On the upside, we get
access to the simpler programming model and potentially superior
performance afforded by garbage collection.
%
% indeed, we incur none of the runtime cost of manual memory management.
%
We also get access, at program-verification time, to powerful mechanisms such
as \emph{bulk \logical deallocation}, which consists in applying a variant of
the reasoning rule \RULE{\FlawedLogicalFree} to a group of memory blocks at
once. Indeed, nothing forces us to logically deallocate only one block at a
time; it is up to us to imagine a variety of fine-grained and coarse-grained
policies for memory management at program-verification time.

Is this approach sound? It does inherit several valuable properties of \SL.
Because the reasoning rule \RULE{\FlawedLogicalFree} consumes the ``points-to''
assertion $x\pointsto\blk$, a memory block cannot be \logically deallocated
twice, and cannot be accessed after it has been \logically deallocated.
% Memory safety is guaranteed, but it is guaranteed for all programs in an
% ML-style language anyway.
Unfortunately, this is insufficient. The approach, as sketched so far, is not
sound.
% the~assertion $\SC1$ does not guarantee the existence of one word of
% free space in the heap.
Indeed, by introducing the idea of \logical
deallocation, we create a distinction between the \emph{physical
  heap}, which exists at runtime and where memory is freed by the garbage
collector, and the \emph{\logical heap}, which is the heap that the
programmer has in mind at program-verification time and where memory is freed
by applying the rule \RULE{\FlawedLogicalFree}. The assertion $\SC1$
guarantees the existence of one word of free space in the \logical heap.
Does this imply the existence of one word of free space in the physical heap
as well? In general, no. This is the case only if the programmer uses
\logical deallocation wisely: when a memory block is \logically
deallocated, it should be the case that this block is inaccessible, that is,
unreachable. This guarantees that the garbage collector, too, can deallocate
this block when it so wishes. This allows us to maintain the invariant that
\emph{every block that has been \logically deallocated has been or can be
  physically deallocated}. This invariant implies that \emph{if there are $k$
  words of free space in the \logical heap, then the garbage collector can
  find or create $k$ words of free space in the physical heap}. Then, the
assertion $\SC1$ \emph{can} be interpreted as a permission to allocate one
word without risking to run out of memory.

% Remarks on the related work:
% 1. To use \logical deallocation wisely is what Hofmann and Jost call the
%    absence of malignant sharing.
% 2. The distinction between the physical and \logical heaps is something
%    I have imagined, but \citet{hur-dreyer-vafeiadis-11} came up with it
%    before.

The outstanding problem, thus, is to restrict the reasoning rule
\RULE{\FlawedLogicalFree} so as to require that the memory block~$\blk$ be
inaccessible: there must exist no path from the roots, through the heap, to
this block. The roots, roughly speaking, are the local variables of all
ongoing procedure calls; they are found in the stack, or (if there are
multiple threads) in each thread's stack.

This might seem a difficult problem, which \SL might be ill-equipped to solve.
Indeed, by design, \SL is good at reasoning about \emph{ownership}, while the
challenge here seems to be reasoning about \emph{reachability}. These notions
are in general unrelated. It is possible to own a block that has become
unreachable, and this is indeed the case when \RULE{\FlawedLogicalFree} is about
to be applied in a correct manner. Conversely, it is possible to retain an
address for which there is no access permission. Furthermore, compositional
reasoning about reachability seems difficult, because reachability is
nonlocal: adding or deleting a single edge in the heap can cause many memory
blocks to become reachable or unreachable.

Fortunately, there is a simple way out of this problem. To prove that a
block is inaccessible, it suffices to prove that it has no incoming
edges, that is, no predecessors.
%
% fP: removed to save space
% (This remark forms the basis for memory management based on reference
% counting: when the number of predecessors of a block drops down to zero,
% this block can be freed.)
%
% fp: already said below
% More generally, to prove that a group of objects is unreachable, it suffices
% to prove that there are no pointers from the outside into this group.
%
This leads us to \emph{extending \SL to keep track of
  the predecessors} of each block,
%
an idea that has appeared in the literature~\cite{kassios-kritikos-13}
but has not been much exploited.
%
In addition to the ordinary ``points-to'' assertion $\loc\pointsto\blk$,
which means that at address~$\loc$ there is a memory block $\blk$,
let the new ``pointed-by'' assertion $\loc\pointedby\lsv$
mean that all pointers to address~$\loc$ originate in blocks
whose addresses inhabit the multiset~$\lsv$.
% (We are being somewhat informal here, as we gloss over the
%  multiplicity issue.)
Then, the assertion $\loc\pointedby\emptyset$ guarantees that
the block at address~$\loc$ has no predecessors,
therefore is inaccessible. \Logical deallocation
can then be soundly expressed like so:
%
\[\begin{array}{c@{\hspace{10mm}}r}
\ghostupdate
  {x\pointsto\blk \star x\pointedby\emptyset}
  {\SC{\sz\blk}}
& \hbox{(\TirNameStyle{\BasicLogicalFree})}
\end{array}\]
%
Although we shall improve this rule later on, the main ideas are present already.
%
A variant of this rule that allows bulk \logical deallocation can also be
given. There, it is not necessary to require every block in the group to have
no predecessors: instead, it suffices to ensure that the group as a whole has
no incoming edges, that is, that the group is closed under predecessors. This
allows \logically deallocating complex data structures, which may involve
sharing and cycles.

The last point that remains to be stressed is that, for this approach to be
sound, the predecessor-tracking logic that we build into the assertion
$\loc\pointedby\lsv$ must be able to see not only the pointers from the heap
into itself, but also the pointers from every stack into the heap. To this
end, we choose to work with a \emph{store} that encompasses both the heap and
the stacks of all threads. A memory block in the store is either an object in
the heap or a cell in some stack. In a ``pointed-by'' assertion
$\loc\pointedby\lsv$, the elements of the multiset~$\lsv$ are addresses of
heap blocks and stack cells. This approach works extremely smoothly. The price
to pay for it is that it leads us to the design of a~rather low-level
calculus, baptized \spacelang, where a variable does not denote a value (as
usual in a call-by-value $\lambda$-calculus), but denotes the address of a
stack cell.
%
% Note that our soundness result includes several sanity checks about the
% definition of the GC. First, it proves that the GC does not endanger safety
% by deallocating too many blocks: indeed, a verified program will never try
% to dereference a pointer to a block that has been deallocated. Second, it
% proves that the GC is capable of deallocating unreachable blocks: indeed, if
% it was not, then we would not be able to establish the reasoning rule that
% \logically deallocates a group of unreachable blocks and produces a number
% of space credits.
%
% ------------------------------------------------------------------------------
%
% Overview.
%
\spacelang features
%
heap-allocated tuples with automatic garbage collection,
% and pointer equality tests,
mutable stack-allocated cells with explicit scoped allocation,
first-class closed procedures, % with a call-by-reference discipline
and shared-memory concurrency:
a $\kw{fork}$ instruction spawns a new thread.
%
It does not have procedures with free variables:
adding support for closures, either directly or
via a form of macro-expansion, is future work (\sref{sec:conclusion}).

We now present the syntax and semantics of \spacelang
(\sref{sec:spacelang}), followed with the reasoning rules of \DIAMS
(\sref{sec:logic}). We sketch the proof of soundness of \DIAMS
(\sref{sec:soundness}), which is machine-checked~\citerepo. We present a
certain style of use of \DIAMS, \emph{ghost reference
  counting}~(\sref{sec:rc}), and provide two examples that illustrate this
style (\sref{sec:examples}). We review the related work (\sref{sec:related})
and conclude (\sref{sec:conclusion}).
