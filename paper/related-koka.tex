% ------------------------------------------------------------------------------

\nocite{ullrich-de-moura-19}
  % reference counting for pure languages
  % introduce reuse analysis
  % which tries to pair deconstruction operations with allocations of same-size objects

\citet{reinking-xie-demoura-leijen-20} propose a memory management technique,
based on reference counting, for the programming language Koka. They perform
precise reference counting: the compiler is able to (automatically) insert
reference counting instructions in such a way that every reference is dropped
as soon as possible. (This means that an instruction of the form
\verb+drop(x)+ is inserted as soon as the variable~\verb+x+ is no longer live,
as opposed to waiting until the lexical scope of~\verb+x+ ends.) Thus, only
live data is retained. (Our technique is also precise in that sense.) A
function such as \verb+iter+ drops each list cell as it progresses, so (if the
list is uniquely owned) the list cells can be deallocated as soon as they are
visited. Our system also has this feature. (Indeed, our system is also based
on transferring ownership, as opposed to borrowing as in Rust.)

Reinking \etal. perform reuse analysis~\cite{ullrich-de-moura-19}. In our eyes,
this is an implementation detail that we need not care about, as it does not
change the size of the live data.

Reinking \etal. count pointers from the stack to the heap (i.e., every use
of a local variable requires a corresponding \verb+dup+, I think).

Reinking \etal. require all control flow to be ``explicit'' (e.g., no implicit
exceptions or control effects). Indeed, if a block could be implicitly
exited, then some \verb+drop+ instructions would be missing, and some
data could leak. A type-directed translation is used to transform program
with control effects into ordinary (pure) programs with explicit control
flow. Type information is thereafter no longer exploited.

As far as the user is concerned, their technique is indistinguishable from a
garbage collector: no user annotations are required, and the space usage is
the same. In fact, we believe that our program logic can be used to reason
about space usage in Koka.

% Handling references in Reinking \etal.'s system can be very expensive,
% especially in a concurrent setting, where reading and writing actually
% become read-and-dup and read-and-write-and-drop, and must be executed
% atomically.

% Also, references can create cycles, which cannot be collected. It is
% up to the programmer to break the cycle by de-initializing one of the
% references that participate in the cycle. (This strategy is also used
% by Swift.)

Section 3 of the paper formalizes a linear resource calculus, $\lambda^1$.
It is an untyped $\lambda$-calculus equipped with reference counting
instructions, \verb+dup+ and \verb+drop+. It does not have mutation.
The authors define a translation judgement
$\Delta, \Gamma \vdash e \rightsquigarrow e'$
that inserts \verb+dup+ and \verb+drop+ instructions into a program.
The borrowed environment $\Delta$ is duplicable; a \verb+dup+
instruction can be used to pick a variable in it and introduce
this variable in $\Gamma$ as well.
The owned environment $\Gamma$ is linear; every variable in it
must be used exactly once, possibly by a \verb+drop+ instruction.
A \verb+dup+ instruction can also be used to duplicate a variable
in $\Gamma$.
% It seems a little strange that this is an algorithm for inserting dup and
% drop instructions, but there is no declarative specification of when a
% program that contains dup and drop instructions is well-formed.
In the Bind rule, the owned variables~$\Gamma_2$ of the continuation~$e_2$
are considered borrowed by $e_1$. This removes the need for redundant
reference count increments and decrements. The App rule contains an
implicit Bind (evaluation is left-to-right).
% One might wonder whether we could use a similar mechanism. However, this
% borrowing mechanism seems useful mostly because Reinking \etal. count
% pointers from the stack to the heap; we do not. Conversely, whereas
% object allocation and initialization are nontrivial operations in our
% system, they are essentially trivial in theirs: indeed, $C(x_1, ..., x_n)$
% is considered a value and is translated to itself (it counts as one use of
% every variable).

Building a closure counts as one use of every free variable, so building a
closure causes the reference count of every free variable to be incremented,
as expected. The function's body is translated under an owned environment
where every free variable appears once, which means that when the function
is called, it receives one copy of the ownership of every argument.

% In the closure "fun b -> if b then 0 else x",
% a "drop x" instruction is inserted in the first branch,
% so calling this function with b=true results in
% dropping x. This is fine because (if x is still be used in
% the future by the caller) the caller must have duplicated x
% beforehand.

% There are two operational semantics; a standard one (Figure 6) and one
% with explicit reference counts (Figure 7).
% The instrumented operational semantics confuses variables and locations,
% which I don't like a lot.
% In this semantics, some operations involve implicit dups and drops: e.g.,
% calling a closure dups its free variables and drops the closure itself,
% prior to the actual call; case analysis on x drops x and dups the newly
% introduced variables.
% drop is recursive and polymorphic; when the reference count is 1,
% dropping a data object frees it and drops its children. Similarly,
% dropping a closure frees it and drops its children (its free variables).

% Two theorems relate the two semantics, and show
% 1- that the instrumented semantics computes a correct result;
%    (it simulates the standard semantics)
% 2- that the instrumented semantics retains only data that is reachable in
%    the *instrumented* semantics (where the roots are the free variables of
%    the expression being executed). This result is true only in the absence
%    of cycles and therefore in the absence of mutable references. This is a
%    weak result: it does not guarantee that data is dropped as soon as
%    possible. So, it does not imply "precision" ("garbage-freeness").

% Section 3.4 defines another translation, which is syntax-directed (the
% previous translation was not, as dup and drop could be inserted
% anywhere). They are designed so as to guarantee precision (dup as
% late as possible, drop as soon as possible).
