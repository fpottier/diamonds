\section{Separation Logic with Space Credits for \spacelang}
\label{sec:logic}

We now describe
% the assertions and reasoning rules of
% the program logic
\DIAMS at an intuitive level.
A more technical soundness argument is deferred to \sref{sec:soundness}.

% We state the rules in terms of Hoare triples, but it is possible to
% give slightly different statements in terms of WP.

% ------------------------------------------------------------------------------

\subsection{Points-To Assertions}
\label{sec:pointsto}

Following a long-established
tradition~\cite{boyland-fractions-03,bornat-permission-accounting-05}, we
describe memory blocks via fractional points-to assertions. The assertion
\mathsmash{\loc\fpointsto\ql\blk} guarantees that the memory block~$\blk$
exists at address~$\loc$. If the fraction~$\ql$ is 1, then this assertion
represents the unique ownership of this memory block, and grants read-write
permission; otherwise, it represents shared ownership, and grants read
permission only.
%
Points-to assertions are split and joined in the usual way;
this is expressed by the law~\RULE{\JoinPointsto} in \fref{fig:laws}.
%
As usual, $\loc\pointsto\blk$ is short for
\mathsmash{\loc\fpointsto1\blk}.

Points-to assertions describe both heap blocks and stack cells. Thus,
$\loc\pointsto\btuple\vals$ describes a heap block, while
$\sloc\pointsto\bcell\val$ describes a stack cell.
%
The assertion $\loc\pointsto\bdeallocated$, which represents the unique
ownership of a deallocated block, is technically well-formed, but is not used.
Instead, we use a persistent assertion $\ddag\locs$ (\sref{sec:ddag}) to
witness the fact that the locations in the set $\locs$ have been deallocated.

\input{fig-laws}

% ------------------------------------------------------------------------------

\subsection{Space Credits}
\label{sec:credits}

To reason about space, we use \emph{space credits}. The assertion
$\SC{n}$ guarantees that there exist $n$~words of available space in the heap.
More precisely, it guarantees that the garbage collector can reclaim enough
space so as to make $n$~words of memory available.
%
Space credits are not duplicable: $\SC1$ does not entail $\SC1\star\SC1$.
%
They are split and joined in an additive way
(\RULE{\ZeroDiams} and \RULE{\JoinDiams}, \fref{fig:laws}),
so $\SC1\star\SC1$ is in fact equivalent to $\SC2$.

The assertion~$\SC{n}$ can be interpreted as the unique ownership of $n$ words
of available space, or as a permission to allocate an object of size~$n$.
%
Indeed, the reasoning rule for dynamic memory allocation, \RULE{Alloc}
(\fref{fig:reasoning}), consumes $\sz\blk$ space credits, where $\blk$ is
the newly-allocated block.
% fp: already said earlier:
% The newly-allocated block is in fact
% $\btuple{\replicate\allocsize\vunit}$,
% that is, a tuple of $\allocsize$ fields,
% each of which is initialized with the value $\vunit$.

Conversely, the reasoning rule for deallocation, \RULE{Free}
(\fref{fig:laws}), produces $n$ space credits
when a group of objects whose total size is $n$
% becomes eligible for deallocation.
is \logically deallocated.
To explain this rule,
we must first introduce more concepts.
Indeed, to prove that a group of objects
can be deallocated, the user % of the program logic
must argue that these objects are unreachable.
To~do so, the user must reason about
% the antecedents of
the pointers to these objects
that exist in the store.
To~enable such reasoning, we use \emph{pointed-by} assertions.
% described next.

% ------------------------------------------------------------------------------

\subsection{Pointed-By Assertions}
\label{sec:pointedby}

Points-to assertions yield a forward view of the heap as a graph. The
points-to assertion $\loc\pointsto\blk$ describes the outgoing edges of the
location~$\loc$: for every location $\loc'\in\pointers\blk$, there is an edge
from $\loc$ to~$\loc'$.
%
We introduce \emph{pointed-by} assertions in order to maintain a view of the
heap as a graph in the reverse direction.
% They keep track of the predecessors of every location.
The assertion $\loc'\vpointedby\lsv$ means that the predecessors of the
location~$\loc'$ inhabit the multiset~$\lsv$.
%
Thus, for every $\loc\in\lsv$, there may be an edge from $\loc$ to~$\loc'$.
More crucially, for every $\loc\not\in\lsv$, \emph{there definitely exists no
  edge} from $\loc$ to~$\loc'$.
%
In particular, the assertion $\loc'\vpointedby\emptyset$ guarantees that the
block at address~$\loc'$ has no predecessors.
%
% This is a simple sufficient condition for a block to be unreachable.

Several design choices come up regarding pointed-by assertions. One such
choice is whether to keep track of predecessors using a set or a multiset. We
choose multisets because they lead to simpler reasoning rules.
% fp: removed to save space
\begin{comment}
\footnote{In particular, destroying one edge from $\loc$ to~$\loc'$ in the
  heap allows us to remove one copy of $\loc$ from the multiset of the
  predecessors of $\loc'$, whereas sets would not support such a simple rule.}
\end{comment}
%
%  Also, the cardinal of the sum of two multisets is the sum of their
%  cardinals, whereas the cardinal of the union of two sets is not necessarily
%  the sum of their cardinals.
%
Another choice is whether, in the assertion $\loc'\vpointedby\lsv$, the
multiset~$\lsv$ should represent exactly the predecessors of~$\loc'$, or
possibly an over-approximation. We choose the latter possibility,
and admit the reasoning rule \RULE{\CovPointedBy} in \fref{fig:laws},
because this makes the system slightly more flexible and convenient.
%
% If the user does not wish to over-approximate the predecessors,
% then she simply refrains from using this rule.
%
A third design choice is whether one should be able to split pointed-by
assertions. It is technically easy to allow this, by annotating them with
fractions, and we believe that this could be important in practice,
although we do not yet have evidence of this.
%
Thus, we introduce fractional pointed-by assertions of the form
\mathsmash{\loc'\vfpointedby\qv\lsv}, and allow them to be split and
joined via the rule \RULE{\JoinPointedBy} in \fref{fig:laws}.
%
This rule involves multiset sum.
%
Thus, while the assertion \mathsmash{\loc'\vfpointedby{1}\lsv}
guarantees that the predecessors of $\loc'$ form a subset of $\lsv$,
the fractional assertion \mathsmash{\loc'\vfpointedby\qv\lsv}, alone,
does not allow making such a claim.
%
One might therefore conclude that such a fractional assertion is of little use
by itself. That is not the case: indeed, this assertion still allows recording
the creation or deletion of a predecessor of~$\loc'$. This is exploited in
several of the reasoning rules in \fref{fig:reasoning}, such as \RULE{Load}
and \RULE{Store}. These rules are explained later on (\sref{sec:reasoning}).

% The reasoning rules ensure that pointed-by assertions are kept up-to-date when
% edges are created or destroyed. Thus,
The ``forward'' and ``reverse'' views of
the heap represented by points-to and pointed-by assertions are always
consistent with one another. This is reflected
by~\RULE{\ConfrontMapstoMapsfrom} in
\fref{fig:laws}. There, the points-to assertion $\loc\fpointsto\qv\blk$
indicates how many edges from $\loc$ to $\loc'$ exist: their number is the
multiplicity
of $\loc'$ in $\pointers\blk$.
%
(We write $\multiplicity\loc\lsv$ for the multiplicity of $\loc$ in the
multiset~$\lsv$.)
%
The pointed-by assertion $\loc'\vfpointedby{1}\lsv$ guarantees that the
incoming edges of~$\loc'$ are contained (and counted) in the multiset~$\lsv$.
Thus, the multiplicity of $\loc$ in $\lsv$ must be at least the multiplicity
of $\loc'$ in $\pointers\blk$.
%
% This is the mirror property.
%
% This rule is used to prove that the reference counter of $\loc'$
% is actually decremented by one when an edge from $\loc$ to $\loc'$
% is destroyed.

\begin{comment}
The law~\RULE{\ConfrontDagMapsfrom} indicates that a deallocation witness
$\ddagsingleton{\loc'}$ and a pointed-by assertion $\loc'\vfpointedby\qv\lsv$
cannot co-exist.
% We could add a rule stating that $\loc\pointsto\ldots$ and
% $\ddagsingleton\loc$ are incompatible.
\end{comment}

In many of the reasoning rules, we need pointed-by assertions of the form
\mathsmash{\val\vfpointedby\qv\lsv}, where $\val$ is a value. This assertion
is defined in a straightforward way, as follows: if $\val$ is a memory
location $\loc'$, then it is synonymous with
\mathsmash{\loc'\vfpointedby\qv\lsv}; otherwise, it is synonymous with
$\iTrue$.

% ------------------------------------------------------------------------------

\subsection{Memory Deallocation as a Ghost Operation}
\label{sec:free}

% Because \spacelang is equipped with a garbage collector,
\spacelang does not have a memory deallocation instruction. Nevertheless, the
user must reason about the points where an object (or a data structure)
becomes eligible for deallocation. For instance, the user might reason, ``at
this point in the code, this list of length~$n$ becomes unreachable, therefore
$4+4n$ words of memory can be reclaimed by the garbage collector''.

In the formal setting of~\DIAMS, this suggests viewing memory deallocation as
a \emph{ghost operation}, also known as a \emph{ghost state update}. Such an
operation has no runtime effect, but transforms an assertion into a different
assertion. For instance, it may consume a permission to access a certain group
of objects and produce a number of space credits. This operation takes the
form of a reasoning rule, namely rule \RULE{Free} in \fref{fig:laws}, which is
explicitly used by the person who verifies a program.

\label{sec:logical}
There is no connection between the point where this ghost operation is
used and the point where the garbage collector actually reclaims memory. An
object can be reclaimed by the garbage collector either before or after the
point where the user reasons that this object has become unreachable. This
creates a difference between the \emph{physical store} that exists at runtime
and the \emph{\logical store} that the user has in mind. In the physical
store, memory is deallocated by the garbage collector at somewhat
unpredictable times. In the \logical store, memory is explicitly
deallocated by the user who applies \RULE{Free}. Still, the program logic is
designed in such a way that the physical and \logical stores remain closely
related: \emph{their reachable fragments coincide}~(\sref{sec:soundness}), so
the differences between them concern unreachable objects only.

Let us now explain the reasoning rule \RULE{Free}, whose statement is
$\cloud\locs{n} \supd \ddag\locs \star \SC{n}$.
%
Although perhaps cryptic at first sight, this statement is actually
fairly simple. It can be read as follows:
%
\begin{quote}
\emph{If a set of heap objects~$\locs$ is closed under predecessors
and has total size $n$,
then, by abandoning the ownership of these objects,
one obtains
a witness $\ddag\locs$ that these objects have been deallocated,
together with
$n$ space credits.
}\end{quote}
%
We explain deallocation witnesses in the next subsection (\sref{sec:ddag}).
%
The logical connective~$\mathord{\supd}$ is a variant of Iris's ghost
state update~\cite[\S7.2]{iris}; it allows transforming its left-hand side
into its right-hand side.%
\footnote{%
  $P \supd Q$ is defined as
  $\forall\store.\;
   P \star \centralinvariant\;\store \fupd Q \star \centralinvariant\;\store$,
  where $\centralinvariant\;\store$ is the state interpretation invariant
  (Definition~\ref{def:invariant}).
}
% LATER say more? explain that this update can be used only if the
%       current instruction is not $\iskip$? (Note that this seems
%       a deficiency of Iris that could be remedied in the future.)
%       Or, perhaps we should place our invariant inside an Iris
%       invariant so that it can be accessed anywhere and we can
%       use a normal view shift?
%
Let us now explain the \emph{cloud}, whose general form is
$\ocloud\antecedents{n}\locs$, where $\locs$ and $\antecedents$ are sets of
locations. In short, this assertion:
%
(1)~guarantees that every object in $\locs$ is a heap object
% (as opposed to a stack cell)
and represents the full ownership of this object,
including points-to and pointed-by assertions;
% with the fraction~1
%
(2)~guarantees that all predecessors of the objects in~$\locs$ lie in the
set~$\antecedents$;
and
%
(3) indicates that the total size of the objects in~$\locs$ is $n$.

The case where the sets~$\locs$ and~$\antecedents$ coincide is of particular
significance. The assertion $\cloud\locs{n}$ guarantees that
% the predecessors of every location in~$\locs$ are members of~$\locs$:
% that is,
the set $\locs$ is closed under predecessors. It also guarantees that no
object in~$\locs$ is a stack cell, that is, no object in~$\locs$ is a root.
Therefore, every object in~$\locs$ must be unreachable.
%
This explains why it is sound to \emph{\logically deallocate} these objects
and obtain $n$ space credits in return.

The predicate $\ocloud\antecedents{n}\locs$ is inductively defined. The
reasoning rules \RULE{CloudEmpty} and \RULE{CloudCons} in \fref{fig:laws}
correspond to the two constructors, and allow constructing clouds of arbitrary
size.%
%
\footnote{\label{domain}In \RULE{CloudCons} and \RULE{Cleanup},
$\dom\cdot$ maps a multiset to its domain, a set.
$\lsv$ denotes a multiset; $\locs$ and $\antecedents$ denote sets.}
%
By combining \RULE{CloudEmpty}, \RULE{CloudCons}, and \RULE{Free}, one obtains
a simplified reasoning rule that allows deallocating a single heap object:
%
\[\begin{array}{r@{\;}c@{\;}l@{\qquad\qquad\qquad}l}
\loc\fpointsto{1}{\btuple\vals} \star \loc\vfpointedby{1}\lsv
\star \pure{\dom{\lsv}\subseteq\{\loc\}}
& \supd &
\ddag{\{\loc\}} \star \SC{\sz{\btuple\vals}}
& \TirNameStyle{FreeSingleton}
\end{array}\]
%
That is, if we have full ownership of the memory block $\btuple\vals$ at
address~$\loc$, including points-to and pointed-by assertions, and if this
block has no predecessors (except possibly itself), then this block can be
deallocated, producing $n$ space credits, where $n$ is the size of the
block.

A careful reader may wonder why, when an object at address~$\loc$ is
deallocated, we do not immediately remove~$\loc$ from the predecessors of
every successor~$\loc'$ of $\loc$. The answer is, we do allow such a removal,
but, for greater simplicity and flexibility, we allow it to be deferred. This
is made possible by the deallocation witness $\ddag{\{\loc\}}$,
or in the general case, $\ddag\locs$.
We explain this next.

% ------------------------------------------------------------------------------

\subsection{Deallocation Witnesses and Deferred Predecessor Deletion}
\label{sec:ddag}

Deallocating a set~$\locs$ of memory locations produces a witness
$\ddag\locs$ that the locations in this set have been deallocated.
This is a persistent assertion: once it holds, it holds forever.
%
(As noted in \sref{sec:spacelang:store}, memory locations
are not recycled.)
% so a location, once deallocated, remains deallocated forever
%
Deallocation witnesses can be split and joined, if necessary, per the rules
\hyperlink{ZeroDag}{\textsc{\ZeroDag}} and
\hyperlink{JoinDag}{\textsc{\JoinDag}} in \fref{fig:laws}.

Deallocation witnesses are exploited by the reasoning rule \RULE{Cleanup},
which states that deallocated locations can be removed from predecessor
multisets. This rule transforms \mathsmash{\val\vfpointedby\qv\lsv} into
\mathsmash{\val\vfpointedby\qv\lsw},
where the multiset~$\lsw$ is contained in the multiset~$\lsv$,
% This is permitted
provided every location in $\lsv\setminus\lsw$ has been deallocated.%
\cref{domain}
%
This allows deferred deletion of predecessors: deallocating a
location~$\loc$ and removing~$\loc$ from the predecessors of some other
location~$\loc'$ are two separate steps.

Removing deallocated locations from predecessor multisets is beneficial
because it helps satisfy the requirements of rule \RULE{Free}. Indeed,
\RULE{Free} ostensibly requires proving that ``every predecessor of an object
in~$\locs$ is in $\locs$''. Thanks to \RULE{Cleanup}, what is really required
by \RULE{Free} is to check that ``every~predecessor of an object in~$\locs$ either
has been deallocated already or is in $\locs$''.

% A reviewer notes that we might need Cleanup for Clouds, that is,
% if some locations are known to be dead, then they can be removed
% from the predecessor set of the cloud.

% ------------------------------------------------------------------------------

\subsection{Reasoning Rules for Instructions}
\label{sec:reasoning}

\input{fig-reasoning}

As is usual in \SL, the reasoning rules of the program logic allow deriving
Hoare triples of the form $\triple\pre\instr\post$, where the
precondition~$\pre$ and the postcondition~$\post$ are assertions,
and where $i$ is an instruction.
%
% Because an instruction does not return a value, the postcondition is
% \emph{not} parameterized over a value.
%
% Every postcondition is technically a function of type
% $\mathit{unit}\ar\iProp$. In the paper, we pretend that it is just an
% assertion, of type $\iProp$.
%
Such a triple receives a standard interpretation as a statement of partial
correctness: the assertion $\triple\pre\instr\post$ means that, in a state
where $\pre$ holds, it is safe to execute the instruction~$\instr$, and if
this execution terminates, then it leads to a final state where $\post$ holds.
A more precise statement is given in the next section (\sref{sec:soundness}).

In light of the description given in the previous subsections
(\sref{sec:pointsto}--\sref{sec:ddag}), the reasoning rules, which appear in
\fref{fig:reasoning}, should seem relatively straightforward, if somewhat
verbose. This verbosity stems from two main causes: (1) the fact that every
instruction takes its parameters in stack cells means that points-to
assertions $\src\pointsto\bcell\val$ must appear in pre- and postconditions;
(2) keeping track of predecessors requires pointed-by assertions
\mathsmash{\val\vfpointedby\qv\lsv} to appear in pre- and postconditions.
Let us now review the reasoning rules.

\paragraph{Control Constructs}

The reasoning rules for sequences (\RULE{Skip}, \RULE{Seq}), conditionals
(\RULE{If}), and procedure calls (\RULE{Call}) are straightforward. In
\RULE{If}, the stack cell~$\src$ is required to contain an integer value~$k$,
and the condition $k\ne 0$ determines which branch is executed.
%
In \RULE{Call}, the stack cell~$\src$ is required to contain a procedure
$\vcode\vars\instr$ whose arity matches the arity of the call.

\paragraph{Memory Allocation in the Heap}

The reasoning rule for dynamic memory allocation, \RULE{Alloc}, is more
difficult to read; several aspects must be disentangled. The most interesting
aspect is that allocation consumes a number of space credits: $\SC{\sz\blk}$
appears in the precondition, where $\blk$ is the newly-allocated block.
% \footnote{In fact, $\blk$ is $\replicate\allocsize\vunit$, a block of
% $\allocsize$ fields, where each field is initialized with the unit value.}
%
This new block is allocated at a fresh address~$\loc$, for which a points-to
assertion $\loc\pointsto\blk$ and a pointed-by assertion
$\loc\pointedby\singleton\dst$ appear in the postcondition.
The latter assertion reflects the fact that the new block has exactly
one predecessor, namely the stack cell~$\dst$.
%
The rest is administrative boilerplate.
% administrative aspects related
% to the fact that the stack cell~$\dst$ is updated.
%
% \footnote{The rule combines the complexity of reasoning about
%   heap allocation and reasoning about an update to a stack cell.}
%
The points-to assertions $\dst\pointsto\bcell\val$ and
$\dst\pointsto\bcell\loc$ in the pre- and postcondition
reflect the manner in which the stack cell~$\dst$ is updated.
%
The pointed-by assertions
\mathsmash{\val\vfpointedby\qv\lsv}
and
\mathsmash{\val\vfpointedby\qv{\lsv\setminus\singleton\dst}}
in the pre- and postcondition
reflect the fact that the value~$\val$
that was \emph{previously} stored in the stack cell
% (and therefore is overwritten)
loses one predecessor, namely~$\dst$:
this is an \emph{edge deletion}.

\paragraph{Edge Deletion}

Several remarks about edge deletion are in order.
%
First, in the potentially common case where the value~$\val$ is
not a memory location, both
%
\mathsmash{\val\vfpointedby\qv\lsv}
and
\mathsmash{\val\vfpointedby\qv{\lsv\setminus\singleton\dst}}
%
are equivalent to $\iTrue$ (\sref{sec:pointedby}),
so these assertions vanish: no edge deletion takes place.

Second, the fraction~$\qv$ is \emph{not} required to be~1.
%
That is, a fractional pointed-by assertion
suffices for edge deletion.
%
In other words, the permission to (add or) delete
pointers to an object can be shared.
%
% (although we lack experience to confirm this)
%

Third, a potential pitfall is that if the fraction~$\qv$ is less than 1, then
the system does not guarantee that $\dst$ is a member of~$\lsv$.
%
In practice, it should most often be the case that $\dst\in\lsv$ holds,
because the user is usually careful to split $\val$'s pointed-by assertion in
such a way that this property holds.
%
Still, it could happen that $\dst$ is not a member of $\lsv$. Then,
$\lsv\setminus\{\dst\}$ is equal to $\lsv$, which means that the deletion of
the edge of $\dst$ to $\val$ is \emph{not} recorded at the logical level.
%
% (The memory location $\dst$ must appear in some other share of the
%  predecessor multiset of the value~$\val$.)
%
This does not compromise the soundness of the program logic; it simply means
that the predecessor multiset of the value~$\val$ becomes over-approximated.
%
The rule \RULE{\CovPointedBy} in \fref{fig:laws}, already discussed in
\sref{sec:pointedby}, is another way of introducing such an
over-approximation.
%
Over-approximation is acceptable if it can be later eliminated.
%
The reasoning rule \RULE{Cleanup} offers one way of doing so: once a memory
location~$\loc$ has been deallocated, all occurrences of~$\loc$ in predecessor
multisets, whether they are ``real'' or ``spurious'', can be removed.
%
The law \RULE{\ConfrontMapstoMapsfrom} offers another way:
it allows re-discovering the identity of a predecessor.

Finally, one can establish simplified variants of the reasoning rules
\RULE{Const}, \RULE{Move}, \RULE{Alloc}, \RULE{Load}, \RULE{Store}, and
\RULE{LocEq}, where edge deletion is not recorded. In the simplified
\RULE{Store} rule (not shown), for instance, the assertions
\mathsmash{\val\vfpointedby\qv\ldots} are removed from the pre- and
postcondition: thus, the deletion of the edge that leads from $\loc$ to~$\val$
is not recorded. As explained above, it is sometimes acceptable to
over-approximate predecessor multisets; then, these more lightweight rules can
be used.
%
% Alexandre Moine proved wp_store_weak and wp_const_weak
% after the paper was submitted.

\paragraph{Loads and Stores}

Read and write access to heap blocks are described by the rules \RULE{Load}
and \RULE{Store} in \fref{fig:reasoning}. Although these rules are
unfortunately quite verbose, all of the ideas required to understand them have
been presented already.
%
As usual in \SL, read access requires a fractional points-to assertion
\mathsmash{\loc\fpointsto\ql\btuple\vals},
whereas write access requires a full points-to assertion
\mathsmash{\loc\fpointsto1\btuple\vals},
which in the postcondition is updated to
\mathsmash{\loc\fpointsto1\btuple{\mupd\ofs\wal\vals}}.
%
The rest is boilerplate. Two points-to assertions describe the stack cells
$\src$ and $\dst$ before and after the operation. Two pointed-by assertions
describe the predecessors of the values $\val$ and $\val'$ before and after
the operation; one edge deletion and one edge addition take place.

\paragraph{Memory Allocation on the Stack}

% I explain the simplified rule where the stack cell is required to hold
% a unit value at the end.

The reasoning rule \RULE{Alloca} states that a fresh stack cell~$\sloc$ exists
during the execution of the instruction~$\instr$. The points-to assertion
$\sloc\pointsto\bcell\vunit$ appears upon entry and disappears upon exit.
%
Although we could, we do not make the pointed-by assertion
$\sloc\vpointedby\emptyset$ available to the user, so she cannot create
pointers to the stack cell~$\sloc$.
%
% As a result, one can *never* create a pointer to a stack cell!
% Yet, our operational semantics and the model of the logic allow it.
%
Because the rule requires the stack cell to contain a unit value upon exit,
the programmer may need to explicitly kill this cell by writing a unit value
into~it. We impose this requirement for the sake of simplicity. We have also
proved a more permissive (and more complex) version of the reasoning rule,
which does not have this requirement, and performs an edge deletion upon exit.

\paragraph{Fork}

The reasoning rule \RULE{Fork} reflects the operational semantics of
$\kw{fork}$. The content of the stack cell~$\src$ changes from $\val$ to
$\vunit$. In the new thread, a fresh stack cell~$\sloc$ appears, and is
initialized with~$\val$. The treatment of this stack cell in the premise of
\RULE{Fork} is analogous to what can be seen in the premise of \RULE{Alloca}.
Finally, the assertions $\pre$ and $\val\vfpointedby\qv\lsv$ are transmitted
from the original thread to the new thread. The choice of a suitable
assertion~$\pre$ is made by the user. The pointed-by assertion is updated to
$\val\vfpointedby\qv{\lsv\setminus\{\src\}\uplus\{\sloc\}}$ in the new thread,
reflecting the fact that the pointer from~$\src$ to $\val$ is replaced with a
pointer from~$\sloc$ to $\val$.
%
% The user can choose what fraction of the pointed-by assertion must be
% transmitted. The choice of the fraction 1 is permitted, and works in
% practice because there is no need to kill the stack cell $\src$ in the
% original thread; this is performed on the fly by $\kw{fork}$ itself.
%
% No assertion $\post$ is transmitted on the way back (in the postcondition)
% because fork is asynchronous.
%

% If there was room, we could emphasize the fact that all of the standard
% rules, such as the Bind rule, the Frame rule, etc., are preserved.
