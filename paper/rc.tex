\section{Reference-Counting Style}
\label{sec:rc}

% Intro.

On many occasions, our pointed-by assertions are too precise. The assertion
$\val\vfpointedby\qv\lsv$ keeps track of a multiset of predecessors~$\lsv$,
where the location and the multiplicity of every predecessor are explicitly
recorded. This information can be needlessly precise; what is worse, it can be
inconvenient or impossible to maintain.
%
A typical example is that of a container data structure, such as a stack or a
priority queue. When an element is inserted into the container, a pointer from
the container to the element is created. So, the pointed-by assertion for this
element must be updated: the existence of a new predecessor must be recorded.
Unfortunately, this predecessor is usually an object that is part of the
internal representation of the container data structure: for instance, it
could be an internal node in a binary tree. Thus, it is an object whose
address cannot be named by a user of the container abstraction. As a result,
it can be unclear at first how to write natural and useful specifications for
the operations that insert and extract elements into and out of the container.

To solve this problem, the predecessor information must be made less precise,
while remaining useful. We envision several ways of making this information
more abstract, while retaining a sufficient level of precision. One approach
would be to keep track of predecessors in terms of abstract entities, instead
of concrete locations. At an intuitive level, the specification of ``insert''
would state that ``the container'' becomes a predecessor of the newly inserted
element, while the specification of ``extract'' or ``remove'' would state that
``the container'' is no longer a predecessor of the element that has been
extracted.
%
% This could be stated in terms of multisets, if desired, so the spec
% would be correct in the case where the container can hold several
% copies of an element.
%
Another approach is to count predecessors, without keeping track of their
identity. At an intuitive level, the specification of ``insert'' would state
that the newly inserted element gains one predecessor, while the specification
of ``extract'' would state that the element that has been extracted loses one
predecessor.
%
In either approach, it is desirable that the sequential composition of
insertion and extraction cause no loss of precision: if an element is inserted
into the container and immediately extracted, then the static information that
is available about its predecessors should be unaffected.

We believe that this abstraction of the predecessor information can be
programmed by the user in a library, providing an additional layer of
abstraction on top of \DIAMS. As an example, in this section, we present a
simple way of keeping track of predecessors in \emph{reference-counting
  style}. We leave the exploration of other styles to future work.

% End of intro.

% ------------------------------------------------------------------------------

% Reference-Counting Style.

A user who wishes to keep track of the number of predecessors, while ignoring
their identity, is naturally led to define a new pointed-by assertion
%
\(
  \val \rcvpointedby \nv
\)
%
where $\nv$ is a natural number. The simplest way of defining such an assertion
is as follows:
%
% there are other ways, but they are more complex;
% possibly allowing subtraction even when the fraction 1 is not available
%
\[
  \val \rcvpointedby \nv \eqdef
  \exists \lsv. \; (
    \val \vfpointedby{1} \lsv \;\star\;
    \pure{\cardinal\lsv = \nv}
  )
\]
%
The existential quantification on~$\lsv$ indicates that it is not known where
% (in the heap and on the stack)
there are pointers to $\val$, but it is known that there exist~$\nv$ such
pointers: indeed, $\nv$ must be the cardinality of the multiset~$\lsv$.
%
We require the fraction~1;
the next paragraph explains why.

For each reasoning rule in \fref{fig:reasoning}, it is possible to
derive a variant of the rule in reference-counting style.
%
The addition of a predecessor, which was expressed by transforming
\mathsmash{\val \vpointedby\lsv}
into
\mathsmash{\val \vpointedby\lsv\uplus\singleton\loc},
is now expressed by transforming
\mathsmash{\val \rcvpointedby \nv}
into
\mathsmash{\val \rcvpointedby \nv+1}.
%
Clearly, this is sound because
the function ``cardinality'' maps multiset sum to addition.
%
The deletion of a predecessor, which was expressed by transforming
\mathsmash{\val \vpointedby\lsv}
into
\mathsmash{\val \vpointedby\lsv\setminus\singleton\loc},
is now expressed by transforming
\mathsmash{\val \rcvpointedby \nv}
into
\mathsmash{\val \rcvpointedby \nv-1}.
%
The reason why this is sound is more subtle.
%
The law $\cardinal{\lsv\setminus\singleton\loc}=\cardinal\lsv-1$
is not true in general: it is true only under the side condition
$\loc\in\lsv$.
%
Fortunately, when a predecessor is deleted,
a points-to assertion is at hand,
stating the existence of a pointer of $\loc$ to $\val$;
the law~\RULE{\ConfrontMapstoMapsfrom}
can then be used to obtain $\loc\in\lsv$.%
%
\footnote{The use of \RULE{\ConfrontMapstoMapsfrom}
  is the reason why the deletion of a
  predecessor requires the fraction~1. It may be possible to remove this
  limitation by adopting a more complex definition of $\val \rcvfpointedby\qv
  \nv$. We leave it to future work to determine whether this is possible and
  desirable.}
%

\input{fig-rc}

As an example,
from~\RULE{Move},
one can derive \RULE{RC-Move} (\fref{fig:rc}).
%
%
By following a similar pattern, one gives a version of almost every reasoning
rule in reference-counting style.
%
We omit these rules; the reader can find them in \longciterepo.
%
One exception is~\RULE{Cleanup}, which has no sensible counterpart in this style.
%
To address this problem, we combine \RULE{\ConfrontMapstoMapsfrom},
\RULE{Free}, and
\RULE{Cleanup}, and obtain a reasoning rule that does
admit a useful counterpart in reference-counting style, namely
\RULE{RC-Free-Singleton} (\fref{fig:rc}).
%
% See supd_free_cleanup_singleton_rc in rc.v.
%
This rule states that, if there is a tuple $\btuple\vals$ at address~$\loc$ in
the heap, and if there are no pointers to this memory block
(as witnessed by the assertion $\loc \rcvpointedby 0$),
then this block can be \logically freed.
%
The reference count of every successor can then be decremented.
This is expressed by the last part of the precondition and postcondition: for
an arbitrary value~$\val$ and natural number~$\nv$,
the assertion $\val \rcvpointedby \nv$ can be updated to
$\val \rcvpointedby {\nv - (\valoccs\val\vals)}$,
%
where $\valoccs\val\vals$ denotes the multiplicity of the value $\val$ in the
tuple $\vals$.%
\footnote{In reality, we adopt a slightly more complex definition of
  $\valoccs\val\vals$, which does not rely on decidable equality of values.
  For memory locations, we define $\valoccs\loc\vals$ as the multiplicity of
  the value $\loc$ in the tuple $\vals$. For a value~$\val$ other than a
  memory location, we define $\valoccs\val\vals$ as $\alen\vals$, the length
  of the tuple $\vals$. This is arbitrary; in this case,
  $\val \rcvpointedby \nv$ is equivalent to $\iTrue$ anyway.}
%
The user chooses the list~$\vns$,
a list of pairs~$(\val, \nv)$,
and is expected
% but not required
to choose a list that
covers every value~$\val$ in the tuple $\btuple\vals$.
%
% If the list $\vns$ is incomplete, then some predecessors are not deleted,
% resulting in a loss of information.
%
In the common case where $\vals$ is a tuple of distinct values, the user is
expected to provide a pointed-by assertion $\val \rcvpointedby \nv$ for each
of these values, and this assertion is updated to $\val \rcvpointedby \nv-1$.

The above rules express the familiar discipline of reference
counting~\cite{collins-60}. In the present case, they implement a form of
\emph{ghost reference counting.} Indeed, no counters exist at runtime. The
counting discipline is purely static; its rules are applied as part of the
verification of a program.

As suggested at the beginning of this section, we believe that reference
counting is just one of several possible styles, or approaches to working with
abstract predecessor information in \DIAMS. In future work, we wish to
investigate other styles, including one where predecessors are not identified
by their address, but are instead grouped in regions. We also plan to
investigate whether and how several styles can be jointly used in the
verification of a single program.

\begin{comment}
% maybe it is worth refining $\cloud\locs{n}$ to some sort of
% $\cloud\locs{\vec{\vals}}$ to get the general free rule
\begin{mathpar}
  \begin{array}{l@{\,}c}
    & \cloud\locs{\vec{\vals}}\\
    \star & \val_1\pointedby\btuple n_1\\[-.2em]
    & \vdots\\
    \star & \val_k\pointedby\btuple n_k\\
  \end{array}
  \supd
  \begin{array}{l@{\,}c}
    & \ddag{\locs}\\
    \star & \SC{\sz{\vec\vals}}\\
    \star & \val_1\pointedby\btuple n_1 - |\vec\vals|_{v_1}\\[-.2em]
    & \vdots\\
    \star & \val_k\pointedby\btuple n_k - |\vec\vals|_{v_k}\\
  \end{array}
  \qquad
  \TirNameStyle{RC-Free}
\end{mathpar}
where $\sz{\vec\vals}$ is $\sum_{\vals\in\vec\vals}\sz{\vals}$
and $|\vec\vals|_v$ is $\sum_{\vals\in\vec\vals}|\vals|_{v}$.
\end{comment}

% couldn't we write
% (l↦[l']) ={E}=> (†l ∗ ◊2 ∗ (∀n, l'↤n -∗ l'↤n-1))
% ?
% ans: no, not with our simple definition of l↤n, otherwise
% (l'↤{l,m} ∗ l↦[l'])
%    ={E}=> (rule above)
% (l'↤{l,m} ∗ †l ∗ ◊2 ∗ (∀n l'↤n -∗ l'↤n-1))
%    ={E}=> (normal cleanup)
% (l'↤{m} ∗ †l ∗ ◊2 ∗ (∀n l'↤n -∗ l'↤n-1))
%    -∗     (def of ↤ℕ)
% (l'↤1 ∗ †l ∗ ◊2 ∗ (∀n l'↤n -∗ l'↤n-1))
%    -∗     (-∗ cancellation)
% (l'↤0 ∗ †l ∗ ◊2)
