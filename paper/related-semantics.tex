Reasoning about space usage in the presence of garbage collection requires
defining a suitable \emph{cost model}, that is, typically, an operational
semantics where the action of the garbage collector is explicitly modeled.
%
% For instance,
\citet{felleisen-hieb-92},
%
% felleisen-hieb-92 seem to have a complex treatment of evaluation contexts;
% for some reason that I do not understand, evaluation is not allowed to take
% place under an evaluation context; instead, the term is transformed in such
% a way that the redex is moved to the root, where it can be reduced. (Is this
% some sort of on-the-fly CPS transformation?) This is fortunate because the
% GC rule would not make sense if it could be applied under an evaluation
% context; the roots of the context would be ignored.
%
followed by \citet{morrisett-felleisen-harper-95},
%
% morrisett-felleisen-harper-95 have a more standard reduction relation on
% configurations of the form letrec H in e, essentially a pair of a heap H
% and an expression e. Evaluation under a context is explicitly built into
% each of the reduction rules. The GC rule is thus sound.
%
present and study several such semantics expressed in a small-step
style.
% The action of the garbage collector is modeled by a reduction rule.
The roots are the memory locations that appear in the current expression: this
is known as the \emph{free-variable rule}.%
\footnote{In these papers, memory locations and variables are conflated.}
% e.g. in morrisett-felleisen-harper-95, memory locations are viewed as
% variables bound by a toplevel $\kw{letrec}$ construct.
%
Garbage collection can also be modeled in big-step
style~\cite{blelloch-greiner-96,minamide-99,spoonhower-08,%
  niu-hoffmann-18}. Then, the evaluation judgement is typically parameterized
with a set of roots. When descending under an evaluation context, this set is
augmented with the memory locations that appear in the context. The effect of
such a setup is analogous to that of the free-variable rule.
%

We follow a different route and consider a low-level calculus where a
variable denotes the address of a stack cell. This contrasts with ordinary
call-by-value \lc, where a variable denotes a value. As a result, we have no
need for a free-variable rule, that is, no need to extract the memory
locations that appear in the current expression:
% (all of them would be locations of stack cells, anyway)
it suffices instead to view every stack cell as a root.
%
This allows us to model garbage collection as a relation between stores,
something that was not possible in the calculi cited above, and to use the
Iris framework without modification.%
\footnote{Iris lets the user choose the definition of \emph{thread-local}
  reduction, but does not allow the user to customize \emph{thread-pool}
  reduction, which is rigidly defined as the interleaving of thread-local
  reduction steps. Thus, unless one is willing to modify Iris, one must build
  garbage collection into thread-local reduction. However, thread-local
  reduction sees only the current thread, not every thread, so it cannot scan
  every thread for its roots; this precludes the use of the free-variable
  rule.}
% As a bonus, we also benefit from Iris's built-in support for evaluation
% contexts, since our GC rule *can* be applied under an evaluation context.
% That said, we have very few evaluation contexts (only two).

% Blelloch and Greiner propose two space-aware semantics for NESL:
% a natural (big-step) semantics and an abstract machine semantics.
% NESL allows parallelism, but one of the measures returned by the
% semantics is the space taken by a sequential execution.
% The natural semantics is explicitly parameterized with a set of roots.
%
% The use of a natural semantics is possible only because
% NESL has structured (fork-join) parallelism, no threads!
% The abstract machine semantics does involve threads.
% It also involves a nontrivial computation of roots (Fig. 9).
%
% \citet{minamide-99} proves that the CPS transformation is space-preserving,
% based on (a variant of?) Blelloch and Greiner's semantics.
% Minamide criticizes Blelloch and Greiner's semantics because it measures
% the size of the closures, but does not measure the size of stack frames.
% This seems consistent to me, provided one is interested in the size of the
% heap and not in the size of the stack.
%
% niu-hoffmann-18 also have a operational semantics with GC,
% where an explicit set of roots appears.

As noted by \citet{morrisett-felleisen-harper-95}, the free-variable rule
offers a conservative answer to an undecidable question, namely: which objects
can be safely reclaimed?
%
% An object can be safely reclaimed iff it won't be accessed in the future.
%
This rule regards every memory location that appears in the current expression
as a root. In a substitution-based semantics, where values are substituted for
variables at runtime, this amounts to \emph{regarding a~variable as a root as
long as there is at least one occurrence of this variable ahead of the current
program point}.
%
This implies, in particular, that a variable can cease to be a root before it
goes out of scope: it is a fairly precise approximation.
%
% and (we believe) it should feel natural to an experienced programmer.
%
% It is equivalent to performing a liveness analysis of the local variables,
% and considering that a dead variable is not a root.
%
In \spacelang, in contrast, a stack cell continues to be viewed as a root
until it is deallocated. This is a less precise approximation; fortunately, by
explicitly writing a unit value into a stack cell, the programmer can
\emph{kill} it, that is, indicate that this cell should no longer be viewed as
a root.
%
% Future work: we are presently missing a mechanism to kill a cell
% without actually writing into it. That said, a good compiler should
% be able to find out that this write instruction is useless and remove
% it.
