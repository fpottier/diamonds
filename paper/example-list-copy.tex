\subsection{Linked Lists and List Copy}
\label{sec:list:copy}

% ------------------------------------------------------------------------------

% Linked lists.

\begin{figure}
\input{fig-islist}
\caption{The Predicate $\isListName$, in Reference-Counting Style}
\Description{The Predicate $\isListName$, in Reference-Counting Style}
\label{fig:isList}
\end{figure}

We represent linked lists in memory as follows: the empty list is represented
as a tuple of one field, containing the tag~0; a nonempty list is represented
as a tuple of three fields, containing the tag~1, the list head, and a pointer
to the tail of the list.
%
This convention is reflected in the definition of the predicate $\isListName$
(\fref{fig:isList}).
%
It is the traditional definition~\cite{reynolds-02},
with the addition of the assertion $\loc' \rcpointedby 1$
in the second line,
which indicates that the pointer from the first cell at address~$\loc$
to the list tail at address~$\loc'$ is the sole pointer to~$\loc'$:
that is, the list tail is not shared.
%
Assuming that one wishes to express the absence of sharing (as we do, here),
this assertion is necessary: indeed, the traditional chain of points-to
assertions alone expresses the unique ownership of every cell but does
not rule out the existence of pointers from the outside into the list.

% ------------------------------------------------------------------------------

% The code.

\begin{figure}
\input{fig-list-copy}
\caption{A List Copy Procedure in \spacelang}
\Description{A List Copy Procedure in \spacelang}
\label{fig:list:copy}
\end{figure}

\input{fig-list-copy-spec}

The code of a list copy procedure, $\listcopy$, appears in
\fref{fig:list:copy}. It is a function of three parameters.
%
The stack cell~$\self$ is expected to hold a pointer to $\listcopy$ itself:
this pointer allows the recursive call.
%
The stack cell~$\destination$, an ``out parameter'', is used to return the
address of the new list.
%
The stack cell~$\source$ holds the address of the original list.
%
One could define a version of $\listcopy$ where a single stack cell plays the
role of both $\destination$ and $\source$, an ``in-out parameter''.
For simplicity, we keep these cells separate.
%
% One could remark that alloca has lower syntactic priority than semicolon,
% so "alloca x in e1; e2" is understood as "alloca x in (e1; e2)".
%
Due to the low-level character of \spacelang, the code is somewhat verbose;
it is however fairly straightforward. Up to three new stack cells,
$\etiquette$, $\head$, and $\tail$, are allocated in order to load
the three fields of the first list cell.
%
Another stack cell, $\res$, is allocated in order to receive the result of the
recursive call.
% removed (a bit complicated)
% \footnote{One cannot simply pass ``$*\destination+2$'' as the second argument
%   of the recursive call. This is not a well-formed memory location, and even
%   if it were, the arguments of every function must be addresses of stack
%   cells. It is possible to formulate a variant of $\listcopy$ in
%   destination-passing style, which offers the advantage of using only
%   $O(1)$ space on the stack, whereas $\listcopy$ uses $O(n)$ space
%   on the stack. It is not shown here.}
%
% What is disturbing is that neither the operational semantics of ICall nor
% the reasoning rule for ICall require that the arguments be addresses of
% stack cells; they could be addresses in the heap. However, once a variable
% has been bound to an address in the heap, it becomes unusable, because
% ILoad, IStore, etc., require the variable to denote the address of a stack
% cell.
%

A specification of $\listcopy$ appears in \fref{fig:list:copy:spec}. This is
a rich specification, which provides precise heap-space-usage information and
keeps track of reference counts both for the list and for its elements. This
explains why the specification may seem intimidating at first.
%
Let us review its key aspects.
%
The first three lines in the pre- and postcondition describe the initial and
final contents of the stack cells $\callee$, $\destination$, and $\source$.
%
% We assume that $\dst$ initially holds a unit value, because this simplifies
% the specification (one predecessor deletion is avoided).
%
The next line describes the original list at address~$\loc$.
The precondition requires the unique ownership of this linked list
and assumes that its reference count is $\nmaster$;
this is expressed by the assertion
$\isList\loc\loglist \star \loc\rcvpointedby\nmaster$.
%
In the postcondition, the reference count is decremented,
because the pointer from $\source$ to $\loc$ is destroyed;
thus,
$\isList\loc\loglist \star \loc\rcvpointedby{\nmaster-1}$
is produced.
In the special case where $\nmaster$ is 1,
the list at address~$\loc$ becomes inaccessible
and is \logically deallocated by $\listcopy$\,:
in that case, nothing
is produced instead of
$\isList\loc\loglist \star \loc\rcvpointedby{0}$.
%
This is a design choice. We could have instead chosen
to not exploit \RULE{RC-Free-Singleton}
in the verification of $\listcopy$ and to
produce
$\isList\loc\loglist \star \loc\rcvpointedby{\nmaster-1}$
in all cases.
%
One positive effect of this choice
is the tight space bound that we are able to express,
which is visible in the next line of the precondition.
When $\nmaster$ is greater than 1, we require
$2 + 4 \times \alen\loglist$ space credits,
that is, enough free space to allocate a new list.
%
When $\nmaster$ is 1, however, we require no space credits,
that is, we guarantee that $\listcopy$ runs in constant
heap space!
%
Indeed, in this case, the first list cell becomes inaccessible when the
pointer stored in the stack cell~$\source$ is destroyed by the
instruction~$\iconst\source\vunit$. Thus, we can apply
\RULE{RC-Free-Singleton} and obtain enough
space credits to allocate a new list cell.
%
% A technical remark: the instruction $\iconst\source\vunit$ must appear
% before the recursive call, otherwise we would not be able to prove that
% the list tail has reference count 1.
%
This is reminiscent of Hofmann's use of space credits in
LFPL~[\citeyear{hofmann-00}], except in the present case, we have garbage
collection instead of an explicit deallocation instruction.

The assertion
$\isList{\loc'}\loglist \star \loc' \rcpointedby 1$
in the postcondition
guarantees that the newly-allocated list is uniquely owned and is not shared.

There remains to explain the last line of the pre- and postcondition.
Its purpose is to update the reference counts of the list elements.
This is done in the same way as in \RULE{RC-Free-Singleton}.
In~the general case where $\nmaster$ is greater than 1,
the reference count of each element~$\val$ is incremented
by the number of times this element occurs in the list,
that is, by $\valoccs\val\loglist$.
This reflects the fact that this element is now accessible
both via the original list and via the copy.
However, in the special case where $\nmaster$ is~1,
the reference count of the element does not change,
because the original list is \logically deallocated
and no longer contributes to the reference count of the elements.
%
The side condition
$\forall\val\in\loglist.\;\exists\nv.\;(\val,\nv)\in\vns$
ensures that the reference count of every list element
is properly updated.
% It would be unsound not to increment a reference count.
% But Jean-Marie notes that even in the case where the
% reference count is not incremented, the side condition
% is still required.

% One could remark that, in the special case where the list elements
% are known not to be memory locations, this part of the specification
% vanishes. However, this is fairly uncommon in practice (it applies
% only to lists of integers, in practice), so we omit this remark.

% The specification may seem surprising, because we require unique ownership
% of the original list (isList), yet we still allow the existence of many
% pointers to this list (\loc \rcpointedby \nv). The reader might think that
% if the list is uniquely owned, then there is no need to allow multiple
% pointers to it. Not true; in concurrent algorithms, it is typical to place
% the unique ownership of a data structure in a shared invariant, and to allow
% the existence of many pointers to this data structure.

% We have required a uniquely-owned list (with fraction 1 and with a
% pointed-by assertion) so that we could deallocate it if its reference count
% is 1. However, one could also provide a spec where we only have fractional
% (read) access to the list and cannot deallocate it.
