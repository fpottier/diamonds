* May wish to show that `ocloud antecedents n loc` is covariant in
  `antecedents`, that is, it is permitted to add antecedents.

* Relax the reasoning rule `wp_store` so that, in the precondition,
  providing a `mapsfrom` assertion about the value that is overwritten
  becomes optional. If it is omitted, then a loss of information occurs,
  but the rule should still be sound.

* Prove lemmas that take two `mapsto` facts as hypotheses (which fractions
  that add up to more than 1) and conclude that the addresses must be distinct.
  (This includes †l as a special case.) (Do the same for `mapsfrom` facts?)
  Write tactics to automate the application of these lemmas to all facts in
  the context.

* A block with zero fields could have size `0` instead of `1`.

* Currently, a block with zero fields has an address, and its predecessors are
  tracked, even though this serves no useful purpose. It would be preferable
  to avoid this overhead.

* Think about weak pointers. Can we say anything about them? It would
  be tempting not to register them in the `predecessors` field, and
  to guarantee that `Weak.get` will return `Some _` if the target
  object still has at least one predecessor. Unfortunately, this is
  unsound; the fact that it has a predecessor implies that we have not
  freed it (as part of our static reasoning), but does not imply that
  it actually is reachable via strong pointers in the eyes of the GC.
  We might guarantee that if `†l` is available then `Weak.get` returns
  `None` (is this true in practice? I don't think so). Conversely, we
  might wish to guarantee that if `Weak.get` returns `None` then `†l`
  holds, but that is unsound (physically deallocated does not imply
  logically deallocated). We might wish to state that if `Weak.get`
  returns `None` then the object has no (strong) predecessors (so a
  `mapsfrom` assertion can be refined).

* Study the gap between our SpaceLang and CakeML's DataLang.
  See [the syntax of DataLang](https://github.com/CakeML/cakeml/blob/master/compiler/backend/dataLangScript.sml),
  [the semantics of DataLang](https://github.com/CakeML/cakeml/blob/master/compiler/backend/semantics/dataSemScript.sml),
  and [the definition of `size_of`](https://github.com/CakeML/cakeml/blob/master/compiler/backend/semantics/dataSemScript.sml#L119).

* Change the semantics slightly to allow a predicate `◊∞` such that `◊∞
  -∗ ◊∞ ∗ ◊1`, which would allow proving algorithms without counting
  diamonds.
