\documentclass[
  usenames,
  dvipsnames,
  hyperref={
    colorlinks=true,
    linkcolor=black,
    citecolor=magenta,
    urlcolor=magenta,
  }
]{beamer}
\usetheme{Pittsburgh}
\usecolortheme{seahorse}
\usepackage[T1]{fontenc}
\usepackage[utf8]{inputenc}
\usepackage{fontawesome}
\usepackage{lmodern} % this makes \textsc work
\usepackage{mathpartir}
\renewcommand{\TirNameStyle}[1]{\footnotesize\textsc{#1}}
\usepackage{moreverb}
\usepackage{stmaryrd} % for \mapsfrom
\usepackage{xspace}
\input{setup}
\usepackage{iris}
\input{macros}

\usepackage{listings}
\input{listings-ocaml}
\lstset{
  language=ocaml,
  escapeinside={(*@}{*)},
  keywordstyle=\bfseries\color{RubineRed},
  frame=single,  % this seems required for framesep to be taken into account
  framerule=0pt, % this counters frame=single by making the lines invisible
}
\usepackage{moreverb}
\usepackage[scaled=0.75]{beramono}

% Show frame numbers.
\setbeamertemplate{footline}[frame number]

% Do not add vertical space around \begin{center} ... \end{center}.
\renewenvironment{center}
 {\parskip=0pt\par\nopagebreak\centering}
 {\par\noindent\ignorespacesafterend}

% ----------------------------------------------------------------------------
% Page de titre.

\title{A Separation Logic for Heap Space under Garbage Collection}
\author{Jean-Marie Madiot \and François Pottier}
\date{December 6, 2021}

\begin{document}

\begin{frame}
\begin{center}
\includegraphics[width=.9\textwidth]{SLtitle}
\end{center}
\end{frame}

% A logo on every slide (except the first slide, already shown).
\logo{\includegraphics[width=.1\textwidth]{SL}}

% ----------------------------------------------------------------------------
% Rationale.

\begin{frame}{Reasoning about Heap Space}

  How can we establish \emph{formal (verified)} bounds \\
    \hspace{3cm} on a program's \emph{heap space} usage?

  We wish to
  \begin{itemize}
  \item work in the setting of a \emph{program logic},
  \item view heap space as a \emph{resource}.
  \end{itemize}
  % so that a function's effect on the amount of free space is visible in its specification.

  % This approach has been applied to control \emph{time} complexity.
  % Can it be applied to heap space?

\end{frame}

\begin{frame}{Reasoning about Heap Space, without GC}

  Following Hofmann (1999, 2000),
  let $\SC{1}$
  represent one \emph{space credit}.

  Allocation consumes credits; deallocation produces credits.
  \begin{center}
  \includegraphics[width=.75\textwidth]{no-gc-rules}
  \end{center}
  A function's space requirement is visible in its specification.

  End of talk...?

\end{frame}

\begin{frame}{In the presence of GC, what Happens?}

  Garbage collection can offer superior \emph{simplicity, safety, performance}.

  In the presence of GC,
  \begin{itemize}
  \item deallocation becomes \emph{implicit},
  \item so we lose the ability to recover space credits while reasoning.
  \end{itemize}

\end{frame}

\begin{frame}{A Ghost Deallocation Operation?}

  It is tempting to switch to a \emph{logical deallocation} operation:
  \vspace{3mm}
  \begin{center}
  \includegraphics[width=.4\textwidth]{candidate-free}
  \end{center}

  This would marry
  \begin{itemize}
  \item \emph{manual reasoning} about memory at verification time
  \item \emph{automatic management} of memory at runtime.
  \end{itemize}

\end{frame}

\begin{frame}{Research Questions}

  At least two questions spring to mind:
  \begin{center}
    Is this approach \emph{practical?} Is it \emph{sound?}
  \end{center}

\end{frame}

\begin{frame}{Practicality?}

  A pitfall would be to get \emph{the worst of both worlds:}
  \begin{itemize}
  \item mental burden of manual reasoning about memory deallocation,
  \item performance issues sometimes caused by GC.
    % latency
    % also lower throughput, if the GC is invoked too often and works too much
  \end{itemize}
  Yet we can strive to get the \emph{best} of each:
  \begin{itemize}
  \item simplicity and possibly superior performance afforded by GC,
  \item reasoning at a suitable level of abstraction:
        \\ \hspace{1cm} e.g., via \emph{bulk logical deallocation}.
  \end{itemize}

\end{frame}

\begin{frame}{Soundness?}

  Is logical deallocation sound?

  \vspace{2mm}
  \begin{center}
  \includegraphics[width=.4\textwidth]{candidate-free}
  \end{center}

  It does have a few good properties: \emph{no double-free, no use-after-free}.
  \begin{itemize}
  \item a block cannot be logically deallocated twice; % (no double-free)
  \item a block cannot be accessed after it has been logically deallocated. % (no use-after-free)
  \end{itemize}

\end{frame}

\begin{frame}{Soundness?}

  Unfortunately, logical deallocation in this form is \emph{not sound}.
  \begin{center}
  \includegraphics[width=.4\textwidth]{flawed-free}
  \end{center}
  Introducing logical deallocation creates a distinction between
  \begin{itemize}
  \item the \emph{logical heap} that the programmer keeps in mind, % where deallocation is manual
  \item the \emph{physical heap} that exists at runtime. % managed by the GC
  \end{itemize}

\end{frame}

\begin{frame}{Logical versus Physical Heaps}

  The following situation is problematic.

  The programmer has logically deallocated a block and obtained $\SC{3}$,

  \vspace{2mm}
  \begin{center}
  \includegraphics[width=.5\textwidth]{distinction}
  \end{center}
  but this block is \emph{reachable} and cannot be reclaimed by the GC.

  We have 3 space credits but \emph{no free space} in the physical heap!

\end{frame}

\begin{frame}{Restricting Logical Deallocation}

  To avoid this problem, we want to \emph{restrict logical deallocation}.
  \begin{itemize}
  \item A block should be logically deallocated only if it is \emph{unreachable,}
  \item which guarantees that the GC \emph{can} reclaim this block,
  \item so the logical and physical heaps remain \emph{synchronized}.
  \end{itemize}

\end{frame}

\begin{frame}{A Global Invariant}

  The logical and physical heaps \emph{coincide on their reachable fragments}.

  \vspace{2mm}
  \begin{center}
  \includegraphics[width=0.6\textwidth]{sync}
  \end{center}

  \(\begin{array}{rcl}
  \text{So,} \quad
  \text{\emph{$\SC{k}$}}
  & \text{implies} &
  \text{$k$~free words \emph{exist} in the logical heap} \\
  & \text{implies} &
  \text{$k$~free words \emph{can be created} in the physical heap.}
  \end{array}\)

\end{frame}

\begin{frame}{Problem Solved? Not Yet}

  The outstanding problem is, \emph{how} do we restrict logical deallocation?
  \begin{itemize}
  \item We want to disallow deallocating a \emph{reachable} block,
  \item but Separation Logic lets us reason about \emph{ownership}.
  \item Ownership and reachability are \emph{unrelated!} % neither implies the other.
  \item Furthermore, reachability is a \emph{nonlocal} property.
  \end{itemize}

  \emph{Not} requiring reachability reasoning is a strength of traditional SL.

\end{frame}

\begin{frame}{A Solution: Predecessor Tracking}

  Following Kassios and Kritikos (2013),
  \begin{itemize}
  \item we \emph{keep track of the predecessors} of every block.
  \item If a block has no predecessor, \emph{then} it is unreachable,
  \item therefore it can be logically deallocated.
  \end{itemize}

\end{frame}

\begin{frame}{Points-To and Pointed-By Assertions}

  In addition to \emph{points-to}, we use \emph{pointed-by} assertions:

  \vspace{2mm}

  \begin{center}
  \includegraphics[width=0.6\textwidth]{permissions}
  \end{center}

\end{frame}

\begin{frame}{Logical Deallocation}

  We get a sound logical deallocation axiom, for a single block:
  \begin{center}
  \includegraphics[width=.5\textwidth]{basic-logical-free}
  \end{center}

\end{frame}

\begin{frame}{Dealing with Roots}

  We want the pointers \emph{from the stack(s) to the heap} to be explicit,
  \begin{itemize}
  \item so the operational semantics views them as GC \emph{roots,}
  \item so our predecessor-tracking logic keeps track of them.
  \end{itemize}
  This leads to a calculus where \emph{stack cells} are explicit \\
  \hspace{3cm} and \emph{a variable denotes an address} on the stack.

\end{frame}

% ----------------------------------------------------------------------------

\section{Syntax, Semantics of \spacelang}

\begin{frame}{Values, Blocks, Stores}

  Memory locations:
  \(
    \loc, \sloc, \src, \dst \in \Loc
  \).

  Values include constants, memory locations, and \emph{closed procedures:}
  \[
    \val ::= \vunit \mid \vk \mid \loc \mid \vcode\vars\instr
  \]
  Memory blocks include \emph{heap tuples}, \emph{stack cells}, and deallocated blocks:
  \[
    \blk ::= \btuple\vals \mid \bcell\val \mid \bdeallocated
  \]
  A \emph{store} maps locations to blocks, encompassing the heap and stack(s).
  The \emph{size} of a block:
  \[
  \sz{\btuple\vals} = 1 + \alen\vals
  \qquad
  \sz{\bcell\val} =
  \sz\bdeallocated = 0
  \]
  The size of the store is the sum of the sizes of all blocks.

\end{frame}

\begin{frame}{Call-by-Reference and GC Roots}

  A \emph{reference} is a variable or a (stack) location and denotes a \emph{stack cell}.
  % (the address of a stack cell).
  \[
    \lval ::= \var \mid \sloc
  \]
  \spacelang uses \emph{call-by-reference.}

  A variable denotes a closed reference, \emph{not} a closed value as is usual.

  % A closed reference is a (stack) location.

  The operational semantics involves substitutions $\subst\var\sloc$.

  This preserves the property that
  \emph{the code never points to the heap}.

  % Think about assembly language,
  % where the code mentions registers,
  % and a register can contain a heap address,
  % but the code itself never contains a pointer to the heap.
  % Therefore, the code is not scanned by the GC.

  \qquad The \emph{roots} of the garbage collection process are \emph{the stack cells}.

\end{frame}

\newcommand{\commentary}[1]{ & \text{\small\it #1} \\}

\begin{frame}{Instructions}

  \spacelang is imperative. An \emph{instruction}~$\instr$ does not return a value.
  \[\begin{array}[t]{@{}l@{\hspace{6mm}}l@{}}
    \begin{array}[t]{@{}ll@{}}
      \iskip
                   \commentary{no-op}
      \iseq\instr\instr
                   \commentary{sequencing}
      \iif\lval\instr\instr
                   \commentary{conditional}
      \icall\lval\lvals
                   \commentary{procedure call}
      \iconst\lval\val
                   \commentary{constant load}
      \imove\lval\lval
                   \commentary{move}
    \end{array}
    &
    \begin{array}[t]{@{}ll@{}}
      \ialloc\lval\allocsize
                   \commentary{heap allocation}
      \iload\lval\lval\ofs
                   \commentary{heap load}
      \istore\lval\ofs\lval
                   \commentary{heap store}
      \iloceq\lval\lval\lval
                   \commentary{address comparison}
      \ialloca\var\instr
                   \commentary{stack allocation}
      \iallocaactive\sloc\instr
                   \commentary{active stack cell}
      \ifork\lval\var\instr
                   \commentary{thread creation}
    \end{array}
  \end{array}\]
  The
  % in and out
  operands of
  every instruction are stack cells ($\lval$).

  There is no deallocation instruction for heap blocks.

\end{frame}

\begin{frame}[fragile]{Operational Semantics: Heap Allocation}

  We fix a \emph{maximum heap size}~$\maxsize$.

  Heap allocation \emph{fails} if the heap size exceeds $\maxsize$.
  \bgroup
  \small
  \begin{mathpar}
    \inferrule[StepAlloc]{
      % implied by the use of \mext below:
      % \loc\not\in\dom\store \\
      \store' = \mext\loc{\btuple{\replicate\allocsize\vunit}}\store \\\\
      \hilite{\valid{\store'}} \\
      \writelval\dst\loc{\store'}{\store''}
    }{
      \hs{\ialloc\dst\allocsize}\store
         \iskip{\store''}
    }
  \end{mathpar}
  \egroup
  $\maxsize$ is a parameter of the operational semantics,

  \qquad but the reasoning rules of \DIAMS are independent of~$\maxsize$.

\end{frame}

\begin{frame}[fragile]{Operational Semantics: Stack Allocation}

  The dynamic semantics of stack allocation is in \emph{three steps}:
  \bgroup
  \small
  \begin{mathpar}
    \inferrule[StepAllocaEntry]{
      % implied by the use of \mext below:
      % \sloc\not\in\dom\store \\
      \store' = \mext\sloc{\bcell\vunit}\store
    }{
      \hs{\ialloca\var\instr}\store
         {\iallocaactive\sloc{\subst\var\sloc\instr}}{\store'}
    }

    \inferrule[StepAllocaExit]{
      \store(\sloc) = \bcell\val \\
      \store' = \mupd\sloc\bdeallocated\store
    }{
      \hs{\iallocaactive\sloc\iskip}\store
         \iskip{\store'}
    }
  \end{mathpar}
  \egroup
  Evaluation contexts: \(
    \ectx ::=
      \hole \mid
      \iseq\ectx\instr \mid
      \ialloca\sloc\ectx
  \).

\end{frame}

\begin{frame}{Garbage Collection}

  To complete the definition of the operational semantics,
  \begin{itemize}
  \item allow \emph{garbage collection} before every reduction step. \\[2mm]
    $\gc\store{\store'}$ holds if
    \begin{itemize}
    \item the stores~$\store$ and~$\store'$ have the same domain;
    \item for every~$\loc$ in this domain, \\
          either $\store'(\loc)=\store(\loc)$,
          or $\loc$ is unreachable in $\store$
          and $\store'(\loc)=\bdeallocated$.
    \end{itemize}
  \item allow \emph{thread interleavings} (comes for free with Iris).
  \end{itemize}

\end{frame}

% ----------------------------------------------------------------------------

\section{Reasoning Rules of \DIAMS}

% TODO check orderings of slides, check that all interesting rules are shown

\begin{frame}{Heap Allocation}

  Heap allocation \emph{consumes space credits}.
  \begin{center}
  \includegraphics[width=.9\textwidth]{alloc}
  \end{center}
  Points-to and pointed-by assertions for the new location appear.

  One pointer to the value~$\val$ is \emph{deleted}. (This aspect is optional.)

\end{frame}

\begin{frame}{Heap Store}

  Writing a heap cell is simple... but involves some administration.

  \vspace{2mm}

  \begin{center}
  \includegraphics[width=.9\textwidth]{store}
  \end{center}
  One pointer to $\val$ is deleted; one pointer to $\val'$ is \emph{created}.

\end{frame}

\begin{frame}{Stack Allocation}

  A points-to assertion for the new stack cell exists throughout its lifetime.

  \vspace{2mm}

  \begin{center}
  \includegraphics[width=.5\textwidth]{alloca}
  \end{center}

  No pointed-by assertion is provided. (A design choice.)
  \begin{itemize}
  \item No pointers (from the heap or stack) to the stack.
  \end{itemize}

  % TODO a stack cell can be "killed" by writing () to it

\end{frame}

\begin{frame}{Logical Deallocation}

  Logical deallocation of a block is a \emph{ghost operation:}

  \vspace{2mm}

  \begin{center}
  \includegraphics[width=.75\textwidth]{free-singleton}
  \end{center}

\end{frame}

\begin{frame}{Deferred Predecessor Deletion}

  Deletion of deallocated predecessors can be \emph{deferred:}
  \begin{center}
  \includegraphics[width=.75\textwidth]{cleanup}
  \end{center}
  A key rule: if $L'$ is empty, then $v$ becomes eligible for deallocation.

\end{frame}

\begin{frame}{Bulk Logical Deallocation}

  A group that is \emph{closed under predecessors} can be deallocated at once:
  \begin{center}
  \includegraphics[width=.9\textwidth]{free}
  \end{center}
  The rules for constructing a ``cloud'' (omitted) are straightforward.
  % TODO add CloudEmpty and CloudCons to the backup slides?

\end{frame}

\begin{frame}{More Axioms}

  Points-to and pointed-by assertions can be \emph{split} and \emph{joined}.

  \vspace{2mm}

  \begin{center}
  \includegraphics[width=.85\textwidth]{axioms-1}
  \end{center}
  Pointed-by assertions are \emph{covariant}.

  Points-to and pointed-by assertions can be \emph{confronted}.

\end{frame}

\begin{frame}{More Axioms}

  Space credits can be \emph{split} and \emph{joined}.

  \vspace{2mm}
  \begin{center}
  \includegraphics[width=.5\textwidth]{axioms-2}
  \end{center}

\end{frame}

\begin{frame}{Soundness of \DIAMS}

  \begin{theorem}[Soundness]
    If $\triple{\SC\maxsize}\instr\iTrue$ holds, then,
    executing~$\instr$ in an empty store \\ cannot
    lead to a situation where a thread is stuck.
  \end{theorem}

  If the code is verified under $\maxsize$ space credits, \\
  then its heap space usage cannot exceed $\maxsize$.

  This guarantee holds \emph{for every $\maxsize$}. \\
  The reasoning rules are \emph{independent} of $\maxsize$.

  \qquad The rules allow \emph{compositional reasoning} about space.

\end{frame}

% TODO internal invariant?

% ----------------------------------------------------------------------------

\section{Ghost Reference Counting}

\begin{frame}{Choice of a Predecessor Tracking Discipline}

Keeping track of a \emph{multiset} of predecessors can be heavy.

Sometimes
\begin{itemize}
\item \emph{counting} predecessors is enough,
\item or recording what \emph{regions} the predecessors inhabit is enough.
\end{itemize}
Can \emph{high-level predecessor tracking disciplines}
be defined on top of \DIAMS?

\end{frame}

\begin{frame}{Example: Ghost Reference Counting}

The simplified pointed-by assertion \emph{$\val \rcvpointedby \nv$}
counts predecessors:
%
\[
  \val \rcvpointedby \nv
  \quad \eqdef \quad
  \exists \lsv. \; (
    \val \vfpointedby{1} \lsv \;\star\;
    \pure{\cardinal\lsv = \nv}
  )
\]
%
Edge addition / deletion
% (not shown)
increment / decrement~$n$.

\end{frame}

% ----------------------------------------------------------------------------

\section{Examples of Specifications}

\subsection{A Stack}

\begin{frame}{Creation}

Creating a stack \emph{consumes 4 space credits}.
\[
% Stack.create
\bightriplex
  {
    \callee\pointsto\bcell\stackcreate \star
    \stack\pointsto\bcell\vunit \star
    \SC{4}
  }
  {
    \icall\callee\stack
  }
  {\loc}
  {
    \callee\pointsto\bcell\stackcreate \star
    \stack\pointsto\bcell\loc \star
    \isStack\loc\lognil \ordinarystar
    \loc \rcpointedby 1 \star
  }
\]
We get unique ownership of the stack and \emph{we have the sole pointer} to it.

\end{frame}

\begin{frame}{Pushing}

Pushing \emph{consumes 4 space credits}.
\[
% Stack.push
\bightriple
  {
    \callee\pointsto\bcell\stackpush \star
    \stack\pointsto\bcell\loc \star
    \elem\pointsto\bcell\val \star
    \SC{4} \ordinarystar % save a line and improve alignment with postcondition
    \isStack\loc\loglist \star
    \val \rcvpointedby n
  }
  {
    \icall\callee{\stack, \elem}
  }
  {
    \callee\pointsto\bcell\stackpush \star
    \stack\pointsto\bcell\loc \star
    \elem\pointsto\bcell\val \star
    \isStack\loc{(\logcons\val\loglist)} \star
    \val \rcvpointedby {n+1}
  }
\]
The value~$\val$ receives \emph{one more antecedent}.

\end{frame}

\begin{frame}{Popping}

Popping \emph{frees up 4 space credits}.
\[
% Stack.pop
\bightriple
  {
    \callee\pointsto\bcell\stackpop \star
    \stack\pointsto\bcell\loc \star
    \elem\pointsto\bcell\vunit \star
    \isStack\loc{(\logcons\val\loglist)} \star
    \val \rcvpointedby n
  }
  {
    \icall\callee{\stack, \elem}
  }
  {
    \callee\pointsto\bcell\stackpop \star
    \stack\pointsto\bcell\loc \star
    \elem\pointsto\bcell\val \star
    \SC{4} \ordinarystar % save a line and improve alignment
    \isStack\loc\loglist \star
    \val \rcvpointedby n % yes, n, not n-1
  }
\]
The number of antecedents of~$\val$ is unchanged, as $\elem$ points to it.

\end{frame}

\begin{frame}{Disposal}

Logically deallocating the entire stack is a \emph{ghost operation}.

It frees up \emph{a linear number of space credits}.
\[
% Stack.free (ghost)
\bigsupd
  {
    \isStack\loc\loglist \;\ordinarystar\;
    \loc \rcpointedby 0 \star
    \bigast{(\val, n)\in\vns}{\val \rcvpointedby n}
  }
  {
    % \ddagsingleton\loc \star
    \SCparen{4 + 4 \times \alen\loglist} \star
    \bigast{(\val, n)\in\vns}{\val \rcvpointedby {n - (\valoccs\val\loglist)}}
  }
\]
The ghost reference counters of the stack elements are decremented.

\end{frame}

% ----------------------------------------------------------------------------

\subsection{List Copy}

\begin{frame}{Lists Without Sharing}

  Each cell owns the next cell and possesses \emph{the sole pointer} to it.

  \[\begin{array}{rcl}
  \isList\loc\lognil
  & \eqdef &
    \phantom{\exists\loc'.\;}
    \loc \pointsto \btuple{\loglistbrackets{0}}
  \\
  \isList\loc{(\logcons\logval\loglist\,)}
  & \eqdef &
    \exists\loc'.\;
    \loc \pointsto \btuple{\loglistbrackets{1; \logval; \loc'}} \star
    \hilite{\loc' \rcpointedby 1} \star
    \isList{\loc'}\loglist
  \end{array}\]

  Let's now have a look at \emph{list copy} and its spec. (Fasten seatbelts!)

\end{frame}

\begin{frame}{List Copy in \spacelang}

\small
\input{fig-list-copy}

\end{frame}

\begin{comment}
% Source of original slide, before annotation:
\begin{frame}{Specification of List Copy}

\small
\input{list-copy-spec}

\end{frame}
\end{comment}

\begin{frame}{Specification of List Copy}

  The case $m=1$, where we have \emph{the sole pointer} to the list, is special.

  \vspace{4mm}
  \begin{center}
  \includegraphics[width=.85\textwidth]{list-copy-spec}
  \end{center}

\end{frame}

% ----------------------------------------------------------------------------

\section{Conclusion}

\begin{frame}{Summary of Contributions}

  A sound logic to reason about space usage in the presence of GC.
  \begin{itemize}
  \item Allocation consumes \emph{space credits} $\SC{n}$.
  \item \emph{Logical deallocation} is a ghost operation.
  \item Logical dellocation requires \emph{predecessor tracking} $\val \pointedby \lsv$.
  \end{itemize}

\end{frame}

\begin{frame}{Future Work}

  Predecessor tracking still requires \emph{too much administration}.

  We are investigating
  \begin{itemize}
  % \item better definitions of the reference-counting assertion $\val \rcpointedby n$;
  \item \emph{deferred} edge deletion; % (delayed, therefore optional)
  \item \emph{automated or simplified} tracking of \emph{roots}; % pointers from the stack to the heap;
  \item predecessor tracking based on \emph{regions};
  \item notions of \emph{single-entry-point} regions. % possibly but not necessarily involving a notion of domination
  \end{itemize}

  We would also like to adapt \DIAMS directly to call-by-value $\lambda$-calculus.

\end{frame}

\begin{frame}[fragile,t]{A Bit of Controversy about OCaml}

\vspace{10mm}

During this traversal, \emph{which part of the tree is live?}

\begin{lstlisting}[backgroundcolor=\color{gray!8}]
  type tree = Leaf | Node of tree * tree

  let rec walk t =
    match t with
    | Leaf          -> ()
    | Node (t1, t2) -> walk t1; walk t2
\end{lstlisting}
\pause
It could (should?) be \emph{the subtrees that have not yet been traversed},

\quad because \oc|t2| remains live while \oc|walk t1| is executed...

\end{frame}

\begin{frame}[fragile,t]{A Bit of Controversy about OCaml}

\vspace{10mm}

But the OCaml compiler transforms the code roughly as follows:

\begin{lstlisting}[backgroundcolor=\color{gray!8},escapechar=!]
  type tree = Leaf | Node of tree * tree

  let rec walk t =
    match t with
    | Leaf          -> ()
    | Node ( _,  _) -> walk !\colorbox{red!15}{t.1}!; walk !\colorbox{red!15}{t.2}!
\end{lstlisting}
Thus, \oc|t| remains live while \oc|walk t.1| is executed.

Every \emph{left subtree remains live} until it has been entirely traversed.

Reasoning about space at this level \\
\quad requires a \emph{precise definition} of where each variable is a root.

\end{frame}

% ----------------------------------------------------------------------------

\section*{Backup}

\logo{}

\begin{frame}{Operational Semantics}

  \input{semantics}

\end{frame}

\end{document}
