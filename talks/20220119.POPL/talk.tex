\documentclass[
  usenames,
  dvipsnames,
  hyperref={
    colorlinks=true,
    linkcolor=black,
    citecolor=magenta,
    urlcolor=magenta,
  }
]{beamer}
\usetheme{Pittsburgh}
\usecolortheme{seahorse}
\usepackage{appendixnumberbeamer}
\usepackage[T1]{fontenc}
\usepackage[utf8]{inputenc}
\usepackage{fontawesome}
\usepackage{lmodern} % this makes \textsc work
\usepackage{mathpartir}
\renewcommand{\TirNameStyle}[1]{\footnotesize\textsc{#1}}
\usepackage{moreverb}
\usepackage{stmaryrd} % for \mapsfrom
\usepackage{xspace}
\input{setup}
\usepackage{iris}
\input{macros}

\usepackage{listings}
\input{listings-ocaml}
\lstset{
  language=ocaml,
  escapeinside={(*@}{*)},
  keywordstyle=\bfseries\color{RubineRed},
  frame=single,  % this seems required for framesep to be taken into account
  framerule=0pt, % this counters frame=single by making the lines invisible
}
\usepackage{moreverb}
\usepackage[scaled=0.75]{beramono}

% Show frame numbers.
\setbeamertemplate{footline}[frame number]

% Do not add vertical space around \begin{center} ... \end{center}.
\renewenvironment{center}
 {\parskip=0pt\par\nopagebreak\centering}
 {\par\noindent\ignorespacesafterend}

% ----------------------------------------------------------------------------
% Page de titre.

\title{A Separation Logic for Heap Space under Garbage Collection}
\author{Jean-Marie Madiot \and François Pottier}
\date{POPL 2022}

\begin{document}

\begin{frame}
\begin{center}
\includegraphics[width=.9\textwidth]{SLtitle}
\end{center}
\end{frame}

% A logo on every slide (except the first slide, already shown).
\logo{\includegraphics[width=.1\textwidth]{SL}}

% ----------------------------------------------------------------------------
% Rationale.

\begin{frame}{Reasoning about Heap Space}

  We wish to verify a program's \emph{heap space} usage,
  \begin{itemize}
  \item using \emph{separation logic},
  \item by viewing heap space as a \emph{resource}...
  \end{itemize}
  % so that a function's effect on the amount of free space is visible in its specification.

  % This approach has been applied to control \emph{time} complexity.
  % Can it be applied to heap space?

\end{frame}

\begin{frame}{Reasoning about Heap Space, without GC}

  \textbf{Idea 1.} Following Hofmann (1999), % (1999, 2000),
  let $\SC{1}$
  represent one \emph{space credit}.

  Allocation consumes credits; deallocation produces credits.
  \begin{center}
  \includegraphics[width=.75\textwidth]{no-gc-rules}
  \end{center}
  A function's space requirement is visible in its specification.

\end{frame}

\begin{frame}{In the presence of GC, what Happens?}

  % Garbage collection can offer superior \emph{simplicity, safety, performance}.

  In the presence of GC,
  \begin{itemize}
  \item deallocation becomes \emph{implicit},
  \item so we lose the ability to recover space credits while reasoning.
  \end{itemize}

\end{frame}

\begin{frame}{A Ghost Deallocation Operation?}

  \textbf{Idea 2.} Switch to a \emph{logical deallocation} operation:
  \vspace{3mm}
  \begin{center}
  \includegraphics[width=.4\textwidth]{candidate-free}
  \end{center}

  A ghost update $\Rrightarrow$ \emph{consumes} an assertion
  and \emph{produces} an assertion.

  This marries
  \begin{itemize}
  \item \emph{manual reasoning} about memory at verification time
  \item \emph{automatic management} of memory at runtime.
  \end{itemize}

\end{frame}

\begin{frame}{Soundness?}

  Is logical deallocation sound?

  \vspace{2mm}
  \begin{center}
  \includegraphics[width=.4\textwidth]{candidate-free}
  \end{center}

  It does have a few good properties: \emph{no double-free, no use-after-free}.

  Because $x \mapsto b$ is consumed,
  \begin{itemize}
  \item a block cannot be logically deallocated twice; % (no double-free)
  \item a block cannot be accessed after it has been logically deallocated. % (no use-after-free)
  \end{itemize}

\end{frame}

\begin{frame}{Soundness?}

  Unfortunately, logical deallocation in this form is \emph{not sound}.
  \begin{center}
  \includegraphics[width=.4\textwidth]{flawed-free}
  \end{center}
  Introducing logical deallocation creates a distinction between
  \begin{itemize}
  \item the \emph{logical heap} that the programmer keeps in mind, % where deallocation is manual
  \item the \emph{physical heap} that exists at runtime. % managed by the GC
  \end{itemize}

\end{frame}

\begin{frame}{Logical versus Physical Heaps}

  The following situation is problematic.

  The programmer has logically deallocated a block and obtained $\SC{3}$,

  \vspace{2mm}
  \begin{center}
  \includegraphics[width=.5\textwidth]{distinction}
  \end{center}
  but this block is \emph{reachable} and cannot be reclaimed by the GC.

  We have 3 space credits but \emph{no free space} in the physical heap!

\end{frame}

\begin{frame}{Restricting Logical Deallocation}

  To avoid this problem, we must \emph{restrict logical deallocation}:
  \begin{itemize}
  \item A reachable block must not be deallocated.
  \end{itemize}
  In the contrapositive,
  \begin{itemize}
  \item A block should be logically deallocatable only if it is \emph{unreachable,}
  \item so the GC \emph{can} reclaim this block,
  \item so the logical and physical heaps remain \emph{synchronized}.
  \end{itemize}

\end{frame}

\begin{frame}{The Desired Global Invariant}

  The logical and physical heaps
  % \\ \hspace{2cm}
  \emph{coincide on their reachable fragments}.
  % at all times.

  \vspace{2mm}
  \begin{center}
  \includegraphics[width=0.6\textwidth]{sync}
  \end{center}

  %% \(\begin{array}{rcl}
  %% \text{So,} \quad
  %% \text{\emph{$\SC{k}$}}
  %% & \text{implies} &
  %% \text{$k$~free words \emph{exist} in the logical heap} \\
  %% & \text{implies} &
  %% \text{$k$~free words \emph{can be created} in the physical heap.}
  %% \end{array}\)

\end{frame}

\begin{frame}{Restricting Logical Deallocation: How?}

  \emph{How} do we restrict logical deallocation?
  \begin{itemize}
  \item We want to disallow deallocating a \emph{reachable} block,
  \item but Separation Logic lets us reason about \emph{ownership}.
% \item Ownership and reachability are \emph{unrelated!} % neither implies the other.
  \item Reachability is a \emph{nonlocal} property.
  \end{itemize}

  % \emph{Not} requiring reachability reasoning is a strength of traditional SL.

\end{frame}

\begin{frame}{Predecessor Tracking}

  \textbf{Idea 3.} Following Kassios and Kritikos (2013),
  \begin{itemize}
  \item we \emph{keep track of the predecessors} of every block.
  \item If a block has no predecessor, \emph{then} it is unreachable,
  \item therefore it can be logically deallocated.
  \end{itemize}

\end{frame}

\begin{frame}{Points-To and Pointed-By Assertions}

  In addition to \emph{points-to}, we use \emph{pointed-by} assertions:

  \vspace{2mm}

  \begin{center}
  \includegraphics[width=0.6\textwidth]{permissions}
  \end{center}

\end{frame}

\begin{frame}{Logical Deallocation}

  We get a sound logical deallocation axiom: % (simplified)
  \begin{center}
  \includegraphics[width=.5\textwidth]{basic-logical-free}
  \end{center}
  This axiom deallocates one block.

  There is also a \emph{bulk logical deallocation} axiom.

\end{frame}

\begin{frame}{Dealing with Roots}

  We want the pointers \emph{from the stack(s) to the heap} to be explicit,
  \begin{itemize}
  \item so the operational semantics views them as GC \emph{roots,}
  \item so our predecessor-tracking logic keeps track of them.
  \end{itemize}
  \textbf{Idea 4.} Use a low-level calculus where \emph{stack cells} are explicit.
  % \hspace{3cm} and \emph{a variable denotes an address} on the stack.

\end{frame}

% ----------------------------------------------------------------------------

\section{A Glimpse of \spacelang}

\newcommand{\commentary}[1]{ & \text{\small\it #1} \\}

\begin{frame}{Instructions}

  \spacelang is imperative. An \emph{instruction}~$\instr$ does not return a value.
  \[\begin{array}[t]{@{}l@{\hspace{6mm}}l@{}}
    \begin{array}[t]{@{}ll@{}}
      \iskip
                   \commentary{no-op}
      \iseq\instr\instr
                   \commentary{sequencing}
      \iif\lval\instr\instr
                   \commentary{conditional}
      \icall\lval\lvals
                   \commentary{procedure call}
      \iconst\lval\val
                   \commentary{constant load}
      \imove\lval\lval
                   \commentary{move}
    \end{array}
    &
    \begin{array}[t]{@{}ll@{}}
      \ialloc\lval\allocsize
                   \commentary{\emph{heap allocation}}
      \iload\lval\lval\ofs
                   \commentary{heap load}
      \istore\lval\ofs\lval
                   \commentary{heap store}
      \iloceq\lval\lval\lval
                   \commentary{address comparison}
      \ialloca\var\instr
                   \commentary{\emph{stack allocation}}
      % \iallocaactive\sloc\instr
      %              \commentary{active stack cell}
      \ifork\lval\var\instr
                   \commentary{thread creation}
    \end{array}
  \end{array}\]
  The operands of every instruction are stack cells $\lval$.

  There is \emph{no heap deallocation} instruction.

\end{frame}

\begin{frame}[fragile]{Operational Semantics of SpaceLang}

  A small-step operational semantics, with a few unique features:
  \begin{itemize}
  \item \emph{Garbage collection} takes place before every reduction step.
  \item % Because we do not substitute values for variables,
  % the code never points to the heap;
  The GC \emph{roots} are the stack cells.
  \item Heap allocation \emph{fails} if the heap size exceeds a fixed limit $\maxsize$.
    % $\maxsize$ is a parameter of the operational semantics,
    % but the reasoning rules of \DIAMS are independent of~$\maxsize$.
  \end{itemize}

\end{frame}

% ----------------------------------------------------------------------------

\section{A Glimpse of the Reasoning Rules}

\begin{frame}{Heap Allocation}

  Heap allocation \emph{consumes space credits}.
  \begin{center}
  \includegraphics[width=.9\textwidth]{alloc}
  \end{center}
  Points-to and pointed-by assertions for the new location appear.

  One pointer to the value~$\val$ is \emph{deleted}. % (This aspect is optional.)

\end{frame}

\begin{frame}{Heap Store}

  Reasoning about a heap store involves some administration...

  \vspace{2mm}

  \begin{center}
  \includegraphics[width=.9\textwidth]{store}
  \end{center}
  One pointer to $\val$ is \emph{deleted}; one pointer to $\val'$ is \emph{created}.

\end{frame}

\begin{frame}{Logical Deallocation}

  Logical deallocation of a block is a \emph{ghost operation:}

  \vspace{2mm}

  \begin{center}
  \includegraphics[width=.75\textwidth]{free-singleton}
  \end{center}

\end{frame}

\begin{frame}{Soundness of \DIAMS}

  \begin{theorem}[Soundness]
    If $\triple{\SC\maxsize}\instr\iTrue$ holds, then,
    executing~$\instr$ in an empty store \\ cannot
    lead to a situation where a thread is stuck.
  \end{theorem}

  \emph{If}, under a precondition of $\maxsize$ space credits, the code can be verified,  \\
  \emph{then} its \emph{live} heap space cannot exceed $\maxsize$.

  This holds \emph{regardless of the value of $\maxsize$} (the heap size limit). \\
  Furthermore, the reasoning rules are \emph{independent} of $\maxsize$.

  \vspace{4mm}

  \begin{center}
    The rules allow \emph{compositional reasoning} about space.
  \end{center}

\end{frame}

% ----------------------------------------------------------------------------

\section{Specification of a Stack}

\begin{frame}{Ghost Reference Counting}

The user may define \emph{custom} (simplified) predecessor tracking disciplines.

For example, sometimes, \emph{counting} predecessors is enough.
%
% The simplified pointed-by assertion \emph{$\val \rcvpointedby \nv$}
% counts predecessors:
%
\[
  \val \rcvpointedby \nv
  \quad \eqdef \quad
  \exists \lsv. \; (
    \val \vfpointedby{1} \lsv \;\star\;
    \pure{\cardinal\lsv = \nv}
  )
\]
%
Edge addition and deletion
% (not shown)
increment and decrement~$n$.

\end{frame}

\begin{frame}{Creation}

Creating a stack \emph{consumes 4 space credits}.
\[
% Stack.create
\bightriplex
  {
    \callee\pointsto\bcell\stackcreate \star
    \stack\pointsto\bcell\vunit \star
    \hilite{\SC{4}}
  }
  {
    \icall\callee\stack
  }
  {\loc}
  {
    \callee\pointsto\bcell\stackcreate \star
    \stack\pointsto\bcell\loc \star
    \isStack\loc\lognil \ordinarystar
    \hilite{\loc \rcpointedby 1} \star
  }
\]
We get unique ownership of the stack and \emph{we have the sole pointer} to it.

\end{frame}

\begin{frame}{Pushing}

Pushing \emph{consumes 4 space credits}.
\[
% Stack.push
\bightriple
  {
    \callee\pointsto\bcell\stackpush \star
    \stack\pointsto\bcell\loc \star
    \elem\pointsto\bcell\val \star
    \hilite{\SC{4}} \ordinarystar % save a line and improve alignment with postcondition
    \isStack\loc\loglist \star
    \val \rcvpointedby n
  }
  {
    \icall\callee{\stack, \elem}
  }
  {
    \callee\pointsto\bcell\stackpush \star
    \stack\pointsto\bcell\loc \star
    \elem\pointsto\bcell\val \star
    \isStack\loc{(\logcons\val\loglist)} \star
    \hilite{\val \rcvpointedby {n+1}}
  }
\]
The value~$\val$ receives \emph{one more antecedent}.

\end{frame}

\begin{frame}{Popping}

Popping \emph{frees up 4 space credits}.
\[
% Stack.pop
\bightriple
  {
    \callee\pointsto\bcell\stackpop \star
    \stack\pointsto\bcell\loc \star
    \elem\pointsto\bcell\vunit \star
    \isStack\loc{(\logcons\val\loglist)} \star
    \val \rcvpointedby n
  }
  {
    \icall\callee{\stack, \elem}
  }
  {
    \callee\pointsto\bcell\stackpop \star
    \stack\pointsto\bcell\loc \star
    \elem\pointsto\bcell\val \star
    \hilite{\SC{4}} \ordinarystar % save a line and improve alignment
    \isStack\loc\loglist \star
    \hilite{\val \rcvpointedby n} % yes, n, not n-1
  }
\]
The number of predecessors of~$\val$ is unchanged, \\
\hspace{1cm} because the out-parameter $\elem$ receives a pointer to it.

\end{frame}

\begin{frame}{Disposal}

Logically deallocation of the stack, a \emph{ghost operation},
is part of the API.

It requires proving that the stack has \emph{zero predecessors}.
\[
% Stack.free (ghost)
\bigsupd
  {
    \isStack\loc\loglist \;\ordinarystar\;
    \hilite{\loc \rcpointedby 0} \star
    \bigast{(\val, n)\in\vns}{\val \rcvpointedby n}
  }
  {
    % \ddagsingleton\loc \star
    \hilite{\SCparen{4 + 4 \times \alen\loglist}} \star
    \bigast{(\val, n)\in\vns}{\val \rcvpointedby {n - (\valoccs\val\loglist)}}
  }
\]
It frees up \emph{a linear number of space credits}.

% The ghost reference counters of the stack elements are decremented.

\end{frame}

% ----------------------------------------------------------------------------

\section{Conclusion}

\begin{frame}{Summary of Contributions}

  A sound logic to reason about heap space usage in the presence of GC.

  Our main insights:
  \begin{itemize}
  \item Allocation consumes \emph{space credits} $\SC{n}$.
  \item \emph{Logical deallocation}, a ghost operation, produces space credits.
  \item Logical dellocation requires \emph{predecessor tracking}, \\
        \quad which we perform via \emph{pointed-by assertions} $\val \pointedby \lsv$.
  \end{itemize}

\end{frame}

\begin{frame}{Limitations and Future Work}

  Currently, predecessor tracking requires \emph{heavy bookkeeping}.

  We are investigating
  \begin{itemize}
  % \item better definitions of the reference-counting assertion $\val \rcpointedby n$;
  \item a more flexible \emph{deferred} logical edge deletion mechanism; % (delayed, therefore optional)
  \item coarse-grained predecessor tracking based on \emph{islands};
  \item \emph{simpler / more automated} tracking of \emph{roots}; % pointers from the stack to the heap;
  \item reasoning directly about call-by-value \emph{$\lambda$-calculus}.
  \end{itemize}

\end{frame}

% ----------------------------------------------------------------------------

\appendix

\input{backup}

\end{document}
