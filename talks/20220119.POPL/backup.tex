% ----------------------------------------------------------------------------

\section{Syntax, Semantics of \spacelang}

\begin{frame}{Values, Blocks, Stores}

  Memory locations:
  \(
    \loc, \sloc, \src, \dst \in \Loc
  \).

  Values include constants, memory locations, and \emph{closed procedures:}
  \[
    \val ::= \vunit \mid \vk \mid \loc \mid \vcode\vars\instr
  \]
  Memory blocks include \emph{heap tuples}, \emph{stack cells}, and deallocated blocks:
  \[
    \blk ::= \btuple\vals \mid \bcell\val \mid \bdeallocated
  \]
  A \emph{store} maps locations to blocks, encompassing the heap and stack(s).
  The \emph{size} of a block:
  \[
  \sz{\btuple\vals} = 1 + \alen\vals
  \qquad
  \sz{\bcell\val} =
  \sz\bdeallocated = 0
  \]
  The size of the store is the sum of the sizes of all blocks.

\end{frame}

\begin{frame}{Call-by-Reference and GC Roots}

  A \emph{reference} is a variable or a (stack) location and denotes a \emph{stack cell}.
  % (the address of a stack cell).
  \[
    \lval ::= \var \mid \sloc
  \]
  \spacelang uses \emph{call-by-reference.}

  A variable denotes a closed reference, \emph{not} a closed value as is usual.

  % A closed reference is a (stack) location.

  The operational semantics involves substitutions $\subst\var\sloc$.

  This preserves the property that
  \emph{the code never points to the heap}.

  % Think about assembly language,
  % where the code mentions registers,
  % and a register can contain a heap address,
  % but the code itself never contains a pointer to the heap.
  % Therefore, the code is not scanned by the GC.

  \qquad The \emph{roots} of the garbage collection process are \emph{the stack cells}.

\end{frame}

\begin{frame}{Instructions}

  \spacelang is imperative. An \emph{instruction}~$\instr$ does not return a value.
  \[\begin{array}[t]{@{}l@{\hspace{6mm}}l@{}}
    \begin{array}[t]{@{}ll@{}}
      \iskip
                   \commentary{no-op}
      \iseq\instr\instr
                   \commentary{sequencing}
      \iif\lval\instr\instr
                   \commentary{conditional}
      \icall\lval\lvals
                   \commentary{procedure call}
      \iconst\lval\val
                   \commentary{constant load}
      \imove\lval\lval
                   \commentary{move}
    \end{array}
    &
    \begin{array}[t]{@{}ll@{}}
      \ialloc\lval\allocsize
                   \commentary{heap allocation}
      \iload\lval\lval\ofs
                   \commentary{heap load}
      \istore\lval\ofs\lval
                   \commentary{heap store}
      \iloceq\lval\lval\lval
                   \commentary{address comparison}
      \ialloca\var\instr
                   \commentary{stack allocation}
      \iallocaactive\sloc\instr
                   \commentary{active stack cell}
      \ifork\lval\var\instr
                   \commentary{thread creation}
    \end{array}
  \end{array}\]
  The
  % in and out
  operands of
  every instruction are stack cells ($\lval$).

  There is no deallocation instruction for heap blocks.

\end{frame}

\begin{frame}[fragile]{Operational Semantics: Heap Allocation}

  We fix a \emph{maximum heap size}~$\maxsize$.

  Heap allocation \emph{fails} if the heap size exceeds $\maxsize$.
  \bgroup
  \small
  \begin{mathpar}
    \inferrule[StepAlloc]{
      % implied by the use of \mext below:
      % \loc\not\in\dom\store \\
      \store' = \mext\loc{\btuple{\replicate\allocsize\vunit}}\store \\\\
      \hilite{\valid{\store'}} \\
      \writelval\dst\loc{\store'}{\store''}
    }{
      \hs{\ialloc\dst\allocsize}\store
         \iskip{\store''}
    }
  \end{mathpar}
  \egroup
  $\maxsize$ is a parameter of the operational semantics,

  \qquad but the reasoning rules of \DIAMS are independent of~$\maxsize$.

\end{frame}

\begin{frame}[fragile]{Operational Semantics: Stack Allocation}

  The dynamic semantics of stack allocation is in \emph{three steps}:
  \bgroup
  \small
  \begin{mathpar}
    \inferrule[StepAllocaEntry]{
      % implied by the use of \mext below:
      % \sloc\not\in\dom\store \\
      \store' = \mext\sloc{\bcell\vunit}\store
    }{
      \hs{\ialloca\var\instr}\store
         {\iallocaactive\sloc{\subst\var\sloc\instr}}{\store'}
    }

    \inferrule[StepAllocaExit]{
      \store(\sloc) = \bcell\val \\
      \store' = \mupd\sloc\bdeallocated\store
    }{
      \hs{\iallocaactive\sloc\iskip}\store
         \iskip{\store'}
    }
  \end{mathpar}
  \egroup
  Evaluation contexts: \(
    \ectx ::=
      \hole \mid
      \iseq\ectx\instr \mid
      \ialloca\sloc\ectx
  \).

\end{frame}

\begin{frame}{Garbage Collection}

  To complete the definition of the operational semantics,
  \begin{itemize}
  \item allow \emph{garbage collection} before every reduction step. \\[2mm]
    $\gc\store{\store'}$ holds if
    \begin{itemize}
    \item the stores~$\store$ and~$\store'$ have the same domain;
    \item for every~$\loc$ in this domain, \\
          either $\store'(\loc)=\store(\loc)$,
          or $\loc$ is unreachable in $\store$
          and $\store'(\loc)=\bdeallocated$.
    \end{itemize}
  \item allow \emph{thread interleavings} (comes for free with Iris).
  \end{itemize}

\end{frame}

\begin{frame}{Complete Operational Semantics}

  \input{semantics}

\end{frame}

% ----------------------------------------------------------------------------

\section{Reasoning Rules of \DIAMS}

\begin{frame}{Heap Allocation}

  Heap allocation \emph{consumes space credits}.
  \begin{center}
  \includegraphics[width=.9\textwidth]{alloc}
  \end{center}
  Points-to and pointed-by assertions for the new location appear.

  One pointer to the value~$\val$ is \emph{deleted}. (This aspect is optional.)

\end{frame}

\begin{frame}{Heap Store}

  Writing a heap cell is simple... but involves some administration.

  \vspace{2mm}

  \begin{center}
  \includegraphics[width=.9\textwidth]{store}
  \end{center}
  One pointer to $\val$ is deleted; one pointer to $\val'$ is \emph{created}.

\end{frame}

\begin{frame}{Stack Allocation}

  A points-to assertion for the new stack cell exists throughout its lifetime.

  \vspace{2mm}

  \begin{center}
  \includegraphics[width=.5\textwidth]{alloca}
  \end{center}

  No pointed-by assertion is provided. (A design choice.)
  \begin{itemize}
  \item No pointers (from the heap or stack) to the stack.
  \end{itemize}

  % TODO a stack cell can be "killed" by writing () to it

\end{frame}

\begin{frame}{Logical Deallocation}

  Logical deallocation of a block is a \emph{ghost operation:}

  \vspace{2mm}

  \begin{center}
  \includegraphics[width=.75\textwidth]{free-singleton}
  \end{center}

\end{frame}

\begin{frame}{Deferred Predecessor Deletion}

  Deletion of deallocated predecessors can be \emph{deferred:}
  \begin{center}
  \includegraphics[width=.75\textwidth]{cleanup}
  \end{center}
  A key rule: if $L'$ is empty, then $v$ becomes eligible for deallocation.

\end{frame}

\begin{frame}{Bulk Logical Deallocation}

  A group that is \emph{closed under predecessors} can be deallocated at once:
  \begin{center}
  \includegraphics[width=.9\textwidth]{free}
  \end{center}
  The rules for constructing a ``cloud'' (omitted) are straightforward.
  % TODO add CloudEmpty and CloudCons to the backup slides?

\end{frame}

\begin{frame}{More Axioms}

  Points-to and pointed-by assertions can be \emph{split} and \emph{joined}.

  \vspace{2mm}

  \begin{center}
  \includegraphics[width=.85\textwidth]{axioms-1}
  \end{center}
  Pointed-by assertions are \emph{covariant}.

  Points-to and pointed-by assertions can be \emph{confronted}.

\end{frame}

\begin{frame}{More Axioms}

  Space credits can be \emph{split} and \emph{joined}.

  \vspace{2mm}
  \begin{center}
  \includegraphics[width=.5\textwidth]{axioms-2}
  \end{center}

\end{frame}

% ----------------------------------------------------------------------------

\section{Specification of List Copy}

\begin{frame}{Lists Without Sharing}

  Each cell owns the next cell and possesses \emph{the sole pointer} to it.

  \[\begin{array}{rcl}
  \isList\loc\lognil
  & \eqdef &
    \phantom{\exists\loc'.\;}
    \loc \pointsto \btuple{\loglistbrackets{0}}
  \\
  \isList\loc{(\logcons\logval\loglist\,)}
  & \eqdef &
    \exists\loc'.\;
    \loc \pointsto \btuple{\loglistbrackets{1; \logval; \loc'}} \star
    \hilite{\loc' \rcpointedby 1} \star
    \isList{\loc'}\loglist
  \end{array}\]

  Let's now have a look at \emph{list copy} and its spec. (Fasten seatbelts!)

\end{frame}

\begin{frame}{List Copy in \spacelang}

\small
\input{fig-list-copy}

\end{frame}

\begin{comment}
% Source of original slide, before annotation:
\begin{frame}{Specification of List Copy}

\small
\input{list-copy-spec}

\end{frame}
\end{comment}

\begin{frame}{Specification of List Copy}

  The case $m=1$, where we have \emph{the sole pointer} to the list, is special.

  \vspace{4mm}
  \begin{center}
  \includegraphics[width=.85\textwidth]{list-copy-spec}
  \end{center}

\end{frame}
