\documentclass[
  aspectratio=169,
  usenames,
  dvipsnames,
  hyperref={
    colorlinks=true,
    linkcolor=black,
    citecolor=magenta,
    urlcolor=magenta,
  }
]{beamer}
\usetheme{Pittsburgh}
\usecolortheme{seahorse}
\usepackage[T1]{fontenc}
\usepackage[utf8]{inputenc}
\usepackage{fontawesome}
\usepackage{lmodern} % this makes \textsc work
\usepackage{stmaryrd} % for \mapsfrom
\usepackage{xspace}
\input{setup}
\usepackage{iris}
\input{macros}

% Show frame numbers.
\setbeamertemplate{footline}[frame number]

% Do not add vertical space around \begin{center} ... \end{center}.
\renewenvironment{center}
 {\parskip=0pt\par\nopagebreak\centering}
 {\par\noindent\ignorespacesafterend}

% ----------------------------------------------------------------------------
% Page de titre.

\title{A Separation Logic for Heap Space under Garbage Collection}
\author{Jean-Marie Madiot \and François Pottier}
\date{January 2022}

\begin{document}

\begin{frame}
\begin{center}
\includegraphics[height=.9\textheight]{SLtitle}
\end{center}
\end{frame}

% A logo on every slide (except the first slide, already shown).
\logo{\includegraphics[width=.1\textwidth]{SL}}

% ----------------------------------------------------------------------------
% Rationale.

% This talk is about establishing verified bounds
% on a program's heap space usage.

% We wish to work in the setting of a program logic,
% and to view heap space as a resource.

% This approach has been applied to control time complexity.
% Can it be applied to heap space?

\begin{frame}{Reasoning about Heap Space, without GC}

  Following Hofmann (1999, 2000),
  let $\SC{1}$
  represent one \emph{space credit}.

  Allocation \emph{consumes} credits; deallocation \emph{produces} credits.
  \[\begin{array}{rcl}
  \atriple{\SC{\sz\blk}}{x := \kw{alloc}(\blk)}{x\pointsto\blk} \\[2mm]
  \atriple{x\pointsto\blk}{\kw{free}(x)}{\SC{\sz\blk}}
  \end{array}\]
  A function's space requirement is visible in its specification.

  Without GC, this approach is sound.
  % and can be made to work in practice

\end{frame}

% In the presence of GC,
% deallocation becomes implicit,
% so we lose the ability to recover space credits while reasoning.

\begin{frame}{Reasoning in the Presence of GC}

  \begin{overlayarea}{\textwidth}{0cm}
  \only<1>{It is tempting to switch to a \emph{logical deallocation} operation:}
  \only<2>{This ghost operation has a few good properties: \emph{no double-free, no use-after-free}...}
  \end{overlayarea}
  \[
  x\pointsto\blk
  \;\fupd\;
  \SC{\sz\blk}
  \]
  \begin{overlayarea}{\textwidth}{\baselineskip}
  \only<1>{\emph{Manual} verification-time reasoning, \emph{automatic} runtime management.}
  \only<2>{Unfortunately, logical deallocation in this form is \emph{not sound}.}
  \end{overlayarea}

\end{frame}

\begin{frame}{Logical versus physical heaps}

  We have created a distinction between
  the \emph{logical heap} % that the programmer keeps in mind, % where deallocation is manual
  and the \emph{physical heap}. % that exists at runtime. % managed by the GC
  \vspace{2mm}
  \begin{center}
  \includegraphics[width=.4\textwidth]{distinction}
  \end{center}
  For soundness, these heaps must \emph{agree on their reachable fragments}.

  To enforce this, we must \emph{restrict logical deallocation:}
  \\ \hspace{3cm}
  \emph{only unreachable blocks} can be logically deallocated.

\end{frame}

% We want to disallow deallocating a reachable block,
% but Separation Logic lets us reason about ownership.
% Ownership and reachability are unrelated.

% We keep track of the predecessors of every block.
% A block with no predecessor is unreachable,
% therefore can be logically deallocated.

\begin{frame}{Points-To and Pointed-By Assertions}

  In addition to \emph{points-to}, we use \emph{pointed-by} assertions (Kassios \& Kritikos, 2013):

  \vspace{2mm}

  \begin{center}
  \includegraphics[height=0.6\textheight]{permissions}
  \end{center}

\end{frame}

\begin{frame}{Sound Logical Deallocation}

  We get a sound logical deallocation axiom: % for a single block:
  \[\begin{array}{rcl}
  x\pointsto\blk
  \;\star\;
  x\pointedby\emptyset
  \;\fupd\;
  \SC{\sz\blk}
  \end{array}\]

  In the talk and in the paper,
  \begin{itemize}
  \item dealing with GC roots;
  \item reasoning rules \& soundness statement;
  \item ghost reference counting;
  \item examples.
  \end{itemize}

\end{frame}

\end{document}
