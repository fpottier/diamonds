1
00:00:02,030 --> 00:00:05,870
This is "A Separation Logic for Heap Space
under Garbage Collection".

2
00:00:05,870 --> 00:00:11,490
I am François Pottier,
and this is joint work with Jean-Marie Madiot.

3
00:00:11,490 --> 00:00:16,500
Our goal is to establish verified bounds
on a program's heap space usage,

4
00:00:16,500 --> 00:00:20,250
in the presence of
a tracing garbage collector.

5
00:00:20,250 --> 00:00:23,410
We want to use a program logic,
and it sems natural

6
00:00:23,410 --> 00:00:28,070
to view heap space as a resource:
we introduce a new assertion,

7
00:00:28,070 --> 00:00:33,340
"one diamond" or one space credit,
which can be understood both

8
00:00:33,340 --> 00:00:38,999
as a guarantee that there exists one word
of free space

9
00:00:38,999 --> 00:00:43,410
and as a permission to allocate one word.

10
00:00:43,410 --> 00:00:47,339
Without GC, this approach works.

11
00:00:47,339 --> 00:00:51,300
In the presence of GC,
deallocation becomes implicit,

12
00:00:51,300 --> 00:00:55,980
so it is no longer clear where and how
we might recover space credits.

13
00:00:55,980 --> 00:01:02,629
It is tempting to view deallocation as
a logical operation, a ghost operation,

14
00:01:02,629 --> 00:01:08,369
which one would explicitly invoke
while verifying the program.

15
00:01:08,369 --> 00:01:14,560
One could present deallocation, as shown on the slide,
as a ghost state update,

16
00:01:14,560 --> 00:01:18,460
which means that,
by abandoning a points-to assertion,

17
00:01:18,460 --> 00:01:21,980
one gains a number of space credits.

18
00:01:21,980 --> 00:01:26,810
This formulation has some good properties:
no double-free, no use-after-free.

19
00:01:26,810 --> 00:01:33,650
Unfortunately, it is not sound.

20
00:01:33,650 --> 00:01:38,320
By making deallocation a ghost operation,
we create a distinction between

21
00:01:38,320 --> 00:01:41,390
the logical heap that the programmer has in
mind,

22
00:01:41,390 --> 00:01:47,060
where deallocation is explicit,
and the physical heap that exists at runtime,

23
00:01:47,060 --> 00:01:50,950
where deallocation is automatic.

24
00:01:50,950 --> 00:01:56,090
For the logic to be sound,
these heaps must have the same reachable fragments,

25
00:01:56,090 --> 00:02:01,200
a property that is violated in this diagram.

26
00:02:01,200 --> 00:02:05,200
To enforce this property,
we must restrict logical deallocation

27
00:02:05,200 --> 00:02:12,560
by imposing the condition
that only unreachable blocks can be deallocated.

28
00:02:12,560 --> 00:02:17,230
This may seem problematic at first,
because traditional Separation Logic

29
00:02:17,230 --> 00:02:21,379
is designed to reason about ownership,
not about reachability,

30
00:02:21,379 --> 00:02:25,290
which is an unrelated concept.

31
00:02:25,290 --> 00:02:29,980
Our solution is to keep track
of the predecessors of every block

32
00:02:29,980 --> 00:02:32,580
using pointed-by assertions.

33
00:02:32,580 --> 00:02:40,110
A block with no predecessor is unreachable,
therefore can be logically deallocated.

34
00:02:40,110 --> 00:02:44,150
In summary,
points-to assertions

35
00:02:44,150 --> 00:02:47,769
allow reasoning about ownership
and can be viewed as permissions

36
00:02:47,769 --> 00:02:52,250
to read or write a block;
pointed-by assertions

37
00:02:52,250 --> 00:02:56,370
allow reasoning about reachability
and can be viewed as permissions

38
00:02:56,370 --> 00:03:03,159
to create or destroy pointers to a block;
the presence of both assertions is required

39
00:03:03,159 --> 00:03:07,610
by the logical deallocation operation.

40
00:03:07,610 --> 00:03:10,370
There is a lot more in the longer talk and
in the paper,

41
00:03:10,370 --> 00:03:15,569
including key technical issues such as
how to identify the GC roots and

42
00:03:15,569 --> 00:03:19,349
how to state the soundness of the program
logic.

43
00:03:19,349 --> 00:03:25,230
We also present examples of verified functions
and specifications.

44
00:03:25,230 --> 00:03:30,969
Thanks for watching, and if you are interested,
do come and talk to us (online) at POPL!
