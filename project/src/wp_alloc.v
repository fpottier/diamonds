From spacelang Require Import wp_state_interp wp_diamonds wp_memory.

(* This module establishes a reasoning rule for memory allocation. *)

(* ------------------------------------------------------------------------ *)

(* Allocating a memory block in the heap. *)

(* This requires [size_block b] diamonds, where [b] is the new block. *)

(* This also requires the ownership of the stack cell [dst] where the
   location of the new block is written. *)

(* This instruction produces [mapsto] and [mapsfrom] assertions for the
   new heap address [l] and an updated description of the stack cell at
   address [dst]. *)

(* Finally, because this instruction overwrites the value [w] that was
   previously stored in the stack cell at [dst], we require a [mapsfrom]
   assertion for this value, and update it. If [w] is not a memory location,
   then [w ↤?{qw} _] is [True], so this aspect can be ignored. *)

Lemma wp_alloc_dep `{!heapG Σ} (precise:bool) s E dst n w qw lsw :
  let b := BTuple (replicate n VUnit) in
  {{{
    ♢(size_block b) ∗
    ▷ dst ↦ BCell w ∗
    ▷ (if precise then w ↤?{qw} lsw else ⌜True⌝)
  }}}
    IAlloc (LLoc dst) n
    @ s; E
  {{{ l, RET tt;
    l ↦ b ∗ l ↤ {[+dst+]} ∗
    dst ↦ BCell (VLoc l) ∗
    (if precise then w↤?{qw}(lsw ∖ {[+dst+]}) else ⌜True⌝)
  }}}.
Proof.
  intros b. iIntros (Φ) "(Hfrag & >Hdst & Hwmapsfrom) HΦ".
  iApply wp_lift_atomic_head_step_no_fork; first done.
  iIntros (σ1 nsteps κ κs nthreads) "Hstate".
  iModIntro. iSplit.

  (* First, prove that [IAlloc] is reducible. The ownership of [size_block b]
     diamonds guarantees that there is enough space in the conceptual heap,
     and the relation between the physical and conceptual heaps guarantees
     that enough space can be made available in the physical heap as well. *)
  { destruct_state_interp.
    exploit_stack_cell dst.
    iDestruct (own_auth_nat_le with "Hauth Hfrag") as %Hle.
    iPureIntro.
    apply related_available in Hrelated.
    destruct Hrelated as (σ'1 & Hgc & ?).
    assert (size_block b ≤ available σ'1)%nat by lia.
    head_reducible.
    eauto using alloc_is_reducible, gc_preserves_stack_cells
      with valid_state. }

  (* Then, examine an arbitrary reduction step, where the fresh address [l]
     is not chosen by us. *)
  examine_step.
  exploit_stack_cell dst.
  match goal with h: write_lvalue _ _ _ _ |- _ => fold b in h end.
  match goal with h: valid_state _ |- _ => fold b in h end.
  (* The memory location [l] is fresh for [σ'1], therefore also for [θ]. *)
  assert (store θ !! l = None) by eauto using related_preserves_freshness.
  assert (l ≠ dst) by (intro; congruence).
  (* The new block has no outgoing pointers. *)
  assert (Hpb: pointers b = ∅) by eauto using block_no_pointers.
  (* Extend the conceptual heap with a mapping of [l] to [b]. *)
  iMod (ph_alloc θ.(store) l b [] with "[Hph]")
    as "(Hph & Hpred & Hmapsto & Hmapsfrom)".
  { assumption. }
  { eauto. }
  { rewrite (block_no_pointers n VUnit) //. }
  { rewrite interpret_nil. iFrame. }
  iClear "Hpred".
  (* Update the conceptual store by writing [VLoc l] into [dst]. *)
  iMod (ph_store_stack_dep precise (<[l:=b]> (store θ)) dst w (VLoc l) qw lsw (1%Qp) ∅
    with "[$]")
      as "(Hph & Hdst & Hmapsfrom & Hwmapsfrom)".
  { eauto. }
  rewrite left_id. (* in Hwmapsfrom *)
  (* This is our new conceptual state. *)
  set (θ' := <[dst:=BCell (VLoc l)]> <$> (<[l:=b]> <$> θ)).
  assert (closed (store θ')) by (rewrite /θ'; eauto 3 with closed).
  (* The new physical state and conceptual state are related. *)
  assert (related σ2 θ') by eauto using related_alloc.
  (* The diamonds brought to us by the user guarantee that there remains
     enough space in the conceptual heap to allocate a new block. *)
  iDestruct (own_auth_nat_le with "Hauth Hfrag") as %Hle.
  assert (valid_state (<[l:=b]> <$> θ)) by eauto with valid_state.
  assert (valid_state θ') by (rewrite /θ'; eauto with valid_state).
  (* Perform a ghost update to consume the diamonds brought to us by the
     user and at the same time decrease the authoritative assertion that
     keeps track of the number of diamonds in circulation. *)
  iMod (auth_nat_update_decr _ _ _ (size_block b) with "Hauth Hfrag")
    as "[Hauth Hfrag]". lia.

  iClear "Hfrag".
  (* Conclude. *)
  iModIntro.
  iSplit; [ eauto |].
  iSplitR "HΦ Hmapsto Hmapsfrom Hdst Hwmapsfrom"; [| iApply "HΦ"; iFrame ].
  (* Close the state interpretation invariant. *)
  intro_state_interp. iExists θ'.
  iFrame "Hph".
  rewrite {2}/θ'.
  erewrite available_update by eauto.
  rewrite available_alloc //.
  iFrame "Hauth".
  iPureIntro. splits; eauto.
Qed.

Lemma wp_alloc `{!heapG Σ} s E dst n w qw lsw :
  let b := BTuple (replicate n VUnit) in
  {{{
    ♢(size_block b) ∗
    ▷ dst ↦ BCell w ∗
    ▷ w ↤?{qw} lsw
  }}}
    IAlloc (LLoc dst) n
    @ s; E
  {{{ l, RET tt;
    l ↦ b ∗ l ↤ {[+dst+]} ∗
    dst ↦ BCell (VLoc l) ∗
    w↤?{qw}(lsw ∖ {[+dst+]})
  }}}.
Proof.
  apply (wp_alloc_dep true).
Qed.

Lemma wp_alloc_weak `{!heapG Σ} s E dst n w :
  let b := BTuple (replicate n VUnit) in
  {{{
    ♢(size_block b) ∗
    ▷ dst ↦ BCell w
  }}}
    IAlloc (LLoc dst) n
    @ s; E
  {{{ l, RET tt;
    l ↦ b ∗ l ↤ {[+dst+]} ∗
    dst ↦ BCell (VLoc l)
  }}}.
Proof.
  intros ?. derive_wp_from (wp_alloc_dep false).
Qed.

(* ------------------------------------------------------------------------ *)

(* This variant of the previous lemma is applicable when we have [n]
   space credits and we are allocating a block whose size is at most
   [n]. *)

Lemma wp_alloc_split `{!heapG Σ} s E dst n w qw lsw k :
  let b := BTuple (replicate n VUnit) in
  (size_block b <= k)%nat →
  {{{
    ♢k ∗
    ▷ dst ↦ BCell w ∗
    ▷ w ↤?{qw} lsw
  }}}
    IAlloc (LLoc dst) n
    @ s; E
  {{{ l, RET tt;
    l ↦ b ∗ l ↤ {[+dst+]} ∗
    dst ↦ BCell (VLoc l) ∗
    w↤?{qw}(lsw ∖ {[+dst+]}) ∗
    ♢(k - size_block b)
  }}}.
Proof.
  intros b ?. iIntros (Φ) "(Hdiamonds & Hdst & Hw) Hpost".
  rewrite (diamond_split (size_block b) (k - size_block b) k); last by lia.
  iDestruct "Hdiamonds" as "[Hpayment Hremainder]".
  iApply (wp_alloc s E dst n w qw lsw with "[$]").
  iNext. iIntros (l) "(? & ? & ? & ?)".
  iApply "Hpost". fold b. iFrame.
Qed.
