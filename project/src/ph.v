From iris.algebra Require Import auth gmap gmultiset.
From iris.base_logic.lib Require Import gen_heap.
From iris.proofmode Require Import proofmode.
From spacelang Require Import hypotheses successors predecessors.
From spacelang Require Import stdpp.
From spacelang Require iris.

(* This module sets up a version of Iris where a ghost mapping [π] of each
   memory location to its predecessors is maintained. In the user's eyes,
   this is done via "mapsfrom" assertion [l' ↤{q} ls]. The file's name,
   [ph], stands for "predecessor heap". *)

(* Suppose [L] is the type of memory locations, and [B] is the type of blocks
   stored in the heap, so the heap is a finite map of [L] to [B]. *)

(* We build on top of Iris's gen_heap library:
      https://plv.mpi-sws.org/coqdoc/iris/iris.base_logic.lib.gen_heap.html
 *)

(* ------------------------------------------------------------------------ *)

(* We want every memory location [l] to carry a ghost field [predecessors]
   that contains a multiset of locations: the predecessors of the location
   [l] in the heap. *)

(* Furthermore, we want to be able to split the ownership of this field,
   so that partial ownership of the [predecessors] field yields partial
   knowledge of the predecessors (and still allows updates), while full
   ownership of the [predecessors] field yields full knowledge of the
   predecessors. *)

(* These considerations lead us to using the following CMRA: *)

Notation predR L :=
  (authUR (gmapUR L (prodR fracR (gmultisetR L)))).

(* If we wanted unique ownership of [predecessors] fields, as opposed to
   fractional / splittable ownership, we would use [exclR (gmultisetO L)]
   instead of [prodR fractR (gmultisetR L)]. *)

(* ------------------------------------------------------------------------ *)

(* This is our view of the heap. *)

Class phG L B Σ `{Countable L} := PhG {
  (* The heap, a mapping of locations to blocks. *)
  phG_heap :> gen_heapGS L B Σ;
  (* The predecessor resource, described above. *)
  phG_pred :> inG Σ (predR L);
  (* A ghost memory location that holds the predecessor mapping. *)
  phG_pred_name : gname;
}.

(* Make only the last argument explicit. *)
Arguments phG_pred      {L B Σ _ _} _ : assert.
Arguments phG_pred_name {L B Σ _ _} _ : assert.

(* ------------------------------------------------------------------------ *)

(* Assumptions and definitions. *)

Section ReasoningRules.

Context `{Countable L} {B : Type}.

Context `{Hypotheses L B}.

(* A heap [σ] is a finite map of locations to blocks. *)

Implicit Types σ : gmap L B.

(* A predecessor map [π] is a finite map of locations to multisets
   of locations. *)

Implicit Types π : gmap L (gmultiset L).

(* We write [ls] and [ps] for multisets of locations. *)

Implicit Types ls ps : gmultiset L.

Notation dom := (dom (gset L)).

(* ------------------------------------------------------------------------ *)

(* Our state interpretation predicate includes: 1- the standard state
   interpretation predicate [gen_heap_interp σ]; 2- the assertion [auth π],
   which represents the authoritative ownership of the ghost cell γ and
   holds the predecessor map π; 3- an invariant that ties [σ] and [π]. *)

Context `{ fG : !phG L B Σ }.

Notation own    := (@own Σ _ (phG_pred fG)).
Notation γ      := (phG_pred_name fG).
  (* [own γ] has type [predR L → iProp Σ]. *)
Notation pred π := (own γ (● (fmap (λ ls, (1%Qp, ls)) π))).
  (* [pred π] has type [iProp Σ]. *)

Definition ph_interp σ : iProp Σ :=
  gen_heap_interp σ ∗
  ∃ π, pred π ∗ ⌜ invariant σ π ⌝.

(* ------------------------------------------------------------------------ *)

(* Notation for various forms of the pointsto assertion. *)

(* Fractional ownership of a memory block, where [q] is a fraction. *)

Notation "l ↦{ q } b" := (mapsto l (DfracOwn q) b)
  (at level 20, q at level 50, format "l  ↦{ q }  b") : bi_scope.

(* Full ownership of a memory block. *)

Notation "l ↦ b" := (l ↦{ 1%Qp } b)%I
  (at level 20) : bi_scope.

(* Persistent knowledge that a location has been deallocated. *)

Notation "† l" := (mapsto l DfracDiscarded deallocated)%I
  (at level 20) : bi_scope.

(* Persistent knowledge that a group of locations have been deallocated. *)

(* We use a set, as opposed to a multiset, because it seems inconvenient
   to have to deal with repeated elements here. *)

Notation "†† ls" := ([∗ set] l ∈ ls, †l)%I
  (at level 20) : bi_scope.

(* ------------------------------------------------------------------------ *)

(* Consequences of the [mapsto] assertion. *)

(* As usual, [l ↦{q} b] guarantees that the heap [σ] maps [l] to [b]. *)

Lemma exploit_mapsto σ l q b :
  ph_interp σ -∗ l ↦{q} b -∗ ⌜σ !! l = Some b⌝.
Proof.
  iIntros "[Hh _] Hmapsto".
  iDestruct (gen_heap_valid with "Hh Hmapsto") as %?.
  eauto.
Qed.

(* [†l] guarantees that [l] has been freed. *)

Lemma exploit_discarded σ l :
  ph_interp σ -∗ †l -∗ ⌜ freed σ l ⌝.
Proof.
  iIntros "[Hh _] Hmapsto".
  iDestruct (gen_heap_valid with "Hh Hmapsto") as %?.
  unfold freed. eauto.
Qed.

(* [††ls] guarantees that every location in [ls] has been freed. *)

Lemma exploit_discarded_group σ locs :
  ph_interp σ -∗ ††locs -∗ ⌜ ∀l, l ∈ locs → freed σ l ⌝.
Proof.
  (* By induction on the set [locs]. *)
  pattern locs. eapply set_ind_L; clear locs.
  (* Case: ∅. *)
  { iIntros. iPureIntro. intros l. rewrite elem_of_empty. tauto. }
  (* Case: {[l]} ∪ locs. *)
  { intros l locs ? IH.
    rewrite -> big_sepS_union by set_solver.
    rewrite big_sepS_singleton.
    iIntros "Hh [Hl Hlocs]".
    iDestruct (exploit_discarded σ l with "Hh Hl") as %?.
    iDestruct (IH with "Hh Hlocs") as %?.
    iPureIntro.
    intros m. set_solver. }
Qed.

(* ------------------------------------------------------------------------ *)

(* The predicate [mapsfrom_exact l q sh] represents partial ownership of the
   ghost field [l.predecessors], and states that the multiset [sh] is one
   share of the multiset [l.predecessors]. If the fraction [q] is 1, then
   [sh] is exactly the full multiset. *)

Definition mapsfrom_exact l q sh :=
  own γ (◯ {[l := (q, sh)]}).

(* The predicate [l ↤{q} ls] represents partial ownership of the ghost field
   [l.predecessors], and states that the multiset [ls] contains one share of
   the full set [l.predecessors]. If the fraction [q] is 1, then [ls]
   contains the full set. *)

(* The reason why we write "contains" above, as opposed to "is", is that we
   allow [ls] to be a superset of a share. That is, we allow the user to work
   with over-approximations of the predecessor sets. This makes the system
   more flexible and does not impose a loss of precision; the user is never
   forced to exploit this feature and lose precision. *)

(* I thought at one point that this feature was necessary in order to allow
   unregistering an edge without giving rise to an obligation to prove that
   this edge exists. However, I think that was a misconception. The predicate
   [mapsfrom_exact] would also support optional unregistration; the ordering
   [π ≾ π'] is the key mechanism that allows this. *)

Definition mapsfrom l q ls : iProp Σ :=
  ∃ sh, mapsfrom_exact l q sh ∗ ⌜ sh ⊆ ls ⌝ .

Notation "l ↤{ q } ls" :=
  (mapsfrom l q ls)
  (at level 20, format "l ↤{ q } ls")
: bi_scope.

Notation "l ↤ ls" :=
  (mapsfrom l 1%Qp ls)
  (at level 20, format "l ↤ ls")
: bi_scope.

(* An abstraction of the previous assertion: in [mapsfrom_set l q ls],
   [ls] is a set, as opposed to a multiset. Thus, the multiplicity of
   each predecessor is unknown. *)

(* TODO propose a notation *)

Definition mapsfrom_set l q locs : iProp Σ :=
  ∃ ps, l ↤{q} ps ∗ ⌜ dom ps ⊆ locs ⌝.

(* ------------------------------------------------------------------------ *)

(* Combining two [mapsfrom] assertions. *)

Lemma mapsfrom_join l' q1 q2 ls1 ls2 :
  l' ↤{q1} ls1 -∗ l' ↤{q2} ls2 -∗
  l' ↤{(q1+q2)%Qp} (ls1 ⊎ ls2).
Proof.
  iIntros "H1 H2".
  (* Drop down to the level of [mapsfrom_exact]. *)
  iDestruct "H1" as (sh1) "(H1 & %Hsub1)".
  iDestruct "H2" as (sh2) "(H2 & %Hsub2)".
  iExists (sh1 ⊎ sh2).
  (* Drop down to the level of the authoritative monoid. *)
  rewrite /mapsfrom_exact.
  iCombine "H1 H2" as "H".
  (* Conclude. *)
  iFrame. iPureIntro. multiset_solver.
Qed.

(* Splitting a [mapsfrom] assertion. *)

Lemma mapsfrom_split l' q ls q1 q2 ls1 ls2 :
  (q = q1 + q2)%Qp →
  ls ⊆ ls1 ⊎ ls2 →
  l' ↤{q} ls -∗
  l' ↤{q1} ls1 ∗ l' ↤{q2} ls2.
Proof.
  intros. subst. iIntros "H".
  (* Drop down to the level of [mapsfrom_exact]. *)
  iDestruct "H" as (sh) "(H & %Hsub)".
  (* Drop down to the level of the authoritative monoid. *)
  rewrite /mapsfrom_exact.
  (* Split our share. *)
  assert (Hsplit: sh = (sh ∩ ls1) ⊎ (sh ∖ ls1)) by multiset_solver.
  rewrite Hsplit.
  replace (◯ {[l' := ((q1 + q2)%Qp, sh ∩ ls1 ⊎ sh ∖ ls1)]})
     with (◯ {[l' := (q1, sh ∩ ls1)]} ⋅ ◯ {[l' := (q2, sh ∖ ls1)]})
       by rewrite -auth_frag_op singleton_op //.
  iDestruct "H" as "(H1 & H2)".
  (* Conclude. *)
  iSplitL "H1".
  - iExists _. iFrame. iPureIntro. multiset_solver.
  - iExists _. iFrame. iPureIntro. multiset_solver.
Qed.

(* A [mapsfrom] assertion with a larger set is weaker *)

Lemma mapsfrom_weaken l q ls1 ls2 :
  ls1 ⊆ ls2 →
  l ↤{q} ls1 -∗
  l ↤{q} ls2.
Proof.
  iIntros (S) "H".
  (* Drop down to the level of [mapsfrom_exact]. *)
  iDestruct "H" as (sh) "(H & %Hsub)".
  iExists sh; iFrame. iPureIntro.
  now transitivity ls1.
Qed.

(* The combination of the previous two lemmas. *)

Lemma mapsfrom_join_equiv l' q q1 q2 ls ls1 ls2 :
  (q1+q2)%Qp = q →
  ls1 ⊎ ls2 = ls →
  l' ↤{q1} ls1 ∗ l' ↤{q2} ls2 ⊣⊢ l' ↤{q} ls.
Proof.
  intros. subst.
  eapply bi.equiv_entails_2.
  + iIntros "[H1 H2]".
    iDestruct (mapsfrom_join with "H1 H2") as "H". iFrame.
  + iIntros "H".
    iDestruct (mapsfrom_split _ _ _ q1 q2 ls1 ls2 with "H") as "[H1 H2]";
      eauto 2.
    iFrame.
Qed.

(* The special case where all multisets are empty. *)

Lemma mapsfrom_split_empty l' q1 q2 :
  l' ↤{q1} ∅ ∗ l' ↤{q2} ∅ ⊣⊢ l' ↤{(q1 + q2)%Qp} ∅.
Proof.
  rewrite mapsfrom_join_equiv. eauto. eauto. multiset_solver.
Qed.

(* Joining and splitting again. *)

Lemma mapsfrom_join_split l' q1 q2 ls1 ls2 ms1 ms2 :
  ls1 ⊎ ls2 ⊆ ms1 ⊎ ms2 →
  l' ↤{q1} ls1 -∗ l' ↤{q2} ls2 -∗
  l' ↤{q1} ms1  ∗ l' ↤{q2} ms2.
Proof.
  intros. iIntros "H1 H2".
  iDestruct (mapsfrom_join with "H1 H2") as "H".
  iDestruct (mapsfrom_split with "H") as "(H1 & H2)".
  { eauto. }
  { eauto. }
  iFrame.
Qed.

(* When the multiset is empty, [mapsfrom_exact] and [mapsfrom] coincide. *)

Lemma mapsfrom_empty l' q :
  l' ↤{q} ∅ ⊣⊢ mapsfrom_exact l' q ∅.
Proof.
  rewrite /mapsfrom.
  iSplit; [| eauto ].
  iIntros "H".
  iDestruct "H" as (sh) "(? & %Hsh)".
  assert (sh = ∅) by multiset_solver. subst sh.
  iFrame.
Qed.

(* ------------------------------------------------------------------------ *)

(* Basic facts about †l and ††ls. *)

Goal ∀ l, Persistent (†l).
Proof. typeclasses eauto. Qed.

Goal ∀ locs, Persistent (††locs).
Proof. typeclasses eauto. Qed.

Lemma ddag_singleton l :
  †l ⊣⊢ ††{[l]}.
Proof.
  rewrite big_sepS_singleton. eauto.
Qed.

Lemma ddag_join locs1 locs2 locs :
  locs1 ∪ locs2 = locs →
  ††locs1 ∗ ††locs2 ⊣⊢ ††locs.
Proof.
  intros. subst locs.
  rewrite !big_sepS_forall.
  eapply bi.equiv_entails_2.
  + iIntros "[H1 H2]" (l).
    rewrite elem_of_union.
    iIntros "%Hl". destruct Hl.
    - by iApply "H1".
    - by iApply "H2".
  + iIntros "#H".
    iSplit; iIntros (l) "%Hl"; iApply "H"; iPureIntro; set_solver.
Qed.

Lemma ddag_repeatm l n : †l -∗ ††dom (repeatm l n).
Proof.
  induction n.
  - rewrite gmultiset_dom_empty. auto.
  - rewrite dom_union.
    rewrite <-ddag_join. 2:reflexivity.
    iIntros "dl".
    iDestruct (bi.persistent_sep_dup with "dl") as "(dl & dl_)".
    iSplitL "dl".
    rewrite dom_singleton.
    + now iApply ddag_singleton.
    + now iApply IHn.
Qed.

(* ------------------------------------------------------------------------ *)

(* Exploiting an exact [mapsfrom] assertion. *)

(* When confronted with the authoritative assertion [pred π], the assertion
   [mapsfrom_exact l' q sh] guarantees that [l'] is in the domain of [π] and
   that [sh] is a subset of [predecessors π l']. Furthermore, if [q] is 1,
   then it guarantees that [sh] is in fact [predecessors π l']. *)

Lemma exploit_mapsfrom_exact π l' q sh :
  pred π -∗
  mapsfrom_exact l' q sh -∗
  ⌜ exists ps, π !! l' = Some ps ∧ sh ⊆ ps ∧ (q = 1%Qp → ps ≡ sh) ⌝.
Proof.
  iIntros "Hauth Hfrag". unfold mapsfrom_exact.
  iDestruct (own_valid_2 with "Hauth Hfrag") as "%Hv".
  iPureIntro.
  (* Confront the authoritative element and the fragmentary element. *)
  apply auth_both_valid_discrete in Hv. destruct Hv as [Hv _].
  (* Perform lots of destruction. *)
  apply singleton_included_l in Hv.
  destruct Hv as [[q' ?] [Hlookup Hord]].
  rewrite lookup_fmap in Hlookup.
  apply fmap_Some_equiv_1 in Hlookup.
  destruct Hlookup as (ps' & Hlookup & Heq).
  simplify_eq.
  exists ps'. split; [| split ].
  - exact Hlookup.
  (* Prove [sh ⊆ ps]. *)
  - apply Some_included in Hord.
    destruct Hord as [ Hord | Hord ].
    { simplify_eq. eauto. }
    { apply pair_included in Hord.
      destruct Hord as [ _ Hord ].
      apply gmultiset_included in Hord.
      exact Hord. }
  (* Now, analyze the case where [q] is 1. Because the fraction 1 is
     exclusive (it does not have a frame), we can apply the lemma
     [Some_included_exclusive], which is more powerful than its little
     brother [Some_included]. The result follows immediately. *)
  - intros ->.
    apply Some_included_exclusive in Hord;
      [| eauto with typeclass_instances | done ].
    simplify_eq. eauto.
Qed.

(* ------------------------------------------------------------------------ *)

(* Exploiting an approximate [mapsfrom] assertion. *)

Lemma exploit_mapsfrom π l' q ls :
  pred π -∗
  l' ↤{q} ls -∗
  ⌜ exists ps, π !! l' = Some ps ∧ (q = 1%Qp → ps ⊆ ls) ⌝.
Proof.
  iIntros "Hauth Hpred".
  iDestruct "Hpred" as (sh) "(Hpred & %Hsub)".
  iDestruct (exploit_mapsfrom_exact π l' q sh with "Hauth Hpred")
    as %(ps & ? & ? & Heq).
  iPureIntro.
  eexists. split; [ eauto |].
  intro Hq. specialize (Heq Hq). simplify_eq. assumption.
Qed.

(* A simplified corollary of the previous lemma. *)

Lemma exploit_mapsfrom_dom π l' q ls :
  pred π -∗
  l' ↤{q} ls -∗
  ⌜ l' ∈ dom π ⌝.
Proof.
  iIntros "Hauth Hmapsfrom".
  iDestruct (exploit_mapsfrom with "Hauth Hmapsfrom") as %(ps & Heq & ?).
  iPureIntro.
  rewrite elem_of_dom Heq. eauto.
Qed.

(* ------------------------------------------------------------------------ *)

(* Exploiting the conjunction of a fractional [mapsto] assertion and a full
   [mapsfrom] assertion. From the fact that there is a forward edge from [l]
   to [l'], we deduce that [l] must appear at least as many times in [ls] as
   [l'] does in [b]. *)

Lemma exploit_mapsto_mapsfrom_multiplicity σ l q b l' ls :
  ph_interp σ -∗ l ↦{q} b -∗ l' ↤{1%Qp} ls -∗
  ⌜ multiplicity l' (pointers b) <= multiplicity l ls ⌝.
Proof.
  iIntros "Hph Hmapsto Hmapsfrom".
  iDestruct (exploit_mapsto σ l q b with "Hph Hmapsto") as %Hσl.
  iDestruct "Hph" as "[Hh Hfoo]".
  iDestruct "Hfoo" as (π) "[Hauth %Hinv]".
  iDestruct (exploit_mapsfrom π l' 1%Qp ls with "Hauth Hmapsfrom")
    as (ps) "%fact". destruct fact as (Hπl' & Hpsls). specialize (Hpsls eq_refl l).
  iPureIntro.
  cut (multiplicity l' (pointers b) <= multiplicity l ps). lia. clear Hpsls.
  assert (pointers b ⊆ successors σ l) as Hbl by rewrite /successors Hσl //.
  specialize (Hbl l'). simpl in Hbl.
  cut (multiplicity l' (successors σ l) <= multiplicity l ps). lia. clear Hbl.
  destruct Hinv as (_ & Hmirror & _). specialize (Hmirror l l').
  unfold predecessors in *. rewrite Hπl' in Hmirror. lia.
Qed.

(* A special case where multiplicity does not matter. *)

Lemma exploit_mapsto_mapsfrom σ l q b l' ls :
  ph_interp σ -∗ l ↦{q} b -∗ l' ↤{1%Qp} ls -∗
  ⌜ l' ∈ pointers b → l ∈ ls ⌝.
Proof.
  iIntros.
  iDestruct (exploit_mapsto_mapsfrom_multiplicity with "[$] [$] [$]") as %ll'.
  iPureIntro.
  unfold elem_of, gmultiset_elem_of.
  lia.
Qed.

(* ------------------------------------------------------------------------ *)

(* Confronting a [mapsfrom] assertion and a deallocation witness. *)

Lemma exploit_mapsfrom_dag σ l' q ls :
  ph_interp σ ∗ l' ↤{q} ls ∗ †l' ⊢ False.
Proof.
  iIntros "(Hh & Hmapsfrom & Hdag)".
  (* The location [l'] has been freed. *)
  iDestruct (exploit_discarded σ l' with "Hh Hdag") as "%Hfreed".
  iClear "Hdag".
  (* Unfold [ph_interp] to obtain [pred π]. *)
  unfold ph_interp.
  iDestruct "Hh" as "(_ & Hh)".
  iDestruct "Hh" as (π) "(Hpred & %Hinv)".
  (* Exploit the [mapsfrom] hypothesis. *)
  iDestruct (exploit_mapsfrom π l' q ls with "Hpred Hmapsfrom") as %Hm.
  destruct Hm as (ps & Hπl' & _).
  (* Conclude. [l'] has been freed, so cannot be in the domain of [π].
     Contradiction. *)
  iPureIntro.
  assert (l' ∈ dom π) by eauto with in_dom.
  destruct Hinv as (Hco & _ & _).
  unfold coincidence in Hco.
  destruct Hco as (Hco & _).
  unfold coincidence_direct_freed in Hco.
  assert (l' ∉ dom π) by eauto.
  tauto.
Qed.

(* ------------------------------------------------------------------------ *)

(* The authoritative assertion [pred π] allows allocation: provided [l] is
   fresh, it can be changed to [pred (<[l:=∅]> π)], while producing [l ↤ ∅],
   which witnesses the fact that a newly allocated location has no
   predecessors. *)

Lemma pred_alloc π l :
  l ∉ dom π →
  pred π ==∗
  pred (<[l:=∅]> π) ∗ l ↤ ∅.
Proof.
  intros.
  assert (Hπl: π !! l = None) by (eapply not_elem_of_dom; eauto).
  iIntros "Hauth".
  iMod (own_update with "Hauth") as "[? ?]".
  { eapply auth_update_alloc.
    eapply alloc_singleton_local_update with (i := l) (x := (1%Qp, ∅)).
    - rewrite lookup_fmap Hπl //.
    - done. }
  rewrite <- fmap_insert.
  iFrame. iExists _. by iFrame.
Qed.

(* Conversely, the conjunction [pred π ∗ l ↤ ls] allows deallocation,
   and produces a witness that the predecessors of [l] form a subset
   of [ls]. *)

Lemma pred_free_singleton π l ls :
  pred π -∗ l ↤ ls ==∗
  pred (delete l π) ∗ ⌜ predecessors π l ⊆ ls ⌝.
Proof.
  iIntros "Hauth Hpred".
  iDestruct "Hpred" as (sh) "[Hpred %Hsub]".
  iDestruct (exploit_mapsfrom_exact π l 1%Qp sh with "Hauth Hpred")
    as %(ps & Hπl & _ & Hps).
  specialize (Hps eq_refl).
  assert (predecessors π l = ps) by rewrite /predecessors Hπl //.
  assert (predecessors π l ⊆ ls) by multiset_solver. clear Hsub Hps.
  (* Update [π] to [delete l π]. *)
  iMod (own_update_2 with "Hauth Hpred") as "?".
  { eapply auth_update_dealloc.
    eapply delete_singleton_local_update with (i := l) (x := (1%Qp, ∅)).
    eauto with typeclass_instances. }
  rewrite <- fmap_delete.
  by iFrame.
Qed.

(* An iterated version of the previous lemma. *)

Lemma pred_free_group antecedents locs : ∀ π,
  pred π ∗
  ([∗ set] l ∈ locs, mapsfrom_set l 1%Qp antecedents)
  ==∗
  pred (gmap_mdelete locs π) ∗
  ⌜ (∀ l l', l' ∈ locs → l ∈ predecessors π l' → l ∈ antecedents) ⌝.
Proof.
  (* By induction on the set [locs]. *)
  pattern locs. eapply set_ind_L; clear locs.
  (* Case: ∅. *)
  { intro. rewrite gmap_mdelete_empty.
    iIntros "[Hauth _]". iFrame "Hauth".
    iPureIntro. set_solver. }
  (* Case: {[l]} ∪ locs. *)
  { intros l locs ? IH π.
    rewrite -> big_sepS_union by set_solver.
    rewrite big_sepS_singleton.
    rewrite -> gmap_mdelete_cons' by set_solver.
    iIntros "(Hauth & Hl & Hlocs)".
    rewrite /mapsfrom_set.
    iDestruct "Hl" as (ps) "[Hl %Hps]".
    iMod (pred_free_singleton with "Hauth Hl") as "[Hauth %Hl]".
    iMod (IH with "[$]") as "[Hauth %Hlocs]".
    iFrame "Hauth". iPureIntro.
    intros m m'. rewrite elem_of_union elem_of_singleton.
    destruct 1.
    + subst. intro.
      assert (m ∈ ps) by eauto using gmultiset_elem_of_subseteq.
      assert (m ∈ dom ps) by (rewrite gmultiset_elem_of_dom; eauto).
      set_solver.
    + rewrite <- (predecessors_delete_ne _ l) by set_solver. eauto. }
Qed.

(* If the predecessor set of [l'] is [ps1] and if we hold a share
   [sh1] of it, then we can update this to [ps2] and [sh2], provided
   [ps1 ⊎ sh2 = ps2 ⊎ sh1] holds. This expresses the fact that the
   share that we do not hold remains unchanged. *)

(* [ps2] is determined: it is [(ps1 ⊎ sh2) ∖ sh1]. *)

Lemma pred_update_exact π l' q ps1 sh1 ps2 sh2 :
  π !! l' = Some ps1 →
  ps1 ⊎ sh2 = ps2 ⊎ sh1 →
  pred π -∗ mapsfrom_exact l' q sh1 ==∗
  pred (<[ l' := ps2 ]> π) ∗ mapsfrom_exact l' q sh2.
Proof.
  intros Hπl' ?.
  rewrite -own_op. eapply own_update_2. eapply auth_update.
  rewrite fmap_insert.
  eapply singleton_local_update.
  - rewrite lookup_fmap Hπl' //.
  - eapply prod_local_update; simpl.
    + done.
    + eapply gmultiset_local_update. eauto.
Qed.

(* The imprecise [mapsfrom] assertion enjoys a similar property in the case
   where we are attempting to increase our share, that is, in the case where
   [ls1 ⊆ ls2] holds. *)

(* [ps2] is still determined: it is [(ps1 ⊎ ls2) ∖ ls1]. *)

Lemma pred_update_increasing π l' q ps1 ls1 ps2 ls2 :
  π !! l' = Some ps1 →
  ps1 ⊎ ls2 = ps2 ⊎ ls1 →
  ps1 ⊆ ps2 → (* equivalent to [ls1 ⊆ ls2] *)
  pred π -∗ l' ↤{q} ls1 ==∗
  pred (<[ l' := ps2 ]> π) ∗ l' ↤{q} ls2.
Proof.
  intros.
  iIntros "Hauth Hpred".
  iDestruct "Hpred" as (sh1) "[Hpred %Hsub]".
  set (sh2 := (ps2 ⊎ sh1) ∖ ps1).
  (* assert (sh2 = sh1 ⊎ (ps2 ∖ ps1)) by multiset_solver. *)
  iMod (pred_update_exact π l' q ps1 sh1 ps2 sh2 with "Hauth Hpred")
    as "[Hauth Hpred]".
  { assumption. }
  { multiset_solver. }
  iModIntro. iFrame. iExists _. iFrame. iPureIntro.
  multiset_solver.
Qed.

(* The imprecise [mapsfrom] assertion behaves in a trickier way in the case
   where we are attempting to decrease our share, that is, in the case where
   [ls2 ⊆ ls1] holds. Because [ls1] is an over-approximation of our actual
   share [sh1], we are not certain that the decrease [ls1 ∖ ls2] can be
   applied to our share [sh1] and (therefore) to [ps1]. We may be forced to
   apply a smaller decrease. Thus, the final state [ps2] is not determined:
   it is comprised somewhere between [(ps1 ⊎ ls2) ∖ ls1] and [ps1]. *)

Lemma pred_update_decreasing π l' q ps1 ls1 ls2 :
  π !! l' = Some ps1 →
  ls2 ⊆ ls1 →
  pred π -∗ l' ↤{q} ls1 ==∗
  ∃ ps2,
  ⌜ ps1 ⊎ ls2 ⊆ ps2 ⊎ ls1 ∧ ps2 ⊆ ps1 ⌝ ∗
  pred (<[ l' := ps2 ]> π) ∗ l' ↤{q} ls2.
Proof.
  intros.
  iIntros "Hauth Hpred".
  iDestruct "Hpred" as (sh1) "[Hpred %Hsub]".
  (* The following two lines give us [sh1 ⊆ ps1], which we need. *)
  iDestruct (exploit_mapsfrom_exact π l' q sh1 with "Hauth Hpred")
    as %(? & ? & ? & _). simplify_eq.
  set (sh2 := sh1 ∖ (ls1 ∖ ls2)).
  set (ps2 := (ps1 ⊎ sh2) ∖ sh1).
  (* assert (ps2 = ps1 ∖ (sh1 ∖ sh2)) by multiset_solver. *)
  iMod (pred_update_exact π l' q ps1 sh1 ps2 sh2 with "Hauth Hpred")
    as "[Hauth Hpred]".
  { assumption. }
  { multiset_solver. }
  iModIntro. iExists ps2. iFrame. iSplit.
  - iPureIntro. multiset_solver.
  - iExists _. iFrame. iPureIntro. multiset_solver.
Qed.

(* When confronted with the authoritative assertion [pred π], the assertion
   [l' ↤{q} ls] allows registering [l] as a new predecessor of [l']. The
   authoritative predecessor map is updated from [π] to [register l l' π],
   while the mapsfrom assertion becomes [l' ↤{q} (ls ⊎ {[l]})]. It is worth
   noting that the fraction [q] is not required to be 1. *)

Lemma pred_register π l l' q ls :
  pred π -∗ l' ↤{q} ls ==∗
  pred (register l l' π) ∗ l' ↤{q} (ls ⊎ {[+l+]}).
Proof.
  iIntros "Hauth Hmapsfrom".
  iDestruct (exploit_mapsfrom with "Hauth Hmapsfrom")
    as %(ps & Hπl' & _).
  rewrite /register.
  erewrite lookup_total_correct by eauto.
  iMod (pred_update_increasing π l' q ps ls (ps ⊎ {[+ l +]}) (ls ⊎ {[+ l +]}) Hπl'
          with "Hauth Hmapsfrom").
  { multiset_solver. }
  { multiset_solver. }
  by iFrame.
Qed.

(* The reverse of the previous lemma. Because this is a decreasing update,
   the statement is more complex, and the final state [π'] is not known
   exactly; we get a lower bound and an upper bound on it. *)

Lemma pred_unregister π l l' q ls :
  pred π -∗ l' ↤{q} ls ==∗
  ∃π',
  pred π' ∗ l' ↤{q} (ls ∖ {[+l+]}) ∗ ⌜ unregister l l' π ≾ π' ∧ π' ≾ π ⌝.
Proof.
  iIntros "Hauth Hmapsfrom".
  iDestruct (exploit_mapsfrom with "Hauth Hmapsfrom")
    as %(ps & Hπl' & _).
  rewrite /unregister.
  erewrite lookup_total_correct by eauto.
  iMod (pred_update_decreasing π l' q ps ls (ls ∖ {[+ l +]}) Hπl'
          with "Hauth Hmapsfrom")
    as (ps2) "(%Hbound & Hauth & Hmapsfrom)".
  { multiset_solver. }
  destruct Hbound as (Hlo & Hhi).
  iModIntro. iExists _. iFrame. iPureIntro.
  split; eauto with approx.
Qed.

(* We now wish to iterate the above lemma so as to register an address [l] as
   a new predecessor of [l'], for every address [l'] in a certain list.

   For each address [l'] in the list, we must require [l' ↤{q} ls], for a
   certain fraction [q] and a certain list [ls] of pre-existing predecessors.

   This leads us to parameterizing the lemma with a list of triples, where
   each triple is of the form [(l', q, ls)], and is interpreted as a
   description of the assertion [l' ↤{q} ls].

   The fact that we allow fractional ownership seems crucial here. Indeed,
   because a newly-allocated block may have two fields that contain the same
   pointer, we must allow two triples in this list to concern the same address
   [l']. If we required full ownership of every triple in the list, then the
   iterated conjunction of these triples would be unsatisfiable. *)

(* That said, considering our current needs, working with lists of triples of
   arbitrary length is slightly overkill. The lemma [ph_alloc] is applied to a
   list of length 0, because we initialize every memory block with unit
   values. The lemma [ph_update] is applied to lists of length at most 1,
   because our store instruction updates only one field at a time, so destroys
   at most one edge and creates at most one edge. *)

Implicit Types triple :      (L * frac * gmultiset L).

Implicit Type triples : list (L * frac * gmultiset L).

Definition interpret1 triple : iProp Σ :=
  let '(l', q, ls) := triple in l' ↤{q} ls.

Definition interpret triples : iProp Σ :=
  [∗ list] triple ∈ triples, interpret1 triple.

Definition address triple :=
  let '(l', q, ls) := triple in l'.

Definition addresses triples : gmultiset L :=
  list_to_set_disj (map address triples).

Definition update l triple :=
  let '(l', q, ls) := triple in (l', q, ls ⊎ {[+l+]}).

Definition remove l triple :=
  let '(l', q, ls) := triple in (l', q, ls ∖ {[+l+]}).

Lemma interpret_nil :
  interpret [] = True%I.
Proof.
  reflexivity.
Qed.

Lemma interpret_singleton l' q ls :
  interpret [(l', q, ls)] ≡ (l' ↤{q} ls)%I.
Proof.
  rewrite /interpret. simpl. iSplit.
  - iIntros "(? & _)". iFrame.
  - iIntros. iFrame.
Qed.

Lemma addresses_cons triple triples :
  addresses (triple :: triples) =
  addresses triples ⊎ {[+ address triple +]}.
Proof.
  unfold addresses. simpl. multiset_solver.
Qed.

Lemma pred_foldr_register l :
  ∀ triples π,
  pred π -∗ interpret triples ==∗
  pred (set_fold (register l) π (addresses triples)) ∗
  interpret (map (update l) triples).
Proof.
  induction triples as [| [[l' q] ls] triples ];
  simpl; intros; subst; simpl; [eauto|].
  iIntros "Hauth [Htriple Htriples]".
  iMod (IHtriples with "Hauth Htriples") as "[Hauth Htriples]".
  iMod (pred_register _ l l' q ls with "Hauth Htriple") as "[Hauth Htriple]".
  rewrite addresses_cons.
  rewrite gmultiset_set_fold_disj_union gmultiset_set_fold_singleton.
  by iFrame.
Qed.

Lemma pred_foldr_unregister l :
  ∀ triples π,
  pred π -∗ interpret triples ==∗
  ∃ π',
  pred π' ∗
  interpret (map (remove l) triples) ∗
  ⌜ set_fold (unregister l) π (addresses triples) ≾ π' ∧ π' ≾ π ⌝.
Proof.
  induction triples as [| [[l' q] ls] triples ];
  simpl; intros; subst; simpl; [eauto 7 with approx|].
  iIntros "Hauth [Htriple Htriples]".
  iMod (IHtriples with "Hauth Htriples")
    as (π1) "(Hauth & Htriples & %Hlo1 & %Hhi1)".
  iMod (pred_unregister _ l l' q ls with "Hauth Htriple")
    as (π2) "(Hauth & Htriple & %Hlo2 & %Hhi2)".
  iModIntro. iExists _. iFrame. iPureIntro.
  rewrite addresses_cons.
  rewrite gmultiset_set_fold_disj_union gmultiset_set_fold_singleton.
  eauto using approx_transitive, unregister_approx.
Qed.

Lemma exploit_triples_dom π triples :
  pred π -∗
  interpret triples -∗
  ⌜ ∀ l', l' ∈ addresses triples → l' ∈ dom π ⌝.
Proof.
  induction triples as [| [[l' q] ls] triples ]; simpl;
  iIntros "Hauth Htriples".
  { iPureIntro. multiset_solver. }
  iDestruct "Htriples" as "[Htriple Htriples]". simpl.
  iDestruct (exploit_mapsfrom_dom π l' q ls with "Hauth Htriple") as %Hhead.
  iDestruct (IHtriples with "Hauth Htriples") as %Htail.
  iPureIntro.
  intro. rewrite addresses_cons. multiset_solver.
Qed.

(* ------------------------------------------------------------------------ *)

(* Our state interpretation predicate is preserved by the allocation of a
   new block [b] at a fresh location [l].

   As explained earlier, the user must provide a list of triples
   [(l', q, ls)], where [l'] ranges over the locations contained
   in the block [b], and must provide the assertion [l' ↤{q} ls]
   that corresponds to each of these triples.

   After the operation, the location [l] is added as an extra predecessor
   in each of these triples: each such triple becomes [l' ↤{q} ls ⊎ {[l]}].

   In addition, the user receives [l ↦ b], as in ordinary Separation Logic,
   and [l ↤ ∅], which states that the new block has no predecessors. *)

Lemma ph_alloc σ l b triples :
  σ !! l = None →
  b ≠ deallocated →
  addresses triples = pointers b →
  ph_interp σ            ∗ interpret triples                ==∗
  ph_interp (<[l:=b]> σ) ∗ interpret (map (update l) triples) ∗
  l ↦ b ∗ l ↤ ∅.
Proof.
  intros Hσl ? Hpointers.
  rewrite /ph_interp.
  iIntros "[[Hh Hph] Htriples]".
  iDestruct "Hph" as (π) "(Hauth & %Hinv)".
  generalize Hinv; intros (Hco & Hmirror & Hclosure).

  (* [l] is fresh, so is not in the domain of [σ]. *)
  assert (l ∉ dom σ) by eauto.
  (* By coincidence, the domain of [π] is a subset of that of [σ],
     so [l] is not the domain of [π] either. *)
  assert (Hπl: l ∉ dom π) by eauto using use_coincidence_reverse.
  (* From the mapsfrom assertions provided to us, we can deduce that every
     successor of [b] is the domain of [π]. *)
  iDestruct (exploit_triples_dom with "Hauth Htriples") as %Hlive.
  rewrite Hpointers in Hlive.

  (* We are now ready to update the ghost state, in three steps: *)

  (* Step 1. Extend the heap resource with a mapping of [l] to [b]. *)
  iMod (gen_heap_alloc σ l b Hσl with "Hh") as "(Hh & Hlb & _)".

  (* Step 2. Move from [π] to [π'], which extends [pi] with a mapping of [l]
     to an empty multiset of predececessors. At the same time, we obtain the
     assertion [l ↤ ∅]. *)
  set (π' := <[l:=∅]> π).
  iMod (pred_alloc π l Hπl with "Hauth") as "[Hauth Hmapsfrom]".

  (* Step 3. Move from [π'] to [π''], which is obtained from [π'] by
     registering [l] as a predecessor of every successor of [b]. *)
  set (π'' := set_fold (register l) π' (pointers b)).
   iMod (pred_foldr_register l triples _ with "Hauth Htriples")
    as "[Hauth Htriples]".
  rewrite Hpointers.

  (* The invariant is preserved. This argument relies on the fact that
     every successor of [b] is the domain of [π].*)
  assert (invariant (<[l:=b]> σ) π'') by eauto using invariant_alloc.

  (* Conclude. *)
  iModIntro. iFrame. iExists _. by iFrame.
Qed.

(* ------------------------------------------------------------------------ *)

(* An ad hoc version of the lemma [gen_heap_valid_big] in iris.v. *)

Local Lemma gen_heap_valid_big (no_roots : bool) σ locs :
  gen_heap_interp σ ∗
  ([∗ set] l ∈ locs, ∃ b, l ↦ b ∗
                        ⌜ if no_roots then ¬is_stack_cell b else True ⌝) -∗
  ⌜ locs ⊆ dom σ ⌝ ∗
  ⌜ if no_roots then ∀ l, l ∈ locs → ¬ is_root σ l else True ⌝.
Proof.
  (* By induction on the set [locs]. *)
  pattern locs. eapply set_ind_L; clear locs.
  (* Case: ∅. *)
  { destruct no_roots; eauto. }
  (* Case: {[l]} ∪ locs. *)
  { intros l locs Hl IH.
    rewrite -> !big_sepS_union, !big_sepS_singleton by set_solver.
    iIntros "(Hh & Hl & Hlocs)".
    iDestruct "Hl" as (b) "(Hl & %Hnroot)".
    iDestruct (gen_heap_valid with "Hh Hl") as %Hone.
    assert (l ∈ dom σ) by eauto.
    assert (if no_roots then ¬ is_root σ l else True).
    { destruct no_roots. intros (b' & ? & ?). congruence. tauto. }
    iDestruct (IH with "[$]") as %Htwo. destruct Htwo.
    iPureIntro.
    destruct no_roots; set_solver. }
Qed.

(* An ad hoc helper lemma. *)

Local Lemma forget P locs :
  ([∗ set] l ∈ locs, ∃b, l ↦ b ∗ ⌜ P b ⌝) ⊢
  ([∗ set] l ∈ locs, ∃b, l ↦ b).
Proof.
  eapply big_opS_gen_proper; try typeclasses eauto.
  iIntros (l _) "H".
  iDestruct "H" as (b) "[??]".
  iExists b. iFrame.
Qed.

(* ------------------------------------------------------------------------ *)

(* The predicate [ph_interp σ] allows deallocating a set of locations [locs],
   provided this set is closed under predecessors. *)

(* This operation consumes [∃ b, l ↦ b] and [mapsfrom_set l 1%Qp locs]
   for every location [l] in the set [locs]. It does not require any
   information about the successors of the block [b], and does not update
   their predecessor fields. We adopt a mechanism that allows delayed
   updates: we produce a proof that the location [l] has been deallocated.
   A separate rule allows removing a dead location from a predecessor set. *)

(* We produce a witness that the set [locs] contains no roots and is closed
   under predecessors. This fact is stated in terms of successors. *)

Lemma ph_free_group (no_roots : bool) σ locs :
  ph_interp σ ∗
  ([∗ set] l ∈ locs, ∃b, l ↦ b ∗ ⌜if no_roots then ¬is_stack_cell b else True⌝) ∗
  ([∗ set] l ∈ locs, mapsfrom_set l 1%Qp locs)
  ==∗
  ph_interp (deallocate locs σ) ∗
  ††locs ∗
  ⌜ if no_roots then ∀ l, l ∈ locs → ¬ is_root σ l else True ⌝ ∗
  ⌜ ∀ m m', m' ∈ successors σ m → m' ∈ locs → m ∈ locs ⌝.
Proof.
  rewrite /ph_interp.
  iIntros "((Hh & Hph) & Hmapsto & Hmapsfrom)".
  iDestruct "Hph" as (π) "(Hauth & %Hinv)".

  (* [locs] is a subset of the domain of [σ], and does not contain any root. *)
  iDestruct (gen_heap_valid_big with "[$]") as "[%Hdom %Hnroot]".
  rewrite forget.

  (* Update [σ] by deallocating every location in the set [locs]. *)
  iMod (iris.gen_heap_update_big locs with "[$]") as "(Hh & Hmapsto)".

  (* For every [l] in [locs], we have full ownership of [l ↦ deallocated].
     Discard a fraction of this fact, so as to make it persistent.
     Deallocation is forever. *)
  iMod (iris.mapsto_persist_big with "[$]") as "Hmapsto".

  (* Update [π] by removing [locs] from its domain. *)
  iMod (pred_free_group with "[$]") as "[Hauth %Hclosed]".

  (* The invariant is preserved. Conclude. *)
  iModIntro. iFrame "Hh Hmapsto".
  iSplitL "Hauth".
  + iExists _. iFrame "Hauth". eauto using invariant_free.
  + eauto using successors_predecessors_corollary.
Qed.

(* Deallocating a single location is a special case. *)

(* The memory block at address [l] may contain one or more pointers
   to itself. *)

(* The lemma is stated in terms of [mapsfrom], as opposed to
   [mapsfrom_set], because this may be more convenient. We may
   end up needing both versions anyway. *)

(* This lemma can be applied to a stack cell by setting [no_roots]
   to [false]. *)

Lemma ph_free_singleton (no_roots : bool) σ l b ls :
  dom ls ⊆ {[l]} →
  (if no_roots then ¬ is_stack_cell b else True) →
  ph_interp σ             ∗ l ↦ b ∗ l ↤ ls ==∗
  ph_interp (<[l:=deallocated]> σ) ∗ †l ∗
  ⌜ if no_roots then ¬ is_root σ l else True ⌝ ∗
  ⌜ ∀ m, l ∈ successors σ m → m = l ⌝.
Proof.
  iIntros (Hls Hb) "(Hph & Hmapsto & Hmapsfrom)".
  iDestruct (exploit_mapsto with "[$] [$]") as %Hdom.
  iMod (ph_free_group no_roots σ {[l]} with "[Hph Hmapsto Hmapsfrom]")
    as "(Hph & Hddag & %Hfacts)".
  { iFrame "Hph". rewrite !big_sepS_singleton. iSplitL "Hmapsto".
    + eauto.
    + unfold mapsfrom_set. iExists ls. iFrame "Hmapsfrom". iPureIntro.
      eauto. }
  destruct Hfacts.
  rewrite /deallocate. rewrite -> gmap_mset_singleton by eauto.
  rewrite -> (ddag_singleton l).
  iFrame "Hph Hddag". iPureIntro. destruct no_roots; set_solver.
Qed.

(* ------------------------------------------------------------------------ *)

(* The "cleanup" reasoning rule states that a deallocated location [m] can
   be removed from a predecessor set. *)

(* This rule can be stated in several different ways; e.g., one may first
   propose a statement that focuses on one location [m] and later generalize
   to a set or multiset of locations, or adopt the converse approach; and if
   one chooses to focus on one location, one may remove just one copy of
   this location from the predecessor multiset, or one may remove all copies
   of it at once. We choose one path. *)

Lemma ph_cleanup σ l' q ls ms :
  (∀m, m ∈ ms → freed σ m) →
  ph_interp σ ∗ l' ↤{q}  ls       ==∗
  ph_interp σ ∗ l' ↤{q} (ls ∖ ms).
Proof.
  intros.
  rewrite /ph_interp.
  iIntros "((Hh & Hph) & Hmapsfrom)".
  iDestruct "Hph" as (π) "(Hauth & %Hinv)".

  (* Name [ps] the predecessors of [l']. *)
  iDestruct (exploit_mapsfrom π l' q ls with "Hauth Hmapsfrom")
    as %(ps & Hπl' & _).

  (* Update [π] by removing [ms] from the predecessor set of [l']. *)
  iMod (pred_update_decreasing π l' q ps ls (ls ∖ ms) Hπl'
          with "Hauth Hmapsfrom") as (ps2) "(%Hbound & Hauth & Hmapsfrom)".
  { multiset_solver. }
  destruct Hbound as (? & ?).

  (* Conclude. *)
  iModIntro. iFrame. iExists _. iFrame. iPureIntro.
  assert (<[l' := ps ∖ ms]> π ≾ <[l' := ps2]> π) by eauto with approx.
  assert (<[l' := ps2]> π ≾ π) by eauto with approx.
  eauto using invariant_approx, invariant_cleanup,
              Some_in_dom, Some_predecessors.
Qed.

Lemma ph_cleanup_singleton σ l' q ls m :
  ph_interp σ ∗ l' ↤{q} ls ∗ †m ==∗
  ph_interp σ ∗ l' ↤{q} (ls ∖ {[+m+]}).
Proof.
  iIntros "(Hh & Hmapsfrom & Hmapsto)".
  iDestruct (exploit_discarded σ m with "Hh Hmapsto") as %?.
  iMod (ph_cleanup σ l' q ls {[+m+]} with "[$]").
  { intros m'. rewrite gmultiset_elem_of_singleton. intros ->. eauto. }
  by iFrame.
Qed.

(* TODO may need to be reformulated in a more convenient way;
   see also wp_cleanup.v *)
Lemma ph_cleanup_group σ l' q ls ms :
  ph_interp σ ∗ l' ↤{q} ls ∗ ††(dom ms) ==∗
  ph_interp σ ∗ l' ↤{q} (ls ∖ ms).
Proof.
  iIntros "(Hh & Hmapsfrom & Hmapsto)".
  iDestruct (exploit_discarded_group σ (dom ms) with "Hh Hmapsto") as %Hdms.
  assert (Hms: ∀ l, l ∈ ms → freed σ l).
  { intro. rewrite <- gmultiset_elem_of_dom. eauto. }
  iMod (ph_cleanup σ l' q ls ms Hms with "[$]").
  by iFrame.
Qed.

Lemma ph_update_dep (precise:bool) σ l b b' old_triples new_triples :
  b ≠ deallocated →
  b' ≠ deallocated →
  (if precise then addresses old_triples = old_pointers b b' else True) →
  addresses new_triples = new_pointers b b' →
  ph_interp (<[l := b]> σ) ∗
  (if precise then interpret old_triples else ⌜True⌝) ∗
  interpret new_triples ∗
  l ↦ b ==∗
  ph_interp (<[l := b']> σ) ∗
  (if precise then interpret (map (remove l) old_triples) else ⌜True⌝) ∗
  interpret (map (update l) new_triples) ∗
  l ↦ b'.
Proof.
  intros Hb Hb' Hoaddr Hnaddr.
  rewrite /ph_interp.
  iIntros "((Hh & Hph) & Hotriples & Hntriples & Hmapsto)".
  iDestruct "Hph" as (π) "(Hauth & %Hinv)".

  iDestruct (exploit_triples_dom with "Hauth Hntriples") as %Hlive.
  rewrite Hnaddr in Hlive.

  (* Update [σ] with a mapping of [l] to [b']. *)
  iMod (gen_heap_update _ l _ b' with "Hh Hmapsto") as "(Hh & Hmapsto)".
  rewrite insert_insert.

  destruct precise. (* Case analysis if we track predecessors. *)
  { (* Update [π] by unregistering every edge from [l] to a location [l']
     in [block_pointers b ∖ block_pointers b'] and by registering every
     edge from [l] to some [l'] in [block_pointers b' ∖ block_pointers b]. *)
    iMod (pred_foldr_unregister l old_triples _ with "Hauth Hotriples")
      as (π') "(Hauth & Hotriples & %Hlo & %Hhi)".
    iMod (pred_foldr_register l new_triples _ with "Hauth Hntriples")
      as "(Hauth & Hntriples)".

    (* Conclude. *)
    iModIntro. iFrame. iExists _. iFrame. iPureIntro.
    (* The invariant is preserved. *)
    rewrite Hnaddr.
    rewrite Hoaddr in Hlo.
    eapply (invariant_update_imprecise σ l b b' π π' Hinv);
      eauto using gmultiset_in_set_in_elements. }
  { iMod (pred_foldr_register l new_triples _ with "Hauth Hntriples")
      as "(Hauth & Hntriples)".

    (* Conclude. *)
    iModIntro. iFrame. iExists _. iFrame. iPureIntro.
    (* The invariant is preserved. *)
    rewrite Hnaddr.
    eapply (invariant_update_imprecise_weak σ l b b' π Hinv);
      eauto using gmultiset_in_set_in_elements. }
Qed.

(* ------------------------------------------------------------------------ *)

(* Timelessness. *)

Global Instance timeless_mapsfrom_exact l q sh :
  Timeless (mapsfrom_exact l q sh).
Proof. apply _. Qed.

Global Instance timeless_mapsfrom l q ls :
  Timeless (mapsfrom l q ls).
Proof. apply _. Qed.

Global Instance timeless_mapsfrom_set l q locs :
  Timeless (mapsfrom_set l q locs).
Proof. apply _. Qed.

Global Instance timeless_dag l :
  Timeless (†l).
Proof. apply _. Qed.

Global Instance timeless_ddag locs :
  Timeless (††locs).
Proof. apply _. Qed.

(* ------------------------------------------------------------------------ *)

End ReasoningRules.

(* ------------------------------------------------------------------------ *)

(* Notations for assertions. *)

(* Fractional ownership of a memory block, where [q] is a fraction. *)

Notation "l ↦{ q } b" := (mapsto l (DfracOwn q) b)
  (at level 20, q at level 50, format "l  ↦{ q }  b") : bi_scope.

(* Full ownership of a memory block. *)

Notation "l ↦ b" := (l ↦{ 1%Qp } b)%I
  (at level 20) : bi_scope.

(* Fractional ownership of a predecessor set, where [q] is a fraction. *)

Notation "l ↤{ q } ls" :=
  (mapsfrom l q ls)
  (at level 20, format "l ↤{ q } ls")
: bi_scope.

(* Full ownership of a predecessor set. *)

Notation "l ↤ ls" :=
  (l ↤{ 1%Qp } ls)%I
  (at level 20, format "l ↤ ls")
: bi_scope.

(* Persistent knowledge that a location has been deallocated. *)

Notation "† l" := (gen_heap.mapsto l DfracDiscarded deallocated)%I
  (at level 20) : bi_scope.

(* Persistent knowledge that a group of locations have been deallocated. *)

Notation "†† ls" := ([∗ set] l ∈ ls, †l)%I
  (at level 20) : bi_scope.

(* ------------------------------------------------------------------------ *)

(* The following definitions have to do with initializing the heap.
   They are used in the proof of adequacy. *)

Module Initialization.

  Definition phΣ L B `{Countable L} : gFunctors := #[
    gen_heapΣ L B;
    GFunctor (predR L)
  ].

  Class phPreG L B Σ `{Countable L} := {
    phPreG_heap :> gen_heapGpreS L B Σ;
    phPreG_pred :> inG Σ (predR L);
  }.

  Global Instance subG_phPreG {L B Σ} `{Countable L} :
    subG (phΣ L B) Σ → phPreG L B Σ.
  Proof. solve_inG. Qed.

  Lemma ph_init `{Countable L, Hypotheses L B, !phPreG L B Σ} :
    ⊢ |==> ∃ _ : phG L B Σ,
    ph_interp ∅.
  Proof.
    iIntros. rewrite /ph_interp.
    (* Allocate the ghost heap component. *)
    iMod (gen_heap_init ∅) as (ghG) "(Hh & _ & _)".
    (* Allocate the ghost cell that holds [π]. *)
    set (π := ∅ : gmap L (gmultiset L)).
    iMod (own_alloc (● (fmap (λ ls, (1%Qp, ls)) π) : predR L)) as (γ) "Hpred".
    { rewrite auth_auth_valid. done. }
    (* Conclude. *)
    iExists (PhG L B Σ _ _ ghG _ γ).
    iModIntro.
    iFrame. (* this cancels out "Hh" *)
    iExists π.
    iFrame. (* this cancels out "Hpred" *)
    iPureIntro.
    eapply invariant_init.
  Qed.

End Initialization.
