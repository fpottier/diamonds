From spacelang Require Import spacelang gc tactics.

(* ------------------------------------------------------------------------ *)

(* [Atomic StronglyAtomic i] means that [i] can reduce (in one step) only to
   a "value", that is, only to [skip].

   [Atomic WeaklyAtomic e] means that [e] can reduce (in one step) only to
   an irreducible configuration. *)

Section atomic.

  Local Ltac solve_atomic :=
    apply strongly_atomic_atomic, ectx_language_atomic; [
      repeat intro; inv_gc_head_step; inv_head_step; naive_solver
    | apply ectxi_language_sub_redexes_are_values; intros [] **; naive_solver
    ].

  Global Instance alloc_atomic a dst n : Atomic a (IAlloc dst n).
  Proof. solve_atomic. Qed.

  Global Instance load_atomic a dst src ofs : Atomic a (ILoad dst src ofs).
  Proof. solve_atomic. Qed.

  Global Instance store_atomic a dst ofs src : Atomic a (IStore dst ofs src).
  Proof. solve_atomic. Qed.

  Global Instance loc_eq_atomic a dst src1 src2 :
    Atomic a (ILocEq dst src1 src2).
  Proof. solve_atomic. Qed.

  Global Instance const_atomic a dst v : Atomic a (IConst dst v).
  Proof. solve_atomic. Qed.

  Global Instance move_atomic a dst src : Atomic a (IMove dst src).
  Proof. solve_atomic. Qed.

  Global Instance alloca_active_skip_atomic a c :
    Atomic a (IAllocaActive c ISkip).
  Proof. solve_atomic. Qed.

End atomic.

(* ------------------------------------------------------------------------ *)

(* The behavior of the [wp_] tactics with regard to lambda-abstractions
   differs in the following way: [wp_code] is able to see through a Coq
   definition whose right-hand side is [VCode ...], whereas [wp_pure] is
   not. *)

(* The rationale, I believe, is that [wp_code] is used to reason about a
   function definition, and must be able to see the function's body, whereas
   [wp_pure] is used to reason about a client, which may contain a function
   call, and must not inadvertently inline the function at its call site.
   Indeed, we wish to reason about a function call by using the function's
   specification, not by inlining the function's code at its call site. *)

(* Following HeapLang, in order to obtain this behavior, we define the class
   [AsVCode v xs e], which takes a value [v] as its input, and turns it
   into a value of the form [VCode self xs e]. *)

Class AsVCode (v : value) xs i :=
  as_vcode :
    v = VCode xs i.

(* This declares the parameter [v] as an input and the remaining parameters
   as outputs of the predicate [AsVCode]. *)

Global Hint Mode AsVCode ! - - : typeclass_instances.

(* We register an instance of this type class via [Hint Extern] so that, by
   default, this instance is used only if [v] is syntactically [VCode ...]. *)

Definition AsVCode_vcode xs i :
  AsVCode (VCode xs i) xs i
:= eq_refl.

Global Hint Extern 0 (AsVCode (VCode _ _) _ _) =>
  apply AsVCode_vcode : typeclass_instances.

(* ------------------------------------------------------------------------ *)

(* [PureExec φ n i1 i2] means that if φ (a pure Coq proposition) holds then
   [i1] reduces to [i2] in [n] pure steps. A pure step must not consult or
   affect the machine state and must be deterministic. *)

(* Because almost every instruction in our calculus reads or writes a stack
   cell, we have very few pure steps; in fact, we have no pure steps at all,
   except the reduction of [skip; i] to [i]. Even worse, because every
   reduction involves a GC step, we have no pure steps at all: every step
   can affect the machine state and is potentially nondeterministic. *)

(* So, the following tactics are now unused. *)

(* The reduction of [skip; i] to [i] is studied in the file [wp_skip.v]. *)

Section pure_exec.

  (* [solve_exec_safe] solves a goal of the form [head_reducible_no_obs]. *)

  Local Ltac solve_exec_safe :=
    intros; subst; do 3 eexists;
    eapply GCHeadStep; [
      eapply gc_id
    | econstructor; eauto ].

  (* [solve_exec_puredet] solves a goal of the form [head_step ... → _]. *)

  Local Ltac solve_exec_puredet :=
    simpl; intros; inv_gc_head_step; inv_head_step; try done.

  Local Ltac solve_pure_exec :=
    subst; intros ?; apply nsteps_once, pure_head_step_pure_step;
      constructor; [ solve_exec_safe | solve_exec_puredet ].

End pure_exec.

(* ------------------------------------------------------------------------ *)

(* The LanguageCtx instances are useful in applications of [wp_bind]. *)

Global Instance LanguageCtx_IAllocaActive c : LanguageCtx (IAllocaActive c).
Proof.
  change (IAllocaActive c) with (ectxi_language.fill_item (KAllocaActive c)).
  apply ectxi_lang_ctx_item.
Qed.

Global Instance LanguageCtx_ISeq i2 : LanguageCtx (λ i1, ISeq i1 i2).
Proof.
  change (λ i1, ISeq i1 i2) with (ectxi_language.fill_item (KSeq i2)).
  apply ectxi_lang_ctx_item.
Qed.

Global Instance into_val_val : IntoVal ISkip tt.
Proof. done. Qed.
