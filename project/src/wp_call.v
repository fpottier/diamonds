From spacelang Require Import wp_state_interp wp_memory.

Lemma wp_call `{!heapG Σ} s E c q args xs i Φ :
  length xs = length args →
  ▷ (c ↦{q} BCell (VCode xs i) -∗ WP (substs (zip xs args) i) @ s; E {{ Φ }})
  ⊢  c ↦{q} BCell (VCode xs i) -∗ WP (ICall (LLoc c) args) @ s; E {{ Φ }}.
Proof.
  iIntros (?) "HWP Hc".
  iApply wp_lift_step; first done.
  iIntros (σ1 ns κ κs n) "Hstate".
  iApply fupd_mask_intro; first set_solver. iIntros "Hclose". iSplit.

  (* First, prove that this instruction is reducible. *)
  { destruct s; [| eauto ].
    destruct_state_interp.
    exploit_stack_cell c.
    head_reducible. }

  (* Then, examine an arbitrary reduction step. *)
  examine_step.
  inv_read_lvalue.
  exploit_stack_cell c. simplify_eq.

  (* Conclude. *)
  iMod "Hclose" as "_".
  iModIntro.
  iSpecialize ("HWP" with "Hc").
  iFrame "HWP".
  iSplitL; [| eauto ].
  iExists _. iFrame. eauto.
Qed.
