From stdpp Require Import gmap.
From iris.program_logic Require Export language ectx_language ectxi_language.
From spacelang Require Export syntax state pointers.
Open Scope Z.

(* This file defines the operational semantics of SpaceLang. *)

(* ------------------------------------------------------------------------ *)

(* Evaluation contexts. *)

(* The following are the evaluation context items, that is, the evaluation
   contexts of depth one. An evaluation context is a list of evaluation
   context items. *)

(* We have two evaluation contexts: [KSeq] allows evaluation in the
   left-hand side of a sequence; [KAllocaActive] allows evaluation
   under an active [alloca] instruction. *)

Inductive ectx_item :=
  | KSeq (i2 : instr)           (* _; i2         *)
  | KAllocaActive (c : loc)     (* alloca c in _ *)
.

(* [fill_item Ki i] is the result of filling the hole of the evaluation
   context item [Ki] with the instruction [i]. *)

Definition fill_item (Ki : ectx_item) (i : instr) : instr :=
  match Ki with
  | KSeq i2         => ISeq i i2
  | KAllocaActive c => IAllocaActive c i
  end.

(* ------------------------------------------------------------------------ *)

(* Substitution. *)

(* Substitution of an lvalue [lv] for a variable [x] in an lvalue [lw]. *)

Definition lsubst (x : binder) (lv : lvalue) (lw : lvalue) : lvalue :=
  match lw with
  | LVar y =>
      if decide (x = y) then lv else lw
  | LLoc _ =>
      lw
  end.

(* Substitution of a closed lvalue for a variable in an instruction. *)

Fixpoint subst (x : binder) (lv : lvalue) (i : instr) : instr :=
  match i with
  | ISkip =>
      ISkip
  | ISeq i1 i2 =>
      ISeq (subst x lv i1) (subst x lv i2)
  | IIf src i1 i2 =>
      IIf (lsubst x lv src) (subst x lv i1) (subst x lv i2)
  | ICall callee args =>
      ICall (lsubst x lv callee) (map (lsubst x lv) args)
  | IAlloc dst n =>
      IAlloc (lsubst x lv dst) n
  | ILoad dst src ofs =>
      ILoad (lsubst x lv dst) (lsubst x lv src) ofs
  | IStore dst ofs src =>
      IStore (lsubst x lv dst) ofs (lsubst x lv src)
  | ILocEq dst src1 src2 =>
      ILocEq (lsubst x lv dst) (lsubst x lv src1) (lsubst x lv src2)
  | IConst dst v =>
      IConst (lsubst x lv dst) v
  | IMove dst src =>
      IMove (lsubst x lv dst) (lsubst x lv src)
  | IAlloca y i =>
      IAlloca y (if decide (x ≠ y) then subst x lv i else i)
  | IAllocaActive c i =>
      (* This case should be irrelevant, because [IAllocaActive c _] is an
         evaluation context, and we never substitute through an evaluation
         context; we substitute under an evaluation context. *)
      IAllocaActive c (subst x lv i)
  | IFork dst y i =>
      IFork (lsubst x lv dst) y (if decide (x ≠ y) then i else subst x lv i)
  end.

(* Iterated substitution of closed lvalues for variables in an instruction. *)

(* Because we are dealing with closed lvalues, iterated substitution coincides
   with parallel substitution, provided the variables [xs] are distinct. We do
   not currently impose this condition; we just use iterated substitution. We
   use a left fold, so that the list [xlvs] can be read as a sequence of
   nested [let] bindings. *)

Definition substs (xlvs : list (binder * lvalue)) (i : instr) : instr :=
  foldl (fun i '(x, lv) => subst x lv i) i xlvs.

(* ------------------------------------------------------------------------ *)

(* Small-step operational semantics. *)

(* The type [observation] is empty, which implies that [list observation] is
   is isomorphic to the unit type. In other words, there are no observations
   in this transition system. *)

Inductive observation := .

(* Accessing a closed lvalue [src] for reading in a machine state [σ]. *)

(* The lvalue [src] must be a location [c]. We read the stack cell at
   address [c], if it exists, and find a value [v]. *)

Inductive read_lvalue : lvalue → state → value → Prop :=
| LVRead c σ v :
    σ.(store) !! c = Some (BCell v) →
    read_lvalue (LLoc c) σ v.

(* Accessing a closed lvalue [dst] for writing in a machine state [σ],
   producing a machine state [σ']. *)

Inductive write_lvalue : lvalue → value → state → state → Prop :=
| LVWrite c w σ v σ' :
    σ.(store) !! c = Some (BCell v) →
    σ' = <[c := BCell w ]> <$> σ →
    write_lvalue (LLoc c) w σ σ'.

(* The predicate [head_step i σ κs e' σ' fk] means that the instruction [i]
   in state [σ] reduces to [i'] in state [σ'] while forking threads [fk].
   In SpaceLang, the list of observations [κs] is always empty. The list of
   forked threads [fk] is always empty except in the reduction of [IFork]. *)

(* When a new block is allocated, a memory location that has never been used
   so far is picked. When a block is deallocated, the memory location
   becomes mapped to [BDeallocated] in the heap, so it cannot be re-used.
   (This is important: in the logic, we wish to be able to have a persistent
   witness that "this memory location has been deallocated".) Deallocation
   is not visible in the reduction rules below; indeed, garbage collection
   is modeled by a separate definition. *)

Inductive head_step :
  instr → state → list observation →
  instr → state → list instr → Prop
:=

| StepSeqSkip i σ :
    head_step
      (ISeq ISkip i) σ []
      i σ []
  (* [skip; i] reduces to [i]. *)

| StepIf src i1 i2 σ n :
    read_lvalue src σ (VInt n) →
    head_step
      (IIf src i1 i2) σ []
      (if Int63.is_zero n then i2 else i1) σ []
  (* [if *src then i1 else i2] first reads an integer value [n] from
     the stack cell at address [src]. Then, it reduces to [i1] if [n]
     is zero and to [i2] otherwise. *)

| StepCall callee args σ xs i :
    read_lvalue callee σ (VCode xs i) →
    length xs = length args →
    head_step
      (ICall callee args) σ []
      (substs (zip xs args) i) σ []
    (* [*callee(args)] first reads a code pointer [λxs.i] from the
       stack cell at address [callee]. Then, it reduces to [i],
       where the actual arguments [args] are substituted for the
       formal arguments [xs]. *)
    (* TODO It would be reasonable to require [args] to be closed,
       i.e., a list of memory locations. *)

| StepAlloc dst n l b σ σ' σ'' :
    σ.(store) !! l = None →
    b = BTuple (replicate n VUnit) →
    σ' = <[l := b]> <$> σ →
    valid_state σ' →
    write_lvalue dst (VLoc l) σ' σ'' →
    head_step
      (IAlloc dst n) σ []
      ISkip σ'' []
    (* [*dst = alloc n] first allocates a fresh block of size [n] in
       the heap, at a fresh address [l], filled with unit values.
       This step is subject to the side condition that the size of
       the heap remain under [maxsize]; if that is not the case,
       this reduction step is not permitted. Then, the address [l]
       is written into the stack cell [dst]. *)

| StepLoad dst src ofs l vs v σ σ' :
    read_lvalue src σ (VLoc l) →
    σ.(store) !! l = Some (BTuple vs) →
    (0 <= ofs < length vs)%nat →
    vs !!! ofs = v →
    write_lvalue dst v σ σ' →
    head_step
      (ILoad dst src ofs) σ []
      ISkip σ' []
    (* [*dst = load *src(ofs)] first finds a location [l] in the stack cell
       [src]. Then, it reads the memory block at address [l] in the heap
       and extracts the field at offset [ofs], yielding a value [v]. Finally,
       the value [v] is written into the stack cell [dst]. *)

| StepStore dst ofs src l vs w σ σ' :
    read_lvalue src σ w →
    read_lvalue dst σ (VLoc l) →
    σ.(store) !! l = Some (BTuple vs) →
    (0 <= ofs < length vs)%nat →
    σ' = <[l := BTuple (<[ ofs := w ]> vs)]> <$> σ →
    head_step
      (IStore dst ofs src) σ []
      ISkip σ' []
    (* *dst(ofs) = *src] first finds a value [w] in the stack cell [src] and
       a location [l] in the stack cell [dst]. Then, it writes the value [w]
       to the field at offset [ofs] in the memory block at address [l] in
       the heap. *)

| StepLocEq dst src1 src2 σ l1 l2 σ' :
    read_lvalue src1 σ (VLoc l1) →
    read_lvalue src2 σ (VLoc l2) →
    write_lvalue dst (VInt (Int63.of_Z (if l1 =? l2 then 1 else 0))) σ σ' →
    head_step
      (ILocEq dst src1 src2) σ []
      ISkip σ' []
    (* [*dst = *src1 == *src2] first two locations [l1] and [l2] in the
       stack cells [src1] and [src2]. Then, it compares these locations,
       yielding an integer result (zero or one), and stores this result
       in the stack cell [dst]. *)

| StepConst dst v σ σ' :
    write_lvalue dst v σ σ' →
    val_pointer_list v = [] →
    head_step
      (IConst dst v) σ []
      ISkip σ' []
    (* [*dst = v] writes the constant [v] to the stack cell [dst].
       The side condition [val_pointer_list v = []] ensures that
       [v] is indeed a constant, that is, not a pointer into the heap. *)

| StepMove dst src σ σ' v :
    read_lvalue src σ v →
    write_lvalue dst v σ σ' →
    head_step
      (IMove dst src) σ []
      ISkip σ' []
    (* [*dst = *src] reads a value from the stack cell [src] and writes
       it into the stack cell [dst]. *)

| StepAllocaEntry x i σ σ' c b :
    σ.(store) !! c = None →
    b = BCell VUnit →
    σ' = <[c := b]> <$> σ →
    head_step
      (IAlloca x i) σ []
      (IAllocaActive c (subst x (LLoc c) i)) σ' []
    (* [alloca x in i] allocates a stack cell at a fresh address [c] and
       reduces to [alloca c in [c/x]i]. The active form [alloca c in _] is
       an evaluation context, so it is expected to eventually reduce to
       [alloca c in skip], which the next reduction rule takes care of. *)

| StepAllocaExit c σ σ' v :
    σ.(store) !! c = Some (BCell v) →
    σ' = <[ c := BDeallocated ]> <$> σ →
    head_step
      (IAllocaActive c ISkip) σ []
      ISkip σ' []
    (* [alloca c in skip] deallocates the stack cell at address [c] and
       reduces to [skip]. *)

| StepFork src x i σ v σ' c b σ'' :
    read_lvalue src σ v →
    σ.(store) !! c = None →
    b = BCell v →
    σ' = <[c := b]> <$> σ →
    write_lvalue src VUnit σ' σ'' →
    head_step
      (IFork src x i) σ []
      ISkip σ'' [IAllocaActive c (subst x (LLoc c) i)]
    (* [fork src x i] reads a value [v] from the stack cell [src]. Then, it
       spawns a new thread, in which the instruction [alloca x = v in i] is
       executed. This an informal description; we actually do not have a
       variant of [alloca] where the stack cell is initialized with a value
       [v], but that is the idea. In the new thread, a new stack cell [c] is
       allocated and initialized with [v]; then, the instruction [alloca c in
       [c/x]i] is executed. Finally, in the original thread, the stack cell
       [src] is overwritten. We build this update into the semantics of [fork]
       because this allows us to propose a simple reasoning rule, where a
       [mapsfrom] assertion for the value [v] is updated and transferred to
       the new thread. *)

.

Global Hint Constructors read_lvalue write_lvalue head_step : head_step.

(* ------------------------------------------------------------------------ *)

(* A few basic facts about this operational semantics. *)

(* [fill_item] is injective in its second argument. *)

Global Instance fill_item_inj Ki :
  Inj (=) (=) (fill_item Ki).
Proof.
  destruct Ki; repeat intro; simpl in *; congruence.
Qed.

(* [fill_item Ki i] is never [skip]. *)

Lemma fill_item_val Ki i :
  is_Some (to_val (fill_item Ki i)) →
  is_Some (to_val i).
Proof.
  intros. elimtype False. eapply is_Some_None.
  destruct Ki; simpl in *; eauto.
Qed.

(* A reducible instruction is never [skip]. *)

Lemma val_head_stuck i1 σ1 κ i2 σ2 fk :
  head_step i1 σ1 κ i2 σ2 fk →
  to_val i1 = None.
Proof.
  destruct 1; reflexivity.
Qed.

(* If [fill_item Ki i] takes a reduction step, then [i] is [skip]. *)

Lemma head_ctx_step_val Ki i σ1 κ i2 σ2 fk :
  head_step (fill_item Ki i) σ1 κ i2 σ2 fk →
  is_Some (to_val i).
Proof.
  destruct Ki; inversion 1; simpl; eauto.
Qed.

(* A unique decomposition lemma. Provided [i1] and [i2] are not values, the
   equality of [Ki1[i1]] and [Ki2[i2]] implies [Ki1 = Ki2], therefore implies
   [i1 = i2] as well. *)

Lemma fill_item_no_val_inj Ki1 Ki2 i1 i2 :
  to_val i1 = None →
  to_val i2 = None →
  fill_item Ki1 i1 = fill_item Ki2 i2 →
  Ki1 = Ki2.
Proof.
  destruct Ki1, Ki2; naive_solver.
Qed.

(* ------------------------------------------------------------------------ *)

(* Notations. *)

Notation dom :=
  (dom (gset loc)).
