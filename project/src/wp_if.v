From spacelang Require Import wp_state_interp wp_memory.

Lemma wp_if `{!heapG Σ} s E src branch1 branch2 k Φ :
  ▷ (src ↦ BCell (VInt k) -∗ WP (if Int63.is_zero k then branch2 else branch1) @ s; E {{ Φ }})
  ⊢ src ↦ BCell (VInt k) -∗ WP (IIf (LLoc src) branch1 branch2) @ s; E {{ Φ }}.
Proof.
  iIntros "HWP Hsrc".
  iApply wp_lift_step; first done.
  iIntros (σ1 ns κ κs n) "Hstate".
  iApply fupd_mask_intro; first set_solver. iIntros "Hclose". iSplit.

  (* First, prove that this instruction is reducible. *)
  { destruct s; [| eauto ].
    destruct_state_interp.
    exploit_stack_cell src.
    head_reducible. }

  (* Then, examine an arbitrary reduction step. *)
  examine_step.
  inv_read_lvalue.
  exploit_stack_cell src. simplify_eq. rename n0 into k. (* TODO *)

  (* Conclude. *)
  iMod "Hclose" as "_".
  iModIntro.
  iSpecialize ("HWP" with "Hsrc").
  iFrame "HWP".
  iSplitL; [| eauto ].
  iExists _. iFrame. eauto.
Qed.
