From stdpp Require Import base gmap gmultiset.
From spacelang Require Export stdpp.
From spacelang Require Export syntax.

(* This file defines stores and machine states. *)

(* ------------------------------------------------------------------------ *)

(* A store is a map of locations to blocks. It represents the union of the
   heap and the stack. *)

Notation STORE :=
  (gmap loc block).

Implicit Types s : STORE.

(* ------------------------------------------------------------------------ *)

(* The size of a block in the heap is measured in words. *)

(* The size of a stack cell is zero, as we are not interested in measuring
   space usage in the stack. *)

Definition size_block (b : block) : nat :=
  match b with
  | BTuple vs => 1 + length vs
  | BCell _ => 0
  | BDeallocated => 0
  end.

(* The size of a store is the sum of the sizes of its blocks.
   It is in fact the size of the heap. *)

Definition size_heap s : nat :=
  map_fold (fun l b acc => acc + size_block b)%nat 0%nat s.

(* ------------------------------------------------------------------------ *)

(* A machine state consists of a store and a natural number [maxsize] that
   represents a hard limit on the size of the heap. This limit is fixed at
   the beginning of the execution of a program, and does not change during
   execution. The operational semantics is designed in such a way that this
   limit cannot be exceeded; an attempt to allocate memory beyond this limit
   causes a runtime error. *)

Record state : Type := {
  store: STORE;
  maxsize: nat;
}.

(* An inhabitation fact. *)

Global Instance state_inhabited : Inhabited state :=
  populate {| store := inhabitant; maxsize := inhabitant |}.

(* ------------------------------------------------------------------------ *)

(* A state is valid if the limit on the size of the heap has not been
   exceeded. This predicate is used to express the dynamic check that is
   performed by [IAlloc]. *)

Definition valid_state (σ : state) : Prop :=
  (size_heap σ.(store) <= σ.(maxsize))%nat.

(* ------------------------------------------------------------------------ *)

(* The initial machine state. *)

Definition init (maxsize : nat) : state :=
  {| store := ∅; maxsize := maxsize |}.

(* ------------------------------------------------------------------------ *)

(* [state_upd_store f σ] applies the function [f] to the store component of
   the state [σ]. *)

Definition state_upd_store f σ :=
  {| store := f σ.(store); maxsize := σ.(maxsize) |}.

Notation "f '<$>' σ" :=
  (state_upd_store f σ).

(* ------------------------------------------------------------------------ *)

(* The available space in the heap is [maxsize] minus the current size of
   the heap. *)

Definition available σ :=
  σ.(maxsize) - size_heap σ.(store).

(* ------------------------------------------------------------------------ *)

(* Lemmas about [size_block]. *)

(* The size of a block is preserved when a field is updated. *)

Lemma size_block_update vs ofs w :
  size_block (BTuple (<[ofs:=w]> vs)) = size_block (BTuple vs).
Proof.
  unfold size_block. rewrite insert_length. eauto.
Qed.

(* ------------------------------------------------------------------------ *)

(* Lemmas about [size_heap]. *)

(* The empty heap has size 0. *)

Lemma size_empty_heap :
  size_heap ∅ = 0.
Proof.
  apply map_fold_empty.
Qed.

(* Allocating a block causes the size of the heap to grow by the size of
   that block. *)

Lemma size_heap_insert_new s l b :
  s !! l = None ->
  size_heap (<[l := b]> s) = size_heap s + size_block b.
Proof.
  intros Hfree.
  unfold size_heap in *.
  rewrite map_fold_insert //. lia.
Qed.

(* Deleting a block causes the size of the heap to shrink by the size of
   that block. (The operational semantics actually never deletes a block,
   but this is still a useful lemma.) *)

Lemma size_heap_delete s l b :
  s !! l = Some b ->
  size_heap (delete l s) + size_block b = size_heap s.
Proof.
  symmetry.
  unfold size_heap in *.
  erewrite map_fold_delete_L; auto.
  lia.
Qed.

(* Updating a block from [b] to [b'] causes the size of the heap to grow
   by the difference between the size of [b'] and the size of [b]. *)

Lemma size_heap_insert_update s l b b' :
  s !! l = Some b ->
  size_heap (<[l := b']> s) + size_block b = size_heap s + size_block b'.
Proof.
  intros Hi.
  pose proof size_heap_insert_new (delete l s) l b' (lookup_delete _ _).
  pose proof size_heap_insert_new (delete l s) l b (lookup_delete _ _).
  pose proof size_heap_delete _ _ _ Hi.
  rewrite <-insert_delete_insert.
  lia.
Qed.

(* In particular, if [b] and [b'] have the same size, then the size of the
   heap is unaffected. *)

Lemma size_heap_insert_same_size s l b b' :
  s !! l = Some b ->
  size_block b = size_block b' ->
  size_heap (<[l := b']> s) = size_heap s.
Proof.
  intros Hi.
  pose proof size_heap_insert_update _ _ _ b' Hi.
  lia.
Qed.

(* Deallocating a block causes the size of the heap to shrink by the
   size of that block. *)

Lemma size_heap_dealloc s l b :
  s !! l = Some b ->
  size_heap (<[l := BDeallocated]> s) + size_block b = size_heap s.
Proof.
  intros Hl.
  pose proof (size_heap_insert_update s l b BDeallocated Hl).
  simpl in *. lia.
Qed.

(* ------------------------------------------------------------------------ *)

(* Lemmas about [valid_state]. *)

(* The initial machine state [init maxsize] is valid. *)

Lemma valid_state_init maxsize :
  valid_state (init maxsize).
Proof.
  unfold valid_state, init.
  rewrite size_empty_heap.
  lia.
Qed.

Global Hint Resolve valid_state_init : valid_state.

(* As stated earlier, updating a block from [b] to [b'] causes the size of
   the heap to grow by the difference between the size of [b'] and the size
   of [b]. Provided the size of the updated heap remains under [maxsize],
   the machine state remains valid. *)

Lemma valid_state_update σ l b b' :
  σ.(store) !! l = Some b ->
  size_block b' + size_heap σ.(store) <= σ.(maxsize) + size_block b ->
  valid_state (<[l := b']> <$> σ).
Proof.
  destruct σ as (s, n); simpl.
  intros Hl Hbound.
  unfold state_upd_store, valid_state; simpl.
  pose proof size_heap_insert_update s l b b' Hl.
  lia.
Qed.

Lemma valid_state_update_tuple σ l vs ofs w :
  σ.(store) !! l = Some (BTuple vs) →
  0 <= ofs < length vs →
  valid_state σ →
  valid_state (<[l := BTuple (<[ofs := w]> vs)]> <$> σ).
Proof.
  unfold valid_state. intros.
  eapply valid_state_update; eauto.
  rewrite size_block_update //.
  lia.
Qed.

Global Hint Resolve valid_state_update_tuple : valid_state.

Lemma valid_state_update_cell σ c v w :
  σ.(store) !! c = Some (BCell v) →
  valid_state σ →
  valid_state (<[c := BCell w]> <$> σ).
Proof.
  unfold valid_state. intros.
  eapply valid_state_update; eauto.
  simpl. lia.
Qed.

Global Hint Resolve valid_state_update_cell : valid_state.

(* Deallocating a block preserves the validity of the machine state. *)

Lemma valid_state_free σ l b :
  valid_state σ →
  σ.(store) !! l = Some b →
  valid_state (<[l:=BDeallocated]> <$> σ).
Proof.
  intros.
  eapply valid_state_update.
  eauto.
  simpl. unfold valid_state in *. lia.
Qed.

Global Hint Resolve valid_state_free : valid_state.

(* Provided there remains sufficient space in the heap, allocating a block
   preserves the validity of the machine state. *)

Lemma valid_state_alloc σ l b :
  σ.(store) !! l = None ->
  size_block b + size_heap σ.(store) <= σ.(maxsize) ->
  valid_state (<[l := b]> <$> σ).
Proof.
  destruct σ as (s, n); simpl.
  intros Hfree Hbound.
  unfold state_upd_store, valid_state; simpl.
  rewrite size_heap_insert_new; auto with lia.
Qed.

(* A reformulation of [valid_state_alloc]. *)

Lemma valid_state_alloc' σ l b :
  valid_state σ →
  size_block b <= available σ →
  store σ !! l = None →
  valid_state (<[l:=b]> <$> σ).
Proof.
  unfold valid_state, available.
  intros. eapply valid_state_alloc.
  - eauto.
  - lia.
Qed.

Global Hint Resolve valid_state_alloc' : valid_state.

(* This helps prove the precondition of the previous lemma. *)

Lemma size_block_BCell v σ :
  size_block (BCell v) <= available σ.
Proof.
  intros. simpl. lia.
Qed.

Global Hint Resolve size_block_BCell : valid_state.

(* Allocating a stack cell preserves the validity of the machine state. *)

Lemma valid_state_alloca σ c :
  σ.(store) !! c = None ->
  valid_state σ →
  valid_state (<[c:=BCell VUnit]> <$> σ).
Proof.
  unfold state_upd_store, valid_state; simpl; intros.
  rewrite size_heap_insert_new; eauto. simpl. lia.
Qed.

Global Hint Resolve valid_state_alloca : valid_state.

(* ------------------------------------------------------------------------ *)

(* Lemmas about [available]. *)

(* The available space in the initial state [init maxsize] is [maxsize]. *)

Lemma available_init maxsize :
  available (init maxsize) = maxsize.
Proof.
  unfold available. simpl. rewrite size_empty_heap. lia.
Qed.

(* Allocating a block decreases the available space by the size of this
   block. *)

Lemma available_alloc σ l b :
  σ.(store) !! l = None →
  available (<[l:=b]> <$> σ) = available σ - size_block b.
Proof.
  unfold available. intros. simpl.
  rewrite size_heap_insert_new //.
  lia.
Qed.

(* Deallocating a block increases the available space by the size of this
   block. *)

Lemma available_dealloc σ l b :
  valid_state σ →
  σ.(store) !! l = Some b →
  available (<[l:=BDeallocated]> <$> σ) =
  available σ + size_block b.
Proof.
  unfold valid_state, available. intros ? H. simpl.
  pose proof (size_heap_dealloc _ _ _ H). lia.
Qed.

(* Updating a block preserves the available space. *)

Lemma available_update σ l b b' :
  σ.(store) !! l = Some b →
  size_block b' = size_block b →
  available (<[l:=b']> <$> σ) = available σ.
Proof.
  unfold available. simpl. intros.
  erewrite size_heap_insert_same_size by eauto.
  eauto.
Qed.

Lemma available_update_tuple σ l vs ofs w :
  (0 <= ofs < length vs)%nat ->
  σ.(store) !! l = Some (BTuple vs) →
  available (<[l:=BTuple (<[ofs:=w]> vs)]> <$> σ) = available σ.
Proof.
  intros. erewrite available_update; eauto using size_block_update.
Qed.

Lemma available_update_cell σ c v w :
  σ.(store) !! c = Some (BCell v) →
  available (<[c:=BCell w]> <$> σ) = available σ.
Proof.
  intros. erewrite available_update; eauto using size_block_update.
Qed.
