From stdpp Require Import gmap.
From iris.program_logic Require Export language ectx_language ectxi_language.
From spacelang Require Export syntax state spacelang pointers successors.

(* This file defines garbage collection in SpaceLang. *)

(* ------------------------------------------------------------------------ *)

(* Most of the work of defining garbage collection is already done in the
   file [successors.v], where the relation [store_evolution] on stores is
   defined. Here, this relation is extended in a trivial way to machine
   states. *)

Definition gc (σ1 σ2 : state) :=
  store_evolution σ1.(store) σ2.(store) ∧
  σ1.(maxsize) = σ2.(maxsize).

(* Similarly, the relation [store_agreement] on stores is extended in a
   trivial way to machine states. *)

Definition related (σ θ : state) :=
  store_agreement σ.(store) θ.(store) ∧
  σ.(maxsize) = θ.(maxsize).

(* ------------------------------------------------------------------------ *)

(* The relation [head_step], which allows one thread to make one step of
   computation, is composed with the relation [gc]. That is, before every
   step of computation, we allow a GC step. This leads to an angelic
   interpretation of the GC's nondeterminism: if there is a way of updating
   the machine state (by freeing memory) so as to avoid a failure of the
   next computation step, then it can be chosen, so the potential failure
   becomes invisible. *)

(* In Iris, the relation [head_step] represents one step of computation by
   one thread, under an unspecified evaluation context. Fortunately, the
   entire machine state is visible to [head_step], so it is possible for one
   thread to perform garbage collection on behalf of all threads, even
   though it is not known what code these other threads are currently
   executing. *)

Inductive gc_head_step :
  instr → state → list observation →
  instr → state → list instr → Prop
:=
| GCHeadStep i1 σ1 σ'1 κ i2 σ2 fk :
    gc σ1 σ'1 →
    head_step i1 σ'1 κ i2 σ2 fk →
    gc_head_step i1 σ1 κ i2 σ2 fk.

(* ------------------------------------------------------------------------ *)

(* Basic lemmas about [gc] and [gc_head_step]. *)

(* The GC may have no effect. *)

Lemma gc_id σ :
  gc σ σ.
Proof.
  econstructor; eauto using store_evolution_reflexive.
Qed.

(* These lemmas build on top of those by the same names in [spacelang.v]. *)

Lemma val_head_stuck i1 σ1 κ i2 σ2 fk :
  gc_head_step i1 σ1 κ i2 σ2 fk →
  to_val i1 = None.
Proof.
  inversion_clear 1. eauto using spacelang.val_head_stuck.
Qed.

Lemma head_ctx_step_val Ki i σ1 κ i2 σ2 fk :
  gc_head_step (fill_item Ki i) σ1 κ i2 σ2 fk →
  is_Some (to_val i).
Proof.
  inversion_clear 1. eauto using spacelang.head_ctx_step_val.
Qed.

(* ------------------------------------------------------------------------ *)

(* Instantiate Iris for SpaceLang with GC. *)

Lemma spacelang_mixin :
  EctxiLanguageMixin of_val to_val fill_item gc_head_step.
Proof.
  econstructor.
  { eauto using to_of_val. }
  { eauto using of_to_val. }
  { eauto using val_head_stuck. }
  { eauto using spacelang.fill_item_val. }
  { eauto using spacelang.fill_item_inj. }
  { eauto using spacelang.fill_item_no_val_inj. }
  { eauto using head_ctx_step_val. }
Qed.

Canonical Structure spacelang_ectxi := EctxiLanguage spacelang_mixin.
Canonical Structure spacelang_ectx := EctxLanguageOfEctxi spacelang_ectxi.
Canonical Structure spacelang := LanguageOfEctx spacelang_ectx.
