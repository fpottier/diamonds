From spacelang Require Import wp_state_interp wp_memory.

Lemma wp_move_dep `{!heapG Σ} (precise:bool) s E (dst src : loc) (v w : value) qv lsv qw lsw :
  {{{
    dst ↦ BCell v ∗
    src ↦ BCell w ∗
    (if precise then v ↤?{qv} lsv else ⌜True⌝) ∗
    w ↤?{qw} lsw
  }}}
    IMove (LLoc dst) (LLoc src)
    @ s; E
  {{{ RET tt;
    dst ↦ BCell w ∗
    src ↦ BCell w ∗
    (if precise then v ↤?{qv} (lsv ∖ {[+dst+]})  else ⌜True⌝) ∗
    w ↤?{qw} (lsw ⊎ {[+dst+]})
  }}}.
Proof.
  iIntros (Φ) "(Hdst & Hsrc & pv & pw) HΦ".
  iApply wp_lift_atomic_head_step_no_fork; first done.
  iIntros (σ1 ns κ κs n) "Hstate".
  iModIntro. iSplit.

  (* First, prove that this instruction is reducible. *)
  { destruct_state_interp.
    exploit_stack_cell src.
    exploit_stack_cell dst.
    head_reducible. }

  (* Then, examine an arbitrary reduction step. *)
  examine_step.
  inv_read_lvalue.
  exploit_stack_cell src.
  assert (v0 = w) as -> by congruence. (* TODO *)
  inv_write_lvalue.
  exploit_stack_cell dst.
  assert (v0 = v) as -> by congruence. (* TODO *)

  iMod (ph_store_stack_dep precise (store θ) dst v w
    with "[$Hph $Hdst $pv $pw]")
      as "(Hph & Hdst & pv & pw)".
  { assumption. }

  iModIntro.
  iSpecialize ("HΦ" with "[$Hsrc $Hdst $pv $pw]").
  iSplit; auto.
  iFrame "HΦ".
  iExists (<[dst:=BCell w]> <$> θ). iFrame "Hph".
  erewrite available_update_cell by eauto. iFrame "Hauth".
  iPureIntro.
  splits; eauto 4 with valid_state closed related reachable.
Qed.

Lemma wp_move `{!heapG Σ} s E (dst src : loc) (v w : value) qv lsv qw lsw :
  {{{
    dst ↦ BCell v ∗
    src ↦ BCell w ∗
    v ↤?{qv} lsv ∗
    w ↤?{qw} lsw
  }}}
    IMove (LLoc dst) (LLoc src)
    @ s; E
  {{{ RET tt;
    dst ↦ BCell w ∗
    src ↦ BCell w ∗
    v ↤?{qv} (lsv ∖ {[+dst+]}) ∗
    w ↤?{qw} (lsw ⊎ {[+dst+]})
  }}}.
Proof.
  apply (wp_move_dep true).
Qed.

Lemma wp_move_weak `{!heapG Σ} s E (dst src : loc) (v w : value) qw lsw :
  {{{
    dst ↦ BCell v ∗
    src ↦ BCell w ∗
    w ↤?{qw} lsw
  }}}
    IMove (LLoc dst) (LLoc src)
    @ s; E
  {{{ RET tt;
    dst ↦ BCell w ∗
    src ↦ BCell w ∗
    w ↤?{qw} (lsw ⊎ {[+dst+]})
  }}}.
Proof.
  derive_wp_from (wp_move_dep false).
Qed.
