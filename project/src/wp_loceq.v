From spacelang Require Import wp_state_interp wp_memory.

Lemma wp_loceq_dep `{!heapG Σ} (precise:bool) s E dst src1 src2 l1 l2 v q ls :
  {{{
    src1 ↦ BCell (VLoc l1) ∗ src2 ↦ BCell (VLoc l2) ∗
    dst ↦ BCell v ∗
    (if precise then v ↤?{q} ls else ⌜True⌝)
  }}}
    (ILocEq (LLoc dst) (LLoc src1) (LLoc src2)) @ s; E
  {{{
    RET ();
    src1 ↦ BCell (VLoc l1) ∗ src2 ↦ BCell (VLoc l2) ∗
    dst ↦ BCell (VInt (Int63.of_Z (if l1 =? l2 then 1 else 0))) ∗
    (if precise then v ↤?{q} (ls ∖ {[+dst+]}) else ⌜True⌝)
  }}}.
Proof.
  iIntros (Φ) "(Hsrc1 & Hsrc2 & Hdst & Hv) Hpost".
  iApply wp_lift_atomic_head_step_no_fork; first done.
  iIntros (σ1 ns κ κs n) "Hstate".
  iModIntro. iSplit.

  (* First, prove that this instruction is reducible. *)
  { destruct_state_interp.
    exploit_stack_cell dst.
    exploit_stack_cell src1.
    exploit_stack_cell src2.
    head_reducible. }

  (* Then, examine an arbitrary reduction step. *)
  examine_step.
  inv_read_lvalue. exploit_stack_cell src2. simplify_eq.
    rename l3 into l2. (* TODO *)
  inv_read_lvalue. exploit_stack_cell src1. simplify_eq.
    rename l0 into l1. (* TODO *)
  exploit_stack_cell dst. simplify_eq.
  set (w := VInt (Int63.of_Z (if l1 =? l2 then 1 else 0))).
  assert (val_pointer_set w = ∅) by eauto.
  (* Update the conceptual store by writing [w] at address [dst]. *)
  iMod (ph_store_stack_dep precise (store θ) dst v w q ls 1%Qp ∅
    with "[$]")
      as "(Hph & Hdst & Hv & _)".
  { eauto. }

  (* Conclude. *)
  iModIntro.
  iSplitR; [ eauto |].
  iSplitL "Hauth Hph".
  { iExists (<[dst:=BCell w]> <$> θ).
    iFrame "Hph".
    erewrite available_update_cell by eauto. iFrame "Hauth".
    iPureIntro. splits; eauto 3 with valid_state closed related. }
  { simpl. iApply "Hpost". iFrame. }
Qed.

Lemma wp_loceq `{!heapG Σ} s E dst src1 src2 l1 l2 v q ls :
  {{{
    src1 ↦ BCell (VLoc l1) ∗ src2 ↦ BCell (VLoc l2) ∗
    dst ↦ BCell v ∗
    v ↤?{q} ls
  }}}
    (ILocEq (LLoc dst) (LLoc src1) (LLoc src2)) @ s; E
  {{{
    RET ();
    src1 ↦ BCell (VLoc l1) ∗ src2 ↦ BCell (VLoc l2) ∗
    dst ↦ BCell (VInt (Int63.of_Z (if l1 =? l2 then 1 else 0))) ∗
    v ↤?{q} (ls ∖ {[+dst+]})
  }}}.
Proof.
  apply (wp_loceq_dep true).
Qed.

Lemma wp_loceq_weak `{!heapG Σ} s E dst src1 src2 l1 l2 v :
  {{{
    src1 ↦ BCell (VLoc l1) ∗ src2 ↦ BCell (VLoc l2) ∗
    dst ↦ BCell v
  }}}
    (ILocEq (LLoc dst) (LLoc src1) (LLoc src2)) @ s; E
  {{{
    RET ();
    src1 ↦ BCell (VLoc l1) ∗ src2 ↦ BCell (VLoc l2) ∗
    dst ↦ BCell (VInt (Int63.of_Z (if l1 =? l2 then 1 else 0)))
  }}}.
Proof.
  derive_wp_from (wp_loceq_dep false).
Qed.
