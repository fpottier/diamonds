From iris.program_logic Require Import adequacy.
From iris.proofmode Require Import proofmode.
From spacelang Require Import wp_state_interp.

(* This module proves the adequacy (i.e. the soundness) of Iris for
   SpaceLang. *)

(* ------------------------------------------------------------------------ *)

(* Boilerplate. *)

Class heapPreG Σ := HeapPreG {
  heapPreG_inv  :> invGpreS Σ;
  heapPreG_heap :> Initialization.phPreG loc block Σ;
  heapPreG_inG  :> inG Σ (authR natUR);
}.

(* This is our closed-world Σ. *)

Local Definition heapΣ : gFunctors :=
  #[invΣ; Initialization.phΣ loc block; GFunctor (authR natUR)].

(* These instance declarations are (implicitly) used in the proof of the
   adequacy lemma that follows. *)

Local Instance subG_heapPreG {Σ} :
  subG heapΣ Σ -> heapPreG Σ.
Proof. solve_inG. Qed.

Local Instance heapPreG_heapΣ : heapPreG heapΣ.
Proof. eauto with typeclass_instances. Qed.

(* ------------------------------------------------------------------------ *)

(* A first adequacy lemma. If (under an arbitrary Σ and an arbitrary
   instance of [heapG Σ]) one can prove [WP i Q], then (starting from an
   initial machine state [σ] of the form [init maxsize]) the instruction [i]
   is adequate with respect to [Q]. This means that 1- if [i/σ] terminates
   then the final state satisfies [Q]; and 2- [i/σ] cannot become stuck, or
   spawn a thread that becomes stuck. *)

Lemma preliminary_adequacy_open Σ `{!heapPreG Σ} s i maxsize (Q : Prop) :
  let σ := init maxsize in
  (∀ `{!heapG Σ}, ⊢ ♢maxsize -∗ WP i @ s; ⊤ {{ _, ⌜Q⌝ }}) ->
  adequate s i σ (λ (_: unit) _, Q).
Proof.
  intros ? Hwp.
  (* Apply Iris's generic adequacy theorem. *)
  eapply (wp_adequacy _ _). iIntros (Hinv κs) "".
  (* Initialize the heap. *)
  iMod (Initialization.ph_init) as (?) "Hph".
  (* Initialize the space credits. This gives rise to an authoritative
     element and a fragmentary element. The authoritative element is
     used to prove that we have successfully established our state
     interpretation invariant. The fragmentary element is passed to
     the user as diamonds. *)
  iMod (auth_nat_alloc (available σ)) as (γ) "[Hauth Hfrag]".
  iModIntro.
  (* Yes, it looks as though the state interpretation predicate defined in
     wp.v is duplicated here. However, removing the duplication is
     difficult; we have tried and failed. *)
  iExists (fun σ κs =>
    ∃θ,
    ⌜valid_state σ⌝ ∗
    ⌜valid_state θ⌝ ∗
    ph_interp θ.(store) ∗
    own γ (●nat (available θ)) ∗
    ⌜ closed σ.(store) ⌝ ∗
    ⌜ closed θ.(store) ⌝ ∗
    ⌜ related σ θ ⌝
  )%I.
  iExists (λ _, True)%I.
  iSplitR "Hfrag".
  { (* The physical and conceptual states initially coincide. *)
    iExists σ.
    iFrame "Hauth Hph".
    iPureIntro. rewrite /σ.
    splits; eauto using closed_init with valid_state related. }
  { (* We have the required diamonds! *)
    iApply (Hwp (HeapG _ _ _ _ _)).
    rewrite available_init. iFrame. }
Qed.

(* Same as above, without the hypothesis [heapPreG Σ]. *)

Local Lemma preliminary_adequacy_closed s i maxsize Q :
  let σ := init maxsize in
  (∀ `{!heapG Σ}, ⊢ ♢maxsize -∗ WP i @ s; ⊤ {{ _, ⌜Q⌝ }}) ->
  adequate s i σ (λ (_: unit) _, Q).
Proof.
  intros.
  eapply preliminary_adequacy_open; eauto with typeclass_instances.
Qed.

(* By instantiating [σ] with the initial machine state [init], by
   instantiating [Q] with a trivial postcondition, and by expanding
   the definition of [adequate], we obtain: if (under an arbitrary Σ
   and an arbitrary instance of [heapG Σ]) one can prove [♢ maxsize
   -∗ WP i {{ _, True }}], then the configuration [i / init maxsize]
   is safe, that is, it cannot become stuck or spawn a thread that
   becomes stuck. *)

Theorem adequacy i maxsize :
  (∀ `{!heapG Σ}, ⊢ ♢maxsize -∗ WP i {{ _, True }}) ->
  forall threads σ,
  rtc erased_step ([i], init maxsize) (threads, σ) ->
  forall thread,
  thread ∈ threads ->
  not_stuck thread σ.
Proof.
  intros.
  (* [s] is instantiated with [NotStuck]. *)
  eapply adequate_not_stuck; eauto.
  eapply preliminary_adequacy_closed with (Q := True).
  assumption.
Qed.
