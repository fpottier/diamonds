From spacelang Require Import wp_state_interp wp_memory.

Lemma wp_const_dep `{!heapG Σ} (precise:bool) s E (dst : loc) (v w : value) qv lsv :
  val_pointer_list w = [] ->
  {{{ dst ↦ BCell v ∗
      (if precise then v ↤?{qv} lsv else ⌜True⌝)
  }}}
    IConst (LLoc dst) w
    @ s; E
  {{{ RET ();
      dst ↦ BCell w ∗
      (if precise then v ↤?{qv} (lsv ∖ {[+dst+]}) else ⌜True⌝)
  }}}.
Proof.
  iIntros (Hw Φ) "(Hdst & pv) HΦ".
  iApply wp_lift_atomic_head_step_no_fork; first done.
  iIntros (σ1 ns κ κs n) "Hstate".
  iModIntro. iSplit.

  (* First, prove that this instruction is reducible. *)
  { destruct_state_interp.
    exploit_stack_cell dst.
    head_reducible. }

  (* Then, examine an arbitrary reduction step. *)
  examine_step.
  exploit_stack_cell dst.
  iMod (ph_store_stack_dep precise (store θ) dst v w _ _ 1%Qp ∅
    with "[$Hph $Hdst $pv]")
      as "(Hph & Hdst & pv & _)".
  { assumption. }
  { destruct w; auto. discriminate. }

  inv_write_lvalue. simplify_eq.
  iModIntro.
  iSplit; auto.
  iSpecialize ("HΦ" with "[$Hdst $pv]").
  iFrame "HΦ".
  iExists (<[dst:=BCell w]> <$> θ). iFrame "Hph".
  erewrite available_update_cell by eauto. iFrame "Hauth".
  iPureIntro.
  splits; eauto 3 with valid_state closed related.
Qed.

Lemma wp_const `{!heapG Σ} s E (dst : loc) (v w : value) qv lsv :
  val_pointer_list w = [] ->
  {{{ dst ↦ BCell v ∗
      v ↤?{qv} lsv
  }}}
    IConst (LLoc dst) w
    @ s; E
  {{{ RET ();
      dst ↦ BCell w ∗
       v ↤?{qv} (lsv ∖ {[+dst+]})
  }}}.
Proof.
  apply (wp_const_dep true).
Qed.

Lemma wp_const_weak `{!heapG Σ} s E (dst : loc) (v w : value) :
  val_pointer_list w = [] ->
  {{{ dst ↦ BCell v}}}
    IConst (LLoc dst) w
    @ s; E
  {{{ RET (); dst ↦ BCell w}}}.
Proof.
  intros ?. derive_wp_from (wp_const_dep false).
Qed.
