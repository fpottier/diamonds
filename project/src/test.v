From spacelang Require Import proofmode notation.

Section proof.

  Context `{!heapG Σ}.

  Definition nop : value :=
    (λ: [[]] , skip)%V.

  Theorem nop_spec (callee : loc) :
    {{{ callee ↦ BCell nop }}}
      callee [[]]%V
    {{{ RET tt; callee ↦ BCell nop }}}.
  Proof.
    iIntros (Φ) "? Post".
    wp_call.
    wp_skip.
    iApply "Post".
    iFrame.
    auto.
  Qed.

  Definition thunked_move : value :=
    (λ: [["dst", "src"]], IMove "dst" "src")%V.

  Theorem move_examplespec (callee dst src : loc) (v w : value) qv lsv qw lsw :
  {{{
    callee ↦ BCell thunked_move ∗
    dst ↦ BCell v ∗
    src ↦ BCell w ∗
    v ↤?{qv} lsv ∗
    w ↤?{qw} lsw
  }}}
    callee [[dst, src]]%V
  {{{ RET tt;
    dst ↦ BCell w ∗
    src ↦ BCell w ∗
    v ↤?{qv} (lsv ∖ {[+dst+]}) ∗
    w ↤?{qw} (lsw ⊎ {[+dst+]})
  }}}.
  Proof.
    iIntros (Φ) "(? & Hdst & Hsrc & pv & pw) Post".
    wp_call.
    wp_move.
    iApply "Post".
    iFrame.
  Qed.

  Theorem swap_cell (a b : loc) (v w : value) qv lsv qw lsw :
    {{{
      a ↦ BCell v ∗
      b ↦ BCell w ∗
      v ↤?{qv} lsv ∗
      w ↤?{qw} lsw
    }}}
      (with: "tmp":
        IMove "tmp" a;;
        IMove a b;;
        IMove b "tmp"
      end)%E
    {{{ RET tt;
      a ↦ BCell w ∗
      b ↦ BCell v ∗
      v ↤?{qv} ((lsv ∖ {[+a+]}) ⊎ {[+b+]}) ∗
      w ↤?{qw} ((lsw ⊎ {[+a+]}) ∖ {[+b+]})
    }}}.
  Proof.
    iIntros (Φ) "(Ha & Hb & pv & pw) Post".
    wp_with.
    wp_move.
    wp_move.
    wp_move.
    prove_neq tmp b. (* need [tmp ≠ b] for multiset rewriting *)
    wp_end_with.
    iApply "Post".
    iFrame.
    cancel_mapsfrom.
  Qed.

  Theorem load_simple_example1 (dst src l : loc) (v w : value) qv lsv :
    {{{
      dst ↦ BCell VUnit ∗
      src ↦ BCell (VLoc l) ∗
      l ↦ BTuple [v; w] ∗
      v ↤?{qv} lsv
    }}}
      (ILoad dst src 0)
    {{{ RET tt;
      dst ↦ BCell v ∗
      src ↦ BCell (VLoc l) ∗
      l ↦ BTuple [v; w] ∗
      v ↤?{qv} (lsv ⊎ {[+dst+]})
    }}}.
  Proof.
    iIntros (Φ) "(Hdst & Hsrc & Hl & pv) Post".
    wp_load.
    iApply "Post".
    iFrame.
  Qed.

  Theorem load_simple_example2 (dst src l : loc) (v1 v2 w : value) qv1 lsv1 qw lsw :
    {{{
      dst ↦ BCell w ∗
      src ↦ BCell (VLoc l) ∗
      l ↦ BTuple [v1; v2] ∗
      v1 ↤?{qv1} lsv1 ∗
      w ↤?{qw} lsw
    }}}
      (ILoad dst src 0);;
      skip
    {{{ RET tt;
      dst ↦ BCell v1 ∗
      src ↦ BCell (VLoc l) ∗
      l ↦ BTuple [v1; v2] ∗
      v1 ↤?{qv1} (lsv1 ⊎ {[+dst+]}) ∗
      w ↤?{qw} (lsw ∖ {[+dst+]})
    }}}.
  Proof.
    iIntros (Φ) "? Post".
    destruct_stars.
    wp_load.
    wp_skip.
    iApply "Post".
    iFrame.
    auto.
  Qed.

  Theorem store_simple_example (dst src l l' : loc) (v w : value) qv lsv :
    {{{
      dst ↦ BCell (VLoc l) ∗
      src ↦ BCell ()%V ∗
      l ↦ BTuple [v; w] ∗
      v ↤?{qv} lsv
    }}}
      (IStore dst 0 src)
    {{{ RET tt;
      dst ↦ BCell (VLoc l) ∗
      src ↦ BCell ()%V ∗
      l ↦ BTuple [(); w] ∗
      v ↤?{qv} (lsv ∖ {[+l+]})
    }}}.
  Proof.
    iIntros (Φ) "? Post".
    destruct_stars.
    wp_store.
    iApply "Post".
    iFrame.
  Qed.

  Theorem alloc_simple_example (dst : loc) :
    {{{
      dst ↦ BCell ()%V ∗
      ♢6
    }}}
      (IAlloc dst 4)
    {{{ l, RET tt;
      dst ↦ BCell (VLoc l) ∗
      l ↦ BTuple [(); (); (); ()] ∗
      ♢1
    }}}.
  Proof.
    iIntros (Φ) "? Post".
    destruct_stars.
    wp_alloc.
    iApply "Post".
    iFrame.
  Qed.

  Theorem init_simple_example (dst r1 r2 : loc) (n1 n2 : Int63.int) (w : value) qw lsw :
    {{{
      r1 ↦ BCell (VInt n1) ∗
      r2 ↦ BCell (VInt n2) ∗
      dst ↦ BCell w ∗
      w ↤?{qw} lsw ∗
      ♢6
    }}}
      dst <:- init [[r1, r2]]%V
    {{{ l, RET tt;
      r1 ↦ BCell (VInt n1) ∗
      r2 ↦ BCell (VInt n2) ∗
      dst ↦ BCell (VLoc l) ∗
      w ↤?{qw} (lsw ∖ {[+dst+]}) ∗
      l ↦ BTuple [VInt n1; VInt n2] ∗
      l ↤ {[+dst+]} ∗
      ♢3
    }}}.
  Proof.
    iIntros (Φ) "? Post".
    destruct_stars.
    wp_init.
    iApply "Post".
    now iFrame.
  Qed.

  Theorem free_simple_example (l : loc) :
    {{{
      l ↦ BTuple [(); (); ()] ∗
      l ↤ ∅ ∗
      ♢ 3
    }}}
      skip;; skip
    {{{ RET tt;
      ♢ 7
    }}}.
  Proof.
    iIntros (Φ) "? Post".
    destruct_stars.
    wp_free l.
    wp_skip.
    wp_skip.
    iApply "Post".
    iFrame. auto.
  Qed.

  Definition swap : value :=
    (λ: [["x", "y"]],
       with: "vx":
         with: "vy":
           "vx" <?- "x"?0;;
           "vy" <?- "y"?0;;
           "x"!0 <!- "vy";;
           "y"!0 <!- "vx"
         end
       end)%V.

  Theorem swap_spec_vv (callee c1 c2 l1 l2 : loc) (v1 v2 : value) q1 q2 :
    {{{ callee ↦ BCell swap ∗
        c1 ↦ BCell (VLoc l1) ∗ c2 ↦ BCell (VLoc l2) ∗
        l1 ↦ BTuple [v1] ∗ l2 ↦ BTuple [v2] ∗
        v1 ↤?{q1} {[+l1+]} ∗ v2 ↤?{q2} {[+l2+]} }}}
      callee [[c1, c2]]%V
    {{{ RET tt;
        callee ↦ BCell swap ∗
        c1 ↦ BCell (VLoc l1) ∗ c2 ↦ BCell (VLoc l2) ∗
        l1 ↦ BTuple [v2] ∗ l2 ↦ BTuple [v1] ∗
        v1 ↤?{q1} {[+l2+]} ∗ v2 ↤?{q2} {[+l1+]} }}}.
  Proof.
    iIntros (Φ) "? Post".
    destruct_stars.
    (* iIntros (Φ) "(Hcallee & Hc1 & Hc2 & Hl1 & Hl2 & pv1 & pv2) Post". *)
    wp_call.
    wp_with.
    wp_with.
    wp_load.
    wp_load.
    wp_store.
    wp_store.
    wp_end_with.
    wp_end_with.
    iApply "Post".
    iFrame.
    cancel_mapsfrom.
  Qed.

  Theorem swap_spec_ll (callee c1 c2 l1 l2 m1 m2 : loc) q1 q2 :
    let v1 := VLoc m1 in
    let v2 := VLoc m2 in
    {{{ callee ↦ BCell swap ∗
        c1 ↦ BCell (VLoc l1) ∗ c2 ↦ BCell (VLoc l2) ∗
        l1 ↦ BTuple [v1] ∗ l2 ↦ BTuple [v2] ∗
        m1 ↤{q1} {[+l1+]} ∗ m2 ↤{q2} {[+l2+]} }}}
      callee [[c1, c2]]%V
    {{{ RET tt;
        callee ↦ BCell swap ∗
        c1 ↦ BCell (VLoc l1) ∗ c2 ↦ BCell (VLoc l2) ∗
        l1 ↦ BTuple [v2] ∗ l2 ↦ BTuple [v1] ∗
        m1 ↤{q1} {[+l2+]} ∗ m2 ↤{q2} {[+l1+]} }}}.
  Proof.
    intros v1 v2; subst v1 v2.
    iIntros (Φ) "? Post".
    destruct_stars.
    wp_call.
    wp_with.
    wp_with.
    wp_load.
    wp_load.
    wp_store.
    wp_store.
    wp_end_with.
    wp_end_with.
    iApply "Post".
    iFrame.
    cancel_mapsfrom.
  Qed.

  Theorem swap_spec_lu (callee c1 c2 l1 l2 m1 : loc) q1 :
    let v1 := VLoc m1 in
    let v2 := VUnit in
    {{{ callee ↦ BCell swap ∗
        c1 ↦ BCell (VLoc l1) ∗ c2 ↦ BCell (VLoc l2) ∗
        l1 ↦ BTuple [v1] ∗ l2 ↦ BTuple [v2] ∗
        m1 ↤{q1} {[+l1+]} }}}
      callee [[c1, c2]]%V
    {{{ RET tt;
        callee ↦ BCell swap ∗
        c1 ↦ BCell (VLoc l1) ∗ c2 ↦ BCell (VLoc l2) ∗
        l1 ↦ BTuple [v2] ∗ l2 ↦ BTuple [v1] ∗
        m1 ↤{q1} {[+l2+]} }}}.
  Proof.
    intros v1 v2; subst v1 v2.
    iIntros (Φ) "? Post".
    destruct_stars.
    wp_call.
    wp_with.
    wp_with.
    wp_load.
    wp_load.
    wp_store.
    wp_store.
    wp_end_with.
    wp_end_with.
    iApply "Post".
    iFrame.
    cancel_mapsfrom.
  Qed.

  Theorem swap_spec_ul (callee c1 c2 l1 l2 m2 : loc) q2 :
    let v1 := VUnit in
    let v2 := VLoc m2 in
    {{{ callee ↦ BCell swap ∗
        c1 ↦ BCell (VLoc l1) ∗ c2 ↦ BCell (VLoc l2) ∗
        l1 ↦ BTuple [v1] ∗ l2 ↦ BTuple [v2] ∗
        m2 ↤{q2} {[+l2+]} }}}
      callee [[c1, c2]]%V
    {{{ RET tt;
        callee ↦ BCell swap ∗
        l1 ↦ BTuple [v2] ∗ l2 ↦ BTuple [v1] ∗
        m2 ↤{q2} {[+l1+]} }}}.
  Proof.
    intros v1 v2; subst v1 v2.
    iIntros (Φ) "? Post".
    destruct_stars.
    wp_call.
    wp_with.
    wp_with.
    wp_load.
    wp_load.
    wp_store.
    wp_store.
    wp_end_with.
    wp_end_with.
    iApply "Post".
    iFrame.
    cancel_mapsfrom.
  Qed.

  Theorem swap_spec_uu (callee c1 c2 l1 l2 : loc) :
    let v1 := VUnit in
    let v2 := VUnit in
    {{{ callee ↦ BCell swap ∗
        c1 ↦ BCell (VLoc l1) ∗ c2 ↦ BCell (VLoc l2) ∗
        l1 ↦ BTuple [v1] ∗ l2 ↦ BTuple [v2] }}}
      callee [[c1, c2]]%V
    {{{ RET tt;
        callee ↦ BCell swap ∗
        l1 ↦ BTuple [v2] ∗ l2 ↦ BTuple [v1] }}}.
  Proof.
    intros v1 v2; subst v1 v2.
    iIntros (Φ) "? Post".
    destruct_stars.
    wp_call.
    wp_with.
    wp_with.
    wp_load.
    wp_load.
    wp_store.
    wp_store.
    wp_end_with.
    wp_end_with.
    iApply "Post".
    iFrame.
  Qed.

  Definition copy : value :=
    (λ: [["dst", "src"]],
    with: "vx":
      "vx" <?- "src"?0;;
      "dst" <:- init [["vx"]]%V;;
      (* In order to logically free the value pointed by ["vx"], we
         need to overwrite ["src"], otherwise it stays reachable. *)
      IConst "src" VUnit;;
      skip
    end)%V.

  (* Copying a unary tuple, keeping the old location reachable: we
     cannot get our space credits back *)

  Theorem copy_no_space_credits (callee dst src l : loc) v q ls :
    l ∈ ls →
    {{{ callee ↦ BCell copy ∗
        dst ↦ BCell VUnit ∗
        src ↦ BCell (#l)%V ∗
        l ↦ BTuple [v] ∗
        l ↤ {[+src+]} ∗
        v ↤?{q} ls }}}
      callee [[dst, src]]%V
    {{{ l', RET tt;
        callee ↦ BCell copy ∗
        dst ↦ BCell (#l')%V ∗
        src ↦ BCell (#l)%V ∗
        l' ↦ BTuple [v] ∗
        †l ∗
        v ↤?{q} (ls ∖ {[+l+]} ⊎ {[+l'+]}) ∗
        ♢2 }}}.
  Proof.
    iIntros (? Φ) "? Post".
    destruct_stars.
    (* iIntros (? Φ) "(Hcallee & Hdst & Hsrc & Hl & pl & pv) Post". *)
    wp_call.
    wp_with.
    wp_load.
    Fail wp_alloc l' as "(Hl' & pl' & Hdst & pw & Hd)".
    (* Appropriate error message:
       "wp_alloc: could not find any space credits in hypothesis." *)
  Abort.

  (* Copying a unary tuple, keeping the old location reachable *)

  Theorem copy_spec_no_free (callee dst src l : loc) v ql lsl qv lsv :
    l ∈ lsv →
    {{{ callee ↦ BCell copy ∗
        dst ↦ BCell VUnit ∗
        src ↦ BCell (#l)%V ∗
        l ↦ BTuple [v] ∗
        l ↤{ql} lsl ∗
        v ↤?{qv} lsv ∗
        ♢2 }}}
      callee [[dst, src]]%V
    {{{ l', RET tt;
        callee ↦ BCell copy ∗
        dst ↦ BCell (#l')%V ∗
        src ↦ BCell ()%V ∗
        l ↤{ql} (lsl ∖ {[+src+]}) ∗
        l' ↦ BTuple [v] ∗
        v ↤?{qv} (lsv ⊎ {[+l'+]})
        (* no space credits remaining *) }}}.
  Proof.
    iIntros (? Φ) "? Post".
    destruct_stars.
    (* iIntros (? Φ) "(Hcallee & Hdst & Hsrc & Hl & pl & pv & dia) Post". *)
    wp_call.
    wp_with.
    wp_load.
    wp_init l'.
    wp_skip.
    wp_const.
    wp_skip.
    wp_skip.
    iModIntro.
    wp_end_with.
    iApply "Post".
    iFrame.
    cancel_mapsfrom.
  Qed.

  (* Copying a unary tuple, when the old location is unreachable: we
     can free the old location and get space credits back *)

  Theorem copy_spec (callee dst src l : loc) (v : value) q ls :
    l ∈ ls →
    {{{ callee ↦ BCell copy ∗
        dst ↦ BCell VUnit ∗
        src ↦ BCell (#l)%V ∗
        l ↦ BTuple [v] ∗
        l ↤ {[+src+]} ∗
        v ↤?{q} ls ∗
        ♢2 }}}
      callee [[dst, src]]%V
    {{{ l', RET tt;
        callee ↦ BCell copy ∗
        dst ↦ BCell (#l')%V ∗
        src ↦ BCell ()%V ∗
        l' ↦ BTuple [v] ∗
        †l ∗
        v ↤?{q} (ls ∖ {[+l+]} ⊎ {[+l'+]}) ∗
        ♢2 }}}.
  Proof.
    iIntros (? Φ) "? Post".
    destruct_stars.
    (* iIntros (? Φ) "(? & ? & ? & Hl & pl & pv & dia) Post". *)
    wp_call.
    wp_with.
    wp_load.
    wp_init l'.
    prove_neq l l'. (* for multiset rewriting *)
    wp_skip.
    wp_const.
    autorewrite with gmultiset_simplify.
    wp_free l.
    wp_cleanup l in v.
    wp_skip.
    wp_skip.
    iModIntro.
    wp_end_with.
    iApply "Post".
    iFrame.
    cancel_mapsfrom.
  Qed.

  (** The proof is the same as above (except that [m] is changed to [v] once) *)
  Theorem copy_spec_loc (callee dst src l : loc) (m : loc) q ls :
    l ∈ ls →
    {{{ callee ↦ BCell copy ∗
        dst ↦ BCell VUnit ∗
        src ↦ BCell (#l)%V ∗
        l ↦ BTuple [VLoc m] ∗
        l ↤ {[+src+]} ∗
        m ↤{q} ls ∗
        ♢2 }}}
      callee [[dst, src]]%V
    {{{ l', RET tt;
        callee ↦ BCell copy ∗
        dst ↦ BCell (#l')%V ∗
        src ↦ BCell ()%V ∗
        l' ↦ BTuple [VLoc m] ∗
        l' ↤ {[+dst+]} ∗
        †l ∗
        m ↤{q} (ls ∖ {[+l+]} ⊎ {[+l'+]}) ∗
        ♢2 }}}.
  Proof.
    iIntros (? Φ) "(Hcallee & Hdst & Hsrc & Hl & pl & pv & dia) Post".
    wp_call.
    wp_with.
    wp_load.
    wp_init l'.
    prove_neq l l'. (* for multiset rewriting *)
    wp_skip.
    wp_const.
    autorewrite with gmultiset_simplify.
    wp_free l.
    wp_cleanup l in m.
    wp_skip.
    wp_skip.
    iModIntro.
    wp_end_with.
    iApply "Post".
    iFrame.
    cancel_mapsfrom.
  Qed.

  Definition test : value :=
    (λ: [["dst", "src"]],
      (with: "tmp":
        "tmp" <:- init [["src", "src"]];;
        "dst" <?- "tmp"?1
       end);;
      skip)%V.

  Theorem test_spec (callee dst src : loc) v q :
    {{{ callee ↦ BCell test ∗
        dst ↦ BCell ()%V ∗
        src ↦ BCell v ∗
        v ↤?{q} {[+src+]} ∗
        ♢3 }}}
      callee [[dst, src]]%V
    {{{ RET tt;
        callee ↦ BCell test ∗
        src ↦ BCell v ∗
        dst ↦ BCell v ∗
        v ↤?{q} ({[+src+]} ⊎ {[+dst+]}) ∗
        ♢3 }}}.
  Proof.
    iIntros (Φ) "? Post".
    destruct_stars.
    wp_call.
    wp_with.
    wp_init l.
    wp_skip.
    wp_load.
    wp_end_with.
    autorewrite with gmultiset_simplify.
    wp_free l.
    wp_cleanup l in v.
    wp_cleanup l in v.
    wp_skip.
    wp_skip.
    iModIntro.
    iApply "Post".
    iFrame.
    cancel_mapsfrom.
  Qed.

  Theorem test_spec_with_more_credits (callee dst src : loc) v q :
    {{{ callee ↦ BCell test ∗
        dst ↦ BCell ()%V ∗
        src ↦ BCell v ∗
        v ↤?{q} {[+src+]} ∗
        ♢4 }}}
      callee [[dst, src]]%V
    {{{ RET tt;
        callee ↦ BCell test ∗
        src ↦ BCell v ∗
        dst ↦ BCell v ∗
        v ↤?{q} ({[+src+]} ⊎ {[+dst+]}) ∗
        ♢4 }}}.
  Proof.
    iIntros (Φ) "? Post".
    destruct_stars.
    wp_call.
    wp_bind (IAlloca _ _).
    wp_with.
    wp_init l.
    wp_skip.
    wp_load.
    wp_end_with.
    autorewrite with gmultiset_simplify.
    wp_free l.
    wp_cleanup l in v.
    wp_cleanup l in v.
    wp_skip.
    wp_skip.
    iModIntro.
    iApply "Post".
    iFrame.
    cancel_mapsfrom.
  Qed.

  Lemma use_test_spec_with_framing (callee dst src : loc) v q :
    {{{ callee ↦ BCell test ∗
        dst ↦ BCell ()%V ∗
        src ↦ BCell v ∗
        v ↤?{q} {[+src+]} ∗
        ♢4 }}}
      callee [[dst, src]]%V
    {{{ RET tt;
        callee ↦ BCell test ∗
        src ↦ BCell v ∗
        dst ↦ BCell v ∗
        v ↤?{q} ({[+src+]} ⊎ {[+dst+]}) ∗
        ♢4 }}}.
  Proof.
    iIntros (Φ) "? Post".
    destruct_stars.
    (* Here, we frame out one diamond, because only 3 diamonds are needed by
       the procedure call. It would be nice if this could be more automated. *)
    withdraw 3%nat as "[Hpayment ?]".
    iApply (test_spec with "[$]").
    iNext. iIntros "?"; destruct_stars.
    diamonds.
    (* Done. *)
    iApply "Post". iFrame.
  Qed.

  Lemma test_intro_zero_diamonds :
     {{{ True }}}
       skip;; skip
     {{{ RET tt; ♢0 }}}.
  Proof.
    iIntros (Φ) "_ Post".
    intro_zero_diamonds.
    wp_skip.
    wp_skip.
    iModIntro. iApply "Post". iFrame.
  Qed.

  Lemma alloc_free (dst c : loc) q ls :
    {{{ ♢ 3 ∗
        dst ↦ BCell ()%V ∗
        dst ↤ ∅ ∗
        c ↦ BCell ()%V ∗
        c ↤{q} ls
    }}}
      dst <:- init [[c, c]];;
      IConst dst ()%V;;
      (* ghost free *)
      skip
    {{{ RET tt; ♢ 3 ∗
        dst ↦ BCell ()%V ∗
        dst ↤ ∅ ∗
        c ↦ BCell ()%V ∗
        c ↤{q} ls
    }}}.
  Proof.
    iIntros (Φ) "? Post".
    destruct_stars.
    wp_init l.
    wp_skip.
    wp_const.
    autorewrite with gmultiset_simplify.
    wp_free l.
    wp_skip.
    wp_skip.
    iApply "Post".
    iFrame.
    auto.
  Qed.

End proof.
