From iris.algebra Require Export dfrac.
From iris.base_logic.lib Require Export invariants.
From iris.program_logic Require Export weakestpre.
From iris.program_logic Require Export ectx_lifting.
From iris.proofmode Require Export proofmode.
From spacelang Require Export iris stdpp.
From spacelang Require Export authnat.
From spacelang Require Export hypotheses successors predecessors ph.
From spacelang Require Export state pointers gc semantics.
From spacelang Require Export spacelang tactics instances.

(* This module instantiates Iris for SpaceLang. *)

(* ------------------------------------------------------------------------ *)

(* The following definitions describe the logical resources that are needed
   in our instantiation of Iris. *)

(* We need:

   1. Support for invariants.
      This is provided by Iris's invariants library:
      https://plv.mpi-sws.org/coqdoc/iris/iris.base_logic.lib.invariants.html

   2. A logical heap, mapping memory locations to blocks,
      plus a ghost mapping of memory locations to their predecessors.
      This is provided by ph.v.

   3. A ghost variable γ, holding an element of the monoid Auth(Nat).
      It is used to implement space credits (diamonds).

 *)

Class heapG Σ := HeapG {
  heapG_inv   :  invGS Σ;
  heapG_heap  :> phG loc block Σ;
  heapG_inG   :> inG Σ (authR natUR);
  heapG_gname : gname;
}.

Notation γ :=
  heapG_gname.

(* ------------------------------------------------------------------------ *)

(* The following definition indicates how we instantiate Iris. *)

(* Thanks to this instance declaration, all of Iris is available
   as soon as we have an assumption [!heapG Σ] in the context. *)

Global Instance heapG_irisG `{hG : !heapG Σ} : irisGS spacelang Σ := {

  (* This tells Iris that we have support for invariants. *)
  iris_invGS := heapG_inv;

  state_interp σ _ _ _ := (

    (* Whereas [σ] represents the physical machine state,
       [θ] is the conceptual machine state. *)
    ∃θ,

    (* The physical and conceptual machine states are valid at all times. *)
    ⌜valid_state σ⌝ ∗
    ⌜valid_state θ⌝ ∗

    (* The conceptual store is synchronized with our [mapsto] and [mapsfrom]
       assertions. *)
    ph_interp θ.(store) ∗

    (* The number of space credits currently in circulation is (at most)
       [available θ], the available space in the conceptual machine state. *)
    own γ (●nat (available θ)) ∗

    (* The physical and conceptual stores are closed under successors. *)
    ⌜ closed σ.(store) ⌝ ∗
    ⌜ closed θ.(store) ⌝ ∗

    (* The physical and conceptual states are related. *)
    ⌜ related σ θ ⌝

  )%I;

  (* A fixed postcondition for any forked-off thread. (Irrelevant here.) *)
  fork_post _ := True%I;

  num_laters_per_step _ := 0%nat;
  state_interp_mono _ _ _ _ := fupd_intro _ _

}.

(* ------------------------------------------------------------------------ *)

(* This tactic is used to destruct the state interpretation predicate
   and name its components in a conventional way. *)

Ltac destruct_state_interp :=
  iDestruct "Hstate" as (θ)
    "(%Hvalidσ & %Hvalidθ & Hph & Hauth & %Hclosedσ & %Hclosedθ & %Hrelated)".

(* This lemma paraphrases the definition of the state interpretation
   predicate. It is used to unfold this predicate when it appears in
   the goal. *)

Lemma state_interp_def `{hG : !heapG Σ} σ ns κ nt :
  state_interp σ ns κ nt ≡
  (
    ∃θ,
    ⌜valid_state σ⌝ ∗
    ⌜valid_state θ⌝ ∗
    ph_interp θ.(store) ∗
    own γ (●nat (available θ)) ∗
    ⌜ closed σ.(store) ⌝ ∗
    ⌜ closed θ.(store) ⌝ ∗
    ⌜ related σ θ ⌝
  )%I.
Proof.
  reflexivity.
Qed.

Ltac intro_state_interp :=
  rewrite state_interp_def.

(* ------------------------------------------------------------------------ *)

(* A GC step preserves the state interpretation predicate. *)

Lemma gc_preserves_state_interp `{hG : !heapG Σ} ns nt σ σ' κ :
  gc σ σ' →
  state_interp σ ns κ nt -∗
  state_interp σ' ns κ nt.
Proof.
  intros. iIntros "Hstate".
  destruct_state_interp. intro_state_interp.
  (* The conceptual heap is unchanged. *)
  iExists θ. iFrame "Hph". iFrame "Hauth".
  (* There remains to check that the GC preserves the pure assertions that
     appear in the state interpretation invariant. *)
  iPureIntro.
  splits; eauto using gc_preserves_closedness with valid_state related.
Qed.

(* ------------------------------------------------------------------------ *)

(* The predicate [vmapsfrom] takes a value [v] as its first argument,
   whereas [mapsfrom] takes a location [l']. *)

Definition vmapsfrom `{!heapG Σ} v q ls : iProp Σ :=
  match v with
  | VLoc l' =>
      l' ↤{q} ls
  | _ =>
      True
  end.

Notation "v ↤?{ q } ls" :=
  (vmapsfrom v q ls)
  (at level 20, format "v ↤?{ q } ls")
: bi_scope.

Notation "v ↤? ls" :=
  (vmapsfrom v 1%Qp ls)
  (at level 20, format "v ↤? ls")
: bi_scope.

Lemma vmapsfrom_join_equiv `{!heapG Σ} v q q1 q2 ls ls1 ls2 :
  (q1+q2)%Qp = q →
  ls1 ⊎ ls2 = ls →
  v ↤?{q1} ls1 ∗ v ↤?{q2} ls2 ⊣⊢ v ↤?{q} ls.
Proof.
  intros. subst. destruct v; simpl vmapsfrom;
  rewrite -> ?mapsfrom_join_equiv by eauto;
  eapply bi.equiv_entails_2; eauto.
Qed.

Global Instance into_sep_vmapsfrom `{!heapG Σ} v q1 q2 ls1 ls2 :
  IntoSep (v ↤?{(q1+q2)%Qp} (ls1 ⊎ ls2)) (v ↤?{q1} ls1) (v ↤?{q2} ls2).
Proof.
  by rewrite /IntoSep vmapsfrom_join_equiv.
Qed.

Global Instance from_sep_vmapsfrom `{!heapG Σ} v q1 q2 ls1 ls2 :
  FromSep (v ↤?{(q1+q2)%Qp} (ls1 ⊎ ls2)) (v ↤?{q1} ls1) (v ↤?{q2} ls2).
Proof.
  by rewrite /FromSep vmapsfrom_join_equiv.
Qed.

Global Instance timeless_vmapsfrom `{!heapG Σ} v q ls :
  Timeless (vmapsfrom v q ls).
Proof. destruct v; apply _. Qed.

(* ------------------------------------------------------------------------ *)

(* A space credit, or diamond, is represented by a fragmentary element
   of the monoid Auth(Nat). The assertion ♢1 can be thought of as the
   ownership of one word of available space in the heap. *)

Definition diamond `{!heapG Σ} (n : nat) : iProp Σ :=
  own γ (◯nat n).

Notation "♢ n" := (diamond n)%I (at level 20) : bi_scope.

(* ------------------------------------------------------------------------ *)

(* The following tactics automate some of the repetitive steps that appear
   in every application of the lemma [wp_lift_atomic_head_step_no_fork]. *)

(* [head_reducible] helps prove a goal of the form [reducible _] or
   [head_reducible _]. This includes a GC step, which we must propose.
   We use a hypothesis of the form [gc _ _] if there is one; otherwise
   we apply [gc_id]. *)

Ltac head_reducible :=
  try iPureIntro;
  do 4 eexists; (* [reducible] and [head_reducible] involve 4 quantifiers *)
  try eapply head_prim_step;        (* convert [prim_step] to [head_step] *)
  eapply GCHeadStep; [
    (* The GC step. *)
    first [ eassumption | eapply gc_id ]
  | (* The head reduction step. *)
    eauto with head_step
  ].

(* [examine_step] helps examine an arbitrary reduction step, which begins
   with a GC step. This step preserves the state interpretation invariant;
   once we have argued about this fact, we can forget about this GC step. *)

(* I considered writing a reformulated lifting lemma, as opposed to a large
   tactic, but that turned out to be rather more heavy and inconvenient. *)

Ltac examine_step :=
  simpl language.state in *;
  simpl language.expr in *;
  simpl language.observation in *;
  simpl language.to_val in *;
  simpl language.prim_step in *;
  simpl ectx_language.state in *;
  simpl ectx_language.expr in *;
  simpl ectx_language.head_step in *;
  (* Introduce the reduction step under the name [Hstep]. *)
  iNext; iIntros (i2 σ2 fk Hstep);
  (* Decompose this step into a GC step and a head step. *)
  try inv_prim_step;
  inv_gc_head_step;
  (* Argue that the GC step preserves the state interpretation invariant. *)
  iDestruct (gc_preserves_state_interp with "Hstate")
    as "Hstate"; [ eauto |];
  (* Open the state interpretation invariant. *)
  destruct_state_interp;
  (* Grab the name of the physical store [σ2]. *)
  lazymatch goal with h: head_step _ _ _ _ ?σ2 _ |- _ =>
  (* Argue that these steps preserve the validity of the physical store,
     which itself is available only after opening the state invariant. *)
  assert (valid_state σ2) by eauto with valid_state;
  assert (closed (store σ2)) by eauto using head_step_preserves_closedness
  end;
  (* Invert the head reduction step. *)
  inv_head_step.
