From spacelang Require Export spacelang.

Bind Scope val_scope with val.
Bind Scope expr_scope with expr.

(* ------------------------------------------------------------------------ *)

(* This allows opening the value scope after [BTuple]. *)

Notation BTuple vs :=
  (BTuple (vs%V)).

(* ------------------------------------------------------------------------ *)

(* This coercion allows providing a string where an lvalue is expected. *)

Coercion LVar : string >-> lvalue.
Coercion LLoc : loc >-> lvalue.

(* ------------------------------------------------------------------------ *)

(* Notations for values. *)

(* These notations can produce ASTs of type [val] and [expr], and are active
   in the scopes [%V] and [%E]. *)

(* [()] denotes the unit value. *)

Notation "()" :=
  (VUnit)
: val_scope.

(* [#l] denotes the memory location [l], viewed as a value. *)

Notation "# l" :=
  (VLoc l%Z%V%stdpp)
  (at level 8, format "# l")
: val_scope.

(* [e1 == e2] is location equality. *)

(* TODO: are [%V], [%E], [: expr_scope] useful and correct, since
[val] is not [value] and [expr] is not [instr]? *)

Notation "dst <- src1 == src2" :=
  (ILocEq dst%V src1%V src2%V)%E
  (at level 70, format "dst <- src1 == src2")
: expr_scope.

(* [^ n] denotes the integer [n], as a value, and an expr. *)

Notation "^ n" :=
  (* TODO: starting in Coq 8.14, Int63.of_Z has been renamed Uint63.of_Z. Change
  this when support for Coq 8.13 is dropped. *)
  (VInt (Int63.of_Z n%Z))
  (at level 8, format "^ n")
  : val_scope.

(* [λ: xs, i] denotes the function whose formal parameters are [xs]
   and whose body is [i]. The formal parameters must be expressed as a
   tuple, delimited with double square brackets. *)

Notation "λ: xs , i" :=
  (VCode xs%binder i%E)
  (at level 200, xs at level 1, i at level 200,
   format "'[' 'λ:'  xs ,  '/  ' i ']'")
: val_scope.

(* This allows using tuple notation to construct a list of the formal
   parameters. *)

Notation "[[]]" :=
  (nil)
: binder_scope.

Notation "[[ x ]]" :=
  (cons (BNamed x) nil)
: binder_scope.

Notation "[[ x1 , x2 , .. , xn ]]" :=
  (cons (BNamed x1) (cons (BNamed x2) (.. (cons (BNamed xn) nil) .. )))
: binder_scope.

(* ------------------------------------------------------------------------ *)

(* Sequencing. *)

Notation "'skip'" :=
  (ISkip)
  (at level 10)
: expr_scope.

Notation "'with:' x : i 'end' " :=
  (IAlloca x%binder i%E)
  (at level 200, x at level 1, i at level 200,
   format "'[' 'with:'  x  :  '[' i ']'  'end' ']' ")
: expr_scope.

Notation "i1 ;; i2" :=
  (ISeq i1%E i2%E)
  (at level 100, i2 at level 200,
   format "'[' '[hv' '[' i1 ']' ;;  ']' '/' i2 ']'")
: expr_scope.

(* ------------------------------------------------------------------------ *)

(* If. *)

Notation "'if:' i1 'then' i2 'else' i3" :=  (IIf i1%E i2%E i3%E)
  (at level 200, i1, i2, i3 at level 200) : expr_scope.

(* ------------------------------------------------------------------------ *)

(* Function calls. *)

(* This allows using juxtaposition syntax for a function call. *)

Coercion ICall : lvalue >-> Funclass.

(* This allows using tuple notation to construct a list of the actual
   parameters. *)

Notation "[[]]" :=
  (nil)
: val_scope.

Notation "[[ v ]]" :=
  (cons (v : lvalue) nil)
: val_scope.

Notation "[[ v1 , v2 , .. , vn ]]" :=
  (cons (v1 : lvalue) (cons (v2 : lvalue) (.. (cons (vn : lvalue) nil) .. )))
: val_scope.

(* ------------------------------------------------------------------------ *)

(* Tuples. *)

Notation "dst <- 'alloc' n" :=
  (IAlloc dst%V (n%nat))
  (at level 10)
: expr_scope.

Notation "dst <?- src '?' ofs" :=
  (ILoad dst%V src%V ofs%nat)
  (at level 10)
: expr_scope.

Notation "dst '!' ofs <!- src" :=
  (IStore dst%V ofs%nat src%V)
  (at level 10)
: expr_scope.

(* ------------------------------------------------------------------------ *)

(* Sugar for allocation followed by initialization. *)

Fixpoint init_loop (dst : lvalue) (ofs : nat) (vs : list lvalue) : instr :=
  match vs with
  | [] =>
      ISkip
  | v :: vs =>
      ISeq
        (IStore dst ofs v)
        (init_loop dst (1 + ofs)%nat vs)
  end.

Definition init_entry (dst : lvalue) (es : list lvalue) : instr :=
  ((* let tmp := "tmp" in *)
   (* with: tmp: *)
        dst <- alloc (length es);;
        init_loop dst 0%nat es
   (* end *))%E.

Notation "dst <:- 'init' vs" :=
  (init_entry dst (vs%V))
  (at level 10)
: expr_scope.
