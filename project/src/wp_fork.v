From spacelang Require Import wp_state_interp wp_memory wp_alloca.

(* A reasoning rule for [IFork (LLoc c) x i].

   The memory location [c] is the address of an existing stack cell in the
   original thread. The precondition requires [c ↦ BCell v ∗ v ↤?{qv} lsv],
   that is, a points-to assertion and a pointed-by assertion, which together
   allow updating this stack cell.

   The postcondition of the original thread contains only [c ↦ BCell VUnit].
   That is, the stack cell is overwritten with a unit value, and the
   pointed-by assertion vanishes; it is transferred to the new thread.

   In the new thread, a fresh stack cell [c'] is created.
   The new thread receives the points-to assertion [c' ↦ BCell v]
   and the updated pointed-by assertion [v↤?{qv}((lsv ∖ {[+c+]}) ⊎ {[+c'+]})],
   which indicates that the pointer from [c] to [v] has disappeared
   and that a pointer from [c'] to [v] has appeared.

   The postcondition of the new thread requires [c' ↦ BCell VUnit]. This
   may seem slightly restrictive, but should not be problematic in practice.
   This restriction allows us to propose a simpler statement. We could
   otherwise use a postcondition identical to the one found in
   the lemma [wp_alloca], allowing [c' ↦ BCell w] and removing an edge
   from [c'] to [w]. But there is little point in removing an edge if
   one cannot transmit the updated pointed-by assertion. *)

Lemma wp_fork `{!heapG Σ} s E c x i v qv lsv :
  {{{
    c ↦ BCell v ∗
    v ↤?{qv} lsv ∗
    (∀ c', c' ↦ BCell v ∗ v↤?{qv}((lsv ∖ {[+c+]}) ⊎ {[+c'+]}) -∗
           WP (subst x (LLoc c') i) @ s; ⊤ {{ _, c' ↦ BCell VUnit }})
  }}}
    IFork (LLoc c) x i
    @ s; E
  {{{ RET (); c ↦ BCell VUnit }}}.
Proof.
  iIntros (Φ) "(Hc & Hv & Hi) HΦ".
  iApply wp_lift_atomic_head_step; first done.
  iIntros (σ1 nsteps κ κs nthreads) "Hstate".
  iModIntro; iSplit.

  (* [IFork c x i] is reducible provided [c] is a stack cell. *)
  { destruct_state_interp.
    exploit_stack_cell c.
    iPureIntro.
    head_reducible. }

  examine_step.
  inv_read_lvalue.
  exploit_stack_cell c. simplify_eq. rename v0 into v. (* TODO *)
  dup_inv_write_lvalue.
  simpl length.
  simpl from_option.
  simpl fork_post.
  (* Let [c'] be the new stack cell. *)
  rename c0 into c'. (* TODO avoid fragile uses of [rename] *)
  assert (c ≠ c') by congruence.

  (* As in [alloca], update the conceptual store by allocating [c']. *)
  assert (store θ !! c' = None) by eauto using related_preserves_freshness.
  (* Update the conceptual store by allocating this stack cell. Because
     this stack cell is initialized with the value [v], this creates new
     edges in the heap. A [mapsfrom] assertion for [v] is required. *)
  set (triples := map (λ l', (l', qv, lsv)) (val_pointer_list v)).
  iMod (ph_alloc θ.(store) c' (BCell v) triples with "[Hph Hv]")
       as "(Hph & Hv & Hc' & pc')".
  { auto. }
  { auto. }
  { rewrite /triples /addresses map_map map_id //. }
  { rewrite /triples interpret_val_pointer. iFrame. }
  rewrite /triples map_map interpret_val_pointer. clear triples.

  (* Update the conceptual store by writing [VUnit] into [c]. *)
  iMod (ph_store_stack_dep true _ c _ VUnit _ _ 1%Qp ∅
    with "[$Hph $Hc $Hv]")
      as "(Hph & Hc & Hv & _)".
  { rewrite lookup_insert_ne //. }

  (* Argue that removing [c] commutes with inserting [c']. This is justified
     by the inequality [c ≠ c']. (Because the fraction [qv] is not 1, we do
     not necessarily have [c ∈ lsv].) *)
  rewrite <- commute_diff_union
    by eauto using gmultiset_intersection_singletons_ne.

  (* Conclude. *)
  iModIntro.

  (* Part 1: the state interpretation still holds. *)
  iSplitL "Hauth Hph".
  { iExists (<[c:=BCell VUnit]> <$> (<[c':=BCell v]> <$> θ)).
    erewrite available_update_cell by eauto.
    erewrite available_alloc by eauto. rewrite Nat.sub_0_r.
    iFrame "Hauth".
    iFrame "Hph".
    iPureIntro. splits; eauto with valid_state closed related. }

  (* Part 2: the postcondition in the original thread holds. *)
  iSplitL "Hc HΦ".
  { iApply "HΦ". iFrame. }

  (* Part 3: the new thread is safe to run. *)
  simpl. iSplitL; [| done ].
  iSpecialize ("Hi" with "[$Hc' $Hv]").
  iApply wp_alloca_active.
  iFrame "pc'".
  iApply (wp_strong_mono _ _ _ _ with "Hi"); eauto 1.
  (* Check that our strengthened postcondition is OK. *)
  iIntros (_) "Hc'".
  iModIntro.
  iExists VUnit. iExists (1%Qp). iExists (∅).
  simpl. iFrame. eauto.
Qed.
