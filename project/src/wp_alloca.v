From spacelang Require Import wp_state_interp wp_diamonds wp_memory wp_cleanup.

(* This auxiliary lemma describes what happens at the end of an [alloca]
   block: the stack cell (which is certain to have no predecessors, as
   we never give the permission [c ↤ ∅] to the end user) is deallocated. *)

Lemma wp_alloca_active_skip `{!heapG Σ} s E c v q ls :
  {{{ c ↦ BCell v ∗ c ↤ ∅ ∗ v ↤?{q} ls }}}
    IAllocaActive c ISkip
    @ s; E
  {{{ RET (); v ↤?{q} (ls ∖ {[+c+]}) }}}.
Proof.
  iIntros (Φ) "(Hc & pc & Hv) HΦ".
  iApply wp_lift_atomic_head_step_no_fork; first done.
  iIntros (σ1 nsteps κ κs nthreads) "Hstate".
  iModIntro; iSplit.

  (* [IAllocaActive c ISkip] is reducible as long as [c] is a stack cell. *)
  { destruct_state_interp.
    exploit_stack_cell c.
    iPureIntro.
    head_reducible. }

  examine_step.
  exploit_stack_cell c.
  (* Update the conceptual store by deallocating the stack cell. *)
  iMod (ph_free_singleton false θ.(store) c with "[$]")
    as "(Hph & dc & %Hnopreds)".
  { set_solver. }
  { tauto. }
  (* Check that the state interpretation predicate still holds. *)
  iAssert (state_interp (<[c:=BDeallocated]> <$> σ'1) nsteps κs nthreads)
    with "[Hauth Hph]" as "Hstate".
  { iExists (<[c:=BDeallocated]> <$> θ). iFrame "Hph".
    erewrite available_dealloc by eauto.
    rewrite Nat.add_0_r. iFrame "Hauth".
    iPureIntro. splits; eauto with valid_state closed related. }
  (* Perform a cleanup step. This can be done only while the state
     interpretation predicate is at hand, which is why we do it now. *)
  iMod (supd_cleanup_singleton_v with "[Hstate Hv dc]") as "[Hstate Hv]";
    first iFrame.
  (* Conclude. *)
  iModIntro. iSplit; first auto. iFrame "Hstate". iApply "HΦ". iFrame.
Qed.

(* This auxiliary lemma describes what happens at the beginning of an
   [alloca] block: a new stack cell is allocated, and [IAlloca x i]
   reduces to [IAllocaActive c _], allowing reduction to continue
   under a context. *)

Lemma wp_alloca_alloca_active `{!heapG Σ} s E x i Φ :
  ▷ (∀ c, c ↦ BCell VUnit ∗ c ↤ ∅ -∗
        WP (IAllocaActive c (subst x (LLoc c) i)) @ s; E {{ Φ }})
  ⊢ WP (IAlloca x i) @ s; E {{ Φ }}.
Proof.
  iIntros "HWP".
  iApply wp_lift_step; first done.
  iIntros (σ1 nsteps κ κs nthreads) "Hstate".
  iApply fupd_mask_intro; first set_solver. iIntros "Hclose". iSplit.

  (* First, prove that this instruction is reducible. *)
  { destruct s; [| eauto ].
    destruct_state_interp.
    head_reducible. }

  examine_step.
  iMod "Hclose" as "_".
  assert (store θ !! c = None) by eauto using related_preserves_freshness.
  (* Update the conceptual store by allocating this stack cell. *)
  iMod (ph_alloc θ.(store) c (BCell VUnit) [] with "[Hph]")
       as "(Hph & _ & Hc & pc)".
  { auto. }
  { auto. }
  { auto. }
  { rewrite interpret_nil. auto. }
  (* Conclude. *)
  iModIntro.
  iSplitL "Hph Hauth".
  - iExists (<[c:=BCell VUnit]> <$> θ). iFrame "Hph".
    erewrite available_alloc by eauto. rewrite Nat.sub_0_r. iFrame "Hauth".
    iPureIntro. splits; eauto with valid_state closed related.
  - iSplitL.
    + iApply "HWP". iFrame.
    + auto.
Qed.

(* This auxiliary lemma provides a big-step reasoning rule for
   [alloca c in [c/x]i]. It is used in the proof of the next lemma,
   and is also used in wp_fork.v. *)

Lemma wp_alloca_active `{!heapG Σ} s E x i Φ c :
  WP (subst x (LLoc c) i) @ s; E {{ _, ∃ w q ls,
    c ↦ BCell w ∗ w ↤?{q} ls ∗
    (w ↤?{q} (ls ∖ {[+c+]}) -∗ Φ tt)
  }}
  ∗ c↤∅
  ⊢
  WP IAllocaActive c (subst x (LLoc c) i) @ s; E {{ v, Φ v }}.
Proof.
  iIntros "(HWP & pc)".
  iApply wp_bind.
  iApply (wp_strong_mono s s E E with "[HWP]").
  { auto. }
  { auto. }
  { iApply "HWP". }
  iIntros ([]) "H".
  iDestruct "H" as (w q ls) "(Hc & Hw & HΦ)".
  iModIntro.
  iApply (wp_alloca_active_skip with "[$Hc $pc $Hw] [HΦ]").
  iNext. auto.
Qed.

(* The main lemma provides a big-step reasoning rule for [alloca x in i]. Its
   proof combines the above auxiliary lemmas and an invocation of the lemma
   [wp_bind] in order to reason about evaluation under a context. Its
   statement indicates that:
   1- upon entry,
      a- a fresh stack cell [c] is allocated,
      b- the variable [x] is bound to the address [c] for the duration
         of the instruction [i];
   2- upon exit,
      a- the user must abandon the ownership of this cell [c ↦ BCell v],
      b- the user must give us [v ↤?{q} ls], which we update to
         [v ↤?{q} (ls ∖ {[+c+]})], reflecting the fact that [c] is no
         longer a predecessor of [v]. *)

Lemma wp_alloca `{!heapG Σ} s E x i Φ :
  ▷ (∀ c,
    c ↦ BCell VUnit -∗
    WP (subst x (LLoc c) i) @ s; E {{ _, ∃v q ls,
      c ↦ BCell v ∗ v ↤?{q} ls ∗
      (v ↤?{q} (ls ∖ {[+c+]}) -∗ Φ tt)
    }}
  )
  ⊢ WP (IAlloca x i) @ s; E {{ Φ }}.
Proof.
  iIntros "HWP".
  iApply wp_alloca_alloca_active. iNext.
  iIntros (c) "[Hc pc]".
  iApply (wp_alloca_active with "[HWP Hc pc]").
  iFrame "pc".
  iApply ("HWP" with "Hc").
Qed.
