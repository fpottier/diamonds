From spacelang Require Import wp_state_interp.

(* This module establishes reasoning laws about diamonds. *)

(* ------------------------------------------------------------------------ *)

(* One can drop diamonds. *)

Lemma diamond_weaken `{!heapG Σ} n1 n2 :
  (n2 <= n1)%nat →
  ♢n1 -∗ ♢n2.
Proof.
  eapply own_auth_nat_weaken.
Qed.

(* ------------------------------------------------------------------------ *)

(* One can split and join diamonds. *)

Lemma diamond_split `{!heapG Σ} n1 n2 n :
  (n = n1 + n2)%nat →
  ♢n ⊣⊢ ♢n1 ∗ ♢n2.
Proof.
  eapply own_auth_nat_split.
Qed.

Lemma diamond_split_3 `{!heapG Σ} n1 n2 n3 n :
  (n = n1 + n2 + n3)%nat →
  ♢n ⊣⊢ ♢n1 ∗ ♢n2 ∗ ♢n3.
Proof.
  intros ->.
  rewrite <-diamond_split; auto.
  apply diamond_split; lia.
Qed.

(* The following instance declaration allows the tactic [iCombine] to
   combine two diamonds. This could be useful in situations where the
   tactic [diamonds] (which gathers all diamonds) is too brutal. *)

Global Instance into_sep_diamond_plus `{!heapG Σ} m n :
  IntoSep (♢(m + n)) (♢m) (♢n).
Proof.
  by rewrite /IntoSep diamond_split.
Qed.

(* The following instance declaration allows the tactics [iSplitL]
   and [iSplitR] to split a goal of the form [♢(m + n)]. This seems
   very marginally useful. It also allows [iDestruct] to split a
   hypothesis of the form [♢(m + n)]; this seems more interesting. *)

Global Instance from_sep_diamond_plus `{!heapG Σ} m n :
  FromSep (♢(m + n)) (♢m) (♢n).
Proof.
  by rewrite /FromSep -diamond_split.
Qed.

(* ------------------------------------------------------------------------ *)

(* One can create zero diamonds out of thin air. *)

Lemma diamond_0 `{!heapG Σ} E :
  True ={{E}}=∗ ♢0.
Proof.
  iIntros (σ nt κs n) "[Hstate _]".
  destruct_state_interp.
  iDestruct (own_auth_nat_null with "Hauth") as "[Hauth Hdiamonds]".
  iModIntro.
  iFrame "Hdiamonds".
  intro_state_interp. iExists _. by iFrame.
Qed.

Lemma wp_diamond_0 `{!heapG Σ} s E i Φ :
  to_val i = None →
  (♢0 -∗ wp s E i Φ) -∗ (wp s E i Φ).
Proof.
  iIntros (nv) "WP".
  (* Grab the state interpretation predicate. *)
  iApply (wp_supd _ _ _ True%I (♢0)%I _ nv
            with "WP"); [| done ].
  eauto using diamond_0.
Qed.

(* ------------------------------------------------------------------------ *)

(* Diamonds are forever. *)

Global Instance diamond_timeless `{!heapG Σ} n :
  Timeless (♢n).
Proof.
  apply _.
Qed.
