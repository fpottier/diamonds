From stdpp Require Import fin_maps.
From spacelang Require Import spacelang gc.

(* ------------------------------------------------------------------------ *)

(* A few general-purpose tactics. *)

Ltac dup h :=
  generalize h; intro.

Ltac splits :=
  repeat lazymatch goal with |- _ ∧ _ => split end.

Ltac cases :=
  repeat lazymatch goal with h: _ ∨ _ |- _ => destruct h end.

Ltac unpack :=
  repeat lazymatch goal with
  | h: ∃ x, _ |- _ => destruct h
  | h: _ ∧ _  |- _ => destruct h
  end.

(* ------------------------------------------------------------------------ *)

(* Inversion tactics for some of the reduction relations. *)

(* The relation [step] is defined by Iris. *)

Ltac inv_step :=
  let Hf := fresh in
  match goal with h: step _ _ _ |- _ =>
    inversion h as [????????? Hf]; clear h; rename Hf into h;
    simpl in h;
    simplify_eq
  end.

(* The relation [gc_head_step] is the composition of [gc] and [head_step]. *)

Ltac inv_gc_head_step :=
  simpl ectx_language.head_step in *;
  let Hf := fresh in
  let Hgc := fresh in
  match goal with h: gc_head_step _ _ _ _ _ _ |- _ =>
    inversion h as [? ? ? ? ? ? ? Hgc Hf]; clear h; rename Hf into h;
    simplify_eq
  end.

(* The tactic [inv_head_step] performs inversion on hypotheses of the
   shape [head_step]. It refutes head reductions that start at a
   value. TODO is that useful? *)

Ltac inv_head_step :=
  repeat match goal with
  | H : to_val ?i = Some _ |- _ => apply of_to_val in H; subst i
  | H : head_step ?i _ _ _ _ _ |- _ =>
     (* Avoid inverting [H] if [i] is a variable. *)
     try (is_var i; fail 1);
     inversion H; subst; clear H
  | _ => progress simplify_eq
  end.

Ltac inv_read_lvalue :=
  match goal with h: read_lvalue _ _ _ |- _ =>
    inversion_clear h
  end.

Ltac inv_write_lvalue :=
  match goal with h: write_lvalue _ _ _ ?σ |- _ =>
    inversion_clear h; subst σ
  end.

Ltac dup_inv_write_lvalue :=
  match goal with h: write_lvalue _ _ _ ?σ |- _ =>
    dup h; inversion_clear h; subst σ
  end.

(* ------------------------------------------------------------------------ *)

(* The tactic [reshape_expr e tac] decomposes the instruction [i] into an
   evaluation context [K] and a subinstruction [i']. It calls the tactic
   [tac K i'] for each possible decomposition until [tac] succeeds or
   until there are no more possibilities. Note the use of [match], wich
   has backtracking semantics. *)

Ltac reshape_expr e tac :=
  let rec go K e :=
    match e with
    | _                            => tac K e
    | ISeq ?i1 ?i2                 => add_item (KSeq i2) K i1
    | IAllocaActive ?c ?i          => add_item (KAllocaActive c) i
    end
  with add_item Ki K e :=
    go (Ki :: K) e
  in
  go (@nil ectx_item) e.

(* ------------------------------------------------------------------------ *)

(* An evaluation context [K] either is empty or has a bottom frame [item]. *)

Lemma ectx_case_bottom_frame (K : list ectx_item) :
  K = [] ∨ ∃ K' item, K = K' ++ [item].
Proof.
  assert (exists K'', K = rev K'') as [K'' ->].
  { eexists (rev K). by rewrite rev_involutive //. }
  destruct K''; eauto.
Qed.

(* [ISkip] cannot take a step. *)

Lemma invert_prim_step_skip i1 σ1 κ i2 σ2 fk :
  prim_step i1 σ1 κ i2 σ2 fk →
  i1 ≠ ISkip.
Proof.
  inversion 1. intro. subst i1. simpl in *.
  destruct (ectx_case_bottom_frame K) as [| (? & item & ?) ]; subst K.
  { simpl in *. inv_gc_head_step. inv_head_step. }
  { match goal with h: _ = ISkip |- _ =>
      rewrite /fill !foldl_app in h; simpl in h end.
    destruct item; simpl in *; congruence. }
Qed.

Local Hint Constructors prim_step : core.

Local Hint Resolve invert_prim_step_skip : core.

(* An inversion lemma for [prim_step]. *)

Lemma invert_prim_step i1 σ1 κ i2 σ2 fk :
  prim_step i1 σ1 κ i2 σ2 fk →
  (
    gc_head_step i1 σ1 κ i2 σ2 fk
  ) ∨ (
    ∃ i i1' i2',
    i1 = ISeq i1' i ∧
    i2 = ISeq i2' i ∧
    prim_step i1' σ1 κ i2' σ2 fk ∧
    i1' ≠ ISkip
  ) ∨ (
    ∃ c i1' i2',
    i1 = IAllocaActive c i1' ∧
    i2 = IAllocaActive c i2' ∧
    prim_step i1' σ1 κ i2' σ2 fk ∧
    i1' ≠ ISkip
  ).
Proof.
  destruct 1; subst.
  (* The context [K] either is empty or has a bottom frame [item]. *)
  destruct (ectx_case_bottom_frame K) as [| (K' & item & ?) ].
  (* Case: [K] is empty. *)
  { subst. simpl. left. eauto. }
  (* Case: [K] has a bottom frame. *)
  { subst. right. destruct item as [ i | c ].
    + left. do 3 eexists. simpl ectx_language.fill.
      rewrite /fill !foldl_app. simpl.
      eauto 7.
    + right. do 3 eexists. simpl ectx_language.fill.
      rewrite /fill !foldl_app. simpl.
      eauto 7. }
Qed.

(* ------------------------------------------------------------------------ *)

(* The tactic [inv_prim_step] inverts a [prim_step] hypothesis. *)

(* The relation [prim_step] is defined by Iris. *)

Ltac inv_prim_step :=
  let Hf := fresh in
  repeat match goal with
  | h: prim_step _ _ _ _ _ _ |- _ =>
      (* Apply [invert_prim_step] if we can prove that only one of the three
         alternatives in its conclusion is possible. *)
      apply invert_prim_step in h;
      cases; try solve [ exfalso; unpack; congruence ];
      []
  | h: prim_step _ _ _ _ _ _ |- _ =>
      inversion h as [????? Hf]; clear h; rename Hf into h;
      simpl in *;
      subst
  end.

(* A test. *)

Goal
  ∀ src i1 j1 σ1 κ i2 σ2 fk,
  prim_step (IIf src i1 j1) σ1 κ i2 σ2 fk →
  gc_head_step (IIf src i1 j1) σ1 κ i2 σ2 fk.
Proof.
  intros. inv_prim_step. assumption.
Qed.
