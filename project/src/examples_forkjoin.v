From iris.algebra Require Import excl_auth frac_auth.
From spacelang Require Import proofmode notation.

Definition StoreConst dst ofs v : instr :=
  (with: "_tmp" :
    IConst "_tmp" v;;
    IStore dst ofs "_tmp"
  end)%E.

Ltac wp_store_const := wp_with; wp_const; try wp_skip; wp_store; wp_end_with.

Section fork_join_with_signal_leak.

  Context `{!heapG Σ, !inG Σ (exclR unitO)}.

  Definition invN : namespace := nroot .@ "inv".

  Definition join_inv (l : loc) (R : iProp Σ) : iProp Σ :=
    (∃ (n : Z), l ↦ BTuple [(^n)%V]%I ∗ if decide (n = 0) then True else ⌜n = 1⌝ ∗ R).

  (** The spawned thread can store [1] at location [l] to signal that its job
      is done. The resource [R] is released into the join invariant. *)
  Lemma done_spec (r l : loc) (R : iProp Σ) :
    {{{ r ↦ BCell (#l)%V ∗ inv invN (join_inv l R) ∗ R }}}
      StoreConst r 0 (^1)%V
    {{{ RET tt; r ↦ BCell (#l)%V }}}.
  Proof.
    iIntros (Φ) "(Hr & #Hinv & HR) post".
    unfold StoreConst.
    wp_with.
    wp_const.
    wp_skip.
    iInv invN as (n) "(>Hl & H)" "Hclose".
    wp_store.
    iMod ("Hclose" with "[Hl HR]").
    { iExists 1%Z. simpl. iNext. iFrame. auto. }
    iModIntro.
    wp_end_with.
    iApply "post".
    iFrame.
  Qed.

  Definition join : value :=
    (λ: [["self", "src"]],
      with: "flag": ILoad "flag" "src" 0;;
      if: "flag" then
        skip
      else
        "self" [["self", "src"]]
      end
    )%V.

  (** [join(join, r)] will loop until [*r] points to a non-zero value. Using
      the invariant, it recovers the resource [R] from the spawned thread. *)

  Lemma join_spec (f r l : loc) (U R : iProp Σ) :
    {{{ f ↦ BCell join ∗ r ↦ BCell (#l)%V ∗ U ∗ (U ∗ U -∗ False) ∗ inv invN (join_inv l (U ∨ R)) }}}
      f [[f, r]]%V
    {{{ RET tt; f ↦ BCell join ∗ r ↦ BCell (#l)%V ∗ R }}}.
  Proof.
    iIntros (Φ) "(Hf & Hr & u & uu & #Hinv) post".
    iLöb as "IH" forall (Φ).
    wp_call. fold join.
    wp_with.
    wp_bind (ILoad _ _ _).
    iInv invN as (n) "(>Hl & H)" "Hclose".
    destruct (decide _) as [-> | _].
    - wp_load.
      iMod ("Hclose" with "[Hl]").
      { iNext. iExists _. iFrame. }
      iModIntro.
      wp_skip.
      wp_if.
      iApply ("IH" with "Hf Hr u uu").
      iNext. iIntros "(Hf & Hr & HR)".
      wp_end_with.
      iApply "post". iFrame.
    - wp_load.
      (* iDestruct "H" as "(-> & [u' | HR])". before adding the mapsfrom *)
      iDestruct "H" as "(-> & [u' | HR])".
      { iDestruct ("uu" with "[$]") as "[]". }
      iMod ("Hclose" with "[Hl u]").
      { iNext. iExists _. iFrame. simpl. iFrame. auto. }
      iModIntro. wp_skip. wp_if. wp_skip.
      iModIntro. wp_end_with.
      iApply "post". iFrame.
  Qed.

  Lemma unique : forall E, (⊢ (|={E}=> ∃ U : iProp Σ, U ∗ (U ∗ U -∗ False)))%I.
  Proof.
    intros E.
    iStartProof.
    iMod (own_alloc (Excl ()) I) as (γ) "u".
    iExists (own γ (Excl ())).
    iModIntro.
    iFrame.
    iIntros "(u1 & u2)".
    iDestruct (own_valid_2 with "u1 u2") as %uu.
    apply exclusive_l in uu. tauto.
    typeclasses eauto.
  Qed.

  Lemma message_passing (c : loc) :
    {{{ c ↦ BCell ()%V ∗ ♢5 }}}
      with: "lock": IAlloc "lock" 1;;
      StoreConst "lock" 0 (^0)%V;;
      with: "s": IAlloc "s" 2;;
      "s" ! 0 <!- "lock";;
      with: "s2": IMove "s2" "s";;
      IFork "s2" "x" (
        StoreConst "x" 1 ^17;;
        with: "lock": ILoad "lock" "x" 0;;
        StoreConst "x" 0 ()%V;;
        IConst "x" ();;
        StoreConst "lock" 0 ^1;;
        IConst "lock" ()  (* --> this should be done at the same time as the Store *)
        end
      )%V;;
      with: "join": IConst "join" join;;
      "join" [["join", "lock"]]%V;;
      ILoad c "s" 1
      end end end end;;
      skip
    {{{ RET tt; c ↦ BCell (^17)%V ∗ ♢3
        (* we can probably recover ◊5 with a more complicated invariant and
        having a special store operation *)
    }}}.
  Proof.
    iIntros (Φ) "(Hc & dia) post".
    wp_with "rlock".
    wp_alloc lock as "(Hlock & plock & Hrlock & _ & ?)".
    iDestruct (mapsfrom_split _ _ _ (1/2)%Qp (1/2)%Qp {[+rlock+]} ∅ with "plock")
      as "(plock1 & plock2)".
    { symmetry; apply Qp_half_half. }
    { multiset_solver. }
    wp_store_const.
    wp_skip.
    wp_with.
    wp_alloc.
    wp_store.
    wp_with.
    wp_move.

    iRename select (l↤_)%I into "pl".
    iRename select (s↦_)%I into "Hs".
    iRename select (l↦_)%I into "Hl".

    iMod unique as (U) "(u & uu)".
    pose (R := (l ↦ BTuple [()%V; (^17)%V] ∗ l ↤ {[+s+]})%I).
    iMod (inv_alloc invN _ (join_inv lock (U ∨ R)) with "[Hlock]") as "#Hinv".
    { iNext. simpl. iExists 0. simpl. iFrame. }

    wp_bind (IFork _ _ _).
    iApply (wp_fork with "[$Hs2 $pl Hl plock2]").
    {
      iIntros (r) "(Hr & pl)".
      simpl. clear _tmp.
      wp_with.
      wp_const.
      wp_skip.
      wp_store.
      wp_end_with.
      wp_skip.
      wp_with "lock'".
      wp_load.
      clear _tmp. wp_store_const. wp_skip. clear _tmp.
      wp_const.
      wp_skip.
      wp_bind (with: _: _ end)%E.
      iApply (done_spec with "[$Hlock' $Hinv Hl pl]").
      { iFrame. simpl. unfold R. iRight. cancel_mapsfrom.  iFrame. }
      iNext. iIntros "Hlock'".
      wp_skip.
      wp_const.
      wp_end_with.
      iFrame.
      (* The predecessors of lock are lost here *)
    }

    iNext; iIntros "Hs2".
    wp_skip.
    wp_with.
    wp_const.
    wp_skip.

    wp_bind (ICall _ _).
    iApply (join_spec with "[$Hjoin $Hrlock $uu $u $Hinv]").
    iNext; iIntros "(Hjoin & Hrlock & Hl & pl)".
    wp_skip.
    wp_load.
    wp_end_with.
    wp_end_with.
    wp_end_with.
    wp_end_with.
    rewrite gmultiset_difference_diag.
    wp_free l.
    wp_skip.
    wp_skip.
    iApply "post".
    iFrame.
    auto.
  Qed.

End fork_join_with_signal_leak.



Section fork_join_no_leak.

  Context `{!heapG Σ, !inG Σ (exclR unitO)}.

  Definition join_inv_pred (l : loc) (R : iProp Σ) : iProp Σ :=
    (†l ∨ ∃ (n : Z), l ↦ BTuple [(^n)%V]%I ∗ if decide (n = 0) then True else ⌜n = 1⌝ ∗ R).

  (** If we want to be able to free the lock and get back all the initial
      space credits, we need to remove [l]'s root [r] from [l]'s
      predecessors when signaling the end of the thread. *)

  (* We would need a variant of [IStore dst ofs src] that erase the [dst]
     register, so that we can release a lock and at the same time remove [dst]
     from its predecessors. Here, for simplicity, we assume a variant of
     [StoreConst], which is a macro made out of [IStore]. *)

  Variable SpecialStoreConst : lvalue -> nat -> value -> instr.

  Variable done_spec_special : forall (r l : loc) (U R : iProp Σ) (q : Qp),
    {{{ r ↦ BCell (#l)%V ∗ l ↤{q} {[+r+]} ∗ R ∗ inv invN (join_inv_pred l (U ∨ l↤{q} ∅ ∗ R)) }}}
      SpecialStoreConst r 0 (^1)%V
    {{{ RET tt; r ↦ BCell ()%V }}}.

  Variable subst_SpecialStoreConst : forall dst ofs v x lv,
      subst x lv (SpecialStoreConst dst ofs v) =
      SpecialStoreConst (lsubst x lv dst) ofs v.

  (* Join and get back also the lock's predecessor information *)

  Lemma join_spec_pred (f r l : loc) (U R : iProp Σ) (q1 q2 : Qp) ls :
    (* [q1] is here to derive a contradiction from [†l]*)
    (* [q2] is here to get back mapsfrom information from the spawned thread *)
    {{{ f ↦ BCell join ∗ l ↤{q1} ls ∗ r ↦ BCell (#l)%V ∗ U ∗ (U ∗ U -∗ False) ∗ inv invN (join_inv_pred l (U ∨ l↤{q2} ∅ ∗ R)) }}}
      f [[f, r]]%V
    {{{ RET tt; f ↦ BCell join ∗ l ↤{q1} ls ∗ r ↦ BCell (#l)%V ∗ l ↤{q2} ∅ ∗ R }}}.
  Proof.
    iIntros (Φ) "(Hf & pl & Hr & u & uu & #Hinv) post".
    iLöb as "IH" forall (Φ).
    wp_call. fold join.
    wp_with.
    wp_bind (ILoad _ _ _).
    iInv invN as "[>dl | I]" "Hclose".
    {
      unshelve iApply (wp_supd _ _ (ILoad _ _ _) _ _ _ _ (supd_exploit_mapsfrom_dag _ l q1 ls) with "[]").
      auto. iIntros "[]". auto.
    }
    iDestruct "I" as (n) "(>Hl & H)".
    destruct (decide _) as [-> | _].
    - wp_load.
      iMod ("Hclose" with "[Hl]").
      { iNext. iRight. iExists _. iFrame. }
      iModIntro.
      wp_skip.
      wp_if.
      iApply ("IH" with "Hf pl Hr u uu").
      iNext. iIntros "(Hf & Hr & HR)".
      wp_end_with.
      iApply "post". iFrame.
    - wp_load.
      (* iDestruct "H" as "(-> & [u' | HR])". before adding the mapsfrom *)
      iDestruct "H" as "(-> & [u' | (pl' & HR)])".
      { iDestruct ("uu" with "[$]") as "[]". }
      iMod ("Hclose" with "[Hl u]").
      { iNext. iRight. iExists _. iFrame. simpl. iFrame. auto. }
      iModIntro. wp_skip. wp_if. wp_skip.
      iModIntro. wp_end_with.
      iApply "post". iFrame.
  Qed.

  Lemma message_passing_pred (c : loc) :
    {{{ c ↦ BCell ()%V ∗ ♢5 }}}
      with: "lock": IAlloc "lock" 1;;
      StoreConst "lock" 0 (^0)%V;;
      with: "s": IAlloc "s" 2;;
      "s" ! 0 <!- "lock";;
      with: "s2": IMove "s2" "s";;
      IFork "s2" "x" (
        StoreConst "x" 1 ^17;;
        with: "lock": ILoad "lock" "x" 0;;
        StoreConst "x" 0 ()%V;;
        IConst "x" ();;
        SpecialStoreConst "lock" 0 ^1;;
        IConst "lock" ()  (* --> this should be done at the same time as the Store *)
        end
      )%V;;
      with: "join": IConst "join" join;;
      "join" [["join", "lock"]]%V;;
      ILoad c "s" 1
      end end end end;;
      skip
    {{{ RET tt; c ↦ BCell (^17)%V ∗ ♢3 (* we can probably recover ◊5 with a more complicated invariant *) }}}.
  Proof.
    iIntros (Φ) "(Hc & dia) post".
    wp_with "rlock".
    wp_alloc lock as "(Hlock & plock & Hrlock & _ & ?)".
    iDestruct (mapsfrom_split _ _ _ (1/2)%Qp (1/2)%Qp {[+rlock+]} ∅ with "plock")
      as "(plock1 & plock2)".
    { symmetry; apply Qp_half_half. }
    { multiset_solver. }
    wp_store_const.
    wp_skip.
    wp_with.
    wp_alloc.
    wp_store.
    wp_with.
    wp_move.

    iRename select (l↤_)%I into "pl".
    iRename select (s↦_)%I into "Hs".
    iRename select (l↦_)%I into "Hl".

    iMod unique as (U) "(u & uu)".
    pose (R := (lock ↤{(1/2)%Qp} ∅ ∗ l ↦ BTuple [()%V; (^17)%V] ∗ l ↤ {[+s+]})%I).
    iMod (inv_alloc invN _ (join_inv_pred lock (U ∨ R)) with "[Hlock]") as "#Hinv".
    { iNext. iRight. simpl. iExists 0. simpl. iFrame. }

    wp_bind (IFork _ _ _).
    iApply (wp_fork with "[$Hs2 $pl Hl plock2]").
    {
      iIntros (r) "(Hr & pl)".
      simpl. clear _tmp.
      wp_with.
      wp_const.
      wp_skip.
      wp_store.
      wp_end_with.
      wp_skip.
      wp_with "lock'".
      wp_load.
      clear _tmp. wp_store_const. wp_skip. clear _tmp.
      wp_const.
      wp_skip.
      rewrite ->2subst_SpecialStoreConst. simpl lsubst.
      (* wp_bind (with: _: _ end)%E. *)
      wp_bind (SpecialStoreConst _ _ _).
      iApply (done_spec_special with "[$Hlock' $Hinv Hl plock2 pl]").
      { iFrame. simpl. cancel_mapsfrom.  }
      iNext. iIntros "Hlock'".
      wp_skip.
      wp_const.
      wp_end_with.
      iFrame.
    }

    iNext; iIntros "Hs2".
    wp_skip.
    wp_with.
    wp_const.
    wp_skip.

    wp_bind (ICall _ _).
    iApply (join_spec_pred with "[$Hjoin $plock1 $Hrlock $uu $u $Hinv]").
    iNext; iIntros "(Hjoin & plock1 & Hrlock & plock2 & Hl & pl)".
    iDestruct (mapsfrom_join lock with "plock1 plock2") as "plock".
    wp_skip.
    wp_load.
    wp_end_with.
    wp_end_with.
    wp_end_with.
    wp_end_with.
    rewrite gmultiset_difference_diag.
    iAssert (lock ↤ ∅)%I with "[plock]" as "plock'".
    rewrite Qp_half_half. cancel_mapsfrom.
    wp_free l.
    wp_skip.
    wp_skip.
    iApply "post".
    iFrame.
    auto.
  Qed.

End fork_join_no_leak.
