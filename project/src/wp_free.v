From iris.base_logic.lib Require Import gen_heap.
From spacelang Require Import wp_state_interp wp_memory.

(* This module establishes a reasoning rule for memory deallocation. *)

(* ------------------------------------------------------------------------ *)

(* Deallocating a memory block requires full ownership of it. It also
   requires proof that this block has no predecessors except possibly
   itself. This prevents the creation of dangling pointers. *)

(* This is a ghost instruction; it has no runtime effect. The memory
   block is deallocated from the conceptual heap; the physical heap
   is unchanged. *)

(* This operation consumes [l ↦ b] and [l ↤ ls], where the multiset [ls] can
   contain the address [l] with an arbitrary multiplicity. It does not require
   any information about the successors of the block [b], and does not update
   their predecessor fields. We adopt a mechanism that allows delayed updates:
   we produce a proof that the location [l] has been deallocated. A separate
   rule allows removing a dead location from a predecessor set. *)

Lemma supd_free_singleton `{!heapG Σ} E l b ls :
  dom ls ⊆ {[l]} →
  ¬ is_stack_cell b →
  l ↦ b ∗ l ↤ ls ={{E}}=∗ †l ∗ ♢(size_block b).
Proof.
  iIntros (Hdom Hncell σ ns κs n) "(Hstate & Hmapsto & Hmapsfrom)".
  destruct_state_interp.
  (* The address [l] must exist in the conceptual heap. *)
  iDestruct (exploit_mapsto with "Hph Hmapsto") as %?.
  (* Deallocate [l] in the conceptual heap. *)
  iMod (ph_free_singleton true θ.(store) l b ls Hdom Hncell with "[$]")
    as "(Hph & Hdag & %facts)".
  destruct facts.
  (* Perform a ghost update to create new diamonds, so as to reflect the
     fact that the available space in the heap increases. *)
  iMod (auth_nat_update_incr _ _ (size_block b) with "Hauth")
    as "[Hauth Hfrag]".
  fold (♢(size_block b))%I.
  erewrite <- available_dealloc by eauto.
  (* Conclude. *)
  iModIntro. iFrame "Hdag Hfrag".
  (* Close the state interpretation invariant. *)
  intro_state_interp. iExists (<[l:=BDeallocated]> <$> θ).
  iFrame "Hph Hauth". iPureIntro.
  splits; eauto 3 with valid_state closed related.
Qed.

Lemma wp_free_singleton `{!heapG Σ} s E i Φ l b ls :
  to_val i = None →
  ¬ is_stack_cell b →
  dom ls ⊆ {[l]} →
  (†l ∗ ♢(size_block b) -∗ wp s E i Φ) -∗
  (l ↦ b ∗ l ↤ ls -∗ wp s E i Φ).
Proof.
  eauto using wp_supd, supd_free_singleton.
Qed.

(* ------------------------------------------------------------------------ *)

(* An open cloud [ocloud antecedents n ls] represents the ownership of a set
   of heap objects whose predecessors are contained in [antecedents], whose
   total size is [n], and whose addresses form the set [ls]. *)

Fixpoint ocloud
  `{!heapG Σ} (antecedents : gset loc) (n : nat) (locs : list loc) : iProp Σ
:=
  match locs with
  | [] =>
      ⌜ n = 0%nat ⌝
  | l :: locs' =>
      ∃ n' b,
        ⌜ n = (size_block b + n')%nat ⌝ ∗
        ⌜ ¬ is_stack_cell b ⌝ ∗
        l ↦ b ∗
        mapsfrom_set l 1%Qp antecedents ∗
        ocloud antecedents n' locs'
  end%I.

(* A closed cloud is an open cloud whose domain is [locs] and whose
   antecedents are contained in [locs]. It is therefore closed under
   predecessors. *)

Definition cloud `{!heapG Σ} (locs : gset loc) n : iProp Σ :=
  ocloud locs n (elements locs).

Notation "locs ☁ n" :=
  (cloud locs n)
  (at level 20) : bi_scope.

(* ------------------------------------------------------------------------ *)

(* Auxiliary lemmas about clouds. *)

(* These two lemmas allow building clouds and are provided for use by an
   end user. *)

Lemma ocloud_empty `{!heapG Σ} antecedents :
  True ⊢ ocloud antecedents 0 [].
Proof.
  simpl ocloud. eauto.
Qed.

Lemma ocloud_cons `{!heapG Σ} antecedents n (locs : list loc) l b ls :
    ocloud antecedents n locs ∗ l ↦ b ∗ l ↤ ls ∗
    ⌜ ¬ is_stack_cell b ⌝ ∗ ⌜ dom ls ⊆ antecedents ⌝
  ⊢ ocloud antecedents (size_block b + n) (l :: locs).
Proof.
  simpl ocloud. iIntros "(Hcloud & ? & ? & %Hncell & %Hdom)".
  do 2 iExists _. rewrite /mapsfrom_set.
  do 2 (iSplit; [ eauto |]).
  iFrame. eauto.
Qed.

(* The following lemmas exploit a cloud and are used in the proof of the
   main lemma, [supd_free_group]. *)

Lemma ocloud_split `{!heapG Σ} antecedents (ls : list loc) : ∀ n,
  ocloud antecedents n ls ⊢
  ([∗ list] l ∈ ls, ∃ b, l ↦ b ∗ ⌜ ¬ is_stack_cell b ⌝) ∗
  ([∗ list] l ∈ ls, mapsfrom_set l 1%Qp antecedents).
Proof.
  induction ls as [| l ls' IH ]; simpl ocloud; intro n.
  { iIntros "_". simpl. eauto. }
  { iIntros "H".
    iDestruct "H" as (n' b) "(_ & %Hncell & ? & ? & Hcloud)".
    rewrite IH. iDestruct "Hcloud" as "[? ?]".
    simpl. iFrame. iExists _. by iFrame. }
Qed.

Lemma cloud_split `{!heapG Σ} locs n :
  cloud locs n ⊢
  ([∗ set] l ∈ locs, ∃ b, l ↦ b ∗ ⌜ ¬ is_stack_cell b ⌝) ∗
  ([∗ set] l ∈ locs, mapsfrom_set l 1%Qp locs).
Proof.
  iIntros "Hcloud".
  rewrite /cloud ocloud_split.
  rewrite big_opS_eq /big_opS_def.
  iFrame.
Qed.

Lemma ocloud_disjoint `{!heapG Σ} l b antecedents n (ls : list loc) :
  l ↦ b ∗ ocloud antecedents n ls ⊢ ⌜ l ∉ ls ⌝.
Proof.
  case (decide (l ∈ ls)); intro; [| eauto ].
  rewrite ocloud_split.
  iIntros "(Hl & Hls & _)".
  rewrite big_sepL_elem_of //.
  iDestruct "Hls" as (b') "(? & _)".
  iDestruct (mapsto_ne with "[$] [$]") as %Hneq.
  exfalso. eauto.
Qed.

Lemma gen_heap_valid_ocloud `{!heapG Σ} σ antecedents (ls : list loc) : ∀ n,
  gen_heap_interp σ ∗
  ocloud antecedents n ls -∗
  let locs := list_to_set ls in
  ⌜ locs ⊆ dom σ ∧
    (size_heap (deallocate locs σ) + n = size_heap σ)%nat ⌝.
Proof.
  induction ls as [| l ls' IH ]; simpl ocloud; intro n.
  { iIntros "(_ & %Hn)". iPureIntro. simpl.
    rewrite /deallocate gmap_mset_empty Hn.
    split. set_solver. lia. }
  { iIntros "(Hh & H)".
    iDestruct "H" as (n' b) "(%Hn & _ & ? & ? & Hcloud)".
    iDestruct (IH with "[$]") as "(%Hdom & %Hsize)".
    iDestruct (gen_heap_valid with "[$] [$]") as %Hl.
    assert (l ∈ dom σ) by eauto.
    iDestruct (ocloud_disjoint with "[$]") as "%Hdisj".
    iPureIntro. split; simpl.
    + set_solver.
    + rewrite -Hsize. rewrite Hn. rewrite /deallocate.
      rewrite -> gmap_mset_snoc' by eauto.
      rewrite Nat.add_assoc.
      rewrite size_heap_dealloc //.
      rewrite gmap_lookup_mset_outside //.
      set_solver. }
Qed.

Lemma exploit_cloud `{!heapG Σ} σ locs n :
  ph_interp σ ∗
  cloud locs n -∗
  ⌜ locs ⊆ dom σ ∧
    (size_heap (deallocate locs σ) + n = size_heap σ)%nat ⌝.
Proof.
  rewrite /ph_interp /cloud.
  iIntros "[(Hh & _) Hcloud]".
  iDestruct (gen_heap_valid_ocloud with "[$]") as "[%Hdom %Hsize]".
  iPureIntro.
  rewrite -> list_to_set_elements_L in Hdom, Hsize.
  eauto.
Qed.

(* ------------------------------------------------------------------------ *)

(* Group deallocation. *)

(* This ghost operation consumes a closed cloud [locs☁n]. It produces a
   witness [††locs] that every location [l] in the set [locs] has been
   deallocated, and produces [n] space credits. *)

Lemma supd_free_group `{!heapG Σ} E locs n :
  locs☁n ={{E}}=∗ ††locs ∗ ♢n.
Proof.
  iIntros (σ ns κs nt) "(Hstate & Hcloud)".
  destruct_state_interp.
  (* The addresses [locs] must exist in the conceptual heap, and represent
     a total size of [n]. *)
  iDestruct (exploit_cloud with "[$]") as "[%Hdom %Hsize]".
  (* Deallocate [locs] in the conceptual heap. *)
  iDestruct (cloud_split with "[$]") as "[Hmapsto Hmapsfrom]".
  iMod (ph_free_group true θ.(store) locs with "[$]")
    as "(Hph & Hddag & %Hfacts)".
  destruct Hfacts.
  set (θ' := deallocate locs <$> θ).
  (* The available space in the heap increases by [n]. *)
  assert (Hspace: (available θ + n = available θ')%nat).
  { rewrite /valid_state in Hvalidθ.
    rewrite /available. destruct θ; simpl in *. lia. }
  assert (valid_state θ').
  { rewrite /valid_state. rewrite /valid_state in Hvalidθ. simpl. lia. }
  (* Perform a ghost update to create [n] new diamonds. *)
  iMod (auth_nat_update_incr _ _ n with "Hauth") as "[Hauth Hfrag]".
  rewrite Hspace.
  fold (♢n)%I.
  (* Conclude. *)
  iModIntro. iFrame "Hddag Hfrag".
  (* Close the state interpretation invariant. *)
  intro_state_interp. iExists θ'.
  iFrame "Hph Hauth". iPureIntro.
  splits; eauto 3 using closed_free_group with valid_state related.
Qed.

Lemma wp_free_group `{!heapG Σ} s E i Φ locs n :
  to_val i = None →
  (††locs ∗ ♢n -∗ wp s E i Φ) -∗
  (locs☁n -∗ wp s E i Φ).
Proof.
  eauto using wp_supd, supd_free_group.
Qed.

Lemma ocloud_cleanup `{!heapG Σ} E n (P : list loc) (D Q : gset loc) :
  ††D ∗ ocloud Q n P ={{E}}=∗ ocloud (Q ∖ D) n P.
Proof.
  (* Transforming the multiset [ls] into a multiset [ls \ D] when [D] is a set *)
  assert (forall ls : gmultiset loc, exists Dm, dom Dm ⊆ D /\ dom (ls ∖ Dm) ⊆ dom ls ∖ D) as dom_diff. {
    intros ls.
    exists (GMultiSet (map_imap (fun l n => if decide (l ∈ D) then Some n else None) (gmultiset_car ls))).
    split; intros x I.
    - apply gmultiset_elem_of_dom in I.
      rewrite /elem_of /gmultiset_elem_of /multiplicity /= map_lookup_imap in I.
      destruct (gmultiset_car ls !! x); simpl in I; try lia.
      destruct decide eqn:Hx in I; auto; lia.
    - apply elem_of_difference.
      apply gmultiset_elem_of_dom in I.
      rewrite /elem_of /gmultiset_elem_of multiplicity_difference /= /multiplicity /= map_lookup_imap in I.
      destruct (gmultiset_car ls !! x) eqn:Ex; try lia.
      destruct decide eqn:Hx in I; auto. simpl in I; lia.
      split; auto. apply gmultiset_elem_of_dom.
      rewrite /elem_of /gmultiset_elem_of /multiplicity Ex //.
  }
  iIntros (σ ns κs nt) "(Hstate & #Dd & Cloud)".
  iInduction P as [ | l P] "IH" forall (n).
  now iFrame; auto.
  simpl ocloud.
  iDestruct "Cloud" as (n' b Heq Hb) "(Hl & Pl & Cloud)".
  iSpecialize ("IH" with "Hstate").
  iMod ("IH" with "Cloud") as "(Hstate & Cloud)".
  iDestruct "Pl" as (ls) "(Pl & %)".
  destruct (dom_diff ls) as (Dm & Hi1 & Hi2) .
  destruct_state_interp.
  iPoseProof (exploit_discarded_group with "Hph Dd") as "%M".
  iMod ((ph_cleanup _ _ _ _ Dm) with "[$Hph $Pl]") as "(Hph & Hpl)".
  { intros; apply M. apply Hi1. apply gmultiset_elem_of_dom. assumption. }
  iModIntro.
  iSplitL "Hph Hauth". now iExists _; iFrame; auto.
  iExists n', b. iFrame. iSplitR; auto. iSplitR; auto.
  iExists _. iFrame. iPureIntro. multiset_solver.
Qed.
