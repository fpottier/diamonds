From stdpp Require Import gmap gmultiset.
From spacelang Require Import stdpp.
From spacelang Require Import spacelang hypotheses gc tactics.
Open Scope nat_scope.

(* This file proves a number of results about the operational semantics
   of SpaceLang (which is defined in the files spacelang.v and gc.v). *)

(* One important result is the fact that in an arbitrary execution, the size
   of the heap is bounded by the limit [maxsize] that was chosen at the
   beginning. This is property of the operational semantics; it is true of
   every program. *)

(* Another important lemma, [alloc_is_reducible], states that the allocation
   instruction [EAlloc] succeeds, provided there remains enough free space
   in the heap. It is later used in the proof of the reasoning rule
   [wp_alloc]. *)

(* ------------------------------------------------------------------------ *)

(* Lemmas about [gc]. *)

(* Garbage collection preserves the validity of the machine state. *)

Lemma block_evolution_non_size_increasing unreachable b b' :
  block_evolution unreachable b b' →
  size_block b' ≤ size_block b.
Proof.
  destruct 1 as [ | (? & ?) ].
  + subst. lia.
  + unfold deallocated in *; subst; simpl in *. lia.
Qed.

Lemma store_evolution_non_size_increasing s s' :
  store_evolution s s' →
  size_heap s' ≤ size_heap s.
Proof.
  intros [ Hdom Hl ].
  rewrite /size_heap.
  eapply map_fold_ind_2 with (P := λ size1 size2 m1 m2, size1 ≤ size2).
  - lia.
  - lia.
  - split; now apply None_equiv.
  - lia.
  - intros l b1 b2 s1 s2 size1 size2 _ _ Hindom1 Hindom2 ?.
    assert (Hlh: l ∈ dom s) by (rewrite elem_of_dom Hindom2; eauto).
    specialize (Hl l Hlh).
    apply block_evolution_non_size_increasing in Hl.
    do 2 (erewrite lookup_total_correct in Hl; eauto).
    lia.
Qed.

Lemma gc_preserves_valid_state σ σ' :
  gc σ σ' ->
  valid_state σ ->
  valid_state σ'.
Proof.
  rewrite /gc /valid_state.
  intros [ Hevol ? ] ?.
  specialize (store_evolution_non_size_increasing _ _ Hevol); intros.
  lia.
Qed.

Global Hint Resolve gc_preserves_valid_state : valid_state.

Lemma gc_preserves_maxsize σ σ' :
  gc σ σ' ->
  maxsize σ = maxsize σ'.
Proof.
  rewrite /gc. tauto.
Qed.

(* The initial state is closed. *)

Lemma closed_init maxsize :
  closed (store (init maxsize)).
Proof.
  intros ? ?.
  rewrite /init /successor /successors lookup_empty. set_solver.
Qed.

(* Garbage collection preserves the fact that the store is closed. *)

Lemma gc_preserves_closedness σ1 σ2 :
  gc σ1 σ2 →
  closed σ1.(store) →
  closed σ2.(store).
Proof.
  intros (? & _). eauto using store_evolution_preserves_closedness.
Qed.

(* Garbage collection preserves the contents of stack cells. *)

Lemma gc_preserves_stack_cells σ1 σ2 c cell :
  gc σ1 σ2 →
  σ1.(store) !! c = Some cell →
  is_stack_cell cell →
  σ2.(store) !! c = Some cell.
Proof.
  intros (Hevol & _). eauto using store_evolution_preserves_stack_cells.
Qed.

(* ------------------------------------------------------------------------ *)

(* Lemmas about [related]. *)

(* Every store is related to itself. *)

Lemma related_reflexive σ :
  related σ σ.
Proof.
  econstructor; eauto using store_agreement_reflexive.
Qed.

Global Hint Resolve related_reflexive : related.

(* Garbage collection preserves the relation between the physical machine
   state and the conceptual machine state. *)

Lemma gc_preserves_related σ1 σ2 σ :
  related σ1 σ →
  gc σ1 σ2 →
  closed σ1.(store) →
  related σ2 σ.
Proof.
  rewrite /related /gc.
  intros (? & ?) (? & ?) ?. split; [| lia ].
  eauto using store_evolution_preserves_store_agreement.
Qed.

Global Hint Resolve gc_preserves_related : related.

(* The available space in the physical heap is always at least as great as in
   the conceptual heap, provided we allow the GC to perform its job. *)

Lemma store_agreement_size_heap s1 s2 :
  store_agreement s1 s2 →
  ∃ s'1,
  store_evolution s1 s'1 ∧ size_heap s'1 ≤ size_heap s2.
Proof.
  intros Hag. exists (collect s1). split.
  { eauto using store_evolution_collect. }
  rewrite /size_heap /collect.
  eapply map_fold_ind_2 with (P := λ size1 size2 m1 m2, size1 ≤ size2).
  { lia. }
  { lia. }
  { intros i.
    destruct Hag as (Hd, _).
    transitivity (s1 !! i = None).
    - rewrite map_lookup_imap.
      destruct (s1 !! i); split; discriminate || auto.
    - split; apply None_equiv; auto. }
  { lia. }
  intros l b1 b2 s'1 s'2 size1 size2.
  intros ? ? Himap ? ?.
  rewrite map_lookup_imap in Himap.
  case_eq (s1 !! l); [
    intros b Hsome; rewrite Hsome in Himap; simpl in Himap
  | intros Hnone; rewrite Hnone in Himap; simpl in Himap; discriminate ].
  unfold collect_block in Himap. simplify_eq.
  case (decide (reachable s1 l)).
  { intros Hreachable.
    destruct Hag as (_ & _ & Hag).
    specialize (Hag l Hreachable).
    erewrite -> !lookup_total_correct in Hag by eauto. subst.
    lia. }
  { intros _. simpl. lia. }
Qed.

Lemma related_available σ θ :
  related σ θ →
  ∃ σ',
  gc σ σ' ∧ available θ ≤ available σ'.
Proof.
  rewrite /related /available. intros (Hag & ?).
  apply store_agreement_size_heap in Hag.
  destruct Hag as (s'1 & ? & ?).
  exists {| store := s'1; maxsize := σ.(maxsize) |}. simpl.
  split.
  + split; eauto.
  + lia.
Qed.

(* The physical and conceptual stores agree on stack cells. *)

Lemma related_at_stack_cell σ θ c v :
  related σ θ →
  store θ !! c = Some (BCell v) →
  store σ !! c = Some (BCell v).
Proof.
  intros (Hag & _). eauto using store_agreement_at_stack_cell.
Qed.

Lemma related_at_reachable_cell σ θ l b :
  related σ θ →
  reachable (store σ) l →
  store θ !! l = Some b →
  store σ !! l = Some b.
Proof.
  intros (Hag & _). eauto using store_agreement_at_reachable_cell.
Qed.

Lemma related_preserves_freshness σ θ l :
  related σ θ →
  store σ !! l = None →
  store θ !! l = None.
Proof.
  intros (Hag & _). eauto using store_agreement_preserves_freshness.
Qed.

Lemma prove_is_root s c v :
  s !! c = Some (BCell v) →
  is_root s c.
Proof.
  rewrite /is_root. eauto.
Qed.

Local Hint Resolve prove_is_root : is_root.

(* Allocating a new heap cell (and writing its value to a stack cell)
   preserves relatedness. *)

Lemma related_alloc σ1 θ dst l b σ2 θ' :
  related σ1 θ →
  closed (store σ1) →
  closed (store θ) →
  store σ1 !! l = None →
  write_lvalue (LLoc dst) (VLoc l) (<[l:=b]> <$> σ1) σ2 →
  pointers b = ∅ →
  ¬ is_stack_cell b →
  l ≠ dst →
  θ' = <[dst:=BCell (VLoc l)]> <$> (<[l:=b]> <$> θ) →
  related σ2 θ'.
Proof.
  intros (Hag & Hsize). intros. subst θ'.
  inv_write_lvalue. split; simpl in *.
  + match goal with h: _ !! _ = _ |- _ =>
      rewrite -> lookup_insert_ne in h by eauto end.
    eapply store_agreement_alloc; eauto with is_root pointers.
  + lia.
Qed.

(* Allocating a new stack cell preserves relatedness. *)

Lemma related_alloc_cell σ θ c cell :
  related σ θ →
  store σ !! c = None →
  (∀ l, l ∈ pointers cell → reachable (store σ) l) →
  is_stack_cell cell →
  related (<[c:=cell]> <$> σ) (<[c:=cell]> <$> θ).
Proof.
  intros (Hag & Hsize). intros.
  split.
  + apply store_agreement_alloc_cell; auto.
  + auto.
Qed.

Global Hint Resolve related_alloc_cell : related.

Lemma related_alloc_stack_cell σ θ c cell :
  related σ θ →
  store σ !! c = None →
  pointers cell = ∅ →
  is_stack_cell cell →
  related (<[c:=cell]> <$> σ) (<[c:=cell]> <$> θ).
Proof.
  intros ? ? Hpointers ?.
  eapply related_alloc_cell; eauto.
  rewrite Hpointers. set_solver.
Qed.

Global Hint Resolve related_alloc_stack_cell : related.

(* Deallocating an unreachable heap cell in the conceptual heap,
   while leaving the physical heap unchanged, preserves relatedness. *)

Lemma related_free_singleton_unreachable σ θ l b :
  related σ θ →
  store θ !! l = Some b →
  ¬ reachable (store θ) l →
  related σ (<[l:=BDeallocated]> <$> θ).
Proof.
  intros (? & ?) ? ?. split; simpl; [| eauto ].
  eauto using store_agreement_free.
Qed.

Lemma related_free_singleton σ θ l b :
  related σ θ →
  store θ !! l = Some b →
  (∀ m, l ∈ successors (store θ) m → m = l) →
  ¬ is_stack_cell b →
  related σ (<[l:=BDeallocated]> <$> θ).
Proof.
  eauto using related_free_singleton_unreachable, prove_unreachable_singleton.
  (* The fact that [l] is unreachable follows from the fact that it is
     not a stack cell (hence not a root) and has no predecessors except
     possibly itself. *)
Qed.

Lemma related_free_group_unreachable σ θ ls :
  related σ θ →
  ls ⊆ dom (store θ) →
  (∀l, l ∈ ls → ¬ reachable (store θ) l) →
  related σ (deallocate ls <$> θ).
Proof.
  intros (? & ?) ? ?. split; simpl; [| eauto ].
  eauto using store_agreement_deallocate.
Qed.

Lemma related_free_group_closed σ θ ls θ' :
  related σ θ →
  ls ⊆ dom (store θ) →
  (∀ l, l ∈ ls → ¬ is_root (store θ) l) →
  (∀ m m', m' ∈ successors (store θ) m → m' ∈ ls → m ∈ ls) →
  θ' = deallocate ls <$> θ →
  related σ θ'.
Proof.
  intros. subst.
  eauto using related_free_group_unreachable, prove_unreachable_region.
Qed.

Lemma related_free_stack_cell σ θ l cell :
  related σ θ →
  store σ !! l = Some cell →
  store θ !! l = Some cell →
  is_stack_cell cell →
  related (<[l:=BDeallocated]> <$> σ) (<[l:=BDeallocated]> <$> θ).
Proof.
  intros (? & ?) ? ? ?. split; simpl; [| eauto ].
  eapply store_agreement_heap_update.
  auto.
  - exists l. split. exists cell; auto. reflexivity.
  - eauto.
  - intros r; inversion 1.
  - inversion 1.
Qed.

Global Hint Resolve
  related_free_singleton related_free_group_closed related_free_stack_cell
: related.

(* Updating a stack cell in the same way in the physical store and in the
   conceptual store preserves relatedness, provided this update does not
   make a previously-unreachable object reachable again. *)

Lemma related_write_lvalue σ1 σ2 θ1 θ2 c v :
  write_lvalue (LLoc c) v σ1 σ2 →
  (∀ l, l ∈ val_pointer_set v → reachable (store σ1) l) →
  θ2 = (<[c:=BCell v]> <$> θ1) →
  related σ1 θ1 →
  related σ2 θ2.
Proof.
  inversion_clear 1. intros ? ? (Hag & ?). subst.
  split; simpl; [| eauto ].
  eauto using store_agreement_cell_update.
Qed.

Global Hint Resolve related_write_lvalue : related.

(* These lemmas help prove the side condition in the previous lemma. *)

Lemma reachable_update_tuple σ l vs ofs c :
  store σ !! l = Some (BTuple vs) →
  (0 <= ofs < length vs)%nat →
  store σ !! c = Some (BCell (VLoc l)) →
  (∀ l, l ∈ val_pointer_set (vs !!! ofs) → reachable (store σ) l).
Proof.
  intros ??? m Hm.
  assert (m ∈ pointers (BTuple vs))
    by eauto using gmultiset_elem_of_subseteq, pointers_lookup.
  (* There is a path from the root [c], to [l], to [m]. *)
  exists c. split.
  + eexists. eauto.
  + eauto with rtc closed pointers. (* yes! *)
Qed.

Lemma reachable_update_no_pointers (ls : gmultiset loc) s :
  ls = ∅ →
  (∀ l, l ∈ ls → reachable s l).
Proof.
  intros ->. set_solver.
Qed.

Global Hint Resolve reachable_update_no_pointers reachable_update_tuple
  : related.

(* Updating a (reachable) heap cell (by writing a reachable value into it)
   preserves relatedness. *)

Lemma related_heap_update σ θ c w l vs ofs :
  related σ θ →
  closed (store σ) →
  store σ !! c = Some (BCell w) →
  store σ !! l = Some (BTuple vs) →
  (0 <= ofs < length vs)%nat →
  reachable (store σ) l →
  let update := <[l:=BTuple (<[ofs:=w]> vs)]> in
  related (update <$> σ) (update <$> θ).
Proof.
  rewrite /related. simpl.
  intros (? & ?); split; [| eauto ].
  assert (reachable (store θ) l)
    by rewrite -store_agreement_preserves_reachable //.
  eapply store_agreement_heap_update; eauto.
  (* There remains to check that every pointer in the new block is reachable.
     This boils down to checking that the value [w] is reachable, which is
     indeed the case, as it is stored in the stack cell [c]. *)
  intros r.
  rewrite pointers_store //. multiset.
  intros [ Hr | Hr ] ?.
  - apply elem_of_difference_is_elem_of_lhs in Hr. multiset_solver.
  - assert (successor (store σ) c r) by eauto using prove_successor.
    exists c. eauto with is_root rtc.
Qed.

Global Hint Resolve related_heap_update : related.

(* This lemma helps prove the side condition in the previous lemma. *)

(* If the memory location [l] appears in the value [v] that is stored in
   the stack cell [c], then [l] is reachable. *)

Lemma content_of_stack_cell_is_reachable s c v l :
  s !! c = Some (BCell v) →
  l ∈ pointers (BCell v) →
  reachable s l.
Proof.
  intros.
  assert (successor s c l) by eauto with closed pointers.
  exists c. eauto with is_root rtc.
Qed.

Global Hint Resolve content_of_stack_cell_is_reachable : related.

(* If the memory location [l] is stored in the stack cell [c], then
   [l] is reachable. *)

Lemma location_in_stack_cell_is_reachable s c l :
  s !! c = Some (BCell (VLoc l)) →
  reachable s l.
Proof.
  eauto with related pointers.
Qed.

Global Hint Resolve location_in_stack_cell_is_reachable : related.

(* Updating a stack cell preserves relatedness. *)

Lemma related_stack_cell_update σ θ c v v' :
  related σ θ →
  (∀l, l ∈ pointers (BCell v') → reachable (store σ) l) →
  store σ !! c = Some (BCell v) →
  store θ !! c = Some (BCell v) →
  related (<[c:=BCell v']> <$> σ) (<[c:=BCell v']> <$> θ).
Proof.
  intros (? & ?) Hv' ? ?.
  assert (reachable (store θ) c).
  { exists c. split. eexists. eauto. eauto with rtc. }
  rewrite /related. simpl. eauto using store_agreement_heap_update.
Qed.

Global Hint Resolve related_stack_cell_update : related.

(* This helps prove the side condition of the previous lemma. *)

Lemma no_pointers s v :
  val_pointer_list v = [] ->
  (∀l, l ∈ pointers (BCell v) → reachable s l).
Proof.
  intros Hv l. erewrite -> cell_no_pointers by eauto. set_solver.
Qed.

Global Hint Resolve no_pointers : related.

(* ------------------------------------------------------------------------ *)

(* Every reduction step preserves the validity of the machine state. *)

Lemma write_lvalue_preserves_valid_state lv v σ σ' :
  write_lvalue lv v σ σ' →
  valid_state σ →
  valid_state σ'.
Proof.
  destruct 1; unfold valid_state; intros; subst.
  eapply valid_state_update; eauto.
  simpl; lia.
Qed.

Global Hint Resolve write_lvalue_preserves_valid_state : valid_state.

Lemma head_step_preserves_valid_state i σ κs i' σ' fk :
  head_step i σ κs i' σ' fk ->
  valid_state σ ->
  valid_state σ'.
Proof.
  induction 1; subst; eauto with valid_state.
Qed.

Global Hint Resolve head_step_preserves_valid_state : valid_state.

Lemma gc_head_step_preserves_valid_state i σ κs i' σ' fk :
  gc_head_step i σ κs i' σ' fk ->
  valid_state σ ->
  valid_state σ'.
Proof.
  induction 1; subst; eauto with valid_state.
Qed.

Global Hint Resolve head_step_preserves_valid_state : valid_state.

(* ------------------------------------------------------------------------ *)

(* Every reduction step preserves the closedness of the physical store. *)

Lemma closed_store_update σ l b' :
  closed (store σ) →
  (∀ l : loc, l ∈ block_pointer_set b' → l ∈ dom (store σ)) →
  closed (store (<[l:=b']> <$> σ)).
Proof.
  unfold closed. simpl. intros Hclosed Hb' m m' Hsucc.
  case (decide (m = l)); intro; [ subst m |].
  { rewrite -> successor_insert in Hsucc. set_solver. }
  { rewrite -> successor_insert_ne in Hsucc by eauto. set_solver. }
Qed.

Global Hint Resolve closed_store_update : closed.

Lemma write_lvalue_preserves_closedness lv v σ σ' :
  write_lvalue lv v σ σ' →
  closed (store σ) →
  (∀ l, l ∈ val_pointer_set v → l ∈ dom (store σ)) →
  closed (store σ').
Proof.
  destruct 1. intros. subst. eauto with closed.
Qed.

Lemma closed_alloc σ l b n :
  closed (store σ) →
  b = BTuple (replicate n VUnit) →
  closed (store (<[l:=b]> <$> σ)).
Proof.
  intros. subst. eapply closed_store_update; [ eauto |].
  erewrite block_no_pointers by eauto.
  set_solver.
Qed.

Global Hint Resolve closed_alloc : closed.

Lemma simplify_val_pointer_set_VLoc σ m :
  m ∈ dom (store σ) →
  (∀ l, l ∈ val_pointer_set (VLoc m) → l ∈ dom (store σ)).
Proof.
  rewrite /val_pointer_set. simpl. set_solver.
Qed.

Global Hint Resolve simplify_val_pointer_set_VLoc : closed.

Global Hint Extern 1 (?l ∈ dom _) => set_solver : closed.

Lemma simplify_val_pointer_set_load σ l vs ofs :
  closed (store σ) →
  store σ !! l = Some (BTuple vs) →
  0 <= ofs < length vs →
  (∀ l, l ∈ val_pointer_set (vs !!! ofs) → l ∈ dom (store σ)).
Proof.
  intros Hclosed Hlookup Hofs l' Hl'.
  pose proof (pointers_lookup vs ofs Hofs).
  assert (Hsucc: successor (store σ) l l').
  { rewrite /successor /successors Hlookup.
    eauto using gmultiset_elem_of_subseteq. }
  eauto.
Qed.

Global Hint Resolve simplify_val_pointer_set_load : closed.

Lemma simplify_val_point_set_load_inert σ v :
  val_pointer_list v = [] →
  (∀ l, l ∈ val_pointer_set v → l ∈ dom (store σ)).
Proof.
  rewrite /val_pointer_set. intros ->. simpl. set_solver.
Qed.

Global Hint Resolve simplify_val_point_set_load_inert : closed.

Lemma exploit_read_lvalue_closed c σ v l' :
  read_lvalue c σ v →
  closed (store σ) →
  l' ∈ val_pointer_set v →
  l' ∈ dom (store σ).
Proof.
  destruct 1. eauto with closed.
Qed.

Lemma closed_store_update_helper_lemma σ v l vs ofs :
  store σ !! l = Some (BTuple vs) →
  0 <= ofs < length vs →
  closed (store σ) →
  ∀ l, l ∈ block_pointer_set (BTuple (<[ofs:=v]> vs)) →
  (∀ l, l ∈ val_pointer_set v → l ∈ dom (store σ)) →
  l ∈ dom (store σ).
Proof.
  intros ???.
  rewrite -pointers_def pointers_store //.
  intros m. multiset. intros [ Hm | Hm ]; intro.
  { apply elem_of_difference_is_elem_of_lhs in Hm. eauto with closed. }
  { eauto. }
Qed.

Global Hint Resolve closed_store_update_helper_lemma : closed.

Lemma head_step_preserves_closedness i σ κs i' σ' fk :
  head_step i σ κs i' σ' fk ->
  closed (store σ) ->
  closed (store σ').
Proof.
  induction 1; subst;
  eauto using write_lvalue_preserves_closedness,
  exploit_read_lvalue_closed with closed.
Qed.

Lemma prove_reachable_in_one_hop s c w :
  s !! c = Some (BCell w) →
  ∀ l, l ∈ pointers (BCell w) → reachable s l.
Proof.
  intros.
  exists c. split.
  + rewrite /is_root. eauto.
  + assert (successor s c l) by eauto with closed pointers.
    eauto with rtc.
Qed.

Global Hint Resolve prove_reachable_in_one_hop : reachable.

Lemma prove_reachable_in_one_hop_VLoc s c l :
  s !! c = Some (BCell (VLoc l)) →
  reachable s l.
Proof.
  eauto with reachable pointers.
Qed.

Global Hint Resolve prove_reachable_in_one_hop_VLoc : reachable.

(* Group deallocation preserves closedness. *)

Lemma closed_free_group s locs s' :
  closed s →
  s' = deallocate locs s →
  closed s'.
Proof.
  rewrite /closed /successor.
  intros Hclosed -> l l'.
  rewrite dom_deallocate.
  case (decide (l ∈ locs)); intro.
  + rewrite successors_deallocate_inside //. set_solver.
  + rewrite successors_deallocate_outside //. eauto.
Qed.

(* ------------------------------------------------------------------------ *)

(* The [maxsize] component of the machine state remains unchanged during
   execution. *)

Lemma write_lvalue_preserves_maxsize lv v σ σ' :
  write_lvalue lv v σ σ' →
  maxsize σ = maxsize σ'.
Proof.
  destruct 1; subst; auto.
Qed.

Lemma allocation_preserves_maxsize σ l b :
  maxsize σ = maxsize (<[l:=b]> <$> σ).
Proof.
  reflexivity.
Qed.

Lemma head_step_preserves_maxsize i σ κs i' σ' fk :
  head_step i σ κs i' σ' fk -> maxsize σ = maxsize σ'.
Proof.
  induction 1; subst; eauto 3 using eq_trans,
  write_lvalue_preserves_maxsize, allocation_preserves_maxsize.
Qed.

Lemma gc_head_step_preserves_maxsize i σ κs i' σ' fk :
  gc_head_step i σ κs i' σ' fk -> maxsize σ = maxsize σ'.
Proof.
  induction 1; subst; eauto 4 using eq_trans,
  gc_preserves_maxsize, head_step_preserves_maxsize.
Qed.

(* ------------------------------------------------------------------------ *)

(* A technical result: if every reduction step from a machine state [σ] to a
   machine state [σ'] is such that [R σ σ'] holds, where [R] is reflexive
   and transitive, then in an arbitrary sequence of reduction steps, the
   initial state and the final state are also related by [R]. *)

Lemma erased_step_state_ind (R : state -> state -> Prop) `{PreOrder _ R} :
  (forall σ1 σ2, gc σ1 σ2 → R σ1 σ2) →
  (forall x y κs fk, head_step x.1 x.2 κs y.1 y.2 fk -> R x.2 y.2) ->
  (forall x y, rtc erased_step x y -> R x.2 y.2).
Proof.
  intros GC SR.
  cut (∀ x y : cfg spacelang, erased_step x y → R x.2 y.2).
  - intros IH x y steps. induction steps.
    + reflexivity.
    + etransitivity; eauto.
  - intros [] [] [κs Hstep].
    inv_step.
    inv_prim_step.
    inv_gc_head_step.
    etransitivity.
    + eauto.
    + apply (SR (_, _) (_, _) _ _ Hstep).
Qed.

(* ------------------------------------------------------------------------ *)

(* If initially the heap size is below the limit, then, at the end of an
   arbitrary execution, the heap size is still below the limit, and the limit
   has not been tampered with. *)

Lemma heap_size_is_bounded_by_maxsize σ ts σ' ts' :
  rtc erased_step (ts, σ) (ts', σ') ->
  size_heap σ.(store) <= σ.(maxsize) ->
  size_heap σ'.(store) <= σ'.(maxsize) /\ σ.(maxsize) = σ'.(maxsize).
Proof.
  unshelve eapply (erased_step_state_ind
    (fun σ σ' => valid_state σ → valid_state σ' ∧ maxsize σ = maxsize σ')).
  { split; auto. intro; intuition congruence. }
  - clear. eauto using gc_preserves_valid_state, gc_preserves_maxsize.
  - clear. intros [] [];
    eauto using head_step_preserves_valid_state, head_step_preserves_maxsize.
Qed.

(* ------------------------------------------------------------------------ *)

(* If the initial machine state is [init maxsize], then, at the end of an
   arbitrary execution, the heap size is still at most [maxsize]. *)

Lemma heap_size_is_bounded_by_maxsize_init maxsize ts ts' σ' :
  rtc erased_step (ts, init maxsize) (ts', σ') ->
  size_heap σ'.(store) <= maxsize.
Proof.
  intros steps.
  apply heap_size_is_bounded_by_maxsize in steps; simpl in *;
  rewrite ?size_empty_heap; lia.
Qed.

(* ------------------------------------------------------------------------ *)

(* A deterministic way of choosing a location [l] outside of a set [locs]. *)

Section Fresh.

Open Scope Z_scope.

Definition fresh_loc (locs : gset loc) : loc :=
  set_fold (λ k r, (1 + k) `max` r) 1 locs.

Local Lemma fresh_loc_fresh locs :
  fresh_loc locs ∉ locs.
Proof.
  cut (∀ l, l ∈ locs → l < fresh_loc locs).
  { intros help Hf%help. lia. }
  apply (set_fold_ind_L (λ r locs, ∀ l, l ∈ locs → l < r));
    set_solver by eauto with lia.
Qed.

Local Lemma fresh_loc_ne σ l l' b' :
  l = fresh_loc (dom (store σ)) →
  store σ !! l' = Some b' →
  l ≠ l'.
Proof.
  intros.
  assert (l ∉ dom (store σ)) by (subst l; eauto using fresh_loc_fresh).
  assert (l' ∈ dom (store σ)) by eauto with in_dom.
  set_solver.
Qed.

Local Lemma fresh_loc_lookup σ l :
  l = fresh_loc (dom (store σ)) →
  store σ !! l = None.
Proof.
  intros; subst. apply not_elem_of_dom, fresh_loc_fresh.
Qed.

End Fresh.

Local Hint Resolve fresh_loc_ne fresh_loc_lookup : fresh.

(* ------------------------------------------------------------------------ *)

(* Because it is always possible to pick a fresh memory location, the
   instruction [IAlloc] is reducible, provided there remains sufficient
   space in the heap. *)

Lemma alloc_is_reducible b dst n v σ :
  valid_state σ →
  size_block b <= available σ →
  b = BTuple (replicate n VUnit) →
  let l := fresh_loc (dom σ.(store)) in
  let σ' := <[l := b]> <$> σ in
  (* write_lvalue dst (VLoc l) σ' σ'' *)
    σ.(store) !! dst = Some (BCell v) →
    let σ'' := <[dst := BCell (VLoc l)]> <$> σ' in
  head_step
    (IAlloc (LLoc dst) n) σ []
    ISkip σ'' [].
Proof.
  intros. unfold valid_state, available in *.
  econstructor; eauto with fresh; fold l.
  - eauto with fresh valid_state lia.
  - econstructor; [| eauto ].
    (* An ad hoc store lookup lemma. *)
    assert (dst ∈ dom (store σ)) by eauto.
    assert (l ≠ dst).
    { pose proof (fresh_loc_fresh (dom (store σ))) as Hfresh.
      fold l in Hfresh. set_solver. }
    rewrite lookup_insert_ne //.
Qed.

Global Hint Resolve alloc_is_reducible : head_step.

(* Because it is always possible to pick a fresh memory location, the
   instruction [IAlloca] is reducible. *)

Lemma alloca_is_reducible x i σ :
  let b := BCell VUnit in
  valid_state σ →
  let c := fresh_loc (dom σ.(store)) in
  let σ' := <[c := b]> <$> σ in
  head_step
    (IAlloca x i) σ []
    (IAllocaActive c (subst x (LLoc c) i)) σ' [].
Proof.
  eauto with fresh head_step.
Qed.

Global Hint Resolve alloca_is_reducible : head_step.

(* Because it is always possible to pick a fresh memory location, the
   instruction [IFork] is reducible. *)

Lemma fork_is_reducible (src : loc) x i σ v :
  let b := BCell v in
  σ.(store) !! src = Some b →
  valid_state σ →
  let c := fresh_loc (dom σ.(store)) in
  let σ' := <[src := BCell VUnit]> <$> (<[c := b]> <$> σ) in
  head_step
    (IFork (LLoc src) x i) σ []
    ISkip σ' [IAllocaActive c (subst x (LLoc c) i)].
Proof.
  intros.
  assert (c ≠ src) by eauto with fresh.
  assert (write_lvalue (LLoc src) VUnit (<[c:=BCell v]> <$> σ) σ').
  { econstructor; [| eauto ]. simpl.
    rewrite lookup_insert_ne //. }
  eauto with fresh head_step.
Qed.

Global Hint Resolve fork_is_reducible : head_step.
