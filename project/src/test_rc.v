From spacelang Require Import proofmode notation rc rcproofmode.

Section reference_counting_examples.

Context `{!heapG Σ}.

Example move_ui (r s : loc) (i : Int63.int) :
  {{{ r ↦ BCell ()%V ∗ s ↦ BCell (VInt i) }}}
    IMove r s
  {{{ RET tt; r ↦ BCell (VInt i) ∗ s ↦ BCell (VInt i) }}}.
Proof.
  iIntros (Φ) "? post".
  destruct_stars.
  wp_move.
  iApply "post".
  iFrame.
Qed.

Example move_ul (r s : loc) (l : loc) :
  {{{ r ↦ BCell ()%V ∗ s ↦ BCell (VLoc l) ∗ l ↤ℕ 3 }}}
    IMove r s
  {{{ RET tt; r ↦ BCell (VLoc l) ∗ s ↦ BCell (VLoc l) ∗ l ↤ℕ 4 }}}.
Proof.
  iIntros (Φ) "? post".
  destruct_stars.
  wp_move.
  iApply "post".
  iFrame.
Qed.

Example move_lu (r s : loc) (l : loc) :
  {{{ r ↦ BCell (VLoc l) ∗ s ↦ BCell ()%V ∗ l ↤ℕ 3 }}}
    IMove r s
  {{{ RET tt; r ↦ BCell ()%V ∗ s ↦ BCell ()%V ∗ l ↤ℕ 2 }}}.
Proof.
  iIntros (Φ) "? post".
  destruct_stars.
  wp_move.
  iApply "post".
  iFrame.
Qed.

Example move_ll (r s : loc) (l : loc) :
  {{{ r ↦ BCell (VLoc l) ∗ s ↦ BCell (VLoc l) ∗ l ↤ℕ 3 }}}
    IMove r s
  {{{ RET tt; r ↦ BCell (VLoc l) ∗ s ↦ BCell (VLoc l) ∗ l ↤ℕ 3 }}}.
Proof.
  iIntros (Φ) "? post".
  destruct_stars.
  Fail wp_move. (* move l l unsupported for now, but one can use
  IConst, see example below *)
Abort.

Example move_ll (r s : loc) (l : loc) :
  {{{ r ↦ BCell (VLoc l) ∗ s ↦ BCell (VLoc l) ∗ l ↤ℕ 3 }}}
    IConst r VUnit;;
    IMove r s
  {{{ RET tt; r ↦ BCell (VLoc l) ∗ s ↦ BCell (VLoc l) ∗ l ↤ℕ 3 }}}.
Proof.
  iIntros (Φ) "? post".
  destruct_stars.
  wp_const.
  wp_move.
  iApply "post".
  iFrame.
Qed.

Example move_ll' (r s : loc) (l l' : loc) :
  {{{ r ↦ BCell (VLoc l) ∗ s ↦ BCell (VLoc l') ∗ l ↤ℕ 3 ∗ l' ↤ℕ 3 }}}
    IMove r s
  {{{ RET tt; r ↦ BCell (VLoc l') ∗ s ↦ BCell (VLoc l') ∗ l ↤ℕ 2 ∗ l' ↤ℕ 4 }}}.
Proof.
  iIntros (Φ) "? post".
  destruct_stars.
  wp_move.
  iApply "post".
  simpl.
  iFrame.
Qed.

Theorem swap_cell (a b : loc) (v w : value) nv nw :
  {{{
    a ↦ BCell v ∗
    b ↦ BCell w ∗
    v ↤ℕ? nv ∗
    w ↤ℕ? nw
  }}}
    (with: "tmp":
      IMove "tmp" a;;
      IMove a b;;
      IMove b "tmp"
    end)%E
  {{{ RET tt;
    a ↦ BCell w ∗
    b ↦ BCell v ∗
    v ↤ℕ? nv ∗
    w ↤ℕ? nw
  }}}.
Proof.
  iIntros (Φ) "(Ha & Hb & pv & pw) Post".
  wp_with.
  wp_move.
  wp_move.
  wp_move.
  wp_end_with.
  iApply "Post".
  iFrame.
  cancel_mapsfrom_nat.
Qed.

Theorem load_simple_example1 (dst src l : loc) (v w : value) qv nv :
  {{{
    dst ↦ BCell VUnit ∗
    src ↦ BCell (VLoc l) ∗
    l ↦ BTuple [v; w] ∗
    v ↤ℕ?{qv} nv
  }}}
    (ILoad dst src 0)
  {{{ RET tt;
    dst ↦ BCell v ∗
    src ↦ BCell (VLoc l) ∗
    l ↦ BTuple [v; w] ∗
    v ↤ℕ?{qv} (nv + 1)
  }}}.
Proof.
  iIntros (Φ) "? Post".
  destruct_stars.
  wp_load.
  iApply "Post".
  iFrame.
Qed.

Theorem load_simple_example2 (dst src l l' : loc) (v w : value) qv nv nl :
  {{{
    dst ↦ BCell (VLoc l') ∗
    src ↦ BCell (VLoc l) ∗
    l ↦ BTuple [v; w] ∗
    l' ↤ℕ nl ∗
    v ↤ℕ?{qv} nv
  }}}
    (ILoad dst src 0)
  {{{ RET tt;
    dst ↦ BCell v ∗
    src ↦ BCell (VLoc l) ∗
    l ↦ BTuple [v; w] ∗
    l' ↤ℕ (nl - 1) ∗
    v ↤ℕ?{qv} (nv + 1)
  }}}.
Proof.
  iIntros (Φ) "? Post".
  destruct_stars.
  (* TODO: clearer error message if [l ↤ nl]'s fraction is not 1? *)
  wp_load.
  iApply "Post".
  iFrame.
Qed.

Theorem store_simple_example (dst src l l' : loc) (v w : value) nv :
  {{{
    dst ↦ BCell (VLoc l) ∗
    src ↦ BCell ()%V ∗
    l ↦ BTuple [v; w] ∗
    v ↤ℕ? nv
  }}}
    (IStore dst 0 src)
  {{{ RET tt;
    dst ↦ BCell (VLoc l) ∗
    src ↦ BCell ()%V ∗
    l ↦ BTuple [(); w] ∗
    v ↤ℕ? (nv - 1)
  }}}.
Proof.
  iIntros (Φ) "? Post".
  destruct_stars.
  wp_store.
  iApply "Post".
  iFrame.
Qed.

Theorem store_simple_example_rev (dst src l l' : loc) (v w : value) qv nv :
  {{{
    dst ↦ BCell (VLoc l) ∗
    src ↦ BCell v ∗
    l ↦ BTuple [(); w] ∗
    v ↤ℕ?{qv} nv
  }}}
    (IStore dst 0 src)
  {{{ RET tt;
    dst ↦ BCell (VLoc l) ∗
    src ↦ BCell v ∗
    l ↦ BTuple [v; w] ∗
    v ↤ℕ?{qv} (nv + 1)
  }}}.
Proof.
  iIntros (Φ) "? Post".
  destruct_stars.
  wp_store.
  iApply "Post".
  iFrame.
Qed.

Theorem alloc_simple_example (dst : loc) (w : value) nw :
  {{{
    dst ↦ BCell w ∗
    w ↤ℕ? nw ∗
    ♢6
  }}}
    (IAlloc dst 4)
  {{{ l, RET tt;
    dst ↦ BCell (VLoc l) ∗
    w ↤ℕ? (nw - 1) ∗
    l ↦ BTuple [(); (); (); ()] ∗
    ♢1
  }}}.
Proof.
  iIntros (Φ) "? Post".
  destruct_stars.
  wp_alloc.
  iApply "Post".
  iFrame.
Qed.

Theorem init_simple_example (dst r1 r2 : loc) (n1 n2 : Int63.int) (l l' : loc) (w : value) nw :
  {{{
    r1 ↦ BCell (VInt n1) ∗
    r2 ↦ BCell (VInt n2) ∗
    dst ↦ BCell w ∗
    w ↤ℕ? nw ∗
    ♢6
  }}}
    dst <:- init [[r1, r2]]%V
  {{{ l, RET tt;
    r1 ↦ BCell (VInt n1) ∗
    r2 ↦ BCell (VInt n2) ∗
    dst ↦ BCell (VLoc l) ∗
    w ↤ℕ? (nw - 1) ∗
    l ↦ BTuple [VInt n1; VInt n2] ∗
    ♢3
  }}}.
Proof.
  iIntros (Φ) "? Post".
  destruct_stars.
  rcwp_init.
  iModIntro.
  iApply "Post".
  iFrame.
Qed.

Theorem free_simple_example (l : loc) :
  {{{
    l ↦ BTuple [(); (); ()] ∗
    l ↤ℕ 0 ∗
    ♢ 3
  }}}
    skip;; skip
  {{{ RET tt;
    ♢ 7
  }}}.
Proof.
  iIntros (Φ) "? Post".
  destruct_stars.
  rcwp_free l.
  wp_skip.
  wp_skip.
  iApply "Post".
  iFrame. auto.
Qed.

Theorem free_locs_example (l l1 l2 : loc) n1 n2 :
  {{{
    l ↦ BTuple [(#l1)%V; #l2; #l2; ()] ∗
    l ↤ℕ 0 ∗
    l1 ↤ℕ n1 ∗
    l2 ↤ℕ n2 ∗
    ♢ 3
  }}}
    skip;; skip
  {{{ RET tt;
    l1 ↤ℕ (n1 - 1) ∗
    l2 ↤ℕ (n2 - 2) ∗
    ♢ 8
  }}}.
Proof.
  iIntros (Φ) "(Hl & pl & p1 & p2 & dia) Post".
  destruct_stars.
  wp_free_cleanup l.
  wp_skip.
  wp_skip.
  iApply "Post"; iFrame; auto.
Qed.

Theorem free_with_vmapsfroms (l l1 l2 : loc) (v3 v4 : value) n1 n2 n3 n4 :
  {{{
    l ↦ BTuple [(#l1)%V; v4; #l2; #l2; v4; v4; (); v3; v3; v4; v3] ∗
    l ↤ℕ 0 ∗
    l1 ↤ℕ n1 ∗
    l2 ↤ℕ n2 ∗
    v3 ↤ℕ? n3 ∗
    v4 ↤ℕ? n4 ∗
    ♢ 3
  }}}
    skip;; skip
  {{{ RET tt;
    l1 ↤ℕ (n1 - 1) ∗
    l2 ↤ℕ (n2 - 2) ∗
    v3 ↤ℕ? (n3 - 3) ∗
    v4 ↤ℕ? (n4 - 4) ∗
    ♢ 15
  }}}.
Proof.
  iIntros (Φ) "? Post".
  destruct_stars.
  wp_free_cleanup l.
  wp_skip.
  wp_skip.
  iApply "Post"; iFrame; auto.
Qed.

End reference_counting_examples.
