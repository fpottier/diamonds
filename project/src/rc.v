From iris.proofmode Require Export coq_tactics reduction.
From iris.proofmode Require Export proofmode.
From iris.program_logic Require Import atomic.
From iris Require Import spec_patterns intro_patterns.
From spacelang Require Export ph wp instances notation.

Lemma trivial_multiset : forall n, ∃ x : gmultiset loc, n = size x.
Proof.
  intros n.
  exists (list_to_set_disj (replicate n 0)).
  induction n. reflexivity.
  rewrite /= gmultiset_size_disj_union gmultiset_size_singleton // -IHn //.
Qed.

Definition mapsfrom_nat `{!heapG Σ} l q n : iProp Σ :=
  ∃ ls, (l ↤{q} ls ∗ ⌜n = size ls⌝)%I.

Definition vmapsfrom_nat `{!heapG Σ} v q n : iProp Σ :=
  (* alternatively we could pattern match on v before this existential
     so that e.g. [vmapsfrom_nat VUnit q n] is convertible to [True], but
     this [∃ls] let us reuse the work already done in the different
     [wp_] lemmas *)
  ∃ ls, (v ↤?{q} ls ∗ ⌜n = size ls⌝)%I.

Notation "v ↤ℕ?{ q } n" :=
  (vmapsfrom_nat v%V q n%nat)
  (at level 20, format "v ↤ℕ?{ q } n")
: bi_scope.

Notation "v ↤ℕ? n" :=
  (vmapsfrom_nat v%V 1%Qp n%nat)
  (at level 20, format "v ↤ℕ? n")
: bi_scope.

Notation "v ↤ℕ{ q } n" :=
  (mapsfrom_nat v%V q n%nat)
  (at level 20, format "v ↤ℕ{ q } n")
: bi_scope.

Notation "v ↤ℕ n" :=
  (mapsfrom_nat v%V 1%Qp n%nat)
  (at level 20, format "v ↤ℕ n")
: bi_scope.

Section reference_counting.

Context `{!heapG Σ}.

Lemma mapsfrom_nat_0 l q : (l ↤ℕ{q} 0)%I ≡ (l ↤{q} ∅)%I.
Proof.
  apply bi.equiv_entails; split.
  - iIntros "p"; iDestruct "p" as (ls) "(p & %)".
    now rewrite (gmultiset_size_empty_inv ls).
  - iIntros "p". iExists ∅; auto.
Qed.

Lemma mapsfrom_nat_weaken l q n1 n2 :
  (n1 <= n2)%nat →
  l ↤ℕ{q} n1 -∗
  l ↤ℕ{q} n2.
Proof.
  iIntros (L) "H". iDestruct "H" as (ls) "(H & %Hn1)".
  iExists (ls ⊎ repeatm 0 (n2 - n1)).
  iSplit.
  - iApply (mapsfrom_weaken with "H").
    apply gmultiset_disj_union_subseteq_l.
  - iPureIntro.
    rewrite gmultiset_size_disj_union -Hn1 size_repeatm.
    lia.
Qed.

(* Converting an iterated separating conjunction of mapsfroms from and
   into the reference counting version *)

Lemma list_mapsfrom_nat locs ns :
  ([∗ list] l; n ∈ locs; ns, l ↤ℕ n)%I
    ≡
  (∃ lss : list (gmultiset loc),
      ⌜Forall2 (fun ls n => n = size ls) lss ns⌝ ∗
      [∗ list] l; ls ∈ locs; lss, l ↤ ls)%I.
Proof.
  revert ns; induction locs as [| loc locs IHlocs]; intros ns; apply bi.equiv_entails; split.
  - iIntros "Hns".
    iDestruct (big_sepL2_nil_inv_l with "Hns") as "->".
    iExists []. auto.
  - iIntros "Hlss". iDestruct "Hlss" as (lss Hlss) "H".
    iDestruct (big_sepL2_nil_inv_l with "H") as "->".
    apply Forall2_nil_inv_l in Hlss. subst; auto.
  - iIntros "Hns".
    iDestruct (big_sepL2_cons_inv_l with "Hns") as (n ns' ->) "(Hn & Hns)".
    iDestruct "Hn" as (ls) "(Hls & %Hn)".
    iDestruct (IHlocs ns') as "(IH & _)".
    iDestruct ("IH" with "Hns") as (lss) "(%Hf & Hlss)".
    iExists (ls :: lss). iSplit; auto.
    iApply big_sepL2_cons. iFrame.
  - iIntros "Hlss". iDestruct "Hlss" as (lss Hlss) "Hlss".
    iDestruct (big_sepL2_cons_inv_l with "Hlss") as (ls lss' ->) "(Hls & Hlss)".
    destruct (Forall2_cons_inv_l _ _ _ _ Hlss) as (n & ns' & Hn & Hlss' & ->).
    iDestruct (IHlocs ns') as "(_ & IH)".
    iDestruct ("IH" with "[Hlss]") as "Hns".
    now iExists lss'; iSplit.
    iApply big_sepL2_cons; iFrame.
    iExists _; auto.
Qed.

Lemma list_mapsfrom_nat_pairs (pairs : list (loc * nat)) :
  ([∗ list] p ∈ pairs, p.1 ↤ℕ p.2)%I
    ≡
  (∃ pairs_ls : list (loc * gmultiset loc),
      ⌜Forall2 (fun p p' => p.1 = p'.1 /\ p.2 = size p'.2) pairs pairs_ls⌝ ∗
       ([∗ list] p ∈ pairs_ls, p.1 ↤ p.2))%I.
Proof.
  induction pairs as [| p pairs IH]; apply bi.equiv_entails; split.
  - iIntros "?". iExists []. auto.
  - iIntros "?". auto.
  - iIntros "H". iDestruct "H" as "(p & H)".
    iDestruct "p" as (ls) "(p & %)".
    iDestruct (IH with "H") as (l) "(%Hf & H)".
    iExists ((p.1, ls) :: l). iFrame; auto.
  - iIntros "H". iDestruct "H" as (l) "(%Hf & H)".
    destruct (Forall2_cons_inv_l _ _ _ _ Hf) as ((l', ls) & pls & [e1 e2] & Hf' & ->).
    simpl. iDestruct "H" as "(p & H)". simpl in *. subst l'.
    iSplitL "p".
    + iExists _; iFrame; auto.
    + iApply IH. auto.
Qed.

(* TODO unused? we use RHS as the canonical representation *)
Lemma simpl_vmapsfrom_nat_loc l q n :
  (#l ↤ℕ?{ q } n)%I = (l ↤ℕ{ q } n)%I.
Proof.
  reflexivity.
Qed.

Lemma vmapsfrom_nat_trivial v q n :
  ⌜forall l, v ≠ VLoc l⌝ -∗
  vmapsfrom_nat v q n.
Proof.
  iIntros (H).
  destruct v; [ | | congruence | ];
  unfold vmapsfrom_nat.
  all: edestruct trivial_multiset; eauto.
Qed.

Lemma vmapsfrom_nat_weaken v q n1 n2 :
  (n1 <= n2)%nat →
  v ↤ℕ?{q} n1 -∗
  v ↤ℕ?{q} n2.
Proof.
  destruct v; try (iIntros (_) "a"; iApply vmapsfrom_nat_trivial; done).
  apply mapsfrom_nat_weaken.
Qed.

Definition value_is_not_a_loc (v : value) : Prop :=
  match v with
  | VLoc _ => False
  | _ => True
  end.

(* TODO unused, I think. Better than [vmapsfrom_nat_trivial_]? *)
Lemma vmapsfrom_nat_trivial_ v q n :
  ⌜value_is_not_a_loc v⌝ -∗
  vmapsfrom_nat v q n.
Proof.
  iIntros (H); iApply vmapsfrom_nat_trivial.
  iPureIntro; intros l ->; apply H.
Qed.

Lemma wp_load s E l dq vs src dst ofs v w qv nv nw :
  (0 <= ofs < length vs)%nat →
  v = vs !!! ofs →
  {{{
    src ↦ BCell (VLoc l) ∗
    l ↦{dq} BTuple vs ∗
    dst ↦ BCell w ∗
    vmapsfrom_nat v qv nv ∗
    vmapsfrom_nat w 1%Qp nw
  }}}
    (ILoad (LLoc dst) (LLoc src) ofs)
    @ s; E
  {{{
    RET tt;
    src ↦ BCell (VLoc l) ∗
    l ↦{dq} BTuple vs ∗
    dst ↦ BCell v ∗
    vmapsfrom_nat v qv (nv + 1) ∗
    vmapsfrom_nat w 1%Qp (nw - 1)
  }}}.
Proof.
  intros Hofs ->. set (v := vs !!! ofs).
  iIntros (Φ) "(Hsrc & Hl & Hdst & Hv & Hw) Post".
  iDestruct "Hv" as (lsv) "(Hv & ->)".
  iDestruct "Hw" as (lsw) "(Hw & ->)".
  assert ((∃ l', w = VLoc l') \/ forall l', w ≠ VLoc l') as [(l', ->) | Hw]
      by now destruct w; eauto.
  - (* [w] is a location [VLoc l'] *)

    (* we need the information that [dst ∈ lsw], which we can get
       thanks to [wp_memory.exploit_mapsto_mapsfrom] *)
    iApply (wp_supd _ _ (dst <?- src ? ofs) _ _ _ eq_refl
      (wp_memory.exploit_mapsto_mapsfrom E dst 1%Qp (BCell (#l')%V) l' lsw)
      with "[-Hdst Hw] [$Hdst $Hw]").
    2: auto with pointers.
    iIntros "(Hdst & Hw & %dst_in_lsw)".

    (* we now apply the original lemma [wp_load] and instanciate the multisets *)
    iApply (wp_load s E l dq vs src dst ofs v (#l')%V qv 1%Qp lsv lsw Hofs eq_refl with "[$]");
      iNext; iIntros "(Hsrc & Hl & Hdst & Hv & Hw)".
    iApply "Post"; iFrame.
    iFrame.
    iSplitL "Hv".
    + iExists _; iFrame.
      rewrite gmultiset_size_disj_union gmultiset_size_singleton //.
    + iExists _; iFrame.
      rewrite gmultiset_size_difference ?gmultiset_size_singleton //.
      now apply gmultiset_singleton_subseteq_l.

  - (* [w] is not a location: [nw - 1] could be anything, it would not
       matter, we can directly apply the original [wp_load] *)
    iApply (wp_load s E l dq vs src dst ofs v _ qv 1%Qp lsv lsw Hofs eq_refl with "[$]").
    iNext; iIntros "?".
    repeat iDestruct select (_ ∗ _)%I as "(?&?)".
    iApply "Post"; iFrame.
    iSplitL.
    + iExists _; iFrame.
      rewrite gmultiset_size_disj_union gmultiset_size_singleton //.
    + now iApply vmapsfrom_nat_trivial.
Qed.

Lemma wp_store s E dst ofs src l v w vs nv qw nw :
  (0 <= ofs < length vs)%nat →
  v = vs !!! ofs →
  {{{
    dst ↦ BCell (VLoc l) ∗
    src ↦ BCell w ∗
    l ↦ BTuple vs ∗
    vmapsfrom_nat v 1%Qp nv ∗
    vmapsfrom_nat w qw nw
  }}}
    IStore (LLoc dst) ofs (LLoc src)
    @ s; E
  {{{ RET tt;
    dst ↦ BCell (VLoc l) ∗
    src ↦ BCell w ∗
    l ↦ BTuple (<[ ofs := w ]> vs) ∗
    vmapsfrom_nat v 1%Qp (nv - 1) ∗
    vmapsfrom_nat w qw (nw + 1)
  }}}.
Proof.
  intros Hofs Heqv.
  iIntros (Φ) "(Hdst & Hsrc & Hl & Hv & Hw) Post".
  iDestruct "Hv" as (lsv) "(Hv & ->)".
  iDestruct "Hw" as (lsw) "(Hw & ->)".
  assert ((∃ l', v = VLoc l') \/ forall l', v ≠ VLoc l') as [(l', ->) | Hv]
      by now destruct v; eauto.
  - (* [v] is a location [VLoc l'] *)

    (* we need the information that [l ∈ lsv], which we can get
       thanks to [wp_memory.exploit_mapsto_mapsfrom] *)
    iApply (wp_supd _ _ (dst ! ofs <!- src) _ _ _ eq_refl
      (wp_memory.exploit_mapsto_mapsfrom E l 1%Qp (BTuple vs) l' lsv)
      with "[-Hl Hv] [$Hl Hv]").
    2:{ iSplit; auto.
        iPureIntro.
        eapply gmultiset_elem_of_subseteq. 2: eapply pointers_lookup; eauto.
        rewrite -Heqv.
        apply pointers_BCell_VLoc_membership. }
    iIntros "(Hl & Hv & %l_in_lsw)".

    (* we now apply the original lemma [wp_load] and instanciate the multisets *)
    iApply (wp_store s E dst ofs src l _ w vs 1%Qp lsv qw lsw Hofs Heqv with "[$Hdst $Hsrc $Hl $Hv $Hw]").
    iNext; iIntros "(Hdst & Hsrc & Hl & Hv & Hw)".
    iApply "Post"; iFrame.
    iFrame.
    iSplitL "Hv".
    + iExists _; iFrame.
      rewrite gmultiset_size_difference ?gmultiset_size_singleton //.
      now apply gmultiset_singleton_subseteq_l.
    + iExists _; iFrame.
      rewrite gmultiset_size_disj_union gmultiset_size_singleton //.

  - (* [w] is not a location: [nw - 1] could be anything, it would not
       matter, we can directly apply the original [wp_load] *)
    iApply (wp_store s E dst ofs src l _ w vs 1%Qp lsv qw lsw Hofs Heqv with "[$Hdst $Hsrc $Hl $Hv $Hw]").
    iNext; iIntros "(Hdst & Hsrc & Hl & Hv & Hw)".
    iApply "Post"; iFrame.
    iFrame.
    iSplitL "Hv".
    + now iApply vmapsfrom_nat_trivial.
    + iExists _; iFrame.
      rewrite gmultiset_size_disj_union gmultiset_size_singleton //.
Qed.

Lemma wp_const s E (dst : loc) (v w : value) (nv : nat) :
  val_pointer_list w = [] ->
  {{{ dst ↦ BCell v ∗ vmapsfrom_nat v 1%Qp nv }}}
    IConst (LLoc dst) w
    @ s; E
  {{{ RET tt; dst ↦ BCell w ∗ vmapsfrom_nat v 1%Qp (nv - 1) }}}.
Proof.
  intros Hw.
  iIntros (Φ) "(Hdst & Hv) Post".
  iDestruct "Hv" as (lsv) "(Hv & ->)".
  assert ((∃ l, v = VLoc l) \/ forall l', v ≠ VLoc l') as [(l, ->) | Hv]
      by now destruct v; eauto.
  - (* [v] is a location [VLoc l'] *)
    iApply (wp_supd _ _ (IConst _ _) _ _ _ eq_refl
      (wp_memory.exploit_mapsto_mapsfrom E dst 1%Qp (BCell (#l)%V) l lsv)
      with "[-Hv Hdst] [$Hdst $Hv]").
    2: auto with pointers.
    iIntros "(Hdst & Hv & %dst_in_lsw)".
    iApply (wp_const with "[$Hdst $Hv]"); auto.
    iNext. iIntros "(Hdst & Hv)"; iApply "Post"; iFrame.
    iExists _; iFrame.
    rewrite gmultiset_size_difference ?gmultiset_size_singleton //.
    now apply gmultiset_singleton_subseteq_l.
  - (* [v] is not a location *)
    iApply (wp_const with "[$Hdst $Hv]"); auto.
    iNext. iIntros "(Hdst & Hv)"; iApply "Post"; iFrame.
    now iApply vmapsfrom_nat_trivial.
Qed.

Lemma wp_move s E (dst src : loc) (v w : value) nv qw nw :
  {{{
    dst ↦ BCell v ∗
    src ↦ BCell w ∗
    vmapsfrom_nat v 1%Qp nv ∗
    vmapsfrom_nat w qw nw
  }}}
    IMove (LLoc dst) (LLoc src)
    @ s; E
  {{{ RET tt;
    dst ↦ BCell w ∗
    src ↦ BCell w ∗
    vmapsfrom_nat v 1%Qp (nv - 1) ∗
    vmapsfrom_nat w qw (nw + 1)
  }}}.
Proof.
  iIntros (Φ) "(Hdst & Hsrc & Hv & Hw) Post".
  iDestruct "Hv" as (lsv) "(Hv & ->)".
  iDestruct "Hw" as (lsw) "(Hw & ->)".
  assert ((∃ l, v = VLoc l) \/ forall l, v ≠ VLoc l) as [(l, ->) | Hv]
      by now destruct v; eauto.
  - (* [v] is a location [VLoc l'] *)
    iApply (wp_supd _ _ (IMove _ _) _ _ _ eq_refl
      (wp_memory.exploit_mapsto_mapsfrom E dst 1%Qp (BCell (#l)%V) l lsv)
      with "[-Hv Hdst] [$Hdst $Hv]").
    2: auto with pointers.
    iIntros "(Hdst & Hv & %dst_in_lsv)".
    iApply (wp_move with "[$Hdst $Hsrc $Hv $Hw]"); auto.
    iNext. iIntros "(Hdst & Hsrc & Hv & Hw)". iApply "Post"; iFrame.
    iSplitL "Hv".
    + iExists _; iFrame.
      rewrite gmultiset_size_difference ?gmultiset_size_singleton //.
      now apply gmultiset_singleton_subseteq_l.
    + iExists _; iFrame.
      rewrite gmultiset_size_disj_union gmultiset_size_singleton //.
  - (* [v] is not a location *)
    iApply (wp_move with "[$Hdst $Hsrc $Hv $Hw]"); auto.
    iNext. iIntros "(Hdst & Hsrc & Hv & Hw)". iApply "Post"; iFrame.
    iSplitL "Hv".
    + now iApply vmapsfrom_nat_trivial.
    + iExists _; iFrame.
      rewrite gmultiset_size_disj_union gmultiset_size_singleton //.
Qed.

Lemma wp_alloc_split s E dst n w nw k :
  let b := BTuple (replicate n VUnit) in
  (size_block b <= k)%nat →
  {{{
    ♢k ∗
    dst ↦ BCell w ∗
    vmapsfrom_nat w 1%Qp nw
  }}}
    IAlloc (LLoc dst) n
    @ s; E
  {{{ l, RET tt;
    l ↦ b ∗ l ↤ℕ 1  ∗
    dst ↦ BCell (VLoc l) ∗
    vmapsfrom_nat w 1%Qp (nw - 1) ∗
    ♢(k - size_block b)
  }}}.
Proof.
  intros b Hk. iIntros (Φ) "(Hfrag & Hdst & Hw) Post".
  iDestruct "Hw" as (lsw) "(Hw & ->)".
  assert ((∃ l, w = VLoc l) \/ forall l, w ≠ VLoc l) as [(l, ->) | Hw]
      by now destruct w; eauto.
  - (* [w] is a location *)
    iApply (wp_supd _ _ (IAlloc _ _) _ _ _ eq_refl
      (wp_memory.exploit_mapsto_mapsfrom E dst 1%Qp (BCell (#l)%V) l lsw)
      with "[-Hdst Hw] [$Hdst $Hw]").
    2: auto with pointers.
    iIntros "(Hdst & Hw & %dst_in_lsw)".
    iApply (wp_alloc_split with "[$Hdst $Hw $Hfrag]"); auto.
    iNext. iIntros (l') "(Hl' & pl' & Hdst & pl & Hdiams)".
    iApply "Post"; iFrame.
    iSplitL "pl'".
    + iExists _; iFrame. now rewrite gmultiset_size_singleton.
    + iExists _; iFrame.
      rewrite gmultiset_size_difference ?gmultiset_size_singleton //.
      now apply gmultiset_singleton_subseteq_l.
  - (* not a location *)
    iApply (wp_alloc_split with "[$Hdst $Hw $Hfrag]"); auto.
    iNext. iIntros (l') "(Hl' & pl' & Hdst & pl & Hdiams)".
    iApply "Post"; iFrame.
    iSplitL "pl'".
    + iExists _; iFrame. now rewrite gmultiset_size_singleton.
    + now iApply vmapsfrom_nat_trivial.
Qed.

Lemma wp_alloca s E x i Φ :
  ▷ (∀ c,
    c ↦ BCell VUnit -∗
    WP (subst x (LLoc c) i) @ s; E {{ _, ∃v n,
      c ↦ BCell v ∗ v ↤ℕ? n ∗
      (v ↤ℕ? (n - 1) -∗ Φ tt)
    }}
  )
  ⊢ WP (IAlloca x i) @ s; E {{ Φ }}.
Proof.
  iIntros "HWP".
  iApply wp_alloca_alloca_active. iNext. iIntros (c) "[Hc pc]".
  iApply wp_bind.
  iApply (wp_strong_mono s s E E with "[HWP Hc]").
  { auto. }
  { auto. }
  { iApply ("HWP" with "Hc"). }
  iIntros ([]) "H".
  iDestruct "H" as (v n) "(Hc & Hv & HΦ)".
  iModIntro.
  destruct v as [ | | l | ]; [ | | shelve | ].
  all: iApply (wp_alloca_active_skip _ _ c _ 1%Qp ∅ with "[$Hc $pc $Hv] [HΦ]").
  all: iNext; iIntros "_"; iApply "HΦ"; now iApply vmapsfrom_nat_trivial.
  Unshelve.

  iDestruct "Hv" as (ls) "(Hl & ->)".
  iApply (wp_supd _ _ (IAllocaActive _ _) _ _ _ eq_refl
                  (wp_memory.exploit_mapsto_mapsfrom E c 1%Qp (BCell (#l)%V) l ls)
      with "[-Hc Hl] [$Hc $Hl]").
  2: auto with pointers.
  iIntros "(Hc & Hl & %c_in_ls)".
  iApply (wp_alloca_active_skip _ _ c _ 1%Qp ls with "[$Hc $pc $Hl] [HΦ]").
  iNext. iIntros "Hl". iApply "HΦ". iExists _; iFrame.
  rewrite gmultiset_size_difference ?gmultiset_size_singleton //.
  now apply gmultiset_singleton_subseteq_l.
Qed.

Lemma wp_free_singleton s E i Φ l b :
  to_val i = None →
  ¬ is_stack_cell b →
  (†l ∗ ♢(size_block b) -∗ wp s E i Φ) -∗
  (l ↦ b ∗ l ↤ℕ 0 -∗ wp s E i Φ).
Proof.
  iIntros (Hi Hb) "post (Hl & pl)".
  iApply (wp_free_singleton s E i Φ l b ∅ Hi Hb with "[$post] [$Hl pl]").
  now intros [].
  iDestruct "pl" as (ls) "(pl & %)".
  rewrite (gmultiset_size_empty_inv ls); auto.
Qed.

(* ------------------------------------------------------------------------ *)

(* Synchronized deallocation and cleanup. *)

(* Predecessor cleanup (the tactic [wp_cleanup]) means combining [†l]
   with [l' ↤ ls] to get [l' ↤ ls \ {l}]. Unfortunately in reference
   counting this [ls] is existentially quantified, so to know [l] is
   in [ls] we must introduce [ls] before freeing [l], when we indeed
   can know whether (and how many times) [l] points to [l'], so that
   we can decrement the reference counters. That means that to
   preserve the reference counting abstraction we must perform free
   and cleanup at the same time.

   The lemma below, stated using multisets, is useful only in
   reference counting. It does free, and cleanup on an arbitrary
   number of mapsfroms, at the same time *)

Lemma supd_free_cleanup_singleton E l b (pairs : list (loc * gmultiset loc)) :
  ¬ is_stack_cell b →
  l ↦ b ∗ l ↤ ∅ ∗ ([∗ list] p ∈ pairs, p.1 ↤ p.2)
  ={{E}}=∗
  ♢(size_block b) ∗
  ([∗ list] p ∈ pairs, p.1 ↤ (p.2 ∖ repeatm l (multiplicity p.1 (pointers b)))).
Proof.
  iIntros (Hb σ ns κs k) "(Hstate & Hl & pl & ps)".
  (* First we indeed free the location *)
  iPoseProof (supd_free_singleton E l b ∅) as "-#upd". now multiset_solver. assumption.
  iMod ("upd" with "[$Hstate $Hl $pl]") as "(Hstate & dl & Hdia)".
  iFrame "Hdia".
  (* Then we cleanup [l] in the iterated star. Note that we could
     remove [l] more times than [multiplicity l' ls], but that much is
     enough. *)
  iInduction pairs as [ | [l' ls] pairs ] "IH".
  - iFrame. auto.
  - iDestruct "ps" as "(p & ps)".
    iDestruct (bi.persistent_sep_dup with "dl") as "(dl & dl_)".
    iMod ("IH" with "ps Hstate dl") as "(Hstate & ps)".
    (* Second important iMod for the cleanup of [l] in [l'] *)
    iMod (supd_cleanup_group E l' _ ls (repeatm l _)
      with "[$Hstate $p dl_]")
      as "(Hstate & p)".
    { now iApply ddag_repeatm. }
    iFrame; auto.
Qed.

(* The free-and-cleanup state_interp update for reference counting. We
   free [l ↦ b] and we can decrement the counters of any [l'] by the
   number of times [l'] appears in the block [b]. *)

Lemma supd_free_cleanup_singleton_rc E l b (pairs : list (loc * nat)) :
  ¬ is_stack_cell b →
  l ↦ b ∗ l ↤ℕ 0 ∗ ([∗ list] p ∈ pairs, p.1 ↤ℕ p.2)
  ={{E}}=∗
  (* †l ∗ *) ♢(size_block b) ∗ ([∗ list] p ∈ pairs, p.1 ↤ℕ (p.2 - multiplicity p.1 (pointers b))).
Proof.
  iIntros (Hb σ ns κs k) "(Hstate & Hl & pl & ps)".
  (* We introduce the multiset that were existentially quantified *)
  rewrite mapsfrom_nat_0.
  epose proof (bi.equiv_entails_1_1 _ _ (list_mapsfrom_nat_pairs _)) as s1.
  iDestruct (s1 with "[$ps]") as (llss Hf) "ps". clear s1.
  (* Since [l ↦ b], for each [l' ↤ ls] we know that [l] appears in
     [ls] as many times as [l'] appears in [b], using
     [exploit_mapsto_mapsfrom_multiplicity] as many times as
     necessary. *)
  iAssert ⌜Forall (fun p => multiplicity p.1 (pointers b) <= multiplicity l p.2) llss⌝%I
    as %Hmult. {
    clear Hf. iClear "pl".
    iInduction llss as [ | lls llss ] "IH".
    - auto.
    - iDestruct ("ps") as "(p & ps)".
      iDestruct ("IH" with "Hstate Hl ps") as %?.
      destruct_state_interp.
      iDestruct (exploit_mapsto_mapsfrom_multiplicity with "Hph Hl p") as %A.
      iPureIntro. constructor; auto. lia.
  }
  (* We perform the free and cleanup on multisets *)
  iMod (supd_free_cleanup_singleton
    with "[$Hstate $Hl $pl $ps]")
    as "(Hstate & dia & ps)". auto.
  iFrame. iModIntro.
  (* We abstract back the multisets with natural numbers, finally
     using the multiplicity of [l] in each [ls] to tighten the
     reference counters *)
  iInduction llss as [ | lls llss] "IH" forall (pairs Hf Hmult).
  - apply Forall2_nil_inv_r in Hf. subst. iFrame. auto.
  - iDestruct "ps" as "(p & ps)".
    apply Forall2_cons_inv_r in Hf. destruct Hf as (n & pairs' & [e1 e2] & Hf & ->).
    apply Forall_cons_1 in Hmult. destruct Hmult as (Hmultl' & Hmult).
    iDestruct ("IH" $! pairs' Hf Hmult with "ps") as "?".
    iFrame. iClear "IH". rewrite e1 e2.
    iExists _; iFrame. iPureIntro.
    rewrite gmultiset_size_difference. now rewrite size_repeatm.
    intros x. rewrite multiplicity_repeatm.
    destruct (decide _); subst; lia.
Qed.

(* Same lemma with [wp] instead of the [={{E}}=∗] update *)

Lemma wp_free_cleanup_singleton_rc s E i Φ l b (pairs : list (loc * nat)) :
  to_val i = None →
  ¬ is_stack_cell b →
  (♢(size_block b) ∗ ([∗ list] p ∈ pairs, p.1 ↤ℕ (p.2 - multiplicity p.1 (pointers b)))
     -∗ wp s E i Φ) -∗
  (l ↦ b ∗ l ↤ℕ 0 ∗ ([∗ list] p ∈ pairs, p.1 ↤ℕ p.2)
     -∗ wp s E i Φ).
Proof.
  eauto using wp_supd, supd_free_cleanup_singleton_rc.
Qed.

(* Same lemma with a list of values (with [↤ℕ?] instead of [↤ℕ]) *)

(* During free-and-cleanup of [l ↤ BTuple vs] together with [v ↤ℕ? n],
   we want to decrement [n] into [n - k] where [k] is the number of
   times [v] appears in [vs]. However, [k] can be grossly
   overestimated in the cases [v] is not a location, which is
   why our counting function is that generous.

   The proof of [wp_free_cleanup_singleton_rc_values] is mostly
   bookkeeping. *)

Definition val_occs (v : value) (vs : list value) :=
  match v with
  | VLoc l => length (List.filter (fun v' => match v' with VLoc l' => Z.eqb l l' | _ => false end) vs)
  | _ => length vs
  end.

Lemma val_occs_loc l vs : val_occs (#l)%V vs = multiplicity l (pointers (BTuple vs)).
Proof.
  induction vs as [ | [ | | l' | ] vs IH ]; try done; try apply IH.
  change (pointers (BTuple (#l' :: vs))) with ({[+l'+]} ⊎ pointers (BTuple vs)).
  rewrite multiplicity_disj_union multiplicity_singleton' -IH. clear IH.
  destruct (decide _) as [<- | n].
  - simpl. now rewrite Z.eqb_refl.
  - simpl. replace (l =? l') with false; auto. now symmetry; apply Z.eqb_neq.
Qed.

Fixpoint list_select {A B} (f : A -> option B) (l : list A) : list B :=
  match l with
  | nil => nil
  | a :: l =>
    match f a with
    | Some b => b :: list_select f l
    | None => list_select f l
    end
  end.

Lemma supd_free_cleanup_singleton_rc_values E l (vsl : list value) (vns : list (value * nat)) :
  l ↦ BTuple vsl ∗ l ↤ℕ 0 ∗ ([∗ list] vn ∈ vns, vn.1 ↤ℕ? vn.2)
  ={{E}}=∗
  ♢(size_block (BTuple vsl)) ∗ ([∗ list] vn ∈ vns, vn.1 ↤ℕ? (vn.2 - val_occs vn.1 vsl)).
Proof.
  iIntros (σ ns κs k) "(Hstate & Hl & pl & ps)".
  pose (f := fun p => match p.1 with VLoc l => Some (l, p.2 : nat) | _ => None end).
  iMod (supd_free_cleanup_singleton_rc E l (BTuple vsl) (list_select f vns)
    with "[$Hstate $Hl $pl ps]") as "(Hstate & dia & ps)"; [ now auto | | ]; swap 1 2.
  intro_state_interp; iFrame "dia Hstate"; iModIntro.
  (* remains to prove the vmapsfrom_nat / mapsfrom_nat equivalences *)
  - clear.
    iInduction vns as [ | [v n] vns ] "IH"; [ done | ].
    destruct v as [ | n0 | l | xs i]; simpl.
    all: try now iSplit; [ iApply vmapsfrom_nat_trivial | iApply ("IH" with "ps") ].
    iDestruct "ps" as "(p & ps)".
    iSplitL "p".
    * now rewrite <-val_occs_loc.
    * iApply ("IH" with "ps").
  - clear.
    iInduction vns as [ | [v n] vs ] "IH"; [ done | ].
    iDestruct "ps" as "(p & ps)".
    destruct v as [ | n0 | l | xs i].
    all: try iApply ("IH" with "ps").
    simpl. iFrame. now iApply "IH".
Qed.

Lemma wp_free_cleanup_singleton_rc_values s E i Φ l (vsl : list value) (vns : list (value * nat)) :
  to_val i = None →
  (♢(size_block (BTuple vsl)) ∗ ([∗ list] vn ∈ vns, vn.1 ↤ℕ? (vn.2 - val_occs vn.1 vsl))
     -∗ wp s E i Φ) -∗
  ( l ↦ BTuple vsl ∗ l ↤ℕ 0 ∗ ([∗ list] vn ∈ vns, vn.1 ↤ℕ? vn.2)
     -∗ wp s E i Φ).
Proof.
  eauto using wp_supd, supd_free_cleanup_singleton_rc_values.
Qed.

Lemma wp_fork s E c x i v nv :
  {{{
    c ↦ BCell v ∗
    v ↤ℕ? nv ∗
    (∀ c', c' ↦ BCell v ∗ v↤ℕ? nv -∗
           WP (subst x (LLoc c') i) @ s; ⊤ {{ _, c' ↦ BCell VUnit }})
  }}}
    IFork (LLoc c) x i
    @ s; E
  {{{ RET tt; c ↦ BCell VUnit }}}.
Proof.
  iIntros (Φ) "(Hc & Hv & Hi) HΦ".
  iDestruct "Hv" as (lsv) "(Hv & ->)".
  assert ((∃ l, v = VLoc l) \/ forall l, v ≠ VLoc l) as [(l, ->) | Hv]
      by now destruct v; eauto.
  - (* [v] is a location [VLoc l'] *)
    iApply (wp_supd _ _ (IFork _ _ _) _ _ _ eq_refl
      (wp_memory.exploit_mapsto_mapsfrom E c 1%Qp (BCell (#l)%V) l lsv)
      with "[-Hv Hc] [$Hc $Hv]").
    2: auto with pointers.
    iIntros "(Hc & Hv & %c_in_lsv)".
    iApply (wp_fork with "[$Hc $Hv Hi]"); auto.
    iIntros (c') "(Hc & Hv)". iApply "Hi"; iFrame.
    iExists _; iFrame. iPureIntro.
    assert ({[c]} ⊆ lsv) as H by now intro; apply gmultiset_singleton_subseteq_l.
    rewrite gmultiset_size_disj_union gmultiset_size_difference // !gmultiset_size_singleton.
    apply gmultiset_subseteq_size in H.
    rewrite gmultiset_size_singleton in H.
    lia.
  - iApply (wp_fork with "[$Hc $Hv Hi]"); auto.
    iIntros (c') "(Hc & Hv)". iApply "Hi"; iFrame.
    now iApply vmapsfrom_nat_trivial.
Qed.

End reference_counting.
