From iris.proofmode Require Export coq_tactics reduction.
From iris.proofmode Require Export proofmode.
From iris.program_logic Require Import atomic.
From iris Require Import spec_patterns intro_patterns.
From spacelang Require Export ph rc wp proofmode.

Ltac fetch_mapsfrom_nat Δ l k :=
  lazymatch Δ with
  | context[Esnoc _ ?H (l ↤ℕ{?q} ?ls)%I] =>
      k constr:((H, q, ls))
  | _ =>
      fail "Could not find" l "↤ℕ{_} _ in context" Δ
  end.

Ltac construct_vmapsfrom_nat Δ v k :=
  iAssert (v ↤ℕ?{1%Qp} 0)%I as "?"; [
    solve
      [ now iApply vmapsfrom_nat_trivial
      | fail 1 "Could not find or construct " v "↤ℕ?{_} _ in" Δ ]
  |];
  lazymatch goal with
  | |- envs_entails ?Δ _ =>
  lazymatch Δ with
  | context[Esnoc _ ?H (v ↤ℕ?{?q} ?n)%I] =>
      k constr:((H, q, n))
  end
  end.

Ltac fetch_or_construct_vmapsfrom_nat Δ v k :=
  lazymatch Δ with
  | context[Esnoc _ ?H (v ↤ℕ?{?q} ?n)%I] =>
      k constr:((H, q, n))
  | _ =>
  lazymatch v with
  | VLoc ?l =>
      fetch_mapsfrom_nat Δ l k
  | _ =>
      construct_vmapsfrom_nat Δ v k
  end
  end.

Ltac clear_trivial_vmapsfrom_nat :=
  repeat iClear select (VInt _ ↤ℕ?{_} _)%I;
  repeat iClear select (() ↤ℕ?{_} _)%I;
  repeat iClear select (VCode _ ↤ℕ?{_} _)%I.

Ltac wp_move :=
  wp_bind (IMove _ _);
  (* get Iris context and operands *)
  lazymatch goal with
  | |- envs_entails ?Δ (wp ?s ?E (IMove (LLoc ?dst) (LLoc ?src)) ?Q) =>
  (* get hypotheses and cell values for [dst] and [src] *)
  fetch_mapsto_cell Δ dst ltac:(fun tuple =>
  lazymatch tuple with (?Hdst, _, ?v) =>
  fetch_mapsto_cell Δ src ltac:(fun tuple =>
  lazymatch tuple with (?Hsrc, _, ?w) =>
  (* get mapsfrom info for [v] and [w] *)
  fetch_or_construct_vmapsfrom_nat Δ v ltac:(fun tuple =>
  lazymatch tuple with (?Hv, ?qv_unused, ?nv) =>
  fetch_or_construct_vmapsfrom_nat Δ w ltac:(fun tuple =>
  lazymatch tuple with (?Hw, ?qw, ?nw) =>
  (* build [wp_move]'s argument list and intro patterns *)
  iApply (rc.wp_move s E dst src v w nv qw nw _ with
              [SGoal (SpecGoal GSpatial false [Hdst; Hsrc; Hv; Hw] [] true)]);
  (* using "[$]" instead of [SGoal (...)] may work in some cases, but
     is slower *)
  iNext;
  let pat := ltac:(eval compute in (conj_idents [Hdst; Hsrc; Hv; Hw])) in
  iIntros pat;
  (* repeat rewrite simpl_vmapsfrom_nat_loc; *)
  clear_trivial_vmapsfrom_nat;
  try wp_skip
  end) end) end) end) end.

Ltac wp_const :=
  wp_bind (IConst _ _);
  (* get Iris context and operands *)
  lazymatch goal with
  | |- envs_entails ?Δ (wp ?s ?E (IConst (LLoc ?dst) ?w) ?Q) =>
  (* find [dst]'s value [v] and the corresponding hypothesis *)
  fetch_mapsto_cell Δ dst ltac:(fun tuple =>
  lazymatch tuple with (?Hdst, _, ?v) =>
  (* get mapsfrom info for [v] *)
  fetch_or_construct_vmapsfrom_nat Δ v ltac:(fun tuple =>
  lazymatch tuple with (?Hv, ?qv, ?nv) =>
  iApply (rc.wp_const s E dst v w nv eq_refl with
              [SGoal (SpecGoal GSpatial false [Hdst; Hv] [] true)]);
  iNext;
  let pat := ltac:(eval compute in (conj_idents [Hdst; Hv])) in
  iIntros pat;
  clear_trivial_vmapsfrom_nat;
  try wp_skip
  end) end) end.

(* Use [wp_with as x "Hx"] to explicitely name the location [x] and
   its Iris hypothesis ["Hx"] *)
Tactic Notation "wp_with" "as" ident(x) constr(Hx) :=
  wp_bind (IAlloca _ _);
  iApply rc.wp_alloca; iNext; iIntros (x) Hx; simpl subst.

(* Use [wp_with "x"] to explicitely name the location [x] and its Iris
   hypothesis ["Hx"] *)
Tactic Notation "wp_with" constr(x) :=
  let Hx := eval compute in ("H" ++ x)%string in
  let x := string_ident.string_to_ident x in
  wp_with as x Hx.

(* Use [wp_with] to use the string used in the program's syntax as
   name for the corresponding location *)
Tactic Notation "wp_with" :=
  wp_bind (IAlloca _ _);
  lazymatch goal with
  | |- envs_entails ?Δ (wp ?s ?E (IAlloca (BNamed ?x) _) ?Q) =>
    wp_with x
  end.

Ltac wp_end_with :=
  (* Let [Δ] be the context and [c] the cell of which we are ending the scope *)
  lazymatch goal with
  | |- envs_entails ?Δ (∃ v n, ?c ↦ BCell v ∗ v ↤ℕ? n ∗ _) =>
  (* Find the final value [finalv] pointed by by [c] *)
  lazymatch Δ with
  | context[Esnoc _ ?Hc (c ↦{_} BCell ?finalv)%I] =>
  (* Find or build its associated mapsfrom *)
  fetch_or_construct_vmapsfrom_nat Δ finalv ltac:(fun tuple =>
  lazymatch tuple with (?Hfinalv, ?qfinalv_unused, ?nfinalv) =>
  (* We can now instantiate the existentials *)
  iExists finalv, nfinalv;
  (* Frame out the final heap [c ↦ BCell finalv] *)
  iFrame Hc;
  (* Find out its predecessors and remove [c] from it, keeping in mind that
     [Hfinalv] could be ["_" : True] if the value is not a location. *)
  (iSplit; [ now auto | ] || iFrame Hfinalv);
  let pat := ltac:(eval compute in (conj_idents [Hfinalv])) in
  iIntros pat;
  clear_trivial_vmapsfrom_nat
  end)
  | _ => fail "wp_end_with: could not find the final value of local variable " c " in context " Δ
  end
  | _ => fail "wp_end_with: goal does not match the expected end of local allocation"
  end.

Ltac cancel_mapsfrom_nat_ Δ G :=
  match G with
  | (?G1 ∗ ?G2)%I =>
    try cancel_mapsfrom_nat_ Δ G1;
    try cancel_mapsfrom_nat_ Δ G2
  | (?l ↤ℕ{?q} ?n)%I =>
    lazymatch Δ with
    | context[Esnoc _ ?Hl (l ↤ℕ{q} ?n')%I] =>
      (iAssert ⌜n = n'⌝%I as %<-);
      [ iPureIntro; try lia | iFrame Hl ]
    end
  | (?v ↤ℕ?{?q} ?n)%I =>
    lazymatch Δ with
    | context[Esnoc _ ?Hl (v ↤ℕ?{q} ?n')%I] =>
      (iAssert ⌜n = n'⌝%I as %<-);
      [ iPureIntro; try lia | iFrame Hl ]
    end
  end.

Ltac cancel_mapsfrom_nat :=
  lazymatch goal with
  | |- envs_entails ?Δ ?G =>
    cancel_mapsfrom_nat_ Δ G
  end.

Ltac wp_load :=
  wp_bind (ILoad _ _ _);
  lazymatch goal with
  (* identify operands [dst, src, ofs] *)
  | |- envs_entails ?Δ (wp ?s ?E (ILoad (LLoc ?dst) (LLoc ?src) ?ofs) ?Q) =>
  (* find [dst]'s old value [w] *)
  fetch_mapsto_cell Δ dst ltac:(fun tuple =>
  lazymatch tuple with (?Hdst, _, ?w) =>
  (* find [src]'s heap location [l] *)
  fetch_mapsto_cell_loc Δ src ltac:(fun tuple =>
  lazymatch tuple with (?Hsrc, _, ?l) =>
  (* find the tuple [vs] pointed by [l] *)
  fetch_mapsto_tuple Δ l ltac:(fun tuple =>
  lazymatch tuple with (?Hl, _, ?vs) =>
  (* of which the [ofs]th item is [v] *)
  let v := eval compute in (vs !!! ofs) in
  (* get/build mapsfroms for [v] and [w] *)
  fetch_or_construct_vmapsfrom_nat Δ v ltac:(fun tuple =>
  lazymatch tuple with (?Hv, ?qv, ?nv) =>
  fetch_or_construct_vmapsfrom_nat Δ w ltac:(fun tuple =>
  lazymatch tuple with (?Hw, ?qw_unused, ?nw) =>
  iApply (rc.wp_load _ _ l _ vs src dst ofs v w qv nv nw with
              [SGoal (SpecGoal GSpatial false [Hdst; Hl; Hsrc; Hv; Hw] [] true)]);
    [ solve [ simpl length; lia | fail 1 "The offset" ofs "seems wrong" ]
    | reflexivity | ];
    iNext;
    let pat := ltac:(eval compute in (conj_idents [Hsrc; Hl; Hdst; Hv; Hw])) in
    iIntros pat;
    clear_trivial_vmapsfrom_nat;
    try wp_skip
  end) end) end) end) end) end.

Ltac wp_store :=
  wp_bind (IStore _ _ _);
  lazymatch goal with
  (* identify operands [dst, src, ofs] *)
  | |- envs_entails ?Δ (wp ?s ?E (IStore (LLoc ?dst) ?ofs (LLoc ?src)) ?Q) =>
  (* find [src]'s value [w] *)
  fetch_mapsto_cell Δ src ltac:(fun tuple =>
  lazymatch tuple with (?Hsrc, _, ?w) =>
  (* find [dst]'s heap location [l] *)
  fetch_mapsto_cell_loc Δ dst ltac:(fun tuple =>
  lazymatch tuple with (?Hdst, _, ?l) =>
  (* find the tuple [vs] pointed by [l] *)
  fetch_mapsto_tuple Δ l ltac:(fun tuple =>
  lazymatch tuple with (?Hl, _, ?vs) =>
  (* of which the [ofs]th item is [v] *)
  let v := eval compute in (vs !!! ofs) in
  (* get/build mapsfroms for [v] and [w] *)
  fetch_or_construct_vmapsfrom_nat Δ v ltac:(fun tuple =>
  lazymatch tuple with (?Hv, ?qv, ?nv) =>
  fetch_or_construct_vmapsfrom_nat Δ w ltac:(fun tuple =>
  lazymatch tuple with (?Hw, ?qw, ?nw) =>
  iApply (rc.wp_store _ _ dst ofs src l v w vs nv qw nw with
              [SGoal (SpecGoal GSpatial false [Hsrc; Hdst; Hl; Hv; Hw] [] true)]);
    [ solve [ simpl length; lia | fail 1 "The offset" ofs "seems wrong" ]
    | reflexivity | ];
    iNext;
    let pat := ltac:(eval compute in (conj_idents [Hdst; Hsrc; Hl; Hv; Hw])) in
    iIntros pat;
    clear_trivial_vmapsfrom_nat;
    try wp_skip
  end) end) end) end) end) end.

Tactic Notation "wp_alloc" ident(l) "as" constr(Hpost) :=
  wp_bind (IAlloc _ _);
  lazymatch goal with
  (* identify operands [dst, src, ofs] *)
  | |- envs_entails ?Δ (wp ?s ?E (IAlloc (LLoc ?dst) ?n) ?Q) =>
  (* find [dst]'s old value [w] *)
  fetch_mapsto_cell Δ dst ltac:(fun tuple =>
  lazymatch tuple with (?Hdst, _, ?w) =>
  (* find/build [w]'s predecessors*)
  fetch_or_construct_vmapsfrom_nat Δ w ltac:(fun tuple =>
  lazymatch tuple with (?Hw, ?qw, ?nw) =>
  (* get space credits *)
  lazymatch Δ with
  | context[Esnoc _ ?Hdiams (♢?ndiams)%I] =>
    (* Apply the lemma [wp_alloc_split] with the hypotheses [Hdiams] and [Hdst]. *)
    iApply (rc.wp_alloc_split  s E dst n w nw ndiams with
                [SGoal (SpecGoal GSpatial false [Hdiams; Hdst; Hw] [] true)]);
    [ first [ simpl; lia | fail 1 "error: not enough space credits?" ] | ];
    iNext;
    (* Introduce the postcondition; simplify it; destruct it as *)
    (* requested by the user. *)
    let tmp := iFresh in
    iIntros (l) tmp;
    iSimpl in tmp;
    iDestruct tmp as Hpost;
    clear_trivial_vmapsfrom_nat;
    diamonds;
    try wp_skip
  | _ =>
    fail "wp_alloc: could not find any space credits in hypothesis"
  end end) end) end.

Tactic Notation "wp_alloc" ident(l) :=
  wp_alloc l as "(?&?&?&?&?)".

Tactic Notation "wp_alloc" :=
  let l := fresh "l" in
  wp_alloc l.

Tactic Notation "rcwp_init" ident(l) "as" constr(Hpost) :=
  unfold init_entry; (* this seems necessary in some cases *)
  wp_alloc l as Hpost;
  try wp_bind (ISeq ISkip _); wp_skip;
  repeat wp_store;
  do 1 wp_skip.

Tactic Notation "rcwp_init" ident(l) :=
  rcwp_init l as "(?&?&?&?&?)".

Tactic Notation "rcwp_init" :=
  let l := fresh "l" in
  rcwp_init l.

Tactic Notation "rcwp_free" ident(l) "as" constr(Hdagger) :=
  lazymatch goal with
  | |- envs_entails ?Δ (wp ?s ?E ?e ?Q) =>
  (* find the tuple [vs] pointed by [l] *)
  (* (it only excludes [BDeallocated] to be deallocated, not it is not
      a big loss when freeing a single location) *)
  fetch_mapsto_tuple Δ l ltac:(fun tuple =>
  lazymatch tuple with (?Hl, _, ?vs) =>
  (* get/build mapsfroms for [l] *)
  fetch_or_construct_vmapsfrom_nat Δ ((#l)%V) ltac:(fun tuple =>
  lazymatch tuple with (?pl, ?ql, ?nl) =>
    iApply (rc.wp_free_singleton s E e _ l (BTuple vs) with
                [SGoal (SpecGoal GSpatial true [] [Hl; pl] false);
                 SGoal (SpecGoal GSpatial false [Hl; pl] [] false)]);
    [ first [ reflexivity | fail 1 "error: " e " is a value and freeing requires a non-value" ]
    | first [ now intros [] | fail 1 "error: block must not be a stack cell" ]
    |
    let pat := eval compute in ("(" ++ Hdagger ++ " & ?)")%string in
      iIntros pat; diamonds
    ]
  end) end)
  | _ => fail "rcwp_free: the goal should be a wp of a non-value"
  end.

Tactic Notation "rcwp_free" ident(l) "as" constr(Hdagger) constr(Hdiamonds) :=
  rcwp_free l as Hdagger;
  iRename select (♢_)%I into Hdiamonds.

Tactic Notation "rcwp_free" ident(l) :=
  rcwp_free l as "?".

(* Calling [all_vmapsfrom_nat [] [] spatial_env] will collect all
   hypotheses in [spatial_env] that are of the form [v ↤ℕ? n] (except
   for [n = 0]) and return the pair of the list of the hypotheses's
   identifiers and the list of pairs [(v, n)]s. *)

Ltac all_vmapsfrom_nat ids vns e :=
  match e with
  | Enil => constr:((ids, vns))
  (* decrementing from 0 is useless, and it prevents from catching
     the to-be-freed location: *)
  | Esnoc ?e ?id (_ ↤ℕ  0)%I => all_vmapsfrom_nat ids vns e
  | Esnoc ?e ?id (_ ↤ℕ? 0)%I => all_vmapsfrom_nat ids vns e
  | Esnoc ?e ?id (?v ↤ℕ? ?n)%I =>
    all_vmapsfrom_nat constr:(id :: ids) constr:((v, n) :: vns) e
  | Esnoc ?e ?id (?l ↤ℕ ?n)%I =>
    all_vmapsfrom_nat constr:(id :: ids) constr:((VLoc l, n) :: vns) e
  | Esnoc ?e _ _ => all_vmapsfrom_nat ids vns e
  end.

(* Those three lemmas help proving that [val_occs v vs] is greater or
   equal than the number of times [v] appears syntactically in [vs].
   It is used in the tactic [simplifies_vmapfrom_nat_val_occs] *)

Lemma val_occs_cons_eq v vs : (val_occs v (v :: vs) = 1 + val_occs v vs)%nat.
Proof.
  destruct v; auto.
  simpl; rewrite Z.eqb_refl; auto.
Qed.

Lemma val_occs_cons_maybe v v' vs : (val_occs v vs <= val_occs v (v' :: vs))%nat.
Proof.
  destruct v, v'; simpl; try lia.
  destruct (_ =? _); simpl; lia.
Qed.

Lemma val_occs_cons v v' vs : val_occs v' (v :: vs) = (val_occs v' [v] + val_occs v' vs)%nat.
Proof.
  destruct v, v'; simpl; auto.
  destruct (_ =? _); auto.
Qed.

Lemma val_occs_nil v : val_occs v [] = 0%nat.
Proof.
  destruct v; reflexivity.
Qed.

(* This tactic finds hypotheses of the form [l ↤ℕ (n - val_occs v vs)]
   and replace them with [l ↤ℕ (n - k)] where [k] is an
   underapproximation of the number of times [v] appears in [vs]. *)

Ltac simplifies_vmapfrom_nat_val_occs :=
  iSelect
    (_ ↤ℕ? (_ - val_occs _ _))%I
    (fun hyp => iPoseProof (vmapsfrom_nat_weaken with hyp) as hyp;
             [ (apply Nat.sub_le_mono_l;
                repeat
                  first
                  [ rewrite val_occs_cons_eq; apply plus_le_compat_l
                  | eapply le_trans; [ | apply val_occs_cons_maybe ]
                  | rewrite val_occs_nil ];
                reflexivity)
             | simpl plus ]).


(* [wp_free_cleanup l] deallocates [l ↦ BTuple vs] and at the same
   time replaces all hypotheses of the form [v ↤ℕ? n] and decrements
   [n] by the number [k] of times [v] appears in [vs].

   [wp_free_cleanup l false] uses [k = val_occs v vs]

   [wp_free_cleanup l true] (default) uses the number of times [k]
   appears syntactically in [vs], which may be an
   underapproximation *)

Tactic Notation "wp_free_cleanup" ident(l) constr(b) :=
  lazymatch goal with
  | |- envs_entails ?Δ (wp ?s ?E ?i ?Φ) =>
  match Δ with Envs _ ?spatialenv _ =>
  (* collects all hypotheses of the form [l' ↤ℕ n] (for [n] not 0) *)
  let x := ltac:(all_vmapsfrom_nat (@nil ident) (@nil (value * nat)) spatialenv) in
  match x with (?ids, ?vns) =>
  (* fetch [l]'s tuple *)
  fetch_mapsto_tuple Δ l ltac:(fun tuple =>
  lazymatch tuple with (?Hl, _, ?vsl) =>
  (* get/build mapsfroms for [l] *)
  fetch_or_construct_vmapsfrom_nat Δ ((#l)%V) ltac:(fun tuple =>
  lazymatch tuple with (?pl, ?ql, ?nl) =>
    iApply (wp_free_cleanup_singleton_rc_values s E i Φ l vsl vns with
                [SGoal (SpecGoal GSpatial true [] ([Hl; pl] ++ ids) false);
                 SGoal (SpecGoal GSpatial false ([Hl; pl] ++ ids) [] true)]);
    [ first [ reflexivity | fail 1 "error: " i " is a value and freeing requires a non-value" ]
    | let pat := ltac:(eval compute in (conj_idents ([Hl] ++ ids ++ [IAnon xH]))) in
      iIntros pat;
      (* the following match replaces [iClear select ([∗ list] y ∈ [[]]%V, _)%I]
         which somehow fails *)
      lazymatch goal with
      | |- envs_entails ?Δ _ =>
        lazymatch Δ with
        | context[Esnoc _ ?H ([∗ list] y ∈ [[]]%V, _)%I] => iClear H
      end end;
      diamonds;
      match b with
      | true => repeat progress simplifies_vmapfrom_nat_val_occs
      | false => idtac
      end;
      simpl (_.1);
      simpl (_.2);
      repeat rewrite simpl_vmapsfrom_nat_loc
    ]
  end)
  end)
  end
  end
  end.

Tactic Notation "wp_free_cleanup" ident(l) := wp_free_cleanup l true.
