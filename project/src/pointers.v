From stdpp Require Import gmap gmultiset.
From spacelang Require Import stdpp hypotheses state.
From spacelang Require Import predecessors.

(* The modules successors.v and predecessors.v are independent of SpaceLang.
   They make a few assumptions, expressed as a type class, [Hypotheses]. In
   this module, we prove that SpaceLang satisfies these hypotheses. *)

(* We also establish several properties of the functions [old_pointers] and
   [new_pointers] in the specific setting of SpaceLang. *)

(* ------------------------------------------------------------------------ *)

(* This is the list of pointers found in a value. *)

Definition val_pointer_list (v : value) : list loc :=
  match v with
  | VLoc l =>
      [l]
  | _ =>
      []
  end.

(* This is the list of pointers found in a memory block. *)

Definition block_pointer_list (b : block) : list loc :=
  match b with
  | BTuple vs =>
      foldr (fun v ps => val_pointer_list v ++ ps) [] vs
  | BCell v =>
      val_pointer_list v
  | BDeallocated =>
      []
  end.

(* These are the multisets of pointers found in a value or block. *)

Definition val_pointer_set v : gmultiset loc :=
  list_to_set_disj (val_pointer_list v).

Definition block_pointer_set b : gmultiset loc :=
  list_to_set_disj (block_pointer_list b).

(* ------------------------------------------------------------------------ *)

(* This predicate recognizes stack cells. *)

Definition block_is_stack_cell (b : block) : Prop :=
  match b with BCell _ => True | _ => False end.

(* ------------------------------------------------------------------------ *)

(* The above definitions satisfy the requirements of hypotheses.v. *)

Global Instance spacelang_hypotheses : @Hypotheses loc _ _ block _.
Proof.
  exists block_pointer_set BDeallocated block_is_stack_cell.
  { reflexivity. }
  { destruct b; simpl; typeclasses eauto. }
  { simpl. tauto. }
  { simpl. tauto. }
Defined.

(* ------------------------------------------------------------------------ *)

(* Immediate consequences of the definitions. *)

Lemma pointers_def b :
  pointers b = block_pointer_set b.
Proof.
  reflexivity.
Qed.

Lemma block_pointer_list_cons v vs :
  block_pointer_list (BTuple (v :: vs)) =
  val_pointer_list v ++ block_pointer_list (BTuple vs).
Proof.
  reflexivity.
Qed.

Lemma block_pointer_cons v vs :
  block_pointer_set (BTuple (v :: vs)) =
  val_pointer_set v ⊎ block_pointer_set (BTuple vs).
Proof.
  rewrite /block_pointer_set block_pointer_list_cons list_to_set_disj_app //.
Qed.

Lemma block_pointer_cell v :
  block_pointer_set (BCell v) = val_pointer_set v.
Proof.
  eauto.
Qed.

Lemma pointers_BCell_VLoc l :
  pointers (BCell (VLoc l)) = {[+l+]}.
Proof.
  rewrite pointers_def /block_pointer_set. simpl. multiset_solver.
Qed.

Lemma pointers_BCell_VLoc_membership l :
  l ∈ pointers (BCell (VLoc l)).
Proof.
  rewrite pointers_BCell_VLoc. set_solver.
Qed.

Global Hint Resolve
  pointers_BCell_VLoc
  pointers_BCell_VLoc_membership
: pointers.

Lemma is_stack_cell_BCell v :
  is_stack_cell (BCell v).
Proof.
  simpl. eauto.
Qed.

Global Hint Resolve is_stack_cell_BCell : core.

Lemma not_deallocated_BTuple b vs :
  b = BTuple vs →
  b ≠ deallocated.
Proof.
  simpl. congruence.
Qed.

Global Hint Resolve not_deallocated_BTuple : core.

(* ------------------------------------------------------------------------ *)

(* The relation [alias v w] means that the values [v] and [w] are both
   pointers, and are the same pointer. This relation is stronger than
   [v = w]. For our purposes below, both relations would work. It is
   easier to show that [same_location] is decidable, though, which is
   why we use it. *)

Definition alias v w :=
  match v, w with
  | VLoc l, VLoc m => l = m
  | _, _ => False
  end.

Global Instance decide_alias v w :
  Decision (alias v w).
Proof.
  destruct v, w; simpl; solve_decision.
Qed.

Lemma alias_implies_eq v w :
  alias v w → v = w.
Proof.
  destruct v, w; simpl; solve [ tauto | congruence ].
Qed.

Lemma alias_implies_equal_locations v w :
  alias v w → ∃ l, v = VLoc l ∧ w = VLoc l.
Proof.
  destruct v, w; simpl; try solve [ tauto ].
  intros. subst. eauto.
Qed.

(* If [v] and [w] are not aliased, then their sets of pointers are
   disjoint. The stronger hypothesis [v ≠ w] would work, too. *)

Lemma nonaliased_values_have_disjoint_pointer_sets v w :
  ~ alias v w →
  val_pointer_set v ∩ val_pointer_set w = ∅.
Proof.
  intros. destruct v, w; rewrite /val_pointer_set; simpl;
  try solve [ multiset_solver ].
  (* The sole case of interest involves two locations. *)
  rename l0 into l'.
  assert (l ≠ l') by congruence.
  rewrite !gmultiset_disj_union_empty_r.
  rewrite gmultiset_intersection_singletons_ne //.
Qed.

(* ------------------------------------------------------------------------ *)

(* If [v] is a field of the block [b], then the pointers of [v]
   form a subset of the pointers of [b]. *)

Lemma pointers_lookup vs : ∀ ofs,
  (0 <= ofs < length vs)%nat ->
  val_pointer_set (vs !!! ofs) ⊆ pointers (BTuple vs).
Proof.
  induction vs as [| v vs ]; simpl; [ lia |]; intros.
  destruct (decide (ofs = 0)%nat);
  [ subst ofs | replace ofs with (S (ofs - 1)) by lia];
  simpl; rewrite !block_pointer_cons.
  { multiset_solver. }
  { transitivity (pointers (BTuple vs)).
    - eapply IHvs. lia.
    - multiset_solver. }
Qed.

(* If [v] is a field of the block [b], then, when [v] is overwritten with [w],
   the pointers of the updated block are those of the block [b], minus the
   pointers of [v], plus the pointers of [w]. *)

Lemma pointers_store_preliminary w vs : ∀ ofs,
  (0 <= ofs < length vs)%nat ->
  pointers (BTuple (<[ ofs := w ]> vs)) ⊎ val_pointer_set (vs !!! ofs) =
  pointers (BTuple vs) ⊎ val_pointer_set w.
Proof.
  induction vs as [| v vs ]; simpl; [ lia |]; intros.
  destruct (decide (ofs = 0)%nat);
  [ subst ofs | replace ofs with (S (ofs - 1)) by lia];
  simpl; rewrite !block_pointer_cons.
  { multiset_solver. }
  { rewrite -assoc IHvs; [| lia ]. multiset_solver. }
Qed.

Lemma pointers_store w vs ofs :
  (0 <= ofs < length vs)%nat ->
  pointers (BTuple (<[ ofs := w ]> vs)) =
  pointers (BTuple vs) ∖ val_pointer_set (vs !!! ofs) ⊎ val_pointer_set w.
Proof.
  intros Hofs.
  pose proof (pointers_lookup vs ofs Hofs).
  pose proof (pointers_store_preliminary w vs ofs Hofs).
  multiset_solver.
Qed.

(* Two ad hoc lemmas used below. *)

Lemma adhoc_multiset_lemma_1 `{Countable A} (B V W : gmultiset A) :
  V ⊆ B →
  V ∩ W = ∅ →
  V ⊆ B ∖ (B ∖ V ⊎ W).
Proof.
  multiset_solver.
Qed.

Lemma adhoc_multiset_lemma_2 `{Countable A} (B V W : gmultiset A) :
  V ∩ W = ∅ →
  W ⊆ (B ∖ V ⊎ W) ∖ B.
Proof.
  multiset_solver.
Qed.

(* If [v] is a field of the block [b], and if [v] and [w] are not
   aliased, then, when [v] is overwritten with [w], the pointers that
   disappear because of this update are exactly the pointers of [v]. *)

Lemma old_pointers_store_nonaliased w v vs ofs :
  (0 <= ofs < length vs)%nat ->
  v = vs !!! ofs →
  ~ alias v w →
  let b  := BTuple vs in
  let b' := BTuple (<[ ofs := w ]> vs) in
  old_pointers b b' = val_pointer_set v.
Proof.
  intros ? Hv Hne ? ?.
  rewrite /b /b' /old_pointers pointers_store //. rewrite -Hv.
  eapply (anti_symm (⊆)).
  (* This inclusion is easy and does not need [v ≠ w]. *)
  { clear Hne. multiset_solver. }
  (* This inclusion is trickier. *)
  { eapply adhoc_multiset_lemma_1.
    - rewrite Hv. eauto using pointers_lookup.
    - eapply nonaliased_values_have_disjoint_pointer_sets. exact Hne. }
Qed.

(* If [v] is a field of the block [b], and if [v] and [w] are not aliased,
   then, when [v] is overwritten with [w], the pointers that appear because
   of this update are exactly the pointers of [w]. *)

Lemma new_pointers_store_nonaliased w v vs ofs :
  (0 <= ofs < length vs)%nat ->
  v = vs !!! ofs →
  ~ alias v w →
  let b  := BTuple vs in
  let b' := BTuple (<[ ofs := w ]> vs) in
  new_pointers b b' = val_pointer_set w.
Proof.
  intros ? Hv Hne ? ?.
  rewrite /b /b' /new_pointers pointers_store //. rewrite -Hv.
  eapply (anti_symm (⊆)).
  (* This inclusion is easy and does not need [v ≠ w]. *)
  { clear Hne. multiset_solver. }
  (* This inclusion is trickier. *)
  { eapply adhoc_multiset_lemma_2.
    - eapply nonaliased_values_have_disjoint_pointer_sets. exact Hne. }
Qed.

(* A newly-allocated block, filled with unit values, has no pointers. *)

Lemma block_no_pointers n v b :
  b = BTuple (replicate n v) →
  val_pointer_list v = [] →
  pointers b = ∅.
Proof.
  intros Hb Hv.
  rewrite pointers_def Hb {Hb} /block_pointer_set.
  rewrite -list_to_set_disj_nil. f_equal.
  induction n.
  - eauto.
  - simpl replicate.
    rewrite block_pointer_list_cons.
    rewrite IHn Hv //.
Qed.

(* Analogous lemmas for stack cells. *)

Lemma old_pointers_cell_nonaliased w v :
  ~ alias v w →
  let b  := BCell v in
  let b' := BCell w in
  old_pointers b b' = val_pointer_set v.
Proof.
  intros Hne ? ?.
  rewrite /b /b' /old_pointers. simpl.
  rewrite !block_pointer_cell.
  eapply (anti_symm (⊆)).
  (* This inclusion is easy and does not need [v ≠ w]. *)
  { clear Hne. multiset_solver. }
  (* This inclusion is trickier. *)
  { pose proof (nonaliased_values_have_disjoint_pointer_sets _ _ Hne).
    multiset_solver. }
Qed.

Lemma new_pointers_cell_nonaliased w v :
  ~ alias v w →
  let b  := BCell v in
  let b' := BCell w in
  new_pointers b b' = val_pointer_set w.
Proof.
  intros Hne ? ?.
  rewrite /b /b' /new_pointers. simpl.
  rewrite !block_pointer_cell.
  eapply (anti_symm (⊆)).
  (* This inclusion is easy and does not need [v ≠ w]. *)
  { clear Hne. multiset_solver. }
  (* This inclusion is trickier. *)
  { pose proof (nonaliased_values_have_disjoint_pointer_sets _ _ Hne).
    multiset_solver. }
Qed.

Lemma cell_no_pointers v b :
  b = BCell v →
  val_pointer_list v = [] →
  pointers b = ∅.
Proof.
  intros Hb Hv.
  rewrite pointers_def Hb {Hb} /block_pointer_set.
  rewrite -list_to_set_disj_nil.
  f_equal.
  eauto.
Qed.

(* -------------------------------------------------------------------------- *)

(* Miscellaneous little lemmas used in wp.v. *)

Lemma lookup_insert_ne_implication l l' b ob' (s : STORE) :
  l ≠ l' →
  s !! l' = ob' →
  <[l:=b]> s !! l' = ob'.
Proof.
  intros. rewrite lookup_insert_ne //.
Qed.

Global Hint Resolve lookup_insert_ne_implication : core.

Lemma simplify_updated_store_lookup l l' b ob' σ :
  <[l:=b]> (store σ) !! l' = ob' →
  store (<[l:=b]> <$> σ) !! l' = ob'.
Proof.
  eauto.
Qed.

Global Hint Resolve simplify_updated_store_lookup : core.
