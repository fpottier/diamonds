From spacelang Require Import wp_state_interp wp_memory.

(* This module establishes a reasoning rule for write accesses to the heap. *)

(* ------------------------------------------------------------------------ *)

(* Writing a block at address [l] requires full ownership of it. *)

(* The location [l] is removed from the predecessor set of the value
   [v] that is overwritten, and added to the predecessor set of the
   new value [w] that is written. To this end, fractional ownership
   of each of these two predecessor sets is required. It is worth
   stressing that:
   1. There is no obligation to prove that [v] and [w] are distinct.
      The rule works also in the case where they are the same value.
   2. There is no obligation to prove that the address [l] is in the
      set [lsv]. (If it is not, then a loss of information takes
      place: the address [l] is not removed from the predecessor
      set of the value [v]. It is up to the user to either avoid this
      information loss or somehow tolerate it.) *)

Lemma wp_store_dep `{!heapG Σ} (precise:bool) s E dst ofs src l v w vs qv lsv qw lsw :
  (0 <= ofs < length vs)%nat →
  v = vs !!! ofs →
  {{{
    ▷ dst ↦ BCell (VLoc l) ∗
    ▷ src ↦ BCell w ∗
    ▷ l ↦ BTuple vs ∗
    ▷ (if precise then v ↤?{qv} lsv else ⌜True⌝) ∗
    ▷ w ↤?{qw} lsw
  }}}
    IStore (LLoc dst) ofs (LLoc src)
    @ s; E
  {{{ RET tt;
    dst ↦ BCell (VLoc l) ∗
    src ↦ BCell w ∗
    l ↦ BTuple (<[ ofs := w ]> vs) ∗
    (if precise then v ↤?{qv} (lsv ∖ {[+l+]}) else ⌜True⌝) ∗
    w ↤?{qw} (lsw ⊎ {[+l+]})
  }}}.
Proof.
  intros ??. subst v. set (v := vs !!! ofs).
  iIntros (Φ) "(>Hdst & >Hsrc & >Hl & Hv & >Hw) HΦ".
  iApply wp_lift_atomic_head_step_no_fork; first done.
  iIntros (σ1 ns κ κs n) "Hstate". iModIntro. iSplit.

  (* First, prove that this instruction is reducible. *)
  { destruct_state_interp.
    exploit_stack_cell src.
    exploit_heap_cell l.
    exploit_stack_cell dst.
    head_reducible. }

  (* Then, examine an arbitrary reduction step. *)
  examine_step.
  do 2 inv_read_lvalue.
  exploit_stack_cell dst. simplify_eq. rename l0 into l. (* TODO *)
  exploit_stack_cell src. simplify_eq. rename w0 into w. (* TODO *)
  exploit_heap_cell l. simplify_eq. rename vs0 into vs. (* TODO *)
  (* Update the conceptual store by writing [w] at offset [ofs]
     in the memory block at address [l]. *)
  iMod (ph_store_heap_dep precise (store θ) l v vs ofs w qv lsv qw lsw
    with "[$]")
      as "(Hph & Hl & Hv & Hw)".
  { eauto. }
  { eauto. }
  { eauto. }
  (* Conclude. *)
  iModIntro. iSplit=> //.
  iSplitL "Hph Hauth"; [| iApply "HΦ"; iFrame ].
  (* Close the state interpretation invariant. *)
  intro_state_interp. iExists (<[l:=BTuple (<[ofs:=w]> vs)]> <$> θ).
  rewrite available_update_tuple //.
  iFrame "Hph Hauth".
  iPureIntro.
  splits; eauto 3 with valid_state related. eauto with closed.
Qed.

Lemma wp_store `{!heapG Σ} s E dst ofs src l v w vs qv lsv qw lsw :
  (0 <= ofs < length vs)%nat →
  v = vs !!! ofs →
  {{{
    ▷ dst ↦ BCell (VLoc l) ∗
    ▷ src ↦ BCell w ∗
    ▷ l ↦ BTuple vs ∗
    ▷ v ↤?{qv} lsv ∗
    ▷ w ↤?{qw} lsw
  }}}
    IStore (LLoc dst) ofs (LLoc src)
    @ s; E
  {{{ RET tt;
    dst ↦ BCell (VLoc l) ∗
    src ↦ BCell w ∗
    l ↦ BTuple (<[ ofs := w ]> vs) ∗
    v ↤?{qv} (lsv ∖ {[+l+]}) ∗
    w ↤?{qw} (lsw ⊎ {[+l+]})
  }}}.
Proof.
  apply (wp_store_dep true).
Qed.

(* We give a weaker rule for store which does not track predecessors of [v].
   It can still be useful if we already know that all predecessors will be unreachable
   someday. *)
Lemma wp_store_weak `{!heapG Σ} s E dst ofs src l v w vs qw lsw :
  (0 <= ofs < length vs)%nat →
  v = vs !!! ofs →
  {{{
    ▷ dst ↦ BCell (VLoc l) ∗
    ▷ src ↦ BCell w ∗
    ▷ l ↦ BTuple vs ∗
    ▷ w ↤?{qw} lsw
  }}}
    IStore (LLoc dst) ofs (LLoc src)
    @ s; E
  {{{ RET tt;
    dst ↦ BCell (VLoc l) ∗
    src ↦ BCell w ∗
    l ↦ BTuple (<[ ofs := w ]> vs) ∗
    w ↤?{qw} (lsw ⊎ {[+l+]})
  }}}.
Proof.
  intros ? ?. derive_wp_from (wp_store_dep false).
Qed.
