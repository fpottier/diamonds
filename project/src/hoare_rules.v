From spacelang Require Import wp_state_interp wp notation proofmode.

(* Several rules are stated in terms of WP in files [wp_*.v]. In this file, we
   state corresponding rules in terms of Hoare triple to make them easier to
   read.

   Note that these Hoare-triple-style rules are less suited for proof
   automation and slightly less general than the WP rules, because the latter
   allow for a finer control over the later modalities, which are useful to
   perform Löb inductions. *)

Notation "{{ P } } e {{ Φ } }" := (⊢ (□ (P -∗ WP e @ NotStuck; ⊤ {{ fun _ => Φ }}))%I)
  (at level 20, P, e, Φ at level 200,
   format "{{  P  } }  e   {{  Φ  } }") : stdpp_scope.

Context `{!heapG Σ}.

Lemma hoare_skip Ψ : {{ Ψ }} ISkip {{ Ψ }}.
Proof.
  iModIntro.
  iIntros "p".
  now iApply wp_skip.
Qed.

Lemma hoare_seq (Φ Χ Ψ : iProp Σ) (i1 i2 : instr) :
 {{ Φ }} i1 {{ Χ }} ->
 {{ Χ }} i2 {{ Ψ }} ->
 {{ Φ }} i1;; i2 {{ Ψ }}.
Proof.
  intros H1 H2; iModIntro; iIntros "p".
  iApply wp_seq.
  iDestruct (H1 with "p") as "H1".
  iApply (wp_mono with "H1").
  iIntros ([]) "χ".
  iDestruct (H2 with "χ") as "H2".
  now iNext.
Qed.

Lemma hoare_if (Φ Ψ : iProp Σ) (r : loc) (k : Int63.int) (i1 i2 : instr) :
  {{ r ↦ BCell (VInt k) ∗ Φ }} if Int63.is_zero k then i2 else i1 {{ Ψ }} ->
  {{ r ↦ BCell (VInt k) ∗ Φ }} IIf r i1 i2 {{ Ψ }}.
Proof.
  intros H; iModIntro; iIntros "(r & p)".
  iApply (wp_if with "[-r] r").
  iNext. iIntros "r".
  iApply (H with "[$]").
Qed.

Lemma hoare_call (Φ Ψ : iProp Σ) (c : loc) (q : Qp) (xs : list binder) (ss : list lvalue) (i : instr) :
  length xs = length ss →
  {{ c ↦{q} BCell (VCode xs i) ∗ Φ }} substs (zip xs ss) i {{ Ψ }} ->
  {{ c ↦{q} BCell (VCode xs i) ∗ Φ }} ICall (LLoc c) (ss)  {{ Ψ }}.
Proof.
  intros Hlen H.
  iModIntro.
  iIntros "(Hc & Φ)".
  iApply (wp_call with "[Φ] Hc"). auto.
  iNext.
  iIntros "Hc". iApply (H with "[$Hc $Φ]").
Qed.

Lemma hoare_alloca (Φ Ψ : iProp Σ) (x : binder) (i : instr) :
  (forall c : loc, {{ Φ ∗ c ↦ BCell VUnit }} subst x (LLoc c) i {{ c ↦ BCell VUnit ∗ Ψ }}) ->
  {{ Φ }} IAlloca x i {{ Ψ }}.
Proof.
  intros H; iModIntro; iIntros "p".
  iApply wp_alloca. iNext. iIntros (c) "Hc".
  iDestruct (H c) as "#H".
  iDestruct ("H" with "[$p $Hc]") as "Hs".
  iApply (wp_mono with "Hs").
  iIntros ([]) "(Hc & ψ)".
  iExists ()%V, 1%Qp, ∅.
  iFrame.
  auto.
Qed.

Lemma hoare_fork (Φ : iProp Σ) (r : loc) (x : binder) (i : instr) v q L :
  (∀ c, {{ c ↦ BCell v ∗ v↤?{q}((L ∖ {[+r+]}) ⊎ {[+c+]}) ∗ Φ }}
          subst x (LLoc c) i
        {{ c ↦ BCell ()%V }}) ->
  {{ r ↦ BCell v ∗ v ↤?{q} L ∗ Φ }}
    IFork (LLoc r) x i
  {{ r ↦ BCell ()%V }}.
Proof.
  intros H; iModIntro; iIntros "(Hc & pv & p)".
  iApply (wp_fork with "[$Hc $pv p]"). 2: auto.
  iIntros (c) "(Hc & pv)".
  iApply (H c with "[$pv $Hc $p]").
Qed.

(* The remaining rules
  -  wp_const
  -  wp_move
  -  wp_alloc
  -  wp_load
  -  wp_loceq
  are in "Texan triple" form
    {{{ Φ }}} i {{{ RET tt; Ψ }}}
  with no hypothesis. This which is a notation for the CPS-style WP rule
    □(∀Φ, P -∗ ▷(Q -∗ Φ) -∗ wp i Φ)
  which is easier to automate inside Iris.

  We do not show the Hoare triple version for each of them, we only show the
  trivial implication from a Texan triples to a normal Hoare triple:
*)

Lemma texan_hoare (Φ Ψ : iProp Σ) (i : instr) : {{{ Φ }}} i {{{ RET tt; Ψ }}} -> {{ Φ }} i {{ Ψ }}.
Proof.
  intros H; iModIntro; iIntros "p".
  iApply (H with "p"). auto.
Qed.
