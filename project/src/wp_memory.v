From spacelang Require Import wp_state_interp.

(* This module establishes several technical lemmas about the meaning of
   our assertions. *)

(* ------------------------------------------------------------------------ *)

(* Exploiting the conjunction of a fractional [mapsto] assertion and a full
   [mapsfrom] assertion. From the fact that there is a forward edge from [l]
   to [l'], we deduce that [l] must appear in the multiset [ls] of all
   predecessors of [l']. *)

(* This is a rephrasing of the lemma by the same name in ph.v. *)

(* This lemma can be used directly by the end user. *)

Lemma exploit_mapsto_mapsfrom `{!heapG Σ} E l q b l' ls :
  l ↦{q} b ∗ l' ↤{1%Qp} ls ∗ ⌜ l' ∈ pointers b ⌝ ={{E}}=∗
  l ↦{q} b ∗ l' ↤{1%Qp} ls ∗ ⌜ l ∈ ls ⌝.
Proof.
  rewrite /supd. iIntros (? ? ? ?) "(Hstate & Hmapsto & Hmapsfrom & %Hl')".
  destruct_state_interp.
  iDestruct (exploit_mapsto_mapsfrom with "Hph Hmapsto Hmapsfrom") as %fact.
  iModIntro.
  iFrame "Hmapsto Hmapsfrom".
  iSplit.
  + intro_state_interp. iExists _. iFrame. done.
  + eauto.
Qed.

(* ------------------------------------------------------------------------ *)

(* The assertion [c ↦ BCell v] guarantees the existence of this cell in the
   physical and conceptual stores. *)

Lemma exploit_stack_cell `{!heapG Σ} σ θ c q v :
  related σ θ →
  ph_interp (store θ) ∗
  c ↦{q} BCell v -∗
  ⌜ store σ !! c = Some (BCell v) ∧ store θ !! c = Some (BCell v) ⌝.
Proof.
  iIntros (?) "(Hph & Hc)".
  (* Exploit the state interpretation predicate to obtain the knowledge
     that the stack cell [c] must exist in the conceptual store. *)
  iDestruct (exploit_mapsto with "Hph Hc") as %Hc.
  (* The physical and conceptual stores agree on stack cells, so this
     stack cell must exist in the physical store as well. *)
  eauto using related_at_stack_cell.
Qed.

Ltac exploit_stack_cell c :=
  iDestruct (exploit_stack_cell _ _ c _ _ with "[$]") as %(? & ?);
    [ eassumption |].

(* ------------------------------------------------------------------------ *)

(* The assertions [c ↦ BCell (VLoc l)] and [l ↦{q} b] guarantee
   the existence of the memory block [b] at address [l] in the
   physical store. *)

Lemma exploit_heap_cell `{!heapG Σ} σ θ c l q b :
  related σ θ →
  ph_interp (store θ) ∗
  c ↦ BCell (VLoc l) ∗
  l ↦{q} b -∗
  ⌜ store σ !! l = Some b ∧ store θ !! l = Some b ⌝.
Proof.
  iIntros (Hrelated) "(Hph & Hc & Hl)".
  exploit_stack_cell c.
  iDestruct (exploit_mapsto with "Hph Hl") as %Hl.
  eauto using related_at_reachable_cell with reachable.
Qed.

Ltac exploit_heap_cell l :=
  iDestruct (exploit_heap_cell _ _ _ l with "[$]") as %(? & ?);
    [ eassumption |].

(* ------------------------------------------------------------------------ *)

(* A technical lemma, for internal use. *)

Lemma interpret_val_pointer `{!heapG Σ} v q ls :
  interpret (map (λ l', (l', q, ls)) (val_pointer_list v)) ≡
  (v ↤?{q} ls)%I.
Proof.
  destruct v; simpl; rewrite ?interpret_nil ?interpret_singleton //.
Qed.

(* ------------------------------------------------------------------------ *)

(* The following technical lemma describes the ghost state update that
   takes place at the level of [ph_interp] when a value [w] is stored
   in a memory block or a stack cell, overwriting a previous value [v]. *)

(* This lemma cannot be placed in the file ph.v (even though it may seem as
   though it belongs there) because it depends on several notions that are
   specific of SpaceLang, whereas ph.v is language-agnostic. *)

(* We assume that [b] is the old block and [b'] is the new block. They
   differ by just one field, so, if the values [v] and [w] are equal,
   then [b] and [b'] are equal. If the values [v] and [w] are not
   equal, then they are not aliased (not the same memory location), so
   the pointers present in [b] but not [b'] are the pointers found in
   [v], and the pointers present in [b'] but not in [b] are the
   pointers found in [w]. *)

(* We introduce a [precise] arguments allowing to distinguish the
   case where we do not need to track predecessors. *)

Lemma ph_update_1_dep `{!heapG Σ} (precise:bool) σ l v b w b' qv lsv qw lsw :
  σ !! l = Some b →
  (v = w → b = b') →
  (¬ alias v w → old_pointers b b' = val_pointer_set v) →
  (¬ alias v w → new_pointers b b' = val_pointer_set w) →
  b ≠ deallocated →
  b' ≠ deallocated →
  let σ' := <[l := b']> σ in
  ph_interp σ  ∗ l ↦ b  ∗ (if precise then v ↤?{qv} lsv             else ⌜True⌝) ∗ w ↤?{qw}  lsw ==∗
  ph_interp σ' ∗ l ↦ b' ∗ (if precise then v ↤?{qv} (lsv ∖ {[+l+]}) else ⌜True⌝) ∗ w ↤?{qw} (lsw ⊎ {[+l+]}).
Proof.
  intros Hl ? Hold Hnew ? ?.
  destruct (decide (alias v w)) as [ Halias | Halias ].

  (* Case: [alias v w]. Then, the store instruction has no effect. *)
  { apply alias_implies_equal_locations in Halias.
    destruct Halias as (l' & ? & Hw).
    assert (w = v) by congruence. subst w.
    assert (b = b') by eauto. subst b'.
    intros σ'.
    assert (Hnop: σ' = σ) by (eapply insert_id; exact Hl).
    rewrite Hnop.
    subst v. simpl.
    iIntros "(? & ? & Hmf1 & Hmf2)". iFrame. iModIntro.

    destruct precise.
    { (* The goal boils down to transforming a pair of [mapsfrom] assertions
       into a pair of slightly different [mapsfrom] assertions. This works
       because the [mapsfrom] assertion is covariant. (If that was not the
       case, we would have to structure this proof differently and to not
       treat [alias v w] as a special case.) *)
      iDestruct (mapsfrom_join_split with "Hmf1 Hmf2")
        as "[Hmf1 Hmf2]"; [ | iFrame ].
      multiset_solver. }
    { iFrame. iApply (mapsfrom_weaken with "Hmf2").
      multiset_solver. } }

  (* Case: [v] and [w] are not aliases, that is, not the same pointer. *)
  intros σ'.

  (* Define appropriate in and out triples. *)
  set (old_triples := map (λ l', (l', qv, lsv)) (val_pointer_list v)).
  set (new_triples := map (λ l', (l', qw, lsw)) (val_pointer_list w)).

  (* Perform a ghost state update. *)
  iIntros "(Hh & Hmapsto & Hmf1 & Hmf2)".
  iMod (ph_update_dep precise σ l b b' old_triples new_triples
          with "[Hh Hmapsto Hmf1 Hmf2]")
    as "(Hh & Hmapsto & Hmf1 & Hmf2)".
  { eauto. }
  { eauto. }
  { destruct precise; try easy. rewrite /addresses /old_triples Hold // map_map map_id //. }
  { rewrite /addresses /new_triples Hnew // map_map map_id //. }
  { destruct precise.
    all:rewrite /old_triples /new_triples !interpret_val_pointer insert_id //;
                iFrame. }

  (* There we are! *)
  destruct precise; rewrite /old_triples /new_triples !map_map !interpret_val_pointer.
  all: now iFrame.
Qed.

(* ------------------------------------------------------------------------ *)

(* The following technical lemma describes (part of) the ghost state update
   that takes place when a value [w] is stored in a memory block, overwriting
   a previous value [v]. It is a special case of [ph_update_1]. *)

(* We use a [precise] argument to indicate if we track predecessors. *)

Lemma ph_store_heap_dep `{!heapG Σ} (precise:bool) σ l v vs ofs w qv lsv qw lsw :
  σ !! l = Some (BTuple vs) →
  (0 <= ofs < length vs)%nat →
  v = vs !!! ofs →
  let b := BTuple vs in
  let b' := BTuple (<[ ofs := w ]> vs) in
  let σ' := <[l := b']> σ in
  ph_interp σ  ∗ l ↦ b  ∗ (if precise then v ↤?{qv}  lsv            else ⌜True⌝) ∗ w ↤?{qw}  lsw ==∗
  ph_interp σ' ∗ l ↦ b' ∗ (if precise then v ↤?{qv} (lsv ∖ {[+l+]}) else ⌜True⌝) ∗ w ↤?{qw} (lsw ⊎ {[+l+]}).
Proof.
  intros.
  eapply (ph_update_1_dep precise);
  eauto using old_pointers_store_nonaliased, new_pointers_store_nonaliased.
  (* There remains to prove [v = w → b = b']. *)
  + intro. subst w b b' v.
    rewrite list_insert_id //.
    eapply list_lookup_lookup_total_lt. lia.
Qed.

(* ------------------------------------------------------------------------ *)

(* The following technical lemma describes (part of) the ghost state update
   that takes place when a value [w] is stored in a stack cell, overwriting
   a previous value [v]. It is a special case of [ph_update_1]. *)

Lemma ph_store_stack_dep `{!heapG Σ} (precise:bool) σ c v w qv lsv qw lsw :
  let b := BCell v in
  let b' := BCell w in
  let σ' := <[c := b']> σ in
  σ !! c = Some (BCell v) →
  ph_interp σ  ∗ c ↦ b  ∗ (if precise then v ↤?{qv}  lsv   else ⌜True⌝)          ∗ w ↤?{qw}  lsw ==∗
  ph_interp σ' ∗ c ↦ b' ∗ (if precise then v ↤?{qv} (lsv ∖ {[+c+]}) else ⌜True⌝) ∗ w ↤?{qw} (lsw ⊎ {[+c+]}).
Proof.
  intros.
  eapply (ph_update_1_dep precise);
  eauto using old_pointers_cell_nonaliased, new_pointers_cell_nonaliased.
  (* [v = w → b = b']. *)
  + intro. subst w b b'. eauto.
Qed.


(* -- Tactics ---------------------------- *)

(* Breaks up hypotheses of the form [P ∗ Q], typically is useful when we do
   not need hypotheses to be named. *)

Ltac destruct_stars :=
  repeat iDestruct select (_ ∗ _)%I as "(?&?)".

(* This is _not_ a general tactic.
   It works well to derive weak lemmas from a specialized version
   of a stronger one. *)

Ltac derive_wp_from Spec :=
  iIntros (Φ) "? HΦ"; (* Set up *)
  destruct_stars; (* split stars to help tactics. *)
  iApply (Spec with "[-HΦ]"); (* Try to iApply using everything available. *)
  try exact inhabitant; try easy; try iFrame; (* Try to solve easy goals. *)
  iModIntro; try easy; (* In case we need to prove ▷ True *)
  iIntros; destruct_stars; iApply "HΦ"; iFrame. (* Conclude. *)
