From spacelang Require Import wp_state_interp.

(* This auxiliary lemma reasons about the reduction of [skip; i] to [i]. *)

Lemma wp_seq_skip `{!heapG Σ} s E i Φ :
  ▷ WP i @ s; E {{ Φ }} ⊢ WP (ISeq ISkip i) @ s; E {{ Φ }}.
Proof.
  iIntros "HWP".
  (* This is not an "atomic" step, because [ISeq ISkip i] reduces to [i],
     which is an arbitrary instruction. This is also not a "pure" step,
     because the GC can affect the machine state. Thus, we have to use
     one of the lowest-level lifting lemmas. *)
  iApply wp_lift_step; first done.
  iIntros (σ1 κ ns κs nt) "Hstate".
  iApply fupd_mask_intro; first set_solver. iIntros "Hclose". iSplit.

  (* First, prove that this instruction is reducible. *)
  { destruct s; [| eauto ]. head_reducible. }

  (* Then, examine an arbitrary reduction step. (Lots happening here.) *)
  examine_step.

  (* Conclude. *)
  iMod "Hclose" as "_".
  iModIntro.
  iFrame "HWP".
  iSplitL; [| eauto ].
  iExists _. iFrame. eauto.
Qed.

(* As a corollary, we obtain a standard rule for sequencing. *)

Lemma wp_seq `{!heapG Σ} s E i1 i2 Φ :
    WP i1 @ s; E {{ λ_, ▷ WP i2 @ s; E {{ Φ }} }}
  ⊢ WP (ISeq i1 i2) @ s; E {{ Φ }}.
Proof.
  iIntros "Hi1".
  iApply (wp_bind (λ i1, ISeq i1 i2)).
  iApply (wp_strong_mono s s E E with "Hi1"); eauto 1.
  iIntros ([]) "Hi2". simpl. unfold of_val.
  iModIntro.
  iApply wp_seq_skip.
  eauto.
Qed.
