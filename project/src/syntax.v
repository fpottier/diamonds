From Coq Require Int63.
From stdpp Require Export binders strings.
From iris.prelude Require Export prelude.

(* This file defines the syntax of SpaceLang. *)

(* ------------------------------------------------------------------------ *)

(* A memory location is an integer address. *)

(* We use a single space of memory locations to represent both addresses
   in the heap and addresses in the stack. This allows us to reason in a
   homogeneous way about pointers from the heap to the heap and pointers
   from the stack to the heap. *)

(* When possible, we let the metavariable [l] range over addresses in the heap
and we use the metavariable [c] for addresses in the stack. *)

Definition loc :=
  Z.

(* ------------------------------------------------------------------------ *)

(* A value is one of the following:

   - a unit value;
   - a 63-bit integer;
   - a memory location (the address of a block in the heap);
   - a closed procedure, which can be thought of as a code pointer.

   A value is closed. (It is technically possible to construct a
   procedure that has free variables, but it will be impossible to
   verify that such a procedure is correct.)

   Procedures can have an arbitrary arity. They are not explicitly
   recursive. We believe that recursive procedures can be simulated
   by passing a code pointer (a pointer to the procedure itself) as
   an extra parameter.

   A value can be represented in one word of memory. It can be stored in
   a memory block in the heap and in a stack cell. *)

Inductive value :=
  | VUnit
  | VInt (n: Int63.int)
  | VLoc (l: loc)
  | VCode (xs : list binder) (i : instr)

(* ------------------------------------------------------------------------ *)

(* An lvalue is either a variable or a memory location, which represents the
   address of a stack cell. *)

(* This syntactic category contains variables and is closed under
   substitutions of lvalues for variables. *)

(* By using this category almost everywhere in the syntax of expressions
   (below), we enforce an administrative normal form and thereby greatly
   simplify the syntax of evaluation contexts. *)

with lvalue :=
  | LVar (x : string)
  | LLoc (c : loc)

(* ------------------------------------------------------------------------ *)

(* Instructions. *)

(* The language is imperative: an instruction does not return a value. The
   reason for this design choice is that we do not want to allow values to
   appear in the syntax of terms during evaluation. Indeed, allowing this
   would imply that memory locations (pointers into the heap) can appear
   inside terms. Thus, there would exist direct pointers from a term under
   evaluation to the heap. In the present design, instead, a value must be
   stored in a stack cell, so every pointer into the heap must originate
   either in the heap or in a stack cell. This means that it is sufficient
   to scan the stack in order to find all roots. *)

(* Because the language is imperative, instead of [return] and [bind], we
   have [skip] and sequencing. *)

(* TODO may need sugar for n-ary sequences that are always terminated with
   a [skip] *)

(* The instruction [IAlloc lv n] allocates a block of size [n], fills it with
   unit values, and stores its address in the stack cell identified by [lv].
   Because every block is mutable, it is easy for the user to initialize the
   block via a series of store instructions. Separating allocation and
   initialization significantly simplifies the reasoning rule for allocation
   (and the tactics that accompany it). *)

(* The deallocation of a memory block is implicit. *)

(* Although the instruction [IConst dst v] mentions a value, which seems
   to contradict what has been written above, this value must in fact be
   a constant: that is, it cannot be a memory location. The reduction rule
   imposes this restriction. *)

(* Allocating a stack cell is done via the instruction [alloca x in i]. A new
   stack cell is allocated and its address is bound to the variable [x]. Thus,
   in one step, this instruction reduces to [alloca c in i], where [c] is the
   address of the new stack cell, and [alloca c in _] is an evaluation
   context, an active [alloca] instruction. The new stack cell exists as long
   as the instruction [i] executes; it is deallocated when [i] terminates. The
   address [c] still exists afterwards, but must no longer be dereferenced. *)

with instr :=

  (* Skip. *)
  | ISkip

  (* Sequencing. *)
  | ISeq (i1 i2 : instr)

  (* A non-zero test and conditional construct. *)
  | IIf (src : lvalue) (i1 i2 : instr)

  (* Procedure call. *)
  | ICall (callee : lvalue) (args : list lvalue)

  (* Allocation of a memory block. *)
  | IAlloc (dst : lvalue) (n : nat)

  (* Read access to a memory block. *)
  | ILoad (dst : lvalue) (src : lvalue) (ofs : nat)

  (* Write access to a memory block. *)
  | IStore (dst : lvalue) (ofs : nat) (src : lvalue)

  (* A location equality test. *)
  | ILocEq (dst src1 src2 : lvalue)

  (* Loading a constant into a stack cell. *)
  | IConst (dst : lvalue) (v : value)

  (* Moving a value from one stack cell to another. *)
  | IMove (dst src : lvalue)

  (* Allocating a stack cell. *)
  | IAlloca (x : binder) (i : instr)

  (* Reduction in the scope of an [alloca] instruction. *)
  | IAllocaActive (c : loc) (i : instr)

  (* Fork. *)
  | IFork (src : lvalue) (x : binder) (i : instr)

.

(* ------------------------------------------------------------------------ *)

(* A memory block describes the content of an object in the heap
   or of a cell in the stack.

   Indeed, we use a "store" that combines the heap and the stack
   in a single map of memory locations to memory blocks.

   A memory block is one of the following:
   - a (mutable) tuple in the heap, currently holding a list of values [vs];
   - a (mutable) cell in the stack, currently holding a value [v];
   - a deallocated block. *)

(* We could allow stack cells to have several fields, but that would make
   the syntax of lvalues more complex (it would need to include an offset)
   and it is not clear that we would again anything in terms of reasoning
   power. *)

Inductive block :=
  | BTuple (vs : list value)
  | BCell (v : value)
  | BDeallocated.

(* ------------------------------------------------------------------------ *)

(* Inhabitation facts. *)

Global Instance val_inhabited : Inhabited value :=
  populate (VUnit).

Global Instance block_inhabited : Inhabited block :=
  populate BDeallocated.

(* ------------------------------------------------------------------------ *)

(* Every instruction eventually reduces to [skip]. As far as Iris is
   concerned, the type of "values" is [unit], and the injection of "values"
   into instructions is the constant function that maps [tt] to [skip]. *)

Definition of_val (_ : unit) := ISkip.

(* The partial function [to_val] determines which instructions are "values",
   that is, have terminated. *)

Definition to_val (i : instr) : option unit :=
  match i with
  | ISkip => Some tt
  | _ => None
  end.

Lemma to_of_val v :
  to_val (of_val v) = Some v.
Proof.
  destruct v. reflexivity.
Qed.

Lemma of_to_val i v :
  to_val i = Some v ->
  of_val v = i.
Proof.
  destruct i, v; simpl; unfold of_val; congruence.
Qed.

Global Instance of_val_inj : Inj (=) (=) of_val.
Proof.
  intros [] [] _. reflexivity.
Qed.
