From spacelang Require Import proofmode notation rcproofmode.

Section examples_lists.

Context `{!heapG Σ}.

Fixpoint isList (l : loc) (vs : list value) : iProp Σ :=
  match vs with
  | [] => l ↦ BTuple [VInt (Int63.of_Z 0)]
  | v :: vs => ∃ l', l ↦ BTuple [VInt (Int63.of_Z 1); v; VLoc l'] ∗ l' ↤ℕ 1 ∗ isList l' vs
  end.

Fixpoint isListInt (l : loc) (vs : list Int63.int) : iProp Σ :=
  match vs with
  | [] => l ↦ BTuple [VInt (Int63.of_Z 0)]
  | v :: vs => ∃ l', l ↦ BTuple [VInt (Int63.of_Z 1); VInt v; VLoc l'] ∗ l' ↤ℕ 1 ∗ isListInt l' vs
  end.

(** List copy function which overwrites the [src] register, so that it
    is at liberty to deallocate the source list if [src] was the only
    root. *)

Definition list_copy : value :=
  (λ: [["self", "dst", "src"]],
    with: "tag": ILoad "tag" "src" 0;;
    if: "tag"
    then
      with: "head": ILoad "head" "src" 1;;
      with: "tail": ILoad "tail" "src" 2;;
      IConst "src" ();;
      with: "res":  ICall "self" [["self", "res", "tail"]];;
      "dst" <:- init [["tag", "head", "res"]]
      end end end
    else
      IConst "src" ();;
      "dst" <:- init [["tag"]]
    end)%V.

(** A first specification for [list_copy] that indeed create a new
    copy of the list if given a linear amount of space credits *)

Lemma list_copy_spec (callee dst src l : loc) (L : list Int63.int) (n : nat) :
  {{{
    callee ↦ BCell list_copy ∗
    dst ↦ BCell ()%V ∗
    src ↦ BCell (#l)%V ∗
    l ↤ℕ n ∗
    isListInt l L ∗
    ♢(2 + 4 * length L)
  }}}
    callee [[callee, dst, src]]%V
  {{{ (l' : loc), RET tt;
    callee ↦ BCell list_copy ∗
    dst ↦ BCell (#l')%V ∗
    src ↦ BCell ()%V ∗
    l' ↤ℕ 1 ∗
    l ↤ℕ (n - 1) ∗
    isListInt l L ∗
    isListInt l' L
  }}}.
Proof.
  iInduction L as [ | x L ] "IH" forall (n callee dst src l);
  iIntros (Φ) "(Hcallee & ? & ? & ? & Hl & ?) p".
  - wp_call; fold list_copy.
    wp_with.
    simpl.
    wp_load.
    wp_if.
    wp_const.
    rcwp_init.
    iModIntro.
    wp_end_with.
    iApply "p"; iFrame.
  - wp_call. fold list_copy.
    wp_with.
    iDestruct "Hl" as (l') "(?&?&Hl')".
    fold isListInt.
    wp_load.
    wp_if.
    wp_with.
    wp_load.
    wp_with.
    wp_load.
    wp_const.
    wp_with.
    simpl length.
    withdraw (2 + 4 * length L)%nat as "(dia & ?)".
    wp_bind (ICall _ _).
    iRename select (#l' ↤ℕ? _)%I into "pl'".
    iApply ("IH" $! _ _ res with "[$Hcallee $Hres $Htail $dia $Hl' $pl']").
    iNext; iIntros. destruct_stars.
    wp_skip.
    rcwp_init.
    iModIntro.
    wp_end_with.
    wp_end_with.
    wp_end_with.
    wp_end_with.
    iApply "p".
    iFrame select (dst ↦ _)%I.
    iFrame.
    rewrite bi.sep_exist_r.
    iExists l'. iFrame.
    iExists _.  iFrame.
Qed.

(* A second specification for [list_copy] that deallocates the source
   list as it builds the new list, if the source list has only one
   precedecessor [src] *)

Lemma list_copy_spec_free (callee dst src l : loc) (L : list Int63.int) :
  {{{
    callee ↦ BCell list_copy ∗
    dst ↦ BCell ()%V ∗
    src ↦ BCell (#l)%V ∗
    l ↤ℕ 1 ∗
    isListInt l L
  }}}
    callee [[callee, dst, src]]%V
  {{{ (l' : loc), RET tt;
    callee ↦ BCell list_copy ∗
    dst ↦ BCell (#l')%V ∗
    src ↦ BCell ()%V ∗
    l' ↤ℕ 1 ∗
    isListInt l' L
  }}}.
Proof.
  iInduction L as [ | x L ] "IH" forall (callee dst src l);
  iIntros (Φ) "(Hcallee & ? & ? & ? & Hl) p".
  - wp_call; fold list_copy.
    wp_with.
    simpl.
    wp_load.
    wp_if.
    wp_const.
    simpl (1 - 1)%nat. (* TODO wp_free_cleanup should signal it did not find [l↤ℕ0] *)
    wp_free_cleanup l.
    rcwp_init.
    iModIntro.
    wp_end_with.
    iApply "p"; iFrame.
  - wp_call. fold list_copy.
    wp_with.
    iDestruct "Hl" as (l') "(?&?&Hl')".
    fold isListInt.
    wp_load.
    wp_if.
    wp_with.
    wp_load.
    wp_with.
    wp_load.
    wp_const.
    wp_with.
    simpl length.
    simpl (1 - 1)%nat.
    wp_free_cleanup l.
    wp_bind (ICall _ _).
    iRename select (l' ↤ℕ _)%I into "pl'".
    iApply ("IH" $! _ res with "[$Hcallee $Hres $Htail $Hl' $pl']").
    iNext; iIntros. destruct_stars.
    wp_skip.
    rcwp_init.
    iModIntro.
    wp_end_with.
    wp_end_with.
    wp_end_with.
    wp_end_with.
    iApply "p".
    iFrame select (dst ↦ _)%I.
    iFrame.
    iExists _.  iFrame.
Qed.

Lemma list_copy_spec_combined (callee dst src l : loc) (L : list Int63.int) (n : nat) :
  {{{
    callee ↦ BCell list_copy ∗
    dst ↦ BCell ()%V ∗
    src ↦ BCell (#l)%V ∗
    l ↤ℕ n ∗
    isListInt l L ∗
    (if decide (n <= 1) then True else ♢(2 + 4 * length L))
  }}}
    callee [[callee, dst, src]]%V
  {{{ (l' : loc), RET tt;
    callee ↦ BCell list_copy ∗
    dst ↦ BCell (#l')%V ∗
    src ↦ BCell ()%V ∗
    l' ↤ℕ 1 ∗
    (if decide (n <= 1) then True else l ↤ℕ (n - 1) ∗ isListInt l L) ∗
    isListInt l' L
  }}}.
Proof.
  iInduction L as [ | x L ] "IH" forall (callee dst src l n);
  iIntros (Φ) "(Hcallee & ? & ? & ? & Hl & ?) p".
  - wp_call; fold list_copy.
    wp_with.
    simpl.
    wp_load.
    wp_if.
    wp_const.
    destruct (decide _).
    + replace (n - 1)%nat with O by lia.
      wp_free_cleanup l.
      rcwp_init.
      iModIntro.
      wp_end_with.
      iApply "p"; iFrame.
    + rcwp_init.
      iModIntro.
      wp_end_with.
      iApply "p"; iFrame.
  - wp_call. fold list_copy.
    wp_with.
    iDestruct "Hl" as (l') "(?&?&Hl')".
    fold isListInt.
    wp_load.
    wp_if.
    wp_with.
    wp_load.
    wp_with.
    wp_load.
    wp_const.
    wp_with.
    simpl length.
    destruct (decide _).
    + replace (n - 1)%nat with O by lia.
      wp_free_cleanup l.
      wp_bind (ICall _ _).
      iRename select (l' ↤ℕ _)%I into "pl'".
      iApply ("IH" $! _ res with "[$Hcallee $Hres $Htail $Hl' $pl']").
      iNext; iIntros. destruct_stars.
      wp_skip.
      rcwp_init.
      iModIntro.
      wp_end_with.
      wp_end_with.
      wp_end_with.
      wp_end_with.
      iApply "p".
      iFrame select (dst ↦ _)%I.
      iFrame.
      iExists _.  iFrame.
    + simpl length.
      withdraw (2 + 4 * length L)%nat as "(dia & ?)".
      wp_bind (ICall _ _).
      iRename select (#l' ↤ℕ? _)%I into "pl'".
      iApply ("IH" $! _ res _ _ 2%nat with "[$Hcallee $Hres $Htail $dia $Hl' $pl']").
      iNext; iIntros. destruct_stars.
      wp_skip.
      rcwp_init.
      iModIntro.
      wp_end_with.
      wp_end_with.
      wp_end_with.
      wp_end_with.
      iApply "p".
      iFrame select (dst ↦ _)%I.
      iFrame.
      simpl.
      rewrite bi.sep_exist_r.
      iExists l'. iFrame.
      iExists _.  iFrame.
Qed.

Lemma list_copy_spec_values (callee dst src l : loc) (L : list value) (n : nat) (vns : list (value * nat)) :
  (forall v, v ∈ L -> ∃ n, (v, n) ∈ vns) ->
  {{{
    callee ↦ BCell list_copy ∗
    dst ↦ BCell ()%V ∗
    src ↦ BCell (#l)%V ∗
    l ↤ℕ n ∗
    isList l L ∗
    (if decide (n <= 1) then True else ♢(2 + 4 * length L)) ∗
    ([∗ list] vn ∈ vns, vn.1 ↤ℕ? vn.2)
  }}}
    callee [[callee, dst, src]]%V
  {{{ (l' : loc), RET tt;
    callee ↦ BCell list_copy ∗
    dst ↦ BCell (#l')%V ∗
    src ↦ BCell ()%V ∗
    l' ↤ℕ 1 ∗
    (if decide (n <= 1) then True else l ↤ℕ (n - 1) ∗ isList l L) ∗
    isList l' L ∗
    (if decide (n <= 1)
     then [∗ list] vn ∈ vns, vn.1 ↤ℕ? vn.2
     else [∗ list] vn ∈ vns, vn.1 ↤ℕ? (vn.2 + val_occs vn.1 L))
  }}}.
Proof.
  iIntros (Hvns).
  iInduction L as [ | v L ] "IH" forall (callee dst src l n vns Hvns);
  iIntros (Φ) "(Hcallee & ? & ? & ? & Hl & ? & ps) p".
  - wp_call; fold list_copy.
    wp_with.
    simpl.
    wp_load.
    wp_if.
    wp_const.
    destruct (decide _).
    + replace (n - 1)%nat with O by lia.
      wp_free_cleanup l.
      rcwp_init.
      iModIntro.
      wp_end_with.
      iApply "p"; iFrame.
    + rcwp_init.
      iModIntro.
      wp_end_with.
      iApply "p"; iFrame.
      iApply (big_sepL_wand with "[$]").
      iApply big_sepL_intro.
      iIntros (i [v ?]) "!> % H".
      iExactEq = "H". iPureIntro. simpl.
      f_equal. destruct v; simpl; lia.
  - wp_call. fold list_copy.
    wp_with.
    iDestruct "Hl" as (l') "(?&?&Hl')".
    fold isListInt.
    wp_load.
    wp_if.
    wp_with.
    destruct (Hvns v ltac:(left)) as (nv, Hnv).
    destruct (elem_of_list_split _ _ Hnv) as (vns1 & vns2 & ->).
    iDestruct (big_sepL_app with "ps") as "(ps1 & ps2)".
    iDestruct (big_sepL_cons with "ps2") as "(pv & ps2)". simpl fst; simpl snd.
    wp_load.
    wp_with.
    wp_load.
    wp_const.
    wp_with.
    simpl length.
    destruct (decide _).
    + replace (n - 1)%nat with O by lia.
      wp_free_cleanup l.
      wp_bind (ICall _ _).
      iRename select (l' ↤ℕ _)%I into "pl'".
      unshelve iApply ("IH" $! _ res _ _ _ (vns1 ++ [(v, nv)] ++ vns2) _
                         with "[$Hcallee $Hres $Htail $Hl' $pl' ps1 ps2 pv]").
      { intros ? ?; apply Hvns; auto. now right. }
      { iSplitR; auto. rewrite ->2big_sepL_app. iFrame.
        replace (nv + 1 - 1)%nat with nv by lia. iFrame. auto. }
      iNext; iIntros (l'0) "(? & ? & ? & ? & ? & ? & ps)".
      wp_skip.
      simpl (if decide _ then _ else _).
      iDestruct (big_sepL_app with "ps") as "(ps1 & ps2)".
      iDestruct (big_sepL_cons with "ps2") as "(pv & ps2)". simpl fst; simpl snd.
      rcwp_init.
      iModIntro.
      wp_end_with.
      wp_end_with.
      wp_end_with.
      wp_end_with.
      iApply "p".
      iFrame select (dst ↦ _)%I.
      replace (nv + 1 - 1)%nat with nv by lia.
      iFrame.
      iExists _.  iFrame.
    + simpl length.
      withdraw (2 + 4 * length L)%nat as "(dia & ?)".
      wp_bind (ICall _ _).
      iRename select (#l' ↤ℕ? _)%I into "pl'".
      unshelve iApply ("IH" $! _ res _ _ 2%nat (vns1 ++ [(v, nv + 1)%nat] ++ vns2) _
                with "[$Hcallee $Hres $Htail $dia $Hl' $pl' ps1 ps2 pv]").
      { intros w Hw. destruct (Hvns w) as [nw H]. now right.
        apply elem_of_app in H. destruct H as [H | H].
        + eexists. apply elem_of_app; eauto.
        + inversion H; subst.
          * eexists. apply elem_of_app; right. left.
          * eexists. apply elem_of_app; right. right. eauto.
      }
      { rewrite ->2big_sepL_app. iFrame. auto. }
      iNext; iIntros (l'0) "(? & ? & ? & ? & ? & ? & ps)".
      wp_skip.
      simpl (if decide _ then _ else _).
      iDestruct (big_sepL_app with "ps") as "(ps1 & ps2)".
      iDestruct (big_sepL_cons with "ps2") as "(pv & ps2)". simpl fst; simpl snd.
      rcwp_init.
      iModIntro.
      wp_end_with.
      wp_end_with.
      wp_end_with.
      wp_end_with.
      iApply "p".
      iFrame select (dst ↦ _)%I.
      iFrame.
      simpl.
      rewrite bi.sep_exist_r.
      iExists l'. iFrame.
      rewrite bi.sep_exist_r.
      iExists _.  iFrame.
      rewrite big_sepL_app big_sepL_cons. iFrame.
      iSplitL "ps1". {
        iApply (big_sepL_wand with "[$]").
        iApply big_sepL_intro.
        iIntros (i [? ?]) "!> % H".
        iApply (vmapsfrom_nat_weaken with "[$]").
        apply plus_le_compat_l, val_occs_cons_maybe.
      }
      iSplitL "pv".
      { iExactEq = "pv". iPureIntro. rewrite val_occs_cons_eq. f_equal. simpl. lia. }
      {
        iApply (big_sepL_wand with "[$]").
        iApply big_sepL_intro.
        iIntros (i [? ?]) "!> % H".
        iApply (vmapsfrom_nat_weaken with "[$]").
        apply plus_le_compat_l, val_occs_cons_maybe.
      }
Qed.

(* [list_copy] is not tail-recursive, so here is a tentative
   tail-recursive version, which requires two definitions *)

Definition copy_2_aux : value :=
  (* [copy_2_aux copy_2_aux dst src] copies the list pointed by
     register [src] into the third element of the tuple pointed by
     [dst]. It is tail-recursive in that the "recursive" call is only
     followed by the end of local allocations. *)
  (λ: [["self", "dst", "src"]],
    with: "tag": ILoad "tag" "src" 0;;
    if: "tag"
    then
      with: "head": ILoad "head" "src" 1;;
      with: "tail": ILoad "tail" "src" 2;;
      IConst "src" ();;
      with: "cell": IAlloc "cell" 3;;
      IStore "dst" 2 "cell";;
      IStore "cell" 0 "tag";;
      IStore "cell" 1 "head";;
      ICall "self" [["self", "cell", "tail"]]
      end end end
    else
      with: "cell": IAlloc "cell" 1;;
      IStore "cell" 0 "tag";;
      IStore "dst" 2 "cell"
      end
    end)%V.

Definition copy_2 : value :=
  (* [copy_2 dst src] copies the list pointed by register [src] and
     stores the location of the copy in register [dst] *)
  (λ: [["dst", "src"]],
    with: "copy_2_aux": IConst "copy_2_aux" copy_2_aux;;
    with: "tag": ILoad "tag" "src" 0;;
    if: "tag"
    then
      with: "v": ILoad "v" "src" 1;;
      with: "tail": ILoad "tail" "src" 2;;
      IConst "src" ();;
      IAlloc "dst" 3;;
      IStore "dst" 0 "tag";;
      IStore "dst" 1 "v";;
      ICall "copy_2_aux" [["copy_2_aux", "dst", "tail"]]
      end end
    else
      IAlloc "dst" 1;;
      IStore "dst" 0 "tag"
      end
    end)%V.

Lemma copy_2_spec (callee dst src l : loc) (L : list value) :
  {{{
    callee ↦ BCell copy_2 ∗
    dst ↦ BCell ()%V ∗
    src ↦ BCell (#l)%V ∗
    isList l L ∗
    ♢4
  }}}
    callee [[dst, src]]%V
  {{{ l', RET tt;
    callee ↦ BCell copy_2 ∗
    dst ↦ BCell (#l')%V ∗
    src ↦ BCell (#l)%V ∗
    isList l' L
  }}}.
Proof.
Abort.

Definition rev_acc : value :=
  (λ: [["self", "dst", "acc", "src"]],
    with: "tag": ILoad "tag" "src" 0;;
    if: "tag"
    then
      with: "head": ILoad "head" "src" 1;;
      with: "tail": ILoad "tail" "src" 2;;
      IConst "src" ();;
      with: "cell": "cell" <:- init [["tag", "head", "acc"]];;
      IConst "acc" ();;
      ICall "self" [["self", "dst", "cell", "tail"]]
      end end end
    else
      IMove "dst" "acc";;
      IConst "src" ();;
      IConst "acc" ()
    end)%V.

Lemma rev_acc_spec (callee dst acc src a l : loc) (A L : list Int63.int) (n : nat) :
  {{{
    callee ↦ BCell rev_acc ∗
    dst ↦ BCell ()%V ∗
    acc ↦ BCell (#a)%V ∗
    src ↦ BCell (#l)%V ∗
    a ↤ℕ 1 ∗
    l ↤ℕ n ∗
    isListInt a A ∗
    isListInt l L ∗
    (if decide (n <= 1) then True else ♢(2 + 4 * length L))
  }}}
    callee [[callee, dst, acc, src]]%V
  {{{ (l' : loc), RET tt;
    callee ↦ BCell rev_acc ∗
    dst ↦ BCell (#l')%V ∗
    acc ↦ BCell ()%V ∗
    src ↦ BCell ()%V ∗
    l' ↤ℕ 1 ∗
    isListInt l' (rev L ++ A) ∗
    (if decide (n <= 1) then True else l ↤ℕ (n - 1) ∗ isListInt l L)
  }}}.
Proof.
  iInduction L as [ | x L ] "IH" forall (n callee dst acc src a l A);
  iIntros (Φ) "(Hcallee & Hdst & ? & ? & pa & ? & Ha & Hl & dia) p".
  - wp_call; fold rev_acc.
    wp_with.
    simpl.
    wp_load.
    wp_if.
    wp_move.
    wp_const.
    destruct (decide _).
    + replace (n - 1)%nat with O by lia.
      wp_free_cleanup l.
      wp_const.
      wp_end_with.
      iApply "p"; iFrame.
    + wp_const.
      wp_end_with.
      iApply "p"; iFrame.
  - wp_call. fold rev_acc.
    wp_with.
    iDestruct "Hl" as (l') "(Hl & pl' & Hl')".
    fold isListInt.
    wp_load.
    wp_if.
    wp_with.
    wp_load.
    wp_with.
    wp_load.
    wp_const.
    wp_with.
    destruct (decide _) as [H | H].
    + replace (n - 1)%nat with O by lia.
      wp_free_cleanup l.
      rcwp_init l0 as "(Hl0 & pl0 & Hcell & ? & dia')".
      wp_skip.
      wp_const.
      iApply ("IH" $! _ _ _ _ _ _ _ (x :: A) with
                  "[$Hcallee $Hdst $Hcell $Htail $pl0 $pl' Ha $Hl' Hl0 pa]").
      { simpl. iExists _; iFrame. }
      iNext; iIntros. destruct_stars.
      wp_end_with.
      wp_end_with.
      wp_end_with.
      wp_end_with.
      iApply "p".
      iFrame select (dst ↦ _)%I.
      simpl rev. rewrite <-app_assoc.
      iFrame.
    + rcwp_init l0 as "(Hl0 & pl0 & Hcell & ? & dia)".
      wp_skip.
      wp_const.
      iApply ("IH" $! _ _ _ _ _ _ _ (x :: A) with
                  "[$Hcallee $Hdst $Hcell $Htail $pl0 $pl' Ha $Hl' dia Hl0 pa]").
      { simpl.
        rewrite bi.sep_exist_r.
        iExists _; iFrame.
        iExactEq = "dia". iPureIntro. f_equal. lia. }
      iNext; iIntros. destruct_stars.
      wp_end_with.
      wp_end_with.
      wp_end_with.
      wp_end_with.
      iApply "p".
      iFrame select (dst ↦ _)%I.
      simpl rev. rewrite <-app_assoc.
      iFrame.
      iExists _.  iFrame.
Qed.

End examples_lists.
