From iris.base_logic.lib Require Import gen_heap.
From spacelang Require Import wp_state_interp.
Open Scope nat_scope.

(* This module establishes some implications (or updates) that have to do
   with [mapsfrom] assertions. *)

(* These are just re-packaged versions of lemmas in ph.v. *)

(* ------------------------------------------------------------------------ *)

(* Exploiting the conjunction of a fractional [mapsto] assertion and a full
   [mapsfrom] assertion. From the fact that there is a forward edge from [l]
   to [l'], we deduce that [l] must appear at least as many times in [ls] as
   [l'] does in [b]. *)

Lemma supd_exploit_mapsto_mapfrom_multiplicity `{!heapG Σ} E l q b l' ls :
  l ↦{q} b ∗ l' ↤{1%Qp} ls
  ={{E}}=∗
  l ↦{q} b ∗ l' ↤{1%Qp} ls ∗
  ⌜ multiplicity l' (pointers b) <= multiplicity l ls ⌝.
Proof.
  unfold supd.
  iIntros (σ nt κs n) "(Hstate & Hmapsto & Hmapsfrom)".
  destruct_state_interp.
  iDestruct (exploit_mapsto_mapsfrom_multiplicity
    with "Hph Hmapsto Hmapsfrom") as "%goal".
  iModIntro.
  iFrame "Hmapsto Hmapsfrom".
  iSplitL; [| iPureIntro; eauto ].
  intro_state_interp. iExists _. iFrame. eauto.
Qed.

(* A special case where multiplicity does not matter. *)

Lemma supd_exploit_mapsto_mapsfrom `{!heapG Σ} E l q b l' ls :
  l ↦{q} b ∗ l' ↤{1%Qp} ls
  ={{E}}=∗
  l ↦{q} b ∗ l' ↤{1%Qp} ls ∗
  ⌜ l' ∈ pointers b → l ∈ ls ⌝.
Proof.
  unfold supd.
  iIntros (σ nt κs n) "(Hstate & Hmapsto & Hmapsfrom)".
  destruct_state_interp.
  iDestruct (exploit_mapsto_mapsfrom
    with "Hph Hmapsto Hmapsfrom") as "%goal".
  iModIntro.
  iFrame "Hmapsto Hmapsfrom".
  iSplitL; [| iPureIntro; eauto ].
  intro_state_interp. iExists _. iFrame. eauto.
Qed.

(* ------------------------------------------------------------------------ *)

(* Confronting a [mapsfrom] assertion and a deallocation witness. *)

Lemma supd_exploit_mapsfrom_dag `{!heapG Σ} E l' q ls :
  l' ↤{q} ls ∗ †l' ={{E}}=∗ False.
Proof.
  unfold supd.
  iIntros (σ nt κs n) "(Hstate & Hmapsfrom & Hdag)".
  destruct_state_interp.
  iDestruct (exploit_mapsfrom_dag with "[$Hph $Hmapsfrom $Hdag]") as "Hfalse".
  eauto.
Qed.
