From spacelang Require Import proofmode notation rcproofmode.


(* Simple macro for storing a value in the heap, using a temporary
   register to hold said value *)

Definition Store_const dst ofs v : instr :=
  (with: "_tmp" :
    IConst "_tmp" v;;
    IStore dst ofs "_tmp"
  end)%E.

Ltac wp_store_const := wp_with; wp_const; wp_store; wp_end_with.



Section stack_example.

Context `{!heapG Σ}.

(* null is hard to load with a IConst (or anything, really): nothing
   in the syntax allows us to put some address that is not already
   reachable into a cell. Instead we use tags: nil is the tuple [(0)]
   and a cons is [(1,v,l)] for some value [v] and location [l]. *)

Fixpoint isList (l : loc) vs : iProp Σ :=
  match vs with
  | [] => l ↦ BTuple [VInt (Int63.of_Z 0)]
  | v :: vs => ∃ l', l ↦ BTuple [VInt (Int63.of_Z 1); v; VLoc l'] ∗ l' ↤ℕ 1 ∗ isList l' vs
  end.

Definition isStack (l : loc) (vs : list value) : iProp Σ :=
  ∃l' : loc, l ↦ BTuple [(#l')%V] ∗ l' ↤ℕ 1 ∗ isList l' vs.

Definition new_stack : value :=
  (λ: [["dst"]],
    IAlloc "dst" 1;;
    with: "newcell":
      IAlloc "newcell" 1;;
      Store_const "newcell" 0 (VInt (Int63.of_Z 0));;
      IStore "dst" 0 "newcell"
    end)%V.

Lemma new_stack_spec (dst callee : loc) :
  {{{ dst ↦ BCell VUnit ∗ ♢4 ∗ callee ↦ BCell new_stack }}}
    callee [[dst]]%V
  {{{ l, RET tt; dst ↦ BCell (VLoc l) ∗ isStack l [] ∗ l ↤ℕ 1 ∗ callee ↦ BCell new_stack }}}.
Proof.
  iIntros (Φ) "? post".
  destruct_stars.
  wp_call.
  rcwp_init.
  wp_with.
  wp_alloc.
  wp_store_const.
  wp_skip.
  wp_store.
  wp_end_with.
  iApply "post".
  iFrame select (dst ↦ _)%I. (* [iFrame] otherwise select the wrong [?x ↤ℕ 1] *)
  iFrame.
  iExists _; iFrame.
Qed.

Definition stack_push : value :=
  (λ: [["rstack", "rval"]],
    with: "newcell":
      IAlloc "newcell" 3;;
      Store_const "newcell" 0 (VInt (Int63.of_Z 1));;
      IStore "newcell" 1 "rval";;
      with: "oldcell":
        ILoad "oldcell" "rstack" 0;;
        IStore "newcell" 2 "oldcell"
      end;;
      IStore "rstack" 0 "newcell"
    end)%V.

Lemma stack_push_spec (callee rl l rv : loc) (v : value) (vs : list value) qv nv :
  {{{
    callee ↦ BCell stack_push ∗
    rl ↦ BCell (VLoc l) ∗
    rv ↦ BCell v ∗
    v ↤ℕ?{qv} nv ∗
    isStack l vs ∗
    ♢4
  }}}
    callee [[rl, rv]]%V
  {{{ RET tt;
    callee ↦ BCell stack_push ∗
    rl ↦ BCell (VLoc l) ∗
    rv ↦ BCell v ∗
    v ↤ℕ?{qv} (nv + 1) ∗
    isStack l (v :: vs)
  }}}.
Proof.
  iIntros (Φ) "? post".
  destruct_stars.
  wp_call. fold stack_push.
  wp_with.
  wp_alloc.
  wp_store_const.
  wp_skip.
  wp_store.
  wp_with.
  iDestruct select (isStack _ _) as (l') "(?&?&?)".
  wp_load.
  wp_store.
  wp_end_with.
  wp_skip.
  wp_store.
  wp_end_with.
  iApply "post".
  iFrame.
  iExists _; iFrame.
  iExists _; iFrame.
Qed.

Definition stack_is_empty : value :=
  (λ: [["dst", "rstack"]],
    with: "firstcell":
      with: "tag":
        ILoad "firstcell" "rstack" 0;;
        ILoad "tag" "firstcell" 0;;
        if: "tag"
        then IConst "dst" (VInt (Int63.of_Z 0))
        else IConst "dst" (VInt (Int63.of_Z 1))
       end
     end)%V.

Lemma stack_is_empty_spec (callee rl l dst : loc) (vs : list value) :
  {{{
    callee ↦ BCell stack_is_empty ∗
    rl ↦ BCell (VLoc l) ∗
    dst ↦ BCell ()%V ∗
    isStack l vs
  }}}
    callee [[dst, rl]]%V
  {{{ RET tt;
    callee ↦ BCell stack_is_empty ∗
    rl ↦ BCell (VLoc l) ∗
    dst ↦ BCell (VInt (Int63.of_Z (if vs then 1 else 0))) ∗
    isStack l vs
  }}}.
Proof.
  iIntros (Φ) "? post".
  destruct_stars.
  wp_call.
  wp_with.
  wp_with.
  iDestruct select (isStack _ _) as (l') "(?&?&?)".
  wp_load.
  destruct vs as [ | v vs ].
  - (* nil *)
    simpl.
    wp_load.
    wp_if.
    wp_const.
    wp_end_with.
    wp_end_with.
    iApply "post"; iFrame.
    iExists _; iFrame.

  - (* cons *)
    iDestruct select (isList _ _) as (c) "(?&?&?)".
    fold isList.
    wp_load.
    wp_if.
    wp_const.
    wp_end_with.
    wp_end_with.
    iApply "post"; iFrame.
    iExists _; iFrame.
    iExists _; iFrame.
Qed.

Definition stack_pop : value :=
  (λ: [["dst", "rstack"]],
    with: "firstcell":
      ILoad "firstcell" "rstack" 0;;
      ILoad "dst" "firstcell" 1;;
      with: "nextcell":
        ILoad "nextcell" "firstcell" 2;;
        IStore "rstack" 0 "nextcell"
      end
     end;;
   skip)%V.

Lemma stack_pop_spec (callee rl l dst : loc) v (vs : list value) nv :
  {{{
    callee ↦ BCell stack_pop ∗
    rl ↦ BCell (VLoc l) ∗
    dst ↦ BCell ()%V ∗
    v ↤ℕ? nv ∗
    isStack l (v :: vs)
  }}}
    callee [[dst, rl]]%V
  {{{ RET tt;
    callee ↦ BCell stack_pop ∗
    rl ↦ BCell (VLoc l) ∗
    dst ↦ BCell v ∗
    v ↤ℕ? nv ∗
    isStack l vs ∗
    ♢4
  }}}.
Proof.
  iIntros (Φ) "? post".
  destruct_stars.
  wp_call. fold stack_pop.
  wp_with.
  iDestruct select (isStack _ _) as (l') "(?&?&?)".
  wp_load.
  iDestruct select (isList _ _) as (l'') "(?&?&?)".
  fold isList.
  wp_load.
  wp_with.
  wp_load.
  wp_store.
  wp_end_with.
  wp_end_with.
  simpl.
  wp_free_cleanup l'.
  wp_skip.
  wp_skip.
  iModIntro.
  iApply "post"; iFrame.
  cancel_mapsfrom_nat.
  iExists _; iFrame.
Qed.

(* We free the stack with the free-and-cleanup-singleton rule. This is
   possible since the relation ↤ terminates. With any circularity with
   at least two locations, the singleton rule would not be enough.

   Another inconvenient of the singleton rule is that it is hard to
   reason about (e.g. do inductions) without exposing the ={{E}}=>
   update, for which not much automation is available. *)

Lemma supd_free_list E (l : loc) (L : list value) (vns : list (value * nat)) :
  l ↤ℕ 0 ∗ isList l L ∗ ([∗ list] vn ∈ vns, vn.1 ↤ℕ? vn.2)
  ={{E}}=∗
  ♢(2 + 4 * length L) ∗ ([∗ list] vn ∈ vns, vn.1 ↤ℕ? (vn.2 - val_occs vn.1 L)).
Proof.
  iIntros (σ ns κs k) "(Hstate & pl & Hl & ps)".
  (* This is an example where, unsuprisingly, using
      [[* list] vn ∈ vns, vn.1 ... vn.2]
    is easier than using
      [[* list] v; n ∈ vs; ns, v ... n]
    so in the future we'll keep the lemma in the first style also in rc.v *)
  iInduction L as [ | v L ] "IH" forall (l vns) "pl ps Hl".
  - simpl isList.
    iMod (supd_free_cleanup_singleton_rc_values
            with "[$Hstate $Hl $pl $ps]")
      as "(Hstate & dia & ps)".
    iModIntro. iFrame.
    (* boring iterated wand reasoning *)
    iApply (big_sepL_wand with "ps").
    iApply big_sepL_intro.
    iIntros (i [v n]) "!> %".
    iApply vmapsfrom_nat_weaken.
    destruct v; simpl; lia.
  - simpl isList.
    iDestruct "Hl" as (l') "(Hl & pl' & Hl')".
    iMod (supd_free_cleanup_singleton_rc_values _ _ _ ((#l', 1%nat) :: vns)%V
            with "[$Hstate $Hl $pl $pl' $ps]")
      as "(Hstate & dia & pl' & ps)".
    pose (vns' := fmap (fun vn => (vn.1, vn.2 - val_occs vn.1 [v])%nat) vns).
    iAssert ([∗ list] vn ∈ vns', vn.1 ↤ℕ? vn.2)%I with "[ps]" as "ps".
    {
      iApply big_sepL_fmap.
      iApply (big_sepL_wand with "ps").
      iApply big_sepL_intro.
      iClear "IH".
      iIntros "!>" (i [vi ni] Ei).
      simpl.
      iApply vmapsfrom_nat_weaken.
      rewrite val_occs_cons.
      rewrite (val_occs_cons v).
      lia.
    }
    simplifies_vmapfrom_nat_val_occs. simpl minus.
    iMod ("IH" $! l' vns' with "Hstate pl' ps Hl'") as
        "(Hstate & dia' & ps)".
    iModIntro. iFrame.
    diamonds.
    iSplitL "dia".
    { iExactEq = "dia". iPureIntro. f_equal. lia. }
    subst vns'.
    rewrite big_sepL_fmap.
    iApply (big_sepL_wand with "ps").
    iApply big_sepL_intro.
    iClear "IH".
    iIntros "!>" (i [vi ni] Ei).
    simpl.
    iApply vmapsfrom_nat_weaken.
    rewrite (val_occs_cons v _ L).
    lia.
Qed.

(* Note that [val_occs] was originally used to count the occurrences
   of values in one tuple stored in the heap, in order to state the
   free+cleanup rule. We can also use it here to count occurrences of
   values in the list of values [L] (which is stored accross [length
   L] tuples) *)

Lemma free_stack {E} (l : loc) (L : list value) (vns : list (value * nat)) :
    l ↤ℕ 0 ∗
    isStack l L ∗
    ([∗ list] vn ∈ vns, vn.1 ↤ℕ? vn.2)
  ={{E}}=∗
    ♢(4 + 4 * length L) ∗
    ([∗ list] vn ∈ vns, vn.1 ↤ℕ? (vn.2 - val_occs vn.1 L)).
Proof.
  iIntros (σ ns κs k) "(Hstate & pl & Hl & ps)".
  iDestruct "Hl"  as (l') "(Hl & pl' & Hl')".
  iMod (supd_free_cleanup_singleton_rc_values _ _ _ [(#l', 1%nat)%V]
    with "[$Hstate $Hl $pl $pl' //]")
    as "(Hstate & ? & pl' & _)".
  simpl (_ ↤ℕ? _)%I. rewrite Z.eqb_refl.
  iMod (supd_free_list
    with "[$Hstate $pl' $Hl' $ps]")
    as "(Hstate & ? & ps)".
  diamonds.
  rewrite plus_comm.
  by iFrame.
Qed.

Ltac make_vmapsfrom_nat v :=
  lazymatch goal with
  | |- envs_entails ?Δ _ =>
  fetch_or_construct_vmapsfrom_nat Δ v ltac:(fun _ => idtac)
  end.

Example using_stack (new pop push ret : loc) :
  {{{
       new ↦ BCell new_stack ∗
       pop ↦ BCell stack_pop ∗
       push ↦ BCell stack_push ∗
       ret ↦ BCell VUnit ∗
       ♢20
  }}}
    (with: "s":
      with: "r":
        new [["s"]];;
        IConst "r" (VInt (Int63.of_Z 1));; push [["s", "r"]];;
        IConst "r" (VInt (Int63.of_Z 2));; push [["s", "r"]];;
        IConst "r" (VInt (Int63.of_Z 3));; push [["s", "r"]];;
        IConst "r" (VInt (Int63.of_Z 4));; push [["s", "r"]];;
        pop [[ret, "s"]];;
        IConst ret ()%V;;
        pop [[ret, "s"]]
      end
    end;;
    skip)%V
  {{{ RET tt;
       new ↦ BCell new_stack ∗
       pop ↦ BCell stack_pop ∗
       push ↦ BCell stack_push ∗
       ret ↦ BCell (VInt (Int63.of_Z 3)) ∗
       ♢20
  }}}.
Proof.
  iIntros (Φ) "? p". destruct_stars.
  wp_with.
  wp_with.
  withdraw 4.

  wp_bind (ICall _ _).
  iApply (new_stack_spec s with "[$]").
  iNext; iIntros (l) "?"; destruct_stars.

  wp_skip.
  wp_const.

  (* convertible to the same with any value *)
  make_vmapsfrom_nat VUnit.

  withdraw 4.
  wp_bind (ICall _ _).
  iApply (stack_push_spec _ s l r with "[$]").
  iNext; iIntros "?"; destruct_stars.

  wp_skip.
  wp_const. (* but wp_const remove the vmapsfrom *)

  withdraw 4.
  wp_bind (ICall _ _).
  make_vmapsfrom_nat VUnit.
  iApply (stack_push_spec _ s l r with "[$]").
  iNext; iIntros "?"; destruct_stars.

  wp_skip.
  wp_const.

  withdraw 4.
  wp_bind (ICall _ _).
  make_vmapsfrom_nat VUnit.
  iApply (stack_push_spec _ s l r with "[$]").
  iNext; iIntros "?"; destruct_stars.

  wp_skip.
  wp_const.

  withdraw 4.
  wp_bind (ICall _ _).
  make_vmapsfrom_nat VUnit.
  iApply (stack_push_spec _ s l r with "[$]").
  iNext; iIntros "?"; destruct_stars.

  wp_skip.
  wp_bind (ICall _ _).
  make_vmapsfrom_nat VUnit.
  iApply (stack_pop_spec with "[$]").
  iNext; iIntros "?"; destruct_stars.

  wp_skip.
  wp_const.

  wp_bind (ICall _ _).
  make_vmapsfrom_nat VUnit.
  iApply (stack_pop_spec with "[$]").
  iNext; iIntros "?"; destruct_stars.

  wp_end_with.
  wp_end_with.

  iAssert ([∗ list] vn ∈ [], vn.1↤ℕ?vn.2)%I as "ps". auto.
  iRename select (_ ↤ℕ? _)%I into "pl".
  iRename select (isStack _ _)%I into "Hl".
  unshelve iApply (wp_supd _ _ _ _ _ _ _ (free_stack _ _ _) with "[-Hl pl ps] [$]"); [easy | ..].
  iIntros "?"; destruct_stars.
  diamonds.
  wp_skip.
  wp_skip.
  iModIntro.
  iApply "p".
  iFrame.
Qed.

End stack_example.
