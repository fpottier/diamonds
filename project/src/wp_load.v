From spacelang Require Import wp_state_interp wp_memory.

(* This module establishes a reasoning rule for read accesses to the heap. *)

(* ------------------------------------------------------------------------ *)

(* Reading a block in the heap. *)

(* Fractional ownership of the block is required. *)

(* Full ownership of the source and destination stack cells is required. *)

(* Two [vmapsfrom] assertions are required in order to keep track of
   the predecessor edges that are potentially destroyed and created. *)

Lemma wp_load_dep `{!heapG Σ} (precise:bool) s E l dq vs src dst ofs v w qv qw lsv lsw :
  (0 <= ofs < length vs)%nat →
  v = vs !!! ofs →
  {{{
    ▷ src ↦ BCell (VLoc l) ∗
    ▷ l ↦{dq} BTuple vs ∗
    ▷ dst ↦ BCell w ∗
    ▷ v ↤?{qv} lsv ∗
    ▷ (if precise then w ↤?{qw} lsw else ⌜True⌝)
  }}}
    (ILoad (LLoc dst) (LLoc src) ofs)
    @ s; E
  {{{
    RET tt;
    src ↦ BCell (VLoc l) ∗
    l ↦{dq} BTuple vs ∗
    dst ↦ BCell v ∗
    v ↤?{qv} (lsv ⊎ {[+dst+]}) ∗
    (if precise then w ↤?{qw} (lsw ∖ {[+dst+]}) else ⌜True⌝)
  }}}.
Proof.
  intros ??. subst v. set (v := vs !!! ofs).
  iIntros (Φ) "(>Hsrc & >Hl & >Hdst & >Hv & Hw) HΦ".
  iApply wp_lift_atomic_head_step_no_fork; first done.
  iIntros (σ1 ns κ κs n) "Hstate". iModIntro. iSplit.

  (* First, prove that this instruction is reducible. *)
  { destruct_state_interp.
    exploit_stack_cell src.
    exploit_heap_cell l.
    exploit_stack_cell dst.
    head_reducible. }

  (* Then, examine an arbitrary reduction step. *)
  examine_step.
  inv_read_lvalue.
  exploit_stack_cell src. simplify_eq. rename l0 into l. (* TODO *)
  exploit_heap_cell l. simplify_eq. rename vs0 into vs. (* TODO *)
  exploit_stack_cell dst.
  (* Update the conceptual store by writing [v] into [dst]. *)
  iMod (ph_store_stack_dep precise (store θ) dst w v qw lsw qv lsv
    with "[$]")
      as "(Hph & Hdst & Hv & Hw)".
  { eauto. }
  (* Conclude. *)
  iModIntro. iSplit=> //.
  iSplitL "Hph Hauth"; [| iApply "HΦ"; iFrame ].
  (* Close the state interpretation invariant. *)
  intro_state_interp. iExists (<[dst:=BCell v]> <$> θ).
  erewrite available_update by eauto.
  iFrame "Hph Hauth".
  iPureIntro.
  splits; eauto 3 with valid_state closed related.
Qed.

Lemma wp_load `{!heapG Σ} s E l dq vs src dst ofs v w qv qw lsv lsw :
  (0 <= ofs < length vs)%nat →
  v = vs !!! ofs →
  {{{
    ▷ src ↦ BCell (VLoc l) ∗
    ▷ l ↦{dq} BTuple vs ∗
    ▷ dst ↦ BCell w ∗
    ▷ v ↤?{qv} lsv ∗
    ▷ w ↤?{qw} lsw
  }}}
    (ILoad (LLoc dst) (LLoc src) ofs)
    @ s; E
  {{{
    RET tt;
    src ↦ BCell (VLoc l) ∗
    l ↦{dq} BTuple vs ∗
    dst ↦ BCell v ∗
    v ↤?{qv} (lsv ⊎ {[+dst+]}) ∗
    w ↤?{qw} (lsw ∖ {[+dst+]})
  }}}.
Proof.
  apply (wp_load_dep true).
Qed.

Lemma wp_load_weak `{!heapG Σ} s E l dq vs src dst ofs v w qv lsv :
  (0 <= ofs < length vs)%nat →
  v = vs !!! ofs →
  {{{
    ▷ src ↦ BCell (VLoc l) ∗
    ▷ l ↦{dq} BTuple vs ∗
    ▷ dst ↦ BCell w ∗
    ▷ v ↤?{qv} lsv
  }}}
    (ILoad (LLoc dst) (LLoc src) ofs)
    @ s; E
  {{{
    RET tt;
    src ↦ BCell (VLoc l) ∗
    l ↦{dq} BTuple vs ∗
    dst ↦ BCell v ∗
    v ↤?{qv} (lsv ⊎ {[+dst+]})
  }}}.
Proof.
  intros ? ?. derive_wp_from (wp_load_dep false s E l dq vs src dst ofs v).
Qed.
