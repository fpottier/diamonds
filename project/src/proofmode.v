From iris.proofmode Require Export coq_tactics reduction.
From iris.proofmode Require Export proofmode.
From iris.program_logic Require Import atomic.
From iris Require Import spec_patterns intro_patterns.
From spacelang Require Export ph wp instances notation.

(* This is a heavily stripped-down version of HeapLang's proofmode support,
   copied and adapted from Tej Chajed's iris-simpl-lang demo. *)

(* ------------------------------------------------------------------------ *)

(* The following rewrite rules are used by [wp_store] and friends to try
   and simplify the multisets of predecessors that appear in the goal. *)

(* TODO What is a good set of simplification rules? *)

Lemma gmultiset_disj_union_l `{Countable A} (X : gmultiset A) :
  (∅ ⊎ X) = X.
Proof.
  multiset_solver.
Qed.

Lemma gmultiset_disj_union_r `{Countable A} (X : gmultiset A) :
  (X ⊎ ∅) = X.
Proof.
  multiset_solver.
Qed.

Lemma gmultiset_diff_l `{Countable A} (X : gmultiset A) :
  (∅ ∖ X) = ∅.
Proof.
  multiset_solver.
Qed.

Lemma gmultiset_diff_r `{Countable A} (X : gmultiset A) :
  (X ∖ ∅) = X.
Proof.
  multiset_solver.
Qed.

Lemma gmultiset_cancel_diff_l `{Countable A} (X Y : gmultiset A) :
  (X ⊎ Y) ∖ X = Y.
Proof.
  multiset_solver.
Qed.

Lemma gmultiset_cancel_diff_r `{Countable A} (X Y : gmultiset A) :
  (Y ⊎ X) ∖ X = Y.
Proof.
  multiset_solver.
Qed.

Hint Rewrite
  @gmultiset_disj_union_l
  @gmultiset_disj_union_r
  @gmultiset_diff_l
  @gmultiset_diff_r
  @gmultiset_cancel_diff_l
  @gmultiset_cancel_diff_r
  @gmultiset_difference_diag
: gmultiset_simplify.

Lemma diff_union_when_elem `{Countable A} (X Y Z : gmultiset A) :
  Y ⊆ X →
  (X ⊎ Z) ∖ Y = (X ∖ Y) ⊎ Z.
Proof.
  multiset_solver.
Qed.

Lemma diff_union_when_elem_singleton `{Countable A} (X Z : gmultiset A) y :
  y ∈ X →
  (X ⊎ Z) ∖ {[+y+]} = (X ∖ {[+y+]}) ⊎ Z.
Proof.
  intros. eapply diff_union_when_elem.
  rewrite gmultiset_singleton_subseteq_l.
  assumption.
Qed.

Hint Rewrite
  @diff_union_when_elem
  @diff_union_when_elem_singleton
  using solve [ eauto ]
: gmultiset_simplify.

(* ------------------------------------------------------------------------ *)

(* The following two tactic notations may be more appropriate in
   tactics.v but that would introduce an additional dependency *)

(* When your goal is not convertible is equal to an Iris hypothesis
   ["H" : hyp], use [iExactEq "H"], which will replace your [goal]
   with [⌜goal ≡ hyp⌝] *)
Tactic Notation "iExactEq" "≡" constr(irisHyp) :=
  match goal with
  | |- envs_entails ?Δ ?goal =>
    match Δ with
    | context[Esnoc _ (INamed irisHyp) ?hyp] =>
      iAssert ⌜goal ≡ hyp⌝%I as %->; [ | iApply irisHyp ]
    end
  end.

(* Same with [=] instead of [≡] *)
Tactic Notation "iExactEq" "=" constr(irisHyp) :=
  match goal with
  | |- envs_entails ?Δ ?goal =>
    match Δ with
    | context[Esnoc _ (INamed irisHyp) ?hyp] =>
      iAssert ⌜goal = hyp⌝%I as %->; [ | iApply irisHyp ]
    end
  end.

(* ------------------------------------------------------------------------ *)

(* The following lemma is used to look in the context for two assertions of
   the form [♢n1] and [♢n2] and replace them with [♢n], where [n] is provably
   equal to [n1+n2]. *)

(* At the moment, [n] is obtained by letting Coq simplify the sum [n1+n2].
   One could conceivably also allow Coq to prove the equality [n = n1+n2]
   by other means, e.g. by performing proof search. *)

Lemma tac_diamond_join `{heapG Σ} Δ1 Δ2 Δ3 i1 i2 n1 n2 n Q :
  envs_lookup_delete false i1 Δ1 = Some (false, ♢n1, Δ2)%I →
  envs_lookup i2 Δ2 = Some (false, ♢n2)%I →
  envs_simple_replace i2 false (Esnoc Enil i2 (♢n)) Δ2 = Some Δ3 →
  (n = n1 + n2)%nat →
  envs_entails Δ3 Q →
  envs_entails Δ1 Q.
Proof.
  rewrite envs_entails_eq.
  intros Hld1 Hl2 Hr2 ? H3Q.
  iIntros "HΔ1".
  iDestruct (envs_lookup_delete_sound _ _ _ _ _ _ Hld1
               with "HΔ1") as "(Hdiams1 & HΔ2)".
  iDestruct (envs_simple_replace_singleton_sound _ _ _ _ _ _ _ Hl2 Hr2
               with "HΔ2") as "(Hdiams2 & HΔ3)".
  rewrite (diamond_split n1 n2 n) //.
  iApply H3Q. iApply "HΔ3". iFrame.
Qed.

(* The tactic [diamonds1] performs one application of the above lemma. *)

Ltac diamonds1 :=
  iStartProof;
  eapply tac_diamond_join; [
    (* Find and delete [♢n1]. *)
    (iAssumptionCore || fail "diamonds: cannot find any diamonds")
  | (* Find [♢n2]. *)
    (iAssumptionCore || fail "diamonds: cannot find two separate diamonds")
  | (* Replace [♢n2] with [♢n], where [n] is still a metavariable. *)
    proofmode.reduction.pm_reflexivity
  | (* Simplify and solve the equality [?n = n1 + n2]. *)
    simpl; reflexivity
  | (* The main subgoal. *)
  ].

(* The tactic [clear_zero_diamonds] removes an assertion of the form [♢0],
   if there is one in the context. *)

Ltac clear_zero_diamonds :=
  lazymatch goal with
  | |- context[Esnoc _ ?h (♢0) ] =>
      iClear h
  end.

(* The tactic [intro_zero_diamonds] introduces an assertion of the form [♢0].
   The goal must be of the form [wp e Φ], where [e] is not a value. *)

Ltac intro_zero_diamonds :=
  lazymatch goal with
  | |- envs_entails _ (wp ?s ?E ?e ?Q) =>
    unshelve iApply (wp_supd s E e _ _ _ _ (diamond_0 _) with "[-] [$]");
    [ try done (* only [try], to show the problem in case of failure *)
    | iIntros "?" ]
  | _ => fail 1 "intro_zero_diamonds: goal should be a wp of a non-value"
  end.

(* The tactic [diamonds] looks for all assertions of the form [♢n] in the
   context and replaces them with a single such assertion. In other words,
   it gathers all of the diamonds in one place. *)

Ltac diamonds :=
  repeat diamonds1;
  try clear_zero_diamonds.

(* The tactic [withdraw m as "[Hm Hrem]"] looks for an assertion of
   the form [♢n] in the context, and splits into [♢m] and [♢(n-m)].
   The side condition [m <= n] must hold. The new assertions are
   named [Hm] and [Hremainder]. *)

Tactic Notation "withdraw" constr(m) "as" constr(Hpost) :=
  lazymatch goal with |- context[Esnoc _ ?h (♢?n)%I] =>
    let sc := fresh "side_condition" in
    assert (sc: (m <= n)%nat) by lia;
    (* We rewrite just one occurrence of [♢n], but we have no guarantee
       that it is the first occurrence. How could we rewrite in [h]?
       The tactic [iRewrite ... in h] does not seem to work. TODO *)
    rewrite {1}(diamond_split _ _ _ (le_plus_minus m n sc));
    iSimpl in h;
    iDestruct h as Hpost
  end.

Tactic Notation "withdraw" constr(m) :=
  let m := ltac:(eval compute in (Z.to_nat m)) in
  withdraw m as "(?&?)".

(* ------------------------------------------------------------------------ *)

(* This lemma is used to perform simplification inside the expression [e]
   that appears in the goal. [e] is replaced with an equivalent expression
   [e']. *)

Lemma tac_wp_expr_eval `{!heapG Σ} Δ s E Φ e e' :
  (∀ (e'':=e'), e = e'') →
  envs_entails Δ (WP e' @ s; E {{ Φ }}) →
  envs_entails Δ (WP e  @ s; E {{ Φ }}).
Proof. by intros ->. Qed.

(* This tactic applies the above lemma to simplify the expression [e] that
   appears in the goal. *)

Tactic Notation "wp_expr_eval" tactic3(t) :=
  iStartProof;
  lazymatch goal with
  | |- envs_entails _ (wp ?s ?E ?e ?Q) =>
      notypeclasses refine (tac_wp_expr_eval _ _ _ _ e _ _ _); [
        (* Here, the subgoal is [∀ (e'':=e'), e = e'']. The simplification
           tactic [t] is invoked in a context where the hypothesis [x] is of
           the form [e'' := ?e'] and the goal is [e = e'']. The tactic [t]
           can simplify [e] in the goal; then we eliminate [e''] and solve
           the goal by reflexivity, thus instantiating [e'] to the result of
           simplifying [e]. *)
        let x := fresh in intros x; t; unfold x; notypeclasses refine eq_refl
      | ]
  | _ =>
      fail "wp_expr_eval: goal is not a 'WP'"
  end.

(* This tactic uses [simpl] to simplify the expression [e] that appears in
   the goal. *)

Ltac wp_expr_simpl := wp_expr_eval simpl.

(* ------------------------------------------------------------------------ *)

(* The following two lemmas simplify the goal when it is a weakest
   precondition of a value. *)

Lemma tac_wp_value_nofupd `{!heapG Σ} Δ s E Φ :
  envs_entails Δ (Φ tt) → envs_entails Δ (WP ISkip @ s; E {{ Φ }}).
Proof. rewrite envs_entails_eq=> ->. by apply wp_value. Qed.

Lemma tac_wp_value `{!heapG Σ} Δ s E (Φ : unit → iPropI Σ) :
  envs_entails Δ (|={E}=> Φ tt) → envs_entails Δ (WP ISkip @ s; E {{ Φ }}).
Proof. rewrite envs_entails_eq=> ->. by rewrite wp_value_fupd. Qed.

(* The tactic [wp_value_head] simplifies the goal when it is a weakest
   precondition of a value. *)

(* If the postcondition already allows a view shift (a fancy update), do not
   add a second one. Otherwise, do add one. This ensures that the lemmas
   applied here are bidirectional and cannot make the goal unprovable. *)

Ltac wp_value_head :=
  lazymatch goal with
  | |- envs_entails _ (wp ?s ?E ISkip (λ _, fupd ?E _ _)) =>
      eapply tac_wp_value_nofupd
  | |- envs_entails _ (wp ?s ?E ISkip (λ _, wp _ ?E _ _)) =>
      eapply tac_wp_value_nofupd
  | |- envs_entails _ (wp ?s ?E ISkip _) =>
      eapply tac_wp_value
  end.

(* The tactic [wp_finish] simplifies the expression in the goal and
   applies [wp_value_head] if possible. *)

Ltac wp_finish :=
  wp_expr_simpl;      (* simplify occurrences of subst/fill *)
  try wp_value_head;  (* in case we have reached a value, get rid of the WP *)
  pm_prettify.        (* prettify ▷s caused by [MaybeIntoLaterNEnvs] and
                         λs caused by wp_value *)

(* Obsolete because no reduction is pure in spacelang
(* ------------------------------------------------------------------------ *)

(* This lemma states that if [e1] reduces to [e2] in a pure manner, then
   [e1] can be replaced in the goal with [e2], even if it appears under
   an evaluation context, and in the process, a few [later] combinators
   may be stripped off the hypotheses that appear in the environment. *)

Lemma tac_wp_pure `{!heapG Σ} Δ Δ' s E K e1 e2 φ n Φ :
  PureExec φ n e1 e2 →
  φ →
  MaybeIntoLaterNEnvs n Δ Δ' →
  envs_entails Δ' (WP (fill K e2) @ s; E {{ Φ }}) →
  envs_entails Δ  (WP (fill K e1) @ s; E {{ Φ }}).
Proof.
  rewrite envs_entails_eq=> ??? HΔ'. rewrite into_laterN_env_sound /=.
  (* We want [pure_exec_fill] to be available to type class search locally. *)
  pose proof @pure_exec_fill.
  rewrite HΔ' -lifting.wp_pure_step_later //.
Qed.

(* The tactic [wp_pure <pattern>] looks in the goal for a subexpression that
   appears under an evaluation context and that matches <pattern>. It then
   attempts to reduce this subexpression.

   For instance, [wp_pure (EIf _ _ _)] searches for a subexpression that
   matches [EIf _ _ _] and reduces it.

   The use of [open _constr] is essential. This converts all wildcards into
   evars, which later are unified when an occurrence is found. *)

(* The Ltac idiom [first [ tactic | fail 1 "message" ]] is preferable
   to [tactic || fail "message"]. The problem with disjunction is that
   it executes the second branch not only when the first branch fails,
   but also when the first branch does not make progress. For more
   information, please consult
   https://gitlab.mpi-sws.org/iris/iris/-/issues/216 *)

Tactic Notation "wp_pure" open_constr(efoc) :=
  iStartProof;
  lazymatch goal with
  | |- envs_entails _ (wp ?s ?E ?e ?Q) =>
      let e := eval simpl in e in
      first [
        reshape_expr e ltac:(fun K e' =>
          unify e' efoc;
          eapply (tac_wp_pure _ _ _ _ K e');
          [iSolveTC                       (* PureExec *)
          |try fast_done                  (* φ *)
          |iSolveTC                       (* IntoLaters *)
          |wp_finish                      (* new goal *)
          ])
        | fail 1 "wp_pure: cannot find" efoc "in" e "or" efoc "is not a redex"
      ]
  | _ =>
    fail "wp_pure: goal is not a 'WP'"
  end.

(* The tactic [wp_pures] applies [wp_pure _] as long as it is able to make
   progress (without creating multiple subgoals). It ends with [wp_finish],
   so if a value has been reached, the goal is simplified to an application
   of the postcondition. *)

Ltac wp_pures :=
  iStartProof;
  first [
    (* The `;[]` makes sure that no side condition appears. *)
    progress repeat (wp_pure _; [])
  | wp_finish (* In case wp_pure never ran, clean up. *)
  ].

(* Special cases of [wp_pure]. *)

Tactic Notation "wp_ret" := wp_pure (ERet (SEVal _)).
Tactic Notation "wp_let" := wp_pure (ELet (BNamed _) (EVal _) _).
Tactic Notation "wp_seq" := wp_pure (ELet BAnon      (EVal _) _).

(* ------------------------------------------------------------------------ *)

(* Unlike [wp_pures], the tactic [wp_code] should reduce a lambda
   abstraction that is hidden behind a Coq definition. To do this, we wish
   to use [AsVCode_vcode] as a normal instance, instead of a [Hint Extern].
   We achieve this by placing [AsVCode_vcode] in the environment, so that it
   can be used as an instance by the typeclass resolution system. We then
   perform reduction, and finally we remove this new hypothesis. *)

Tactic Notation "wp_code" :=
  let H := fresh in
  pose proof (H := AsVCode_vcode);
  wp_pure (ECall _ _);
  clear H.
*)

(* ------------------------------------------------------------------------ *)

(* The tactic [wp_bind <pattern>] looks in the goal for a subexpression that
   appears under an evaluation context and that matches <pattern>. It then
   applies the bind rule so as to reason about this subexpression first and
   reason about the evaluation context as a second step. *)

Lemma tac_wp_bind `{!heapG Σ} K Δ s E Φ e f :
  f = (λ e, fill K e) → (* eta expanded, so that we can `simpl` it *)
  envs_entails Δ (WP e @ s; E {{ _, WP f ISkip @ s; E {{ Φ }} }})%I →
  envs_entails Δ (WP fill K e @ s; E {{ Φ }}).
Proof. rewrite envs_entails_eq=> -> ->. by apply: wp_bind. Qed.

Ltac wp_bind_core K :=
  lazymatch eval hnf in K with
  | [] => idtac
  | _ => eapply (tac_wp_bind K); [simpl; reflexivity | reduction.pm_prettify]
  end.

Tactic Notation "wp_bind" open_constr(efoc) :=
  iStartProof;
  lazymatch goal with
  | |- envs_entails _ (wp ?s ?E ?e ?Q) =>
      first [
        reshape_expr e ltac:(fun K e' => unify e' efoc; wp_bind_core K)
      | fail 1 "wp_bind: cannot find" efoc "in" e
      ]
  | _ =>
      fail "wp_bind: goal is not a 'WP'"
  end.

(* ------------------------------------------------------------------------ *)

(* The tactic [wp_cleanup "Hmapsfrom" "Hdagger"] requires "Hmapsfrom"
   to be an assertion of the form [l' ↤{q} ls] or [v ↤?{q} ls], and
   requires "Hdagger" to be an assertion of the form [†l]. It removes
   (one copy of) the address [l] from the multiset [ls]. *)

(* The hypothesis [Hdagger] should be in the persistent context. *)

Ltac wp_cleanup Hmapsfrom Hdagger :=
  first [
    iApply (wp_cleanup_singleton with Hmapsfrom);
      [ done | iFrame Hdagger | iIntros Hmapsfrom ]
  | iApply (wp_cleanup_singleton_v with Hmapsfrom);
      [ done | iFrame Hdagger | iIntros Hmapsfrom ]
  ];
  autorewrite with gmultiset_simplify.

(* TODO generalize the tactic to remove several copies of [l] if needed?
        all copies of [l]? *)

(* TODO define a tactic that applies [wp_cleanup_group] or
   [wp_cleanup_group_v]. One difficulty is that [††ls] does
   not necessarily give us the multiset that we want to remove;
   we might want to remove several copies of each address in [ls]. *)

(* TODO we may wish to define a multiset operation [X \\ Y] that removes
   *all* copies of [Y]'s elements from [X]. This "dramatic difference"
   operation would distribute over union. *)

(* ------------------------------------------------------------------------ *)

(* [fetch_mapsto Δ_tuple l k] looks for a mapsto fact about the heap
   location [l] in the context [Δ], that is, for an assertion of the
   form [l ↦{q} BTuple vs]. If such a fact is found then the 3-tuple
   [H, q, vs] is passed to [k], where [H] is the hypothesis
   identifier.

   [fetch_mapsto_cell Δ l k] does the same for a stack cell pointing
   to a value

   [fetch_mapsto_cell_loc Δ l k] does the same for a stack cell
   pointing to a location *)

Ltac fetch_mapsto_tuple Δ l k :=
  lazymatch Δ with
  | context[Esnoc _ ?H (l ↦{?q} BTuple ?vs)%I] =>
      k constr:((H, q, vs))
  | _ =>
      fail "Could not find" l "↦{_} BTuple _ in context" Δ
  end.

Ltac fetch_mapsto_cell Δ l k :=
  lazymatch Δ with
  | context[Esnoc _ ?H (l ↦{?q} BCell ?v)%I] =>
      k constr:((H, q, v))
  | _ =>
      fail "Could not find" l "↦{_} BCell _ in context" Δ
  end.

Ltac fetch_mapsto_cell_loc Δ l k :=
  lazymatch Δ with
  | context[Esnoc _ ?H (l ↦{?q} BCell (VLoc ?l))%I] =>
      k constr:((H, q, l))
  | _ =>
      fail "Could not find" l "↦{_} BCell (VLoc _) in context" Δ
  end.

(* [fetch_mapsfrom Δ l k] looks for a mapsfrom fact about the location [l] in
   the context [Δ], that is, for an assertion of the form [l ↤{q} ls]. If such
   a fact is found then the 3-tuple [H, q, ls] is passed to [k], where [H] is
   the hypothesis identifier. *)

Ltac fetch_mapsfrom Δ l k :=
  lazymatch Δ with
  | context[Esnoc _ ?H (l ↤{?q} ?ls)%I] =>
      k constr:((H, q, ls))
  | _ =>
      fail "Could not find" l "↤{_} _ in context" Δ
  end.

(* [construct_vmapsfrom Δ v k] builds a trivial vmapsfrom assertion
   about the value [v], which must not be a memory location. The
   3-tuple [H, 1, ∅] is passed to [k], where [H] is an anonymous
   hypothesis. Note that since this assertion is trivial, it is
   generally removed after calling any [wp_] tactic. The environment
   [Δ] is only used to improve the error message. *)

Ltac construct_vmapsfrom Δ v k :=
  iAssert (v ↤?{1%Qp} ∅)%I as "?"; [
    solve [ done | fail 1 "Could not find or construct " v "↤?{_} _ in" Δ ]
  |];
  (* Rematching on the goal, since the anonymous hypothesis has an
     identifying number*)
  lazymatch goal with
  | |- envs_entails ?Δ _ =>
  lazymatch Δ with
  | context[Esnoc _ ?H (v ↤?{?q} ?ls)%I] =>
      k constr:((H, q, ls))
  end
  end.

(* [fetch_or_construct_vmapsfrom Δ v k] fetches a vmapsfrom assertion
   about the value [v] from the context [Δ], or builds such an
   assertion out of thin air (if possible). A 3-tuple [H, q, ls] is
   passed to the continuation [k], where [H] is the identifier (named
   or anonymous, of type [iris.proofmode.base.ident.ident]) of this
   hypothesis, and [q] and [ls] are the fraction and the multiset that
   appear in the assertion. *)

Ltac fetch_or_construct_vmapsfrom Δ v k :=
  lazymatch Δ with
  | context[Esnoc _ ?H (v ↤?{?q} ?ls)%I] =>
      k constr:((H, q, ls))
  | _ =>
  lazymatch v with
  | VLoc ?l =>
      fetch_mapsfrom Δ l k
  | _ =>
      construct_vmapsfrom Δ v k
  end
  end.

(* The tactic [wp_call] handles a function call [callee args]: it uses
   an hypothesis [callee ↦ BCell (λ: xs, i)] to replace [callee args]
   with [i] where [xs] have been replaced with [args]. *)

Ltac wp_call :=
  wp_bind (ICall _ _);
  (* get Iris context and function location *)
  lazymatch goal with
  | |- envs_entails ?Δ (wp ?s ?E (ICall (LLoc ?c) ?args) ?Q) =>
  (* get hypothesis for function location *)
  lazymatch Δ with
  | context[Esnoc _ ?Hfun (c ↦{_} _)%I] =>
  (* give hypothesis as second argument of [wp_call] *)
    iApply (wp_call s E c with
                [SGoal (SpecGoal GSpatial true  [] [Hfun] false);
                 SGoal (SpecGoal GSpatial false [] [Hfun] true)]);
    [ try reflexivity
    | iNext; iIntros Hfun; simpl substs ]
  | _ => fail 1 "wp_call: could not find " c "↦ BCell <function> in hypothesis"
  end end.

Ltac wp_skip :=
  (iApply wp_seq_skip; iNext) || wp_finish.

(* The functions [intro_ident] and [conj_idents] are used in this file
   to facilitate the re-introduction of hypotheses that may or may not
   be named.

   Named hypotheses such as [Hsrc : src ↦ BCell ()%V] will be given to
   a [wp_] lemma, to be re-introduced, for example as [Hsrc : src ↦
   BCell (#l)%V].

   Anonymous hypotheses [_ : src ↦ BCell ()%V] are identified by
   [IAnon p] for some [p], which needs to be replaced with [IFresh] at
   re-introduction as to ensure freshness.

   Finally for re-introduction of a conjunctive postcondition of the
   form [_ ∗ _ ∗ _ ∗ _] one needs to use a nested combination of
   [IList] using [iris.proofmode.intro_patterns.intro_pat.big_conj] *)

Definition intro_ident (i : ident) : intro_pat :=
  match i with
  | IAnon _ => IFresh
  | INamed i => IIdent (INamed i)
  end.

Definition conj_idents : list ident -> intro_pat :=
  fun idents =>
    match idents with
    | [] => IFresh
    | [x] => intro_ident x
    | l => intro_pat.big_conj (map intro_ident l)
    end.

(* The tactic [wp_if] handles [wp (IIf src i1 i2) ..] by using Lemma
   [wp_if] by fetching [src]'s integer value in a stack cell. It
   transforms the goal into [wp (if b then i1 else i2) ...] (or [wp i1
   ..] or [wp i2 ..] if it is convertible) *)

Ltac wp_if :=
  wp_bind (IIf _ _ _);
  lazymatch goal with
  | |- envs_entails ?Δ (wp ?s ?E (IIf (LLoc ?src) ?i1 ?i2) ?Φ) =>
  lazymatch Δ with
  | context[Esnoc _ ?Hsrc (src ↦{?q} BCell (VInt ?k))%I] =>
    iApply (wp_if s E src i1 i2 k Φ with
                [SGoal (SpecGoal GSpatial true [] [Hsrc] false);
                 SGoal (SpecGoal GSpatial false [Hsrc] [] true)]);
    let pat := ltac:(eval compute in (conj_idents [Hsrc])) in
    iNext; iIntros pat;
      try change (envs_entails Δ (wp s E i2 Φ));
      try change (envs_entails Δ (wp s E i1 Φ))
  | _ =>
      fail "wp_if: Could not find" src "↦{_} BCell (VInt _) in context" Δ
  end
  end.

(* The tactic [wp_move] handles [IMove dst src] by providing arguments
   and hypotheses to Lemma [wp_move]. *)

Ltac wp_move :=
  wp_bind (IMove _ _);
  (* get Iris context and operands *)
  lazymatch goal with
  | |- envs_entails ?Δ (wp ?s ?E (IMove (LLoc ?dst) (LLoc ?src)) ?Q) =>
  (* get hypotheses and cell values for [dst] and [src] *)
  fetch_mapsto_cell Δ dst ltac:(fun tuple =>
  lazymatch tuple with (?Hdst, _, ?v) =>
  fetch_mapsto_cell Δ src ltac:(fun tuple =>
  lazymatch tuple with (?Hsrc, _, ?w) =>
  (* get mapsfrom info for [v] and [w] *)
  fetch_or_construct_vmapsfrom Δ v ltac:(fun tuple =>
  lazymatch tuple with (?Hv, ?qv, ?lsv) =>
  fetch_or_construct_vmapsfrom Δ w ltac:(fun tuple =>
  lazymatch tuple with (?Hw, ?qw, ?lsw) =>
  (* build [wp_move]'s argument list and intro patterns *)
  iApply (wp_move s E dst src v w qv lsv qw lsw _ with
              [SGoal (SpecGoal GSpatial false [Hdst; Hsrc; Hv; Hw] [] true)]);
  (* using "[$]" instead of [SGoal (...)] may work in some cases, but
     is slower *)
  iNext;
  let pat := ltac:(eval compute in (conj_idents [Hdst; Hsrc; Hv; Hw])) in
  iIntros pat;
  simpl vmapsfrom; repeat iClear select (True)%I;
  try wp_skip
  end) end) end) end) end.

(* The tactic [wp_const] handles [IConst dst v]. *)

Ltac wp_const :=
  wp_bind (IConst _ _);
  (* get Iris context and operands *)
  lazymatch goal with
  | |- envs_entails ?Δ (wp ?s ?E (IConst (LLoc ?dst) ?w) ?Q) =>
  (* find [dst]'s value [v] and the corresponding hypothesis *)
  fetch_mapsto_cell Δ dst ltac:(fun tuple =>
  lazymatch tuple with (?Hdst, _, ?v) =>
  (* get mapsfrom info for [v] *)
  fetch_or_construct_vmapsfrom Δ v ltac:(fun tuple =>
  lazymatch tuple with (?Hv, ?qv, ?lsv) =>
  iApply (wp_const s E dst v w qv lsv eq_refl with
              [SGoal (SpecGoal GSpatial false [Hdst; Hv] [] true)]);
  iNext;
  let pat := ltac:(eval compute in (conj_idents [Hdst; Hv])) in
  iIntros pat
  end) end) end.

(* Use [wp_with as x "Hx"] to explicitely name the location [x] and
   its Iris hypothesis ["Hx"] *)
Tactic Notation "wp_with" "as" ident(x) constr(Hx) :=
  wp_bind (IAlloca _ _);
  iApply wp_alloca; iNext; iIntros (x) Hx; simpl subst.

(* Use [wp_with "x"] to explicitely name the location [x] and its Iris
   hypothesis ["Hx"] *)
Tactic Notation "wp_with" constr(x) :=
  let Hx := eval compute in ("H" ++ x)%string in
  let x := string_ident.string_to_ident x in
  wp_with as x Hx.

(* Use [wp_with] to use the string used in the program's syntax as
   name for the corresponding location *)
Tactic Notation "wp_with" :=
  wp_bind (IAlloca _ _);
  lazymatch goal with
  | |- envs_entails ?Δ (wp ?s ?E (IAlloca (BNamed ?x) _) ?Q) =>
    wp_with x
  end.

(* Use [wp_end_with] on a goal after a [IAlloca], which should be of the form
   [∃ v q ls, ?c ↦ BCell v ∗ v ↤?{q}ls ...] *)
Ltac wp_end_with :=
  (* Let [Δ] be the context and [c] the cell of which we are ending the scope *)
  lazymatch goal with
  | |- envs_entails ?Δ (∃ v q ls, ?c ↦ BCell v ∗ v ↤?{q}ls ∗ _) =>
  (* Find the final value [finalv] pointed by by [c] *)
  lazymatch Δ with
  | context[Esnoc _ ?Hc (c ↦{_} BCell ?finalv)%I] =>
  (* Find or build its associated mapsfrom *)
  fetch_or_construct_vmapsfrom Δ finalv ltac:(fun tuple =>
  lazymatch tuple with (?Hfinalv, ?qfinalv, ?lsfinalv) =>
  (* We can now instantiate the existentials *)
  iExists finalv, qfinalv, lsfinalv;
  (* Frame out the final heap [c ↦ BCell finalv] *)
  iFrame Hc;
  (* Find out its predecessors and remove [c] from it, keeping in mind that
     [Hfinalv] could be ["_" : True] if the value is not a location. *)
  (iSplit; [ now auto | ] || iFrame Hfinalv);
  let pat := ltac:(eval compute in (conj_idents [Hfinalv])) in
  iIntros pat;
  simpl vmapsfrom; repeat iClear select (True)%I
  end)
  | _ => fail "wp_end_with: could not find the final value of local variable " c " in context " Δ
  end
  | _ => fail "wp_end_with: goal does not match the expected end of local allocation"
  end.

Ltac wp_load :=
  wp_bind (ILoad _ _ _);
  lazymatch goal with
  (* identify operands [dst, src, ofs] *)
  | |- envs_entails ?Δ (wp ?s ?E (ILoad (LLoc ?dst) (LLoc ?src) ?ofs) ?Q) =>
  (* find [dst]'s old value [w] *)
  fetch_mapsto_cell Δ dst ltac:(fun tuple =>
  lazymatch tuple with (?Hdst, _, ?w) =>
  (* find [src]'s heap location [l] *)
  fetch_mapsto_cell_loc Δ src ltac:(fun tuple =>
  lazymatch tuple with (?Hsrc, _, ?l) =>
  (* find the tuple [vs] pointed by [l] *)
  fetch_mapsto_tuple Δ l ltac:(fun tuple =>
  lazymatch tuple with (?Hl, _, ?vs) =>
  (* of which the [ofs]th item is [v] *)
  let v := eval simpl in (vs !!! ofs) in
  (* get/build mapsfroms for [v] and [w] *)
  fetch_or_construct_vmapsfrom Δ v ltac:(fun tuple =>
  lazymatch tuple with (?Hv, ?qv, ?lsv) =>
  fetch_or_construct_vmapsfrom Δ w ltac:(fun tuple =>
  lazymatch tuple with (?Hw, ?qw, ?lsw) =>
  iApply (wp_load _ _ l _ vs src dst ofs v w qv qw lsv lsw with
              [SGoal (SpecGoal GSpatial false [Hdst; Hl; Hsrc; Hv; Hw] [] true)]);
    [ solve [ simpl length; lia | fail 1 "The offset" ofs "seems wrong" ]
    | reflexivity | ];
    iNext;
    let pat := ltac:(eval compute in (conj_idents [Hsrc; Hl; Hdst; Hv; Hw])) in
    iIntros pat;
    simpl vmapsfrom; repeat iClear select (True)%I;
    try wp_skip
  end) end) end) end) end) end.

Ltac wp_store :=
  wp_bind (IStore _ _ _);
  lazymatch goal with
  (* identify operands [dst, src, ofs] *)
  | |- envs_entails ?Δ (wp ?s ?E (IStore (LLoc ?dst) ?ofs (LLoc ?src)) ?Q) =>
  (* find [src]'s value [w] *)
  fetch_mapsto_cell Δ src ltac:(fun tuple =>
  lazymatch tuple with (?Hsrc, _, ?w) =>
  (* find [dst]'s heap location [l] *)
  fetch_mapsto_cell_loc Δ dst ltac:(fun tuple =>
  lazymatch tuple with (?Hdst, _, ?l) =>
  (* find the tuple [vs] pointed by [l] *)
  fetch_mapsto_tuple Δ l ltac:(fun tuple =>
  lazymatch tuple with (?Hl, _, ?vs) =>
  (* of which the [ofs]th item is [v] *)
  let v := eval simpl in (vs !!! ofs) in
  (* get/build mapsfroms for [v] and [w] *)
  fetch_or_construct_vmapsfrom Δ v ltac:(fun tuple =>
  lazymatch tuple with (?Hv, ?qv, ?lsv) =>
  fetch_or_construct_vmapsfrom Δ w ltac:(fun tuple =>
  lazymatch tuple with (?Hw, ?qw, ?lsw) =>
  iApply (wp_store _ _ dst ofs src l v w vs qv lsv qw lsw with
              [SGoal (SpecGoal GSpatial false [Hsrc; Hdst; Hl; Hv; Hw] [] true)]);
    [ solve [ simpl length; lia | fail 1 "The offset" ofs "seems wrong" ]
    | reflexivity | ];
    iNext;
    let pat := ltac:(eval compute in (conj_idents [Hdst; Hsrc; Hl; Hv; Hw])) in
    iIntros pat;
    simpl vmapsfrom; repeat iClear select (True)%I;
    try wp_skip
  end) end) end) end) end) end.

(* The tactic [wp_alloc l as "(Hto & Hfrom & Hdst & pw & Hd)"] is used
   to reason about a memory allocation. It is based on the lemma
   [wp_alloc_split]. The new memory location is named [l]. The new
   hypotheses [Hto], [Hfrom], [pw], [Hdst] and [Hd] correspond to [l ↦
   b], [l ↤ ∅], [dst ↦ BCell l], [w ↤ ls \ {[dst]}] and [♢(n -
   size_block b)] where [w] is [dst]'s old value. [pw] is discarded if
   [w] was not a location.

   It is also possible to use [wp_alloc l] to introduce anonymous
   hypotheses. *)

Tactic Notation "wp_alloc" ident(l) "as" constr(Hpost) :=
  wp_bind (IAlloc _ _);
  lazymatch goal with
  (* identify operands [dst, src, ofs] *)
  | |- envs_entails ?Δ (wp ?s ?E (IAlloc (LLoc ?dst) ?n) ?Q) =>
  (* find [dst]'s old value [w] *)
  fetch_mapsto_cell Δ dst ltac:(fun tuple =>
  lazymatch tuple with (?Hdst, _, ?w) =>
  (* find/build [w]'s predecessors*)
  fetch_or_construct_vmapsfrom Δ w ltac:(fun tuple =>
  lazymatch tuple with (?Hw, ?qw, ?lsw) =>
  (* get space credits *)
  lazymatch Δ with
  | context[Esnoc _ ?Hdiams (♢?ndiams)%I] =>
    (* Apply the lemma [wp_alloc_split] with the hypotheses [Hdiams] and [Hdst]. *)
    iApply (wp_alloc_split  s E dst n w qw lsw ndiams with
                [SGoal (SpecGoal GSpatial false [Hdiams; Hdst; Hw] [] true)]);
    [ first [ simpl; lia | fail 1 "error: not enough space credits?" ] | ];
    iNext;
    (* Introduce the postcondition; simplify it; destruct it as *)
    (* requested by the user. *)
    let tmp := iFresh in
    iIntros (l) tmp;
    iSimpl in tmp;
    iDestruct tmp as Hpost;
    simpl vmapsfrom; repeat iClear select (True)%I;
    diamonds;
    try wp_skip
  | _ =>
    fail "wp_alloc: could not find any space credits in hypothesis"
  end end) end) end.

Tactic Notation "wp_alloc" ident(l) :=
  wp_alloc l as "(?&?&?&?&?)".

Tactic Notation "wp_alloc" :=
  let l := fresh "l" in
  wp_alloc l.

(* The tactic [wp_init l as "(Hto & Hfrom & Hdst & pw & Hd)"] works
   similarly as [wp_alloc], but also handles the additional stores
   performed by the macro [init_entry]

   It is also possible to use [wp_init l] to introduce anonymous
   hypotheses. *)

Tactic Notation "wp_init" ident(l) "as" constr(Hpost) :=
  unfold init_entry; (* this seems necessary in some cases *)
  wp_alloc l as Hpost;
  try wp_bind (ISeq ISkip _); wp_skip;
  repeat wp_store;
  do 1 wp_skip.

Tactic Notation "wp_init" ident(l) :=
  wp_init l as "(?&?&?&?&?)".

Tactic Notation "wp_init" :=
  let l := fresh "l" in
  wp_init l.


(* Use [wp_free l] to free a single location [l] and introduce the
   fact that [l] is no longer alive [†l] as an anonymous hypothesis.
   The resulting space credits are added to existing space credits or
   to an unnamed hypothesis if there are no space credits already.
   Alternative syntaxes:

   [wp_free l as "Hdagger"] to name the hypothesis [†l]

   [wp_free l as "Hdagger" "Hdiamonds"] to name the hypothesis [†l] an
   rename the space credits hypothesis as "Hdiamonds" *)

Tactic Notation "wp_free" ident(l) "as" constr(Hdagger) :=
  lazymatch goal with
  | |- envs_entails ?Δ (wp ?s ?E ?e ?Q) =>
  (* find the tuple [vs] pointed by [l] *)
  (* (it only excludes [BDeallocated] to be deallocated, not it is not
      a big loss when freeing a single location) *)
  fetch_mapsto_tuple Δ l ltac:(fun tuple =>
  lazymatch tuple with (?Hl, _, ?vs) =>
  (* get/build mapsfroms for [l] *)
  fetch_or_construct_vmapsfrom Δ ((#l)%V) ltac:(fun tuple =>
  lazymatch tuple with (?pl, ?ql, ?lsl) =>
    iApply (wp_free_singleton s E e _ l (BTuple vs) lsl with
                [SGoal (SpecGoal GSpatial true [] [Hl; pl] false);
                 SGoal (SpecGoal GSpatial false [Hl; pl] [] false)]);
    [ first [ reflexivity | fail 1 "error: " e " is a value and freeing requires a non-value" ]
    | first [ now intros [] | fail 1 "error: block must not be a stack cell" ]
    | try multiset_solver
    |
    let pat := eval compute in ("(" ++ Hdagger ++ " & ?)")%string in
      iIntros pat; diamonds
    ]
  end) end)
  | _ => fail "wp_free: the goal should be a wp of a non-value"
  end.

Tactic Notation "wp_free" ident(l) "as" constr(Hdagger) constr(Hdiamonds) :=
  wp_free l as Hdagger;
  iRename select (♢_)%I into Hdiamonds.

Tactic Notation "wp_free" ident(l) :=
  wp_free l as "?".

(* The tactic [wp_cleanup l in v] will remove one [l] from [v]'s
   predecessors. It can be used when there is a hypothesis of the form
   [†l] and another of the form [v ↤?{q} ls] or [v ↤{q} ls] (if [v] is
   a location): then, one copy of the address [l] will be removed from
   the multiset [ls]. *)

Tactic Notation "wp_cleanup" constr(l) "in" constr(v) :=
  lazymatch goal with
  | |- envs_entails ?Δ (wp ?s ?E ?e ?Φ) =>
  lazymatch Δ with
  | context[Esnoc _ ?Hdagger (†l)%I] =>
  lazymatch Δ with
  | context[Esnoc _ ?Hmapsfrom (v ↤?{_} _)%I] =>
    iApply (wp_cleanup_singleton_v s E e Φ v _ _ l with
                [SGoal (SpecGoal GSpatial false [] [Hmapsfrom] true);
                 SGoal (SpecGoal GIntuitionistic false [] [] true)]);
    [ first [ reflexivity | fail 1 "error: " e " is a value and we need a non-value" ]
    | iIntros Hmapsfrom ]
  | context[Esnoc _ ?Hmapsfrom (v ↤{_} _)%I] =>
    iApply (wp_cleanup_singleton s E e Φ v _ _ l with
                [SGoal (SpecGoal GSpatial false [] [Hmapsfrom] true);
                 SGoal (SpecGoal GIntuitionistic false [] [] true)]);
    [ first [ reflexivity | fail 1 "error: " e " is a value and we need a non-value" ]
    | iIntros Hmapsfrom ]
  | _ => fail "could not find" v "↤{_} _ or" v "↤?{_} in context " Δ
  end
  | _ => fail "could not find hypothesis †" l " in context " Δ
  end
  | _ => fail "wp_cleanup: the goal should be a wp of a non-value"
  end.


(** Use [prove_neq x y] to add [x ≠ y] as a pure hypothesis, when
    there are spatial hypotheses of the form [x ↦ _] and [y ↦ _].

    Use [prove_neq x] to add [x ≠ y] for every possible [y], and
    simply [prove_neq] for every possible [x] and [y]. *)

Tactic Notation "prove_neq" ident(x) ident(y) :=
  (iAssert (⌜x ≠ y⌝)%I as %?);
  [ by iApply (gen_heap.mapsto_ne x y with "[$] [$]") | ].

Local Ltac prove_neq_1 Δ x :=
  (* Try to prove [x ≠ y] for [(y ↦ _) ∈ Δ] *)
  match constr:(Δ) with
  | context[Esnoc ?Δ1 ?Hy (?y ↦ _)%I] =>
    try prove_neq x y;
    prove_neq_1 Δ1 x
  | _ => idtac
  end.

Tactic Notation "prove_neq" ident(x) :=
  lazymatch goal with
  | |- envs_entails ?Δ ?G =>
    prove_neq_1 Δ x
  end.

Local Ltac prove_neq_2 Δ :=
  (* Try to prove [x ≠ y] for [(x ↦ _), (y ↦ _) ∈ Δ] *)
  match constr:(Δ) with
  | context[Esnoc ?Δ1 ?Hy (?x ↦ _)%I] =>
    prove_neq_1 Δ1 x;
    prove_neq_2 Δ1
  | _ => idtac
  end.

Tactic Notation "prove_neq" :=
  lazymatch goal with
  | |- envs_entails ?Δ ?G =>
    prove_neq_2 Δ
  end.

(* The tactic [cancel_mapsfrom] looks in the goal for conjuncts of the
   form [l ↤{q} ls] where [l ↤{q} ls'] is an hypothesis, and tries to
   cancel them out by establishing [ls = ls'] using [multiset_solver].
   It does the same for [↤?{q}] instead of [↤{q}]. *)

Ltac cancel_mapsfrom_ Δ G :=
  match G with
  | (?G1 ∗ ?G2)%I =>
    try cancel_mapsfrom_ Δ G1;
    try cancel_mapsfrom_ Δ G2
  | (?l ↤{?q} ?ls)%I =>
    lazymatch Δ with
    | context[Esnoc _ ?Hl (l ↤{q} ?ls')%I] =>
      (iAssert ⌜ls = ls'⌝%I as %<-);
      [ iPureIntro; solve [ multiset_solver ] | iFrame Hl ]
    end
  | (?v ↤?{?q} ?ls)%I =>
    lazymatch Δ with
    | context[Esnoc _ ?Hl (v ↤?{q} ?ls')%I] =>
      (iAssert ⌜ls = ls'⌝%I as %<-);
      [ iPureIntro; solve [ multiset_solver ] | iFrame Hl ]
    end
  end.

Ltac cancel_mapsfrom :=
  lazymatch goal with
  | |- envs_entails ?Δ ?G =>
    cancel_mapsfrom_ Δ G
  end.
