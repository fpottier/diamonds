From spacelang Require Import wp_state_interp.

(* This trivial lemma allows reasoning about [skip]. *)

Lemma wp_skip `{!heapG Σ} s E Φ :
  Φ tt ⊢ WP ISkip @ s; E {{ Φ }}.
Proof.
  eapply wp_value. typeclasses eauto.
Qed.
