From spacelang Require Import wp_state_interp.

(* This module establishes reasoning rules for cleaning up deallocated
   locations in predecessor sets. *)

(* ------------------------------------------------------------------------ *)

(* A deallocated location [m] can be removed from a predecessor set. *)

Lemma supd_cleanup_singleton `{!heapG Σ} E l' q ls m :
  l' ↤{q} ls ∗ †m ={{E}}=∗ l' ↤{q} (ls ∖ {[+m+]}).
Proof.
  iIntros (σ ns κs n) "(Hstate & Hmapsfrom & Hdagger)".
  destruct_state_interp.
  iMod (ph_cleanup_singleton with "[Hph Hmapsfrom Hdagger]")
    as "(Hph & Hmapsfrom)"; [ iFrame |].
  iModIntro. intro_state_interp.
  iFrame "Hmapsfrom". iExists _. iFrame. eauto.
Qed.

Lemma wp_cleanup_singleton `{!heapG Σ} s E i Φ l' q ls m :
  to_val i = None →
  l' ↤{q} ls -∗
  †m -∗
  (l' ↤{q} (ls ∖ {[+m+]}) -∗ wp s E i Φ) -∗
  wp s E i Φ.
Proof.
  iIntros (nv) "mapsfrom #dagger WP".
  (* Grab the state interpretation predicate. *)
  iApply (wp_supd _ _ _
           (l' ↤{q} ls ∗ †m)%I
           (l' ↤{q} (ls ∖ {[+m+]}))%I
           _ nv
           with "WP"); [| iFrame; eauto ].
  apply supd_cleanup_singleton.
Qed.

(* ------------------------------------------------------------------------ *)

(* A set [ms] of deallocated locations can be removed from a predecessor
   set. *)

Lemma supd_cleanup_group `{!heapG Σ} E l' q ls ms :
  l' ↤{q} ls ∗ ††(dom ms) ={{E}}=∗ l' ↤{q} (ls ∖ ms).
Proof.
  iIntros (σ ns κs n) "(Hstate & Hmapsfrom & Hddagger)".
  destruct_state_interp.
  iMod (ph_cleanup_group with "[Hph Hmapsfrom Hddagger]")
    as "(Hph & Hmapsfrom)"; [ iFrame |].
  iModIntro. intro_state_interp.
  iFrame "Hmapsfrom". iExists _. iFrame. eauto.
Qed.

Lemma wp_cleanup_group `{!heapG Σ} s E i Φ l' q ls ms :
  to_val i = None →
  l' ↤{q} ls -∗
  ††(dom ms) -∗
  (l' ↤{q} (ls    ∖ ms) -∗ wp s E i Φ) -∗
  wp s E i Φ.
Proof.
  iIntros (nv) "Hmapsfrom #Hddagger WP".
  (* Grab the state interpretation predicate. *)
  iApply (wp_supd _ _ _
           (l' ↤{q} ls ∗ ††(dom ms))%I
           (l' ↤{q} (ls ∖ ms))%I
           _ nv
           with "WP"); [| iFrame; eauto ].
  apply supd_cleanup_group.
Qed.

(* ------------------------------------------------------------------------ *)

(* Reformulations of the previous lemmas in terms of [vmapsfrom]. *)

Lemma supd_cleanup_singleton_v `{!heapG Σ} E v q ls m :
  v ↤?{q} ls ∗ †m ={{E}}=∗ v ↤?{q} (ls ∖ {[+m+]}).
Proof.
  (* If [v] is not a memory location, then the result is immediate. *)
  destruct v; simpl vmapsfrom; eauto using supd_True.
  (* If [v] is a memory location, then the previous lemma applies. *)
  eauto using supd_cleanup_singleton.
Qed.

Lemma wp_cleanup_singleton_v `{!heapG Σ} s E i Φ v q ls m :
  to_val i = None →
  v ↤?{q} ls -∗
  †m -∗
  (v ↤?{q} (ls ∖ {[+m+]}) -∗ wp s E i Φ) -∗
  wp s E i Φ.
Proof.
  (* If [v] is not a memory location, then the result is immediate. *)
  destruct v; simpl vmapsfrom; try solve [
    iIntros (_) "_ _ WP"; iApply "WP"; done
  ].
  (* If [v] is a memory location, then the previous lemma applies. *)
  eauto using wp_cleanup_singleton.
Qed.

Lemma supd_cleanup_group_v `{!heapG Σ} E v q ls ms :
  v ↤?{q} ls ∗ ††(dom ms) ={{E}}=∗ v ↤?{q} (ls ∖ ms).
Proof.
  (* If [v] is not a memory location, then the result is immediate. *)
  destruct v; simpl vmapsfrom; eauto using supd_True.
  (* If [v] is a memory location, then the previous lemma applies. *)
  eauto using supd_cleanup_group.
Qed.

Lemma wp_cleanup_group_v `{!heapG Σ} s E i Φ v q ls ms :
  to_val i = None →
  v ↤?{q} ls -∗
  ††(dom ms) -∗
  (v ↤?{q} (ls    ∖ ms) -∗ wp s E i Φ) -∗
  wp s E i Φ.
Proof.
  (* If [v] is not a memory location, then the result is immediate. *)
  destruct v; simpl vmapsfrom; try solve [
    iIntros (_) "_ _ WP"; iApply "WP"; done
  ].
  (* If [v] is a memory location, then the previous lemma applies. *)
  eauto using wp_cleanup_group.
Qed.
