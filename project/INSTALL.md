# A Separation Logic for Heap Space under Garbage Collection

## Instructions

  To compile our proofs, you need `opam`, the OCaml package manager.
    (A recent version of `opam` is recommended; we use 2.0.8.)
  To install `opam`, follow [these instructions](https://opam.ocaml.org/doc/Install.html).

  Once you have `opam`, please type:

    ./setup.sh

  This should create a *local opam switch* in the current directory
  (in a subdirectory named `_opam`),
  install *specific* versions of Coq, stdpp, and Iris
  (listed in the file `opam`),
  and compile our proofs.

  This process can take about 40 minutes on a single-core machine
  and about 15 minutes on a modern multi-core machine.
