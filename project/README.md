# A Separation Logic for Heap Space under Garbage Collection

To build the project, please follow the instructions in
[INSTALL.md](INSTALL.md).

## Module Index

### Additions to Existing Libraries

- [stdpp.v](src/stdpp.v): additions to the library `stdpp`.

- [gset_fixpoint.v](src/gset_fixpoint.v): general results about the
  fixed point of a function of finite sets to finite sets; used to
  prove that reachability is decidable.

- [iris.v](src/iris.v): additions to Iris.

### Generic Theory About Heaps

- [hypotheses.v](src/hypotheses.v): generic hypotheses about the heap,
  used in `predecessors.v`, `successors.v`, and `ph.v`.

- [successors.v](src/successors.v): generic theory of reachability.

- [predecessors.v](src/predecessors.v): generic theory of the predecessor map
  `π` and of the invariant that ties the heap `σ` with the predecessor map
  `π`.

- [ph.v](src/ph.v): generic construction of the `mapsfrom` assertion;
  definition of points-to assertions; definition of the assertions
  `††ls` and `†l`.
  Laws about these assertions, including `Join→`,
  `Join←`, `Covariance←`,
  `Zero††`, `Join††`.

### Operational Semantics of SpaceLang

- [syntax.v](src/syntax.v): syntax of SpaceLang.

- [state.v](src/state.v): definition of stores and machine states.

- [spacelang.v](src/spacelang.v): operational semantics of SpaceLang:
  the *head step* relation.

- [pointers.v](src/pointers.v): proof that SpaceLang satisfies the
  generic hypotheses expressed in [hypotheses.v](src/hypotheses.v);
  plus various lemmas.

- [gc.v](src/gc.v): definition of garbage collection in SpaceLang;
  instantiation of Iris for SpaceLang.

- [semantics.v](src/semantics.v): a number of technical results
  about the operational semantics of SpaceLang.

- [tactics.v](src/tactics.v): a number of Coq tactics,
  including inversion tactics for some of the judgements
  in the operational semantics of SpaceLang.

- [instances.v](src/instances.v): a number of type class instances,
  which complete the instantiation of Iris for SpaceLang.

### The Program Logic SL♢

- [wp_state_interp.v](src/wp_state_interp.v): definition of the
  state interpretation invariant; definition of several assertions,
  including `vmapsfrom` and space credits.

- [wp_diamonds.v](src/wp_diamonds.v): laws about space credits,
  including `Zero♢` and `Join♢`.

- [wp_mapsfrom_laws.v](src/wp_mapsfrom_laws.v): the law `Confront→←`.

- [wp_memory.v](src/wp_memory.v): internal lemmas about memory accesses.

- [wp_alloc.v](src/wp_alloc.v): the reasoning rule `Alloc`.

- [wp_alloca.v](src/wp_alloca.v): the reasoning rule `Alloca`.

- [wp_free.v](src/wp_free.v): the laws `CloudEmpty`, `CloudCons`, and `Free`.

- [wp_cleanup.v](src/wp_cleanup.v): the reasoning rule `Cleanup`.

- [wp_load.v](src/wp_load.v): the reasoning rule `Load`.

- [wp_store.v](src/wp_store.v): the reasoning rule `Store`.

- [wp_seq.v](src/wp_seq.v): the reasoning rule `Seq`.

- [wp_skip.v](src/wp_skip.v): the reasoning rule `Skip`.

- [wp_if.v](src/wp_if.v): the reasoning rule `If`.

- [wp_call.v](src/wp_call.v): the reasoning rule `Call`.

- [wp_loceq.v](src/wp_loceq.v): the reasoning rule `LocEq`.

- [wp_const.v](src/wp_const.v): the reasoning rule `Const`.

- [wp_move.v](src/wp_move.v): the reasoning rule `Move`.

- [wp_fork.v](src/wp_fork.v): the reasoning rule `Fork`.

- [adequacy.v](src/adequacy.v): statement and proof of the soundness theorem
  for SL♢.

- [hoare_rules.v](src/hoare_rules.v): check that the reasoning rules shown
  in the paper are consequences of the reasoning rules that we actually use.
  The former are expressed in terms of Hoare triples, whereas the latter are
  expressed in terms of weakest preconditions (the predicate `wp`).

### Reference-Counting Style

- [rc.v](src/rc.v): reasoning rules in reference-counting style.

### Infrastructure

- [proofmode.v](src/proofmode.v): tactics that help apply the reasoning rules.

- [rcproofmode.v](src/rcproofmode.v): tactics that help apply
  the reasoning rules in reference-counting style.

- [notation.v](src/notation.v): notations that help write SpaceLang code
  with a somewhat tolerable surface syntax.

### Examples

- [stack_example.v](src/stack_example.v): specification and proof of a stack.

- [examples_lists.v](src/examples_lists.v): specification and proof of some
  functions on linked lists.

- [examples_forkjoin.v](src/examples_forkjoin.v): specification and proof of
  some examples that involve `fork` and `join`.
