# Vue d'ensemble

J'ai repris à partir de zéro mes réflexions sur une logique de séparation qui
permette de raisonner sur l'espace. J'ai mis temporairement de côté la notion
de région, et j'ai une proposition concrète qui me semble simple.

## Définition du calcul

On s'intéresse à un lambda-calcul non typé muni de valeurs primitives (par
exemple la valeur unit), de sommes-de-produits allouées dans le tas, de
reférences mutables, et de fonctions (récursives) closes.

On veut définir la classe des *valeurs* de telle sorte que les valeurs
n'occupent pas d'espace dans le tas. Les valeurs sont donc les valeurs
primitives, les adresses mémoire, et les fonctions closes (qui, plus loin dans
la chaîne de compilation, pourront être représentées par des pointeurs de
code).

    v ::= () | l | μf.λxs.e

La restriction aux fonctions closes est bien sûr très forte. Une notion de
clôture sera introduite plus tard, sous forme d'une macro (voir plus loin).

Un *tas* `H` est une fonction finie des locations vers des blocs.

Un *bloc* est soit un bloc de données immutable, soit une référence mutable.

    b ::= C(v, ..., v)          # un bloc de données d'étiquette C
        | ref v                 # une référence

Appelons *valeurs* d'un bloc la liste des valeurs contenues dans ce bloc, et
*arité* d'un bloc la longueur de cette liste.

On définit une notion de *taille* d'un bloc. On peut a priori la définir de
façon relativement arbitraire. On pourrait par exemple choisir la taille `1`
pour tous les blocs: à un facteur constant près, cela conduirait à une analyse
correcte de la complexité en espace. Toutefois, une telle idée n'est pas très
réaliste, et ne résisterait pas à l'ajout de tableaux dans le langage. Il
semble préférable de choisir une notion de taille plus réaliste, par exemple:

    0                           # pour un bloc d'arité 0
    1 + arité                   # pour un bloc d'arité 1 ou plus

Une telle notion de taille conduit à des bornes concrètes qu'un compilateur
comme CakeML ou OCaml pourra respecter sans difficulté. De plus, le fait de
compter `0` pour les constructeurs constants (comme `Nil`) et pour les
fonctions closes facilitera sans doute la vie des utilisateurs.

La syntaxe des *expressions* est la suivante:

    e ::=
        | x                     # variable
        | v                     # valeur
        | let x = e in e        # séquence
        | e(es)                 # appel de fonction
        | C(e, ..., e)          # allocation de bloc de données
        | ref e                 # allocation de référence
        | match e with branch*  # utilisation d'un bloc de données
        | !e                    # utilisation d'une référence (lecture)
        | e := e                # utilisation d'une référence (écriture)

    branch ::=
          C(x, ..., x).e

Les expressions d'allocation d'un bloc ont la même syntaxe qu'un bloc, sauf
qu'on les autorise à contenir des sous-expressions au lieu de valeurs. Leur
sémantique est celle que l'on imagine: une fois les sous-expressions réduites
en des valeurs, on obtient un bloc, que l'on alloue dans le tas à une adresse
fraîche `l`, et l'expression d'allocation se réduit en `l`.

La syntaxe est bien stable par substitution de valeurs pour les variables.
Notons que les valeurs sont toujours closes, que la substitution n'est jamais
appliquée à une valeur ni à un bloc. En particulier, la substitution ne
descend jamais sous un lambda, puisque celui-ci fait partie de la syntaxe des
valeurs.

## Définition de la logique

### Prédicats

De quels prédicats a-t-on besoin?

* Un prédicat `l.predecessors = L` où `L` est un ensemble de locations.
  Ce prédicat est affine.

* Un prédicat `l pointsto b`. Ce prédicat donne la structure du bloc
  situé à l'adresse `l`. Ce prédicat est affine.

* Un prédicat affine `1♢` pour un crédit-espace.

* Un prédicat duplicable `†l`.

Dans un premier temps, on peut considérer que cette logique s'applique à un
langage sans GC, avec une opération `free` qui libère réellement l'objet. Dans
un second temps, on pourra montrer que (modulo quelques conditions) elle
s'applique aussi à un langage avec GC où `free` est une opération fantôme. Il
faudrait alors que `free` libère la mémoire dans le tas fantôme, mais pas dans
le tas réel, et que l'invariant qui relie le tas réel et le tas fantôme soit
relâché: j'imagine que le tas fantôme devrait être contraint à rester un
sur-ensemble de la partie accessible du tas réel. L'intérêt que `free` soit
une opération fantôme est qu'on peut alors exprimer une idée telle que: "si
l'objet n'a plus de prédécesseurs, alors je le libère, sinon non".

### Prédicats auxiliaires

Pour énoncer plus facilement certaines règles de raisonnement, donnons-nous
aussi une pseudo-instruction `register l v` dont l'effet est d'enregistrer
l'existence d'un pointeur de `l` vers `v` en mettant à jour le champ
`predecessors` de l'objet situé à l'adresse `v`, si `v` est une adresse.
Définissons `register l v*`, où `v*` est une liste de valeurs, comme un
sucre pour une séquence d'instructions `register`. Donnons-nous également
une pseudo-instruction `unregister l v`. Ces pseudo-instructions sont en
fait définies de la façon suivante:

* Le prédicat `register l v P` signifie que, une fois qu'on aura enregistré le
  fait que `l` est un prédécesseur de `v`, l'assertion `P` sera vraie. Ce
  prédicat est défini par deux règles, suivant que `v` est une adresse ou une
  valeur primitive:

      l'.predecessors = L -*
      (
        l'.predecessors = L U {l} -*
        P
      ) -*
      register l l' P


      P -*
      register l () P

* Le prédicat `unregister l v P` signifie que, une fois qu'on aura enregistré
  le fait que `l` n'est plus un prédécesseur de `v`, l'assertion `P` sera
  vraie. Ce prédicat est défini par deux règles, suivant que `v` est une
  adresse ou une valeur primitive:

      l'.predecessors = L -*
      (
        l'.predecessors = L \ {l} -*
        P
      ) -*
      unregister l l' P


      P -*
      unregister l () P

Il faudra faire attention au fait que, au moment où ces prédicats sont
utilisés, l'invariant du tas n'est pas satisfait: les champs `predecessors` ne
sont pas cohérents. Le rôle de ces pseudo-instructions est justement de
rétablir cette cohérence. Bien sûr, la charge de prouver que l'invariant est
bien rétabli doit revenir au concepteur du système, qui fait cette preuve une
fois pour toutes, et non pas à l'utilisateur.

### Règles de raisonnement

Quelles sont les règles de raisonnement?

* L'expression d'allocation de bloc `b` consomme `sizeof(b)♢`.
  Elle produit une location `l` qui satisfait
  `l.predecessors = empty` et `l pointsto b`.
  De plus, il faut exprimer le fait que toutes les adresses qui apparaissent
  dans le bloc `b` reçoivent `l` comme prédécesseur, ce que l'on peut exprimer
  indirectement via une séquence de pseudo-instructions `register`.

      sizeof(b)♢ -*
      (
        forall l,
        l.predecessors = empty -*
        l pointsto b -*
        register l (values b) φ(l)
      ) -*
      wp b φ

* L'opération de désallocation exige
  `l.predecessors = empty * l pointsto b`
  et produit `sizeof(b)♢ * †l`.
  C'est en fait une opération fantôme, un view shift.

      l.predecessors ⊆ {l} * l pointsto b |=>
      sizeof(b)♢ * †l

  On peut la généraliser pour libérer d'un coup un ensemble `L` de locations,
  auquel cas la condition `l.predecessors ⊆ {l}` est relâchée et devient
  `l.predecessors ⊆ L`. Pour écrire cela joliment, on introduit
  un prédicat auxiliaire `L pointsto #n`, défini comme une conjonction
  itérée de `pointsto`, qui signifie que l'on possède tous les blocs de
  l'ensemble `L` et que la somme de leurs tailles est `n`.
  On peut aussi écrire `L.predecessors ⊆ L` pour la conjonction itérée
  des `l.predecessors ⊆ L` et `†L` pour la conjonction itérée des `†l`.
  Cela fait, la règle de désallocation s'écrit:

      L.predecessors ⊆ L * L pointsto #n |=>
      n♢ * †L

* Un second view shift permet de supprimer l'adresse `l` d'un ensemble
  de prédécesseurs si l'on sait que cette adresse a été libérée:

      †l * l'.predecessors = L |=>
      l'.predecessors = L \ {l}

* L'opération de lecture d'une référence exige un prédicat `l pointsto ref v`,
  comme d'habitude.

* L'opération d'écriture exige ce même prédicat, et utilise de plus `unregister`
  et `register`.

      l pointsto ref v -*
      (
        l pointsto ref v' -*
        unregister l v (register l v' φ)
      ) -*
      wp (l := v') φ

* Tout objet soit est alloué, soit a été désalloué. On a donc l'implication

      (l pointsto b * †l) -* false

* Comme une valeur primitive `()` n'est pas une adresse,
  on a l'implication `†() -* false`.

C'est tout pour la définition de la logique, dans sa forme la plus simple.

Il faut souligner que le champ `predecessors` ne garde trace que des pointeurs
du tas vers le tas. On peut avoir des pointeurs vers le tas stockés dans des
variables locales; ceux-ci ne sont pas comptés (et c'est tant mieux, car ce
serait très lourd). Cela ne compromet pas memory safety, car (comme toujours
en logique de séparation) un dangling pointer est inutilisable: si on libère
un objet, on perd la permission `pointsto`, donc on ne peut plus déréférencer
le pointeur.

Telle que définie ci-dessus, la logique est donc sûre au sens habituel (memory
safety).

## Gestion des racines

Si le langage n'avait pas de GC et si l'opération `free` n'était pas fantôme,
c'est-à-dire si chaque objet était vraiment libéré au moment où nous le
libérons par la pensée, alors la logique définie ci-dessus permettrait de
raisonner correctement à propos de la complexité en espace. Si tel était notre
but, toutefois, cette logique serait inutilement complexe, car maintenir les
champs `predecessors` ne servirait alors à rien.

Dans un système avec GC, nous devons nous interdire de libérer un objet par la
pensée *avant* que le GC ne soit capable de le libérer réellement. Pour cela,
il faut ajouter une règle supplémentaire à la discipline statique: tant qu'une
adresse `l` est considérée comme une racine par le GC, nous ne devons pas
libérer par la pensée l'objet situé à cette adresse.

Comment savoir jusqu'où une variable est considérée comme une racine? Une
approximation sûre serait de considérer que c'est le cas jusqu'à la fin de sa
portée lexicale. Une autre approximation serait de considérer que c'est le cas
jusqu'à sa dernière occurrence (le long de chaque chemin). La première
approximation est certainement trop pessimiste, et la seconde peut l'être
également (quoique de façon plus marginale peut-être) (par exemple parce
qu'une optimisation effectuée par le compilateur peut faire disparaître une
occurrence d'une variable et réduire ainsi sa durée de vie).

En réalité, le GC s'appuie sur une analyse de durée de vie effectuée par le
compilateur. Il semble raisonnable de supposer que les résultats de cette
analyse sont connus, et visibles dans la syntaxe. On peut donc matérialiser
dans le code tous les points où le GC peut être appelé, et indiquer
explicitement en chacun de ces points quelles variables (ou valeurs) sont
vivantes à travers l'appel (i.e., sont des racines pendant l'appel). Ces
points sont les instructions primitives d'allocation et les appels de fonction
utilisateur.

Pour ce qui est du raisonnement logique, la façon dont je souhaite raisonner
au moment d'un appel de fonction (ou d'une allocation primitive) est la
suivante:

* On fait apparaître une adresse `l` abstraite, que l'on peut interpréter
  intuitivement comme l'adresse de la stack frame actuelle;

* Pour chaque variable vivante, on ajoute `l` à ses prédécesseurs juste avant
  l'appel, et on supprime `l` de ses prédécesseurs juste après l'appel.

D'un point de vue pratique, cette approche signifie que, pour pouvoir
raisonner à propos de la complexité en espace d'un programme, il faut
d'abord le transformer pour faire apparaître les annotations de durée
de vie.

On peut imaginer plusieurs façons simples d'insérer ces annotations:

* Considérer que tant qu'on est dans la portée lexicale d'une variable `x`,
  cette variable reste vivante. Avantage: toujours correct. Inconvénient: cela
  conduit à surestimer la complexité en espace.

* Considérer que seules les variables qui apparaissent syntaxiquement dans la
  continuation de l'expression courante sont vivantes. Avantages: critère
  intuitivement simple; durée de vie réduite, donc approximation plus précise
  de la complexité en espace. Inconvénient: si le compilateur ne respecte pas
  cela, on obtient une analyse incorrecte. J'en ai un exemple en OCaml: quand
  on écrit `match xs with y :: ys -> ... ys ...`, le compilateur peut
  remplacer une occurrence de `ys` par un accès à l'objet `xs`. La durée de
  vie de `xs` s'étend donc au-delà de sa dernière occurrence syntaxique. (On
  peut argumenter que c'est un comportement indésirable du compilateur.)

## Gestion des racines, version 2

Pour clarifier la gestion des racines, il semble essentiel de garder trace
explicitement des pointeurs de la pile vers le tas. Cela suppose que les
cellules de mémoire situées dans la pile aient une adresse. Cela permettra à
ces adresses d'apparaître parmi les prédécesseurs d'un bloc du tas. Un bloc
pour lequel on a l'assertion `l ↤ ∅` sera donc bien un bloc inaccessible,
puisqu'il n'y aura aucun pointeur vers lui, ni depuis le tas, ni depuis la
pile.

Toutefois, il faut plus que cela: il faut non seulement que les cellules de
pile aient une adresse, et puissent contenir un pointeur vers le tas, mais
aussi que ces pointeurs vers le tas soient nécessairement stockés dans la
pile, et ne puissent pas se promener tous nus, c'est-à-dire apparaître dans
l'expression en cours d'exécution. Sinon, ils formeraient de facto, à nouveau,
des pointeurs vers le tas, qui ne seraient pas enregistrés dans un ensemble de
prédécesseurs.

Quand on pousse cette logique jusqu'au bout, on s'aperçoit qu'on ne peut pas
avoir un λ-calcul habituel, où les variables dénotent des valeurs, et où les
pointeurs vers le tas font partie des valeurs. Il faut restreindre le calcul
de façon à ce qu'une variable dénote non pas une valeur, mais l'adresse d'une
cellule de pile, laquelle contient à son tour une valeur. On s'aperçoit aussi
qu'une expression ne peut pas renvoyer une valeur; au lieu de cela, une
expression ne doit renvoyer aucun résultat (mais elle peut écrire une valeur
dans une cellule de pile). Une expression devient en fait une instruction; le
calcul devient impératif. Dans le même esprit, les fonctions ne peuvent plus
renvoyer de résultat, mais peuvent prendre en argument des adresses (de
cellules de pile) dans lesquelles elles pourront écrire des valeurs. Elles
deviennent donc des procédures, avec appel par référence: au moment de
l'appel, les arguments effectifs sont des adresses dans la pile. Ainsi, les
arguments formels (des variables) sont remplacées par les arguments effectifs
(des adresses dans la pile). On obtient bien la propriété souhaitée, à savoir
que les variables sont substituées par des adresses dans la pile, pas par des
valeurs.

```
  Valeurs
    v ::=
        | ()                      -- valeur unit
        | k                       -- valeur entière
        | l                       -- adresse d'un objet dans le tas
        | λ(x,...).i              -- pointeur de code

  LValues
    lv ::=
        | x                       -- variable
        | c                       -- adresse d'une cellule dans la pile

  Instructions
    i ::=
        | skip                    -- instruction inerte
        | i; i                    -- séquence
        | if [lv] then i else i   -- conditionnelle
        | [lv](lv, ...)           -- appel de procédure (en appel par réf.)

        | [lv] <- alloc n         -- allocation dans le tas
        | [lv] <- [lv].ofs        -- lecture dans le tas
        | [lv].ofs := [lv]        -- écriture dans le tas
        | [lv] <- [lv] == [lv]    -- comparaison d'adresses du tas

        | [lv] <- v               -- chargement d'une constante (v ≠ l)
        | [lv] <- [lv]            -- déplacement (move) entre cellules
        | alloca x in i           -- allocation d'une cellule de pile
        | with c i                -- marqueur d'existence d'une cellule de pile
```

La notation `[lv]` suggère que l'on déréférence l'adresse d'une cellule de
pile; par exemple, l'instruction `[c] <- v` est censée stocker la valeur `v`
dans la cellule de pile dont l'adresse est `c`. (Dans cette instruction, la
valeur `v` ne peut pas être une location, car on interdit la présence de
locations du tas dans le code.)

L'instruction `alloca x in i` est censée allouer une cellule de pile, à une
adresse fraîche `c`, puis se réduire en `with c [c/x]i`, c'est-à-dire exécuter
l'instruction `i` où la variable `x` a été remplacée par l'adresse `c`, sous
un marqueur `with c _` qui matérialise la pile. La construction `with c _` est
un contexte d'évaluation. (La construction `_; i` en est un autre.) La
construction `with c skip` se réduit en `skip` après avoir désalloué la
cellule de pile à l'adresse `c`: ainsi, les cellules de pile ont bien une
durée de vie lexicale.

Dans cette proposition, chaque cellule de pile a une taille 1. On pourrait
autoriser l'allocation de cellules de taille arbitraire, en paramétrant
`alloca` par un entier `n`. Les accès `[lv]` devraient alors spécifier un
offset. Ce n'est sans doute pas très intéressant, car de toute façon on
souhaite séparer la possession de chaque cellule de la pile; on ne souhaite
pas posséder tout une stack frame d'un seul coup.

Le fait que les cellules de pile soient mutables peut être pratique pour
l'utilisateur (cela peut lui éviter d'allouer une référence). De façon plus
fondamentale, si une cellule de pile contient un pointeur et si à un certain
moment ce pointeur devient inutile, on peut l'écraser (avec une valeur unit)
et la cellule de pile n'est alors plus une racine. On modélise donc le fait
qu'une racine peut disparaître avant la fin de sa portée lexicale, ce qui est
important pour l'analyse de la complexité en espace de certains programmes.

## Clôtures

Le calcul décrit plus haut n'a pas de notion de clôture. L'idée est
d'effectuer une closure conversion à l'aide de deux macros (une macro pour
allouer une clôture, une macro pour appeler une clôture), de sorte que le
programmeur aura l'illusion que le langage offre des clôtures primitives. Il
faudra également proposer des règles de raisonnement (dérivées) à propos des
clôtures, pour que le raisonnement soit (presque) aussi simple que dans un
λ-calcul ordinaire. On pourra proposer un prédicat abstrait `isClosure` qui
cache le contenu de la clôture et n'en expose que la taille et la
fonctionnalité.

## Applications

On peut maintenant commencer à se demander quels prédicats inductifs on
utilisera pour définir des structures standard (par exemple des listes), et
quelles specs on donnera pour les fonctions usuelles (par exemple `iter` sur les
listes).

Une fonction `iter` sur une liste semble n'avoir besoin que de l'accès en
lecture. On peut imaginer de lui donner un prédicat `list` ordinaire, défini
inductivement à partir de prédicats `pointsto`. Toutefois, dans le cas
particulier d'une liste à possesseur unique, on pourrait vouloir libérer les
cellules au fur et à mesure qu'on parcourt la liste. Dans ce cas, il faudrait
donner un prédicat `ulist` défini inductivement à partir de prédicats `pointsto`
et `predecessors`; ce prédicat affirmerait que la tête de liste n'a aucun
prédécesseur et que les cellules suivantes ont un seul prédécesseur. Alors, la
fonction `iter` peut désallouer les cellules au fur et à mesure, ce qui lui
permet d'amasser `3n♢` (où `n` est la longueur de la liste) qui apparaissent dans
sa postcondition. Enfin, encore mieux, on peut donner ces crédits-espace à la
fonction utilisateur `f` au fur et à mesure qu'ils apparaissent; alors, au
lieu de mentionner `3n♢` dans la postcondition de `iter`, on donne à la fonction `f`
la formule `I * 3♢` et on lui demande de nous rendre `I`. Libre alors à elle de
consommer ces crédits-espace ou bien de les accumuler dans son invariant.

## Méta-théorie

Je crois que cette logique tient la route et qu'on devrait pouvoir en prouver
la correction assez facilement. Je veux dire par là, montrer que cette logique
permet de prouver memory safety comme d'habitude, et en plus, permet de
prouver une borne sur l'espace maximum utilisé dans le tas (le *high water
mark*): pour un programme complet, l'espace utilisé dans le tas ne dépasse
jamais la quantité de crédits-espace donnés dans la précondition.

De plus, on voit très facilement que l'abstraction du comptage de références
fonctionne. Je veux dire qu'on peut définir une variante de la logique où on
remplace le prédicat `l.predecessors = L` par un prédicat `l.#predecessors = n`,
et on peut encore donner une règle de raisonnement viable pour chaque
opération.

Enfin, plus tard, on peut espérer proposer une autre abstraction à base de
régions, où le champ `predecessors` stockerait un ensemble ou un multi-ensemble
de régions, mais je n'y ai pas encore réfléchi en détail.
